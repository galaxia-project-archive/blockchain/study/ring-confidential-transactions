/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <crypto/CryptoTypes.h>

namespace Xi {
namespace Crypto {
using EllipticCurvePoint = ::Crypto::EllipticCurvePoint;
using EllipticCurveScalar = ::Crypto::EllipticCurveScalar;
using Byte = ::Crypto::Byte;
using ByteVector = ::Crypto::ByteVector;
using Hash = ::Crypto::Hash;
using HashVector = ::Crypto::HashVector;
using PublicKey = ::Crypto::PublicKey;
using PublicKeyVector = ::Crypto::PublicKeyVector;
using SecretKey = ::Crypto::SecretKey;
using SecretKeyVector = ::Crypto::SecretKeyVector;
using KeyDerivation = ::Crypto::KeyDerivation;
using KeyDerivationVector = ::Crypto::KeyDerivationVector;
using KeyImage = ::Crypto::KeyImage;
using KeyImageVector = ::Crypto::KeyImageVector;
using Signature = ::Crypto::Signature;
using SignatureVector = ::Crypto::SignatureVector;
}  // namespace Crypto
}  // namespace Xi
