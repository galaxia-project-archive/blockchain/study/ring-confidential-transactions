/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Global.h>

#include "Xi/Crypto/Device/ICryptoDevice.h"

namespace Xi {
namespace Crypto {
namespace Device {
class DefaultDevice final : public ICryptoDevice {
 public:
  static DefaultDevice &instance();

 private:
  DefaultDevice() = default;

 public:
  XI_DELETE_COPY(DefaultDevice);
  XI_DELETE_MOVE(DefaultDevice);
  ~DefaultDevice() override = default;

  // ------------------------------------------------- ICryptoDevice --------------------------------------------------
  [[nodiscard]] bool generateKeyDerivation(const ::Crypto::PublicKey &pub, const ::Crypto::SecretKey &sec,
                                           ::Crypto::KeyDerivation &derivation) override;
  [[nodiscard]] bool generateConcealKeyDerivation(const ::Crypto::PublicKey &tx_pub_key,
                                                  const ::Crypto::PublicKeyVector &additional_tx_pub_keys,
                                                  const ::Crypto::KeyDerivation &main_derivation,
                                                  const ::Crypto::KeyDerivationVector &additional_derivations,
                                                  ::Crypto::KeyDerivation &out) override;
  [[nodiscard]] bool generateKeyImage(const ::Crypto::PublicKey &pub, const ::Crypto::SecretKey &sec,
                                      ::Crypto::KeyImage &image) override;
  [[nodiscard]] bool generateKeyPair(Crypto::KeyPair &out) override;
  [[nodiscard]] bool derivePublicKey(const ::Crypto::KeyDerivation &derivation, const std::size_t output_index,
                                     const ::Crypto::PublicKey &pub, ::Crypto::PublicKey &derived_pub) override;
  [[nodiscard]] bool deriveSecretKey(const ::Crypto::KeyDerivation &derivation, const std::size_t output_index,
                                     const ::Crypto::SecretKey &sec, ::Crypto::SecretKey &derived_sec) override;
  // ------------------------------------------------- ICryptoDevice --------------------------------------------------

  // ------------------------------------------------ SubAdresses ----------------------------------------------------
  [[nodiscard]] bool deriveSubaddressPublicKey(const ::Crypto::PublicKey &out_key,
                                               const ::Crypto::KeyDerivation &derivation,
                                               const std::size_t output_index,
                                               ::Crypto::PublicKey &derived_key) override;
  [[nodiscard]] bool deriveSubaddressSpendPublicKey(const Account::AccountKeys &keys,
                                                    const Account::SubAddressIndex &index,
                                                    ::Crypto::PublicKey &out) override;
  [[nodiscard]] bool deriveSubaddressSpendPublicKey(const Account::AccountKeys &keys, uint32_t account, uint32_t begin,
                                                    uint32_t end, ::Crypto::PublicKeyVector &out) override;
  [[nodiscard]] bool deriveSubaddress(const Account::AccountKeys &keys, const Account::SubAddressIndex &index,
                                      Account::AccountPublicAddress &out) override;
  [[nodiscard]] bool deriveSubaddressSecretKey(const ::Crypto::SecretKey &sec, const Account::SubAddressIndex &index,
                                               ::Crypto::SecretKey &out) override;
  // ------------------------------------------------ SubAdresses ----------------------------------------------------

  // -------------------------------------------------- IRctDevice ----------------------------------------------------
  bool isInFakeMode() const override;
  // -------------------------------------------------- IRctDevice ----------------------------------------------------

  // ---------------------------------------------- IEcdhDevice -------------------------------------------------------
  [[nodiscard]] bool ecdhEncode(Rct::EcdhTuple &unmasked, const Rct::Key &sharedSec, bool short_amount) override;
  [[nodiscard]] bool ecdhDecode(Rct::EcdhTuple &masked, const Rct::Key &sharedSec, bool short_amount) override;
  // ---------------------------------------------- IEcdhDevice -------------------------------------------------------

  // --------------------------------------------- IMlsagDevice -------------------------------------------------------
  [[nodiscard]] bool mlsagPrehash(const Rct::ByteVector &blob, size_t inputs_size, size_t outputs_size,
                                  const Rct::KeyVector &hashes, const Rct::CtKeyVector &outPk,
                                  Rct::Key &prehash) override;
  [[nodiscard]] bool mlsagPrepare(const Rct::Key &H, const Rct::Key &xx, Rct::Key &a, Rct::Key &aG, Rct::Key &aHP,
                                  Rct::Key &rvII) override;
  [[nodiscard]] bool mlsagPrepare(Rct::Key &a, Rct::Key &aG) override;
  [[nodiscard]] bool mlsagHash(const Rct::KeyVector &long_message, Rct::Key &c) override;
  [[nodiscard]] bool mlsagSign(const Rct::Key &c, const Rct::KeyVector &xx, const Rct::KeyVector &alpha,
                               const size_t rows, const size_t dsRows, Rct::KeyVector &ss) override;
  // --------------------------------------------- IMlsagDevice -------------------------------------------------------

  // ------------------------------------------------- Arithmetic ----------------------------------------------------
  [[nodiscard]] bool addSecretScalar(const ::Crypto::SecretKey &a, const ::Crypto::SecretKey &b,
                                     ::Crypto::SecretKey &out) override;
  [[nodiscard]] bool secretKeyToPublicKey(const ::Crypto::SecretKey &secretKey,
                                          ::Crypto::PublicKey &publicKey) override;
  // ------------------------------------------------- Arithmetic ----------------------------------------------------

  [[nodiscard]] bool beginTransactionConstruction(::Crypto::SecretKey &txKey) override;
  [[nodiscard]] bool endTransactionConstruction() override;
  [[nodiscard]] bool keyDerivationToScalar(const ::Crypto::KeyDerivation &derivation, size_t outputIndex,
                                           ::Crypto::EllipticCurveScalar &out) override;

  DeviceMode currentMode() const override;
  void resetMode() override;
  Xi::Result<DeviceModeLock> switchMode(DeviceMode targetMode) override;

 private:
  DeviceMode m_currentMode;
};
}  // namespace Device
}  // namespace Crypto
}  // namespace Xi
