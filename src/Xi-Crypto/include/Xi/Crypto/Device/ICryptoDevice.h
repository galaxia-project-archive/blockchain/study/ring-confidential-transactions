/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Exceptional.h>
#include <Xi/Result.h>
#include <Xi/Rct/Types/Key.h>
#include <Xi/Rct/DeviceInterfaces/DeviceInterfaces.h>
#include <crypto/CryptoTypes.h>

#include "Xi/Crypto/KeyPair.h"
#include "Xi/Crypto/Account/AccountKeys.h"
#include "Xi/Crypto/Account/SubAddressIndex.h"
#include "Xi/Crypto/Device/DeviceMode.h"
#include "Xi/Crypto/Device/DeviceModeLock.h"

namespace Xi {
namespace Crypto {
namespace Device {
class ICryptoDevice : public Rct::IRctDevice {
 public:
  virtual ~ICryptoDevice() = default;

  // ------------------------------------------------ SubAdresses ----------------------------------------------------
  [[nodiscard]] virtual bool deriveSubaddressPublicKey(const ::Crypto::PublicKey &out_key,
                                                       const ::Crypto::KeyDerivation &derivation,
                                                       const std::size_t output_index,
                                                       ::Crypto::PublicKey &derived_key) = 0;
  [[nodiscard]] virtual bool deriveSubaddressSpendPublicKey(const Account::AccountKeys &keys,
                                                            const Account::SubAddressIndex &index,
                                                            ::Crypto::PublicKey &out) = 0;
  [[nodiscard]] virtual bool deriveSubaddressSpendPublicKey(const Account::AccountKeys &keys, uint32_t account,
                                                            uint32_t begin, uint32_t end,
                                                            ::Crypto::PublicKeyVector &out) = 0;
  [[nodiscard]] virtual bool deriveSubaddress(const Account::AccountKeys &keys, const Account::SubAddressIndex &index,
                                              Account::AccountPublicAddress &out) = 0;
  [[nodiscard]] virtual bool deriveSubaddressSecretKey(const ::Crypto::SecretKey &sec,
                                                       const Account::SubAddressIndex &index,
                                                       ::Crypto::SecretKey &out) = 0;
  // ------------------------------------------------ SubAdresses ----------------------------------------------------

  // ----------------------------------------------- KeyGeneration ---------------------------------------------------
  [[nodiscard]] virtual bool generateKeyDerivation(const ::Crypto::PublicKey &pub, const ::Crypto::SecretKey &sec,
                                                   ::Crypto::KeyDerivation &derivation) = 0;
  [[nodiscard]] virtual bool generateConcealKeyDerivation(const ::Crypto::PublicKey &tx_pub_key,
                                                          const ::Crypto::PublicKeyVector &additional_tx_pub_keys,
                                                          const ::Crypto::KeyDerivation &main_derivation,
                                                          const ::Crypto::KeyDerivationVector &additional_derivations,
                                                          ::Crypto::KeyDerivation &out) = 0;
  [[nodiscard]] virtual bool generateKeyImage(const ::Crypto::PublicKey &pub, const ::Crypto::SecretKey &sec,
                                              ::Crypto::KeyImage &image) = 0;
  [[nodiscard]] virtual bool generateKeyPair(Crypto::KeyPair &out) = 0;
  // ----------------------------------------------- KeyGeneration ---------------------------------------------------

  // ----------------------------------------------- KeyDerivation ---------------------------------------------------
  [[nodiscard]] virtual bool derivePublicKey(const ::Crypto::KeyDerivation &derivation, const std::size_t output_index,
                                             const ::Crypto::PublicKey &pub, ::Crypto::PublicKey &derived_pub) = 0;
  [[nodiscard]] virtual bool deriveSecretKey(const ::Crypto::KeyDerivation &derivation, const std::size_t output_index,
                                             const ::Crypto::SecretKey &sec, ::Crypto::SecretKey &derived_sec) = 0;
  // ----------------------------------------------- KeyDerivation ---------------------------------------------------

  // ------------------------------------------------- Arithmetic ----------------------------------------------------
  [[nodiscard]] virtual bool addSecretScalar(const ::Crypto::SecretKey &a, const ::Crypto::SecretKey &b,
                                             ::Crypto::SecretKey &out) = 0;
  [[nodiscard]] virtual bool secretKeyToPublicKey(const ::Crypto::SecretKey &secretKey,
                                                  ::Crypto::PublicKey &publicKey) = 0;
  // ------------------------------------------------- Arithmetic ----------------------------------------------------

  [[nodiscard]] virtual bool beginTransactionConstruction(::Crypto::SecretKey &txKey) = 0;
  [[nodiscard]] virtual bool endTransactionConstruction() = 0;
  [[nodiscard]] virtual bool keyDerivationToScalar(const ::Crypto::KeyDerivation &derivation, size_t outputIndex,
                                                   ::Crypto::EllipticCurveScalar &out) = 0;

  virtual DeviceMode currentMode() const = 0;
  virtual void resetMode() = 0;
  virtual Xi::Result<DeviceModeLock> switchMode(DeviceMode targetMode) = 0;
};
}  // namespace Device
}  // namespace Crypto
}  // namespace Xi
