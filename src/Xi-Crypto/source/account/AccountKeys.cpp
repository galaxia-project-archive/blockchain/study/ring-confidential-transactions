/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Account/AccountKeys.h"

#include <utility>

#include "Xi/Crypto/Device/DefaultDevice.h"

Xi::Crypto::Account::AccountKeys Xi::Crypto::Account::AccountKeys::createViewOnlyKeys(
    Xi::Crypto::Account::AccountPublicAddress publicKeys, ::Crypto::SecretKey viewSecret) {
  AccountKeys reval;
  reval.Address = std::move(publicKeys);
  reval.ViewSecretKey = std::move(viewSecret);
  reval.SpendSecretKey = ::Crypto::SecretKey::Null;
  return reval;
}

Xi::Crypto::Device::ICryptoDevice &Xi::Crypto::Account::AccountKeys::device() const {
  return Device::DefaultDevice::instance();
}

bool Xi::Crypto::Account::AccountKeys::isMultiSignatureAccount() const { return !MultiSignatureKeys.empty(); }
