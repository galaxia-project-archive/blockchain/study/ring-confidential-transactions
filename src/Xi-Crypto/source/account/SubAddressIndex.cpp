/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Account/SubAddressIndex.h"

const Xi::Crypto::Account::SubAddressIndex Xi::Crypto::Account::SubAddressIndex::Null{0, 0};

bool Xi::Crypto::Account::SubAddressIndex::isNull() const { return Major == 0 && Minor == 0; }

void Xi::Crypto::Account::SubAddressIndex::nullify() {
  Major = 0;
  Minor = 0;
}

void Xi::Crypto::Account::SubAddressIndex::serialize(CryptoNote::ISerializer &serializer) {
  serializer(Major, "major");
  serializer(Minor, "minor");
}
