/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Crypto/Device/DefaultDevice.h"

#include <memory>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/endian/conversion.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Exceptional.h>
#include <crypto/crypto.h>
#include <Xi/Rct/Algorithm/EcdhEncoding.h>
#include <Xi/Rct/Algorithm/GenerateKey.h>
#include <Xi/Rct/Algorithm/KeyArithmetic.h>
#include <Xi/Rct/Algorithm/KeyHashing.h>
#include <Xi/Rct/Mlsag/Mlsag.h>

#include "Xi/Crypto/Exceptions.h"

Xi::Crypto::Device::DefaultDevice &Xi::Crypto::Device::DefaultDevice::instance() {
  static std::unique_ptr<DefaultDevice> __Instance{new DefaultDevice};
  return *__Instance;
}

bool Xi::Crypto::Device::DefaultDevice::deriveSubaddressPublicKey(const ::Crypto::PublicKey &out_key,
                                                                  const ::Crypto::KeyDerivation &derivation,
                                                                  const std::size_t output_index,
                                                                  ::Crypto::PublicKey &derived_key) {
  return ::Crypto::derive_subaddress_public_key(out_key, derivation, output_index, derived_key);
}

bool Xi::Crypto::Device::DefaultDevice::deriveSubaddressSpendPublicKey(
    const Xi::Crypto::Account::AccountKeys &keys, const Xi::Crypto::Account::SubAddressIndex &index,
    ::Crypto::PublicKey &out) {
  if (index.isNull()) {
    out = keys.Address.SpendPublicKey;
    return out.isValid();
  } else {
  }

  // m = Hs(a || index_major || index_minor)
  ::Crypto::SecretKey m;
  if (!deriveSubaddressSecretKey(keys.ViewSecretKey, index, m)) {
    // TODO Logging
    return false;
  }

  // M = m*G
  ::Crypto::PublicKey M;
  if (!::Crypto::secret_key_to_public_key(m, M)) {
    // TODO Logging
    return false;
  }

  // D = B + M
  try {
    out = Rct::key_add(Rct::key_cast(keys.Address.SpendPublicKey), Rct::key_cast(M));
  } catch (...) {
    // TODO Logging
    return false;
  }
  return out.isValid();
}

bool Xi::Crypto::Device::DefaultDevice::deriveSubaddressSpendPublicKey(const Xi::Crypto::Account::AccountKeys &keys,
                                                                       uint32_t account, uint32_t begin, uint32_t end,
                                                                       ::Crypto::PublicKeyVector &out) {
  if (begin < end) {
    // TODO Logging
    return false;
  }

  out.clear();
  out.reserve(end - begin);
  Account::SubAddressIndex index{account, begin};

  ge_p3 p3;
  ge_cached cached;
  if (ge_frombytes_vartime(&p3, keys.Address.SpendPublicKey.data()) != 0) {
    // TODO Logging
    return false;
  }
  ge_p3_to_cached(&cached, &p3);

  for (uint32_t idx = begin; idx < end; ++idx) {
    index.Minor = idx;
    if (index.isNull()) {
      out.push_back(keys.Address.SpendPublicKey);
      if (!out.back().isValid()) {
        return false;
      } else {
        continue;
      }
    }
    ::Crypto::SecretKey m;
    if (!deriveSubaddressSecretKey(keys.ViewSecretKey, index, m)) {
      // TODO Logging
      return false;
    }

    // M = m*G
    ge_scalarmult_base(&p3, m.data());

    // D = B + M
    ::Crypto::PublicKey D;
    ge_p1p1 p1p1;
    ge_add(&p1p1, &p3, &cached);
    ge_p1p1_to_p3(&p3, &p1p1);
    ge_p3_tobytes(D.data(), &p3);
    if (!D.isValid()) {
      // TODO Logging
      return false;
    }
    out.push_back(D);
  }
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::deriveSubaddress(const Xi::Crypto::Account::AccountKeys &keys,
                                                         const Xi::Crypto::Account::SubAddressIndex &index,
                                                         Xi::Crypto::Account::AccountPublicAddress &out) {
  if (index.isNull()) {
    out = keys.Address;
    return true;
  } else {
    ::Crypto::PublicKey D;
    if (!deriveSubaddressSpendPublicKey(keys, index, D)) {
      // TODO Logging
      return false;
    }

    // C = a*D
    ::Crypto::PublicKey C;
    try {
      C = Rct::key_multiply_scalar(Rct::key_cast(D), Rct::key_cast(keys.ViewSecretKey));
    } catch (...) {
      // TODO Logging
      return false;
    }

    // result: (C, D)
    out.ViewPublicKey = C;
    out.SpendPublicKey = D;
    return C.isValid() && D.isValid();
  }
}

bool Xi::Crypto::Device::DefaultDevice::deriveSubaddressSecretKey(const ::Crypto::SecretKey &sec,
                                                                  const Xi::Crypto::Account::SubAddressIndex &index,
                                                                  ::Crypto::SecretKey &out) {
  const char prefix[] = "XiSubAddr";
  char data[sizeof(prefix) + sizeof(::Crypto::SecretKey) + 2 * sizeof(uint32_t)];
  memcpy(data, prefix, sizeof(prefix));
  memcpy(data + sizeof(prefix), sec.data(), sizeof(::Crypto::SecretKey));
  uint32_t idx = boost::endian::native_to_little(index.Major);
  memcpy(data + sizeof(prefix) + sizeof(::Crypto::SecretKey), &idx, sizeof(uint32_t));
  idx = boost::endian::native_to_little(index.Minor);
  memcpy(data + sizeof(prefix) + sizeof(::Crypto::SecretKey) + sizeof(uint32_t), &idx, sizeof(uint32_t));
  ::Crypto::hash_to_scalar(data, sizeof(data), reinterpret_cast<::Crypto::EllipticCurveScalar &>(out));
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::generateKeyDerivation(const ::Crypto::PublicKey &pub,
                                                              const ::Crypto::SecretKey &sec,
                                                              ::Crypto::KeyDerivation &derivation) {
  return ::Crypto::generate_key_derivation(pub, sec, derivation);
}

bool Xi::Crypto::Device::DefaultDevice::generateConcealKeyDerivation(
    const ::Crypto::PublicKey &tx_pub_key, const ::Crypto::PublicKeyVector &additional_tx_pub_keys,
    const ::Crypto::KeyDerivation &main_derivation, const ::Crypto::KeyDerivationVector &additional_derivations,
    ::Crypto::KeyDerivation &out) {
  XI_UNUSED(tx_pub_key, additional_tx_pub_keys, main_derivation, additional_derivations, out);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::generateKeyImage(const ::Crypto::PublicKey &pub, const ::Crypto::SecretKey &sec,
                                                         ::Crypto::KeyImage &image) {
  ::Crypto::generate_key_image(pub, sec, image);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::generateKeyPair(Xi::Crypto::KeyPair &out) {
  ::Crypto::generate_keys(out.PublicKey, out.SecretKey);
  return out.PublicKey.isValid();
}

bool Xi::Crypto::Device::DefaultDevice::derivePublicKey(const ::Crypto::KeyDerivation &derivation,
                                                        const std::size_t output_index, const ::Crypto::PublicKey &pub,
                                                        ::Crypto::PublicKey &derived_pub) {
  return ::Crypto::derive_public_key(derivation, output_index, pub, derived_pub);
}

bool Xi::Crypto::Device::DefaultDevice::deriveSecretKey(const ::Crypto::KeyDerivation &derivation,
                                                        const std::size_t output_index, const ::Crypto::SecretKey &sec,
                                                        ::Crypto::SecretKey &derived_sec) {
  ::Crypto::derive_secret_key(derivation, output_index, sec, derived_sec);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::isInFakeMode() const { return false; }

bool Xi::Crypto::Device::DefaultDevice::ecdhEncode(Xi::Rct::EcdhTuple &unmasked, const Xi::Rct::Key &sharedSec,
                                                   bool short_amount) {
  Rct::ecdh_encode(unmasked, sharedSec, short_amount);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::ecdhDecode(Xi::Rct::EcdhTuple &masked, const Xi::Rct::Key &sharedSec,
                                                   bool short_amount) {
  Rct::ecdh_decode(masked, sharedSec, short_amount);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::mlsagPrehash(const Xi::Rct::ByteVector &blob, size_t inputs_size,
                                                     size_t outputs_size, const Xi::Rct::KeyVector &hashes,
                                                     const Xi::Rct::CtKeyVector &outPk, Xi::Rct::Key &prehash) {
  XI_UNUSED(blob, inputs_size, outputs_size, outPk, prehash);
  prehash = Rct::key_fast_hash(hashes);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::mlsagPrepare(const Xi::Rct::Key &H, const Xi::Rct::Key &xx, Xi::Rct::Key &a,
                                                     Xi::Rct::Key &aG, Xi::Rct::Key &aHP, Xi::Rct::Key &II) {
  Rct::generate_random_key_pair_inplace(aG, a);
  Rct::key_mulitply_scalar_inplace(H, a, aHP);
  Rct::key_mulitply_scalar_inplace(H, xx, II);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::mlsagPrepare(Xi::Rct::Key &a, Xi::Rct::Key &aG) {
  Rct::generate_random_key_pair_inplace(aG, a);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::mlsagHash(const Xi::Rct::KeyVector &long_message, Xi::Rct::Key &c) {
  Rct::key_fast_hash_to_scalar_inplace(long_message, c);
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::mlsagSign(const Xi::Rct::Key &c, const Xi::Rct::KeyVector &xx,
                                                  const Xi::Rct::KeyVector &alpha, const size_t rows,
                                                  const size_t dsRows, Xi::Rct::KeyVector &ss) {
  try {
    exceptional_if_not<Rct::MlsagDeviceError>(dsRows <= rows, "dsRows greater than rows");
    exceptional_if_not<Rct::MlsagDeviceError>(xx.size() == rows, "xx size does not match rows");
    exceptional_if_not<Rct::MlsagDeviceError>(alpha.size() == rows, "alpha size does not match rows");
    exceptional_if_not<Rct::MlsagDeviceError>(ss.size() == rows, "ss size does not match rows");
    for (size_t j = 0; j < rows; j++) {
      sc_mulsub(ss[j].data(), c.data(), xx[j].data(), alpha[j].data());
    }
    return true;
  } catch (...) {
    // TODO: Logging
    return false;
  }
}

bool Xi::Crypto::Device::DefaultDevice::addSecretScalar(const ::Crypto::SecretKey &a, const ::Crypto::SecretKey &b,
                                                        ::Crypto::SecretKey &out) {
  sc_add(out.data(), a.data(), b.data());
  return true;
}

bool Xi::Crypto::Device::DefaultDevice::secretKeyToPublicKey(const ::Crypto::SecretKey &secretKey,
                                                             ::Crypto::PublicKey &publicKey) {
  if (!::Crypto::secret_key_to_public_key(secretKey, publicKey)) {
    return false;
  } else {
    return publicKey.isValid();
  }
}

bool Xi::Crypto::Device::DefaultDevice::beginTransactionConstruction(::Crypto::SecretKey &txKey) {
  Crypto::KeyPair keyPair;
  if (!generateKeyPair(keyPair)) {
    return false;
  } else {
    txKey = std::move(keyPair.SecretKey);
    return true;
  }
}

bool Xi::Crypto::Device::DefaultDevice::endTransactionConstruction() { return true; }

bool Xi::Crypto::Device::DefaultDevice::keyDerivationToScalar(const ::Crypto::KeyDerivation &derivation,
                                                              size_t outputIndex, ::Crypto::EllipticCurveScalar &out) {
  ::Crypto::derivation_to_scalar(derivation, outputIndex, out);
  return true;
}

Xi::Crypto::Device::DeviceMode Xi::Crypto::Device::DefaultDevice::currentMode() const { return m_currentMode; }

void Xi::Crypto::Device::DefaultDevice::resetMode() { m_currentMode = DeviceMode::None; }

Xi::Result<Xi::Crypto::Device::DeviceModeLock> Xi::Crypto::Device::DefaultDevice::switchMode(
    Xi::Crypto::Device::DeviceMode targetMode) {
  XI_ERROR_TRY();
  exceptional_if<CryptoDeviceError>(currentMode() != DeviceMode::None, "device mode already set, reset first");
  m_currentMode = targetMode;
  return DeviceModeLock{*this};
  XI_ERROR_CATCH();
}
