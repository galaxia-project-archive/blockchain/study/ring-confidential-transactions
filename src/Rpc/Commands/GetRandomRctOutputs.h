/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include <Serialization/ISerializer.h>
#include <crypto/Types/PublicKey.h>
#include <Xi/Rct/Types/Key.h>

#include "Rpc/Commands/EmptyRequest.h"

namespace CryptoNote {
namespace RpcCommands {

struct GetRandomRctOutputs {
  static inline std::string identifier() {
    static const std::string __Identifier{"get_random_rct_outputs"};
    return __Identifier;
  }

  struct request {
    uint64_t count;

    KV_BEGIN_SERIALIZATION
    KV_MEMBER(count)
    KV_END_SERIALIZATION
  };

  struct out_response {
    ::Crypto::PublicKey public_key;
    Xi::Rct::Key rct_mask;
    uint64_t global_index;
    uint64_t local_index;

    KV_BEGIN_SERIALIZATION
    KV_MEMBER(public_key)
    KV_MEMBER(rct_mask)
    KV_MEMBER(global_index)
    KV_MEMBER(local_index)
    KV_END_SERIALIZATION
  };

  struct response {
    std::string status;
    std::vector<out_response> random_outputs;

    KV_BEGIN_SERIALIZATION
    KV_MEMBER(status)
    KV_MEMBER(random_outputs)
    KV_END_SERIALIZATION
  };
};

}  // namespace RpcCommands
}  // namespace CryptoNote
