﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
// Copyright (c) 2018, The TurtleCoin Developers
// Copyright (c) 2018, The Karai Developers
// Copyright (c) 2018, The Calex Developers
//
// Please see the included LICENSE file for more information.

#include "RpcServer.h"

#include <future>
#include <unordered_map>
#include <cmath>
#include <algorithm>
#include <ctime>

#include <Xi/Version/Version.h>
#include <Xi/Global.h>
#include <Xi/Concurrent/SystemDispatcher.h>

// CryptoNote
#include "Common/StringTools.h"
#include "CryptoNoteCore/CryptoNoteTools.h"
#include "CryptoNoteCore/Core.h"
#include "CryptoNoteCore/Transactions/TransactionExtra.h"
#include <Xi/Config.h>
#include "CryptoNoteProtocol/CryptoNoteProtocolHandlerCommon.h"
#include "P2p/NetNode.h"
#include "CoreRpcServerErrorCodes.h"
#include "JsonRpc.h"

#undef ERROR

using namespace Logging;
using namespace Crypto;
using namespace Common;

namespace CryptoNote {

static inline void serialize(COMMAND_RPC_GET_BLOCKS_FAST::response& response, ISerializer& s) {
  KV_MEMBER(response.blocks)
  KV_MEMBER(response.start_height)
  KV_MEMBER(response.current_height)
  KV_MEMBER(response.status)
}

void serialize(BlockFullInfo& blockFullInfo, ISerializer& s) {
  KV_MEMBER(blockFullInfo.block_id);
  KV_MEMBER(blockFullInfo.block);
  s(blockFullInfo.transactions, "txs");
}

void serialize(TransactionPrefixInfo& transactionPrefixInfo, ISerializer& s) {
  KV_MEMBER(transactionPrefixInfo.txHash);
  KV_MEMBER(transactionPrefixInfo.txPrefix);
}

void serialize(BlockShortInfo& blockShortInfo, ISerializer& s) {
  KV_MEMBER(blockShortInfo.blockId);
  KV_MEMBER(blockShortInfo.block);
  KV_MEMBER(blockShortInfo.txPrefixes);
}

namespace {

template <typename Command>
RpcServer::HandlerFunction jsonMethod(bool (RpcServer::*handler)(typename Command::request const&,
                                                                 typename Command::response&)) {
  return [handler](RpcServer* obj, const Xi::Http::Request& request, Xi::Http::Response& response) {
    boost::value_initialized<typename Command::request> req;
    boost::value_initialized<typename Command::response> res;

    if (!loadFromJson(static_cast<typename Command::request&>(req), request.body())) {
      return false;
    }

    if (!obj->getCorsDomain().empty()) {
      response.headers().set(Xi::Http::HeaderContainer::AccessControlAllowOrigin, obj->getCorsDomain());
    }
    response.headers().setContentType(Xi::Http::ContentType::Json);

    if (request.method() != Xi::Http::Method::Post && request.method() != Xi::Http::Method::Get) {
      response = obj->makeBadRequest("Only OPTIONS, POST and GET requests are allowed.");
      return false;
    }

    auto result = (obj->*handler)(req, res);
    response.headers().setContentType(Xi::Http::ContentType::Json);
    response.setBody(storeToJson(res.data()));
    return result;
  };
}

}  // namespace

std::unordered_map<std::string, RpcServer::RpcHandler<RpcServer::HandlerFunction>> RpcServer::s_handlers = {
    // json handlers
    // Enabled on block explorer
    {"/getinfo", {jsonMethod<COMMAND_RPC_GET_INFO>(&RpcServer::on_get_info), true, true}},
    {"/getheight", {jsonMethod<COMMAND_RPC_GET_HEIGHT>(&RpcServer::on_get_height), true, true}},
    {"/gettransactions", {jsonMethod<COMMAND_RPC_GET_TRANSACTIONS>(&RpcServer::on_get_transactions), false, true}},
    {"/getpeers", {jsonMethod<COMMAND_RPC_GET_PEERS>(&RpcServer::on_get_peers), true, true}},
    {"/getblocks", {jsonMethod<COMMAND_RPC_GET_BLOCKS_FAST>(&RpcServer::on_get_blocks), false, true}},
    {"/queryblocks", {jsonMethod<COMMAND_RPC_QUERY_BLOCKS>(&RpcServer::on_query_blocks), false, true}},
    {"/queryblockslite", {jsonMethod<COMMAND_RPC_QUERY_BLOCKS_LITE>(&RpcServer::on_query_blocks_lite), false, true}},
    {"/queryblocksdetailed",
     {jsonMethod<COMMAND_RPC_QUERY_BLOCKS_DETAILED>(&RpcServer::on_query_blocks_detailed), false, true}},
    {"/get_pool_changes", {jsonMethod<COMMAND_RPC_GET_POOL_CHANGES>(&RpcServer::onGetPoolChanges), false, true}},
    {"/get_pool_changes_lite",
     {jsonMethod<COMMAND_RPC_GET_POOL_CHANGES_LITE>(&RpcServer::onGetPoolChangesLite), false, true}},
    {"/get_block_details_by_height",
     {jsonMethod<COMMAND_RPC_GET_BLOCK_DETAILS_BY_HEIGHT>(&RpcServer::onGetBlockDetailsByHeight), false, true}},
    {"/get_blocks_details_by_heights",
     {jsonMethod<COMMAND_RPC_GET_BLOCKS_DETAILS_BY_HEIGHTS>(&RpcServer::onGetBlocksDetailsByHeights), false, true}},
    {"/get_blocks_details_by_hashes",
     {jsonMethod<COMMAND_RPC_GET_BLOCKS_DETAILS_BY_HASHES>(&RpcServer::onGetBlocksDetailsByHashes), false, true}},
    {"/get_blocks_hashes_by_timestamps",
     {jsonMethod<COMMAND_RPC_GET_BLOCKS_HASHES_BY_TIMESTAMPS>(&RpcServer::onGetBlocksHashesByTimestamps), false, true}},
    {"/get_transaction_details_by_hashes",
     {jsonMethod<COMMAND_RPC_GET_TRANSACTION_DETAILS_BY_HASHES>(&RpcServer::onGetTransactionDetailsByHashes), false,
      true}},
    {"/get_transaction_hashes_by_payment_id",
     {jsonMethod<COMMAND_RPC_GET_TRANSACTION_HASHES_BY_PAYMENT_ID>(&RpcServer::onGetTransactionHashesByPaymentId),
      false, true}},

    // remove me in 2019
    // Not enabled on block explorer
    {"/sendrawtransaction", {jsonMethod<COMMAND_RPC_SEND_RAW_TX>(&RpcServer::on_send_raw_tx), false, false}},
    {"/feeinfo", {jsonMethod<COMMAND_RPC_GET_FEE_ADDRESS>(&RpcServer::on_get_fee_info), true, false}},
    {"/getNodeFeeInfo", {jsonMethod<COMMAND_RPC_GET_FEE_ADDRESS>(&RpcServer::on_get_fee_info), true, false}},
    {"/get_o_indexes",
     {jsonMethod<COMMAND_RPC_GET_TX_GLOBAL_OUTPUTS_INDEXES>(&RpcServer::on_get_indexes), false, false}},
    {"/getrandom_outs",
     {jsonMethod<COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS>(&RpcServer::on_get_random_outs), false, false}},

    // json rpc
    {"/json_rpc",
     {std::bind(&RpcServer::processJsonRpcRequest, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3),
      true, true}}};

RpcServer::RpcServer(System::Dispatcher& dispatcher, Logging::ILogger& log, Core& c, NodeServer& p2p,
                     ICryptoNoteProtocolHandler& protocol)
    : Xi::Http::Server(),
      logger(log, "RpcServer"),
      m_core(c),
      m_p2p(p2p),
      m_protocol(protocol),
      m_isBlockexplorer{false},
      m_isBlockexplorerOnly{false},
      m_submissionAccess{} {
  setDispatcher(std::make_shared<Xi::Concurrent::SystemDispatcher>(dispatcher));
}

Xi::Http::Response RpcServer::doHandleRequest(const Xi::Http::Request& request) {
  auto it = s_handlers.find(request.target());
  if (it == s_handlers.end())
    return makeNotFound();
  else if (!it->second.allowBusyCore && !isCoreReady())
    return makeInternalServerError("core is busy");
  else if (isBlockexplorerOnly() && !it->second.isBlockexplorerRequest) {
    return makeBadRequest("only block explorer requests are allowed");
  } else {
    Xi::Http::Response response;
    if (!on_options_request(request, response)) {
      if (request.method() != Xi::Http::Method::Post && request.method() != Xi::Http::Method::Get)
        return makeBadRequest("Only OPTIONS, GET and POST methods are allowed.");
      it->second.handler(this, request, response);
    }
    return response;
  }
}

bool RpcServer::processJsonRpcRequest(const HttpRequest& request, HttpResponse& response) {
  using namespace JsonRpc;

  response.headers().setContentType(Xi::Http::ContentType::Json);

  JsonRpcRequest jsonRequest;
  JsonRpcResponse jsonResponse;

  try {
    logger(TRACE) << "JSON-RPC request: " << request.body();
    jsonRequest.parseRequest(request.body());
    jsonResponse.setId(jsonRequest.getId());  // copy id

    static std::unordered_map<std::string, RpcServer::RpcHandler<JsonMemberMethod>> jsonRpcHandlers = {
        // Enabled on block explorer
        {"f_blocks_list_json", {makeMemberMethod(&RpcServer::f_on_blocks_list_json), false, true}},
        {"f_block_json", {makeMemberMethod(&RpcServer::f_on_block_json), false, true}},
        {"f_blocks_list_raw", {makeMemberMethod(&RpcServer::f_on_blocks_list_raw), false, true}},
        {"f_transaction_json", {makeMemberMethod(&RpcServer::f_on_transaction_json), false, true}},
        {"f_on_transactions_pool_json", {makeMemberMethod(&RpcServer::f_on_transactions_pool_json), false, true}},
        {"f_p2p_ban_info", {makeMemberMethod(&RpcServer::f_on_p2p_ban_info), false, true}},
        {"getblockcount", {makeMemberMethod(&RpcServer::on_getblockcount), true, true}},
        {"on_getblockhash", {makeMemberMethod(&RpcServer::on_getblockhash), false, true}},
        {"getcurrencyid", {makeMemberMethod(&RpcServer::on_get_currency_id), true, true}},
        {"getlastblockheader", {makeMemberMethod(&RpcServer::on_get_last_block_header), false, true}},
        {"getblockheaderbyhash", {makeMemberMethod(&RpcServer::on_get_block_header_by_hash), false, true}},
        {"getblockheaderbyheight", {makeMemberMethod(&RpcServer::on_get_block_header_by_height), false, true}},

        // clang-format off
        {RpcCommands::GetBlockTemplate::identifier(), {makeMemberMethod(&RpcServer::onGetBlockTemplate), false, false}},
        {RpcCommands::GetBlockTemplateState::identifier(), {makeMemberMethod(&RpcServer::onGetBlockTemplateState), false, false}},
        {RpcCommands::SubmitBlock::identifier(), {makeMemberMethod(&RpcServer::onSubmitBlock), false, false}},
        {RpcCommands::GetRandomRctOutputs::identifier(), {makeMemberMethod(&RpcServer::onGetRandomRctOutputs), false, false}},
        // clang-format on

        // Not enabled on block explorer
        {"getblocktemplate", {makeMemberMethod(&RpcServer::on_getblocktemplate), false, false}},
        {"submitblock", {makeMemberMethod(&RpcServer::on_submitblock), false, false}}};

    auto it = jsonRpcHandlers.find(jsonRequest.getMethod());
    if (it == jsonRpcHandlers.end()) {
      throw JsonRpcError(JsonRpc::errMethodNotFound);
    }

    if (!it->second.allowBusyCore && !isCoreReady()) {
      throw JsonRpcError(CORE_RPC_ERROR_CODE_CORE_BUSY, "Core is busy");
    }

    if (isBlockexplorerOnly() && !it->second.isBlockexplorerRequest) {
      throw JsonRpcError(CORE_RPC_ERROR_CODE_BLOCK_EXPLORER_ONLY, "only block explorer requests are allowed");
    }

    it->second.handler(this, jsonRequest, jsonResponse);

  } catch (const JsonRpcError& err) {
    jsonResponse.setError(err);
  } catch (const std::exception& e) {
    jsonResponse.setError(JsonRpcError(JsonRpc::errInternalError, e.what()));
  }

  response.setBody(jsonResponse.getBody());
  logger(TRACE) << "JSON-RPC response: " << jsonResponse.getBody();
  return true;
}

bool RpcServer::setFeeAddress(const std::string fee_address) {
  m_fee_address = fee_address;
  return true;
}

bool RpcServer::setFeeAmount(const uint32_t fee_amount) {
  m_fee_amount = fee_amount;
  return true;
}

bool RpcServer::enableCors(const std::string& domain) {
  m_cors = domain;
  return true;
}

const std::string& RpcServer::getCorsDomain() { return m_cors; }

bool RpcServer::isBlockexplorer() const { return m_isBlockexplorer; }
void RpcServer::setBlockexplorer(bool enabled) {
  m_isBlockexplorer = enabled;
  logger(INFO) << "Blockexplorer " << (enabled ? "enabled" : "disabled");
}

bool RpcServer::isBlockexplorerOnly() const { return m_isBlockexplorerOnly; }
void RpcServer::setBlockexplorerOnly(bool enabled) { m_isBlockexplorerOnly = enabled; }

bool RpcServer::isCoreReady() {
  return m_core.getCurrency().isTestNet() || m_p2p.get_payload_object().isSynchronized();
}

bool RpcServer::on_options_request(const RpcServer::HttpRequest& request, RpcServer::HttpResponse& response) {
  if (!getCorsDomain().empty()) {
    response.headers().set(Xi::Http::HeaderContainer::AccessControlAllowOrigin, getCorsDomain());
    response.headers().setAccessControlRequestMethods({Xi::Http::Method::Post, Xi::Http::Method::Options});
  }
  response.headers().setAllow({Xi::Http::Method::Post, Xi::Http::Method::Get, Xi::Http::Method::Options});
  response.headers().setContentType(Xi::Http::ContentType::Json);
  return request.method() == Xi::Http::Method::Options;
}

bool RpcServer::on_get_blocks(const COMMAND_RPC_GET_BLOCKS_FAST::request& req,
                              COMMAND_RPC_GET_BLOCKS_FAST::response& res) {
  // TODO code duplication see InProcessNode::doGetNewBlocks()
  if (req.block_ids.empty()) {
    res.status = "Failed";
    return false;
  }

  if (req.block_ids.back() != m_core.getBlockHashByIndex(0)) {
    res.status = "Failed";
    return false;
  }

  uint32_t totalBlockCount;
  uint32_t startBlockIndex;
  auto supplementQuery = m_core.findBlockchainSupplement(
      req.block_ids, Xi::Config::Limits::maximumRPCBlocksQueryCount(), totalBlockCount, startBlockIndex);

  if (supplementQuery.isError()) {
    logger(Logging::ERROR) << "Failed to query blockchain supplement: " << supplementQuery.error().message();
    res.status = "Failed";
    return false;
  }

  res.current_height = totalBlockCount;
  res.start_height = startBlockIndex + 1;

  std::vector<Crypto::Hash> missedHashes;
  m_core.getBlocks(supplementQuery.value(), res.blocks, missedHashes);
  assert(missedHashes.empty());

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_query_blocks(const COMMAND_RPC_QUERY_BLOCKS::request& req, COMMAND_RPC_QUERY_BLOCKS::response& res) {
  uint32_t startIndex;
  uint32_t currentIndex;
  uint32_t fullOffset;

  if (!m_core.queryBlocks(req.block_ids, req.timestamp, startIndex, currentIndex, fullOffset, res.items)) {
    res.status = "Failed to perform query";
    return false;
  }

  res.start_height = startIndex + 1;
  res.current_height = currentIndex + 1;
  res.full_offset = fullOffset;
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_query_blocks_lite(const COMMAND_RPC_QUERY_BLOCKS_LITE::request& req,
                                     COMMAND_RPC_QUERY_BLOCKS_LITE::response& res) {
  uint32_t startIndex;
  uint32_t currentIndex;
  uint32_t fullOffset;
  if (!m_core.queryBlocksLite(req.blockIds, req.timestamp, startIndex, currentIndex, fullOffset, res.items)) {
    res.status = "Failed to perform query";
    return false;
  }

  res.startHeight = startIndex + 1;
  res.currentHeight = currentIndex + 1;
  res.fullOffset = fullOffset;
  res.status = CORE_RPC_STATUS_OK;

  return true;
}

bool RpcServer::on_query_blocks_detailed(const COMMAND_RPC_QUERY_BLOCKS_DETAILED::request& req,
                                         COMMAND_RPC_QUERY_BLOCKS_DETAILED::response& res) {
  uint32_t startIndex;
  uint32_t currentIndex;
  uint32_t fullOffset;
  if (!m_core.queryBlocksDetailed(req.blockIds, req.timestamp, startIndex, currentIndex, fullOffset, res.blocks)) {
    res.status = "Failed to perform query";
    return false;
  }

  res.startHeight = startIndex + 1;
  res.currentHeight = currentIndex + 1;
  res.fullOffset = fullOffset;
  res.status = CORE_RPC_STATUS_OK;

  return true;
}

bool RpcServer::on_get_indexes(const COMMAND_RPC_GET_TX_GLOBAL_OUTPUTS_INDEXES::request& req,
                               COMMAND_RPC_GET_TX_GLOBAL_OUTPUTS_INDEXES::response& res) {
  std::vector<uint64_t> outputIndexes;
  if (!m_core.getTransactionGlobalIndexes(req.txid, outputIndexes)) {
    res.status = "Failed";
    return true;
  }

  res.o_indexes.assign(outputIndexes.begin(), outputIndexes.end());
  res.status = CORE_RPC_STATUS_OK;
  logger(TRACE) << "COMMAND_RPC_GET_TX_GLOBAL_OUTPUTS_INDEXES: [" << res.o_indexes.size() << "]";
  return true;
}

bool RpcServer::on_get_random_outs(const COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS::request& req,
                                   COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS::response& res) {
  res.status = "Failed";

  if (req.outs_count > COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS::request::maxOutsCount()) {
    res.status = "maximum outs count exceeded";
    return false;
  }

  for (uint64_t amount : req.amounts) {
    std::vector<uint64_t> globalIndexes;
    std::vector<Crypto::PublicKey> publicKeys;
    if (!m_core.getRandomOutputs(amount, req.outs_count, globalIndexes, publicKeys)) {
      return true;
    }

    assert(globalIndexes.size() == publicKeys.size());
    res.outs.emplace_back(COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS_outs_for_amount{amount, {}});
    for (size_t i = 0; i < globalIndexes.size(); ++i) {
      res.outs.back().outs.push_back({globalIndexes[i], publicKeys[i]});
    }
  }

  res.status = CORE_RPC_STATUS_OK;

  std::stringstream ss;
  typedef COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS::outs_for_amount outs_for_amount;
  typedef COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS::out_entry out_entry;

  std::for_each(res.outs.begin(), res.outs.end(), [&](outs_for_amount& ofa) {
    ss << "[" << ofa.amount << "]:";

    assert(ofa.outs.size() && "internal error: ofa.outs.size() is empty");

    std::for_each(ofa.outs.begin(), ofa.outs.end(), [&](out_entry& oe) { ss << oe.global_amount_index << " "; });
    ss << ENDL;
  });
  std::string s = ss.str();
  logger(TRACE) << "COMMAND_RPC_GET_RANDOM_OUTPUTS_FOR_AMOUNTS: " << ENDL << s;
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::onGetPoolChanges(const COMMAND_RPC_GET_POOL_CHANGES::request& req,
                                 COMMAND_RPC_GET_POOL_CHANGES::response& rsp) {
  rsp.status = CORE_RPC_STATUS_OK;
  rsp.isTailBlockActual = m_core.getPoolChanges(req.tailBlockId, req.knownTxsIds, rsp.addedTxs, rsp.deletedTxsIds);

  return true;
}

bool RpcServer::onGetPoolChangesLite(const COMMAND_RPC_GET_POOL_CHANGES_LITE::request& req,
                                     COMMAND_RPC_GET_POOL_CHANGES_LITE::response& rsp) {
  rsp.status = CORE_RPC_STATUS_OK;
  rsp.isTailBlockActual = m_core.getPoolChangesLite(req.tailBlockId, req.knownTxsIds, rsp.addedTxs, rsp.deletedTxsIds);

  return true;
}

bool RpcServer::onGetBlocksDetailsByHeights(const COMMAND_RPC_GET_BLOCKS_DETAILS_BY_HEIGHTS::request& req,
                                            COMMAND_RPC_GET_BLOCKS_DETAILS_BY_HEIGHTS::response& rsp) {
  try {
    std::vector<BlockDetails> blockDetails;
    for (const uint32_t& height : req.blockHeights) {
      if (height == 0) {
        throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_SMALL_HEIGHT, "Height must be greater zero"};
      }
      blockDetails.push_back(m_core.getBlockDetails(height - 1));
    }

    rsp.blocks = std::move(blockDetails);
  } catch (std::system_error& e) {
    rsp.status = e.what();
    return false;
  } catch (std::exception& e) {
    rsp.status = "Error: " + std::string(e.what());
    return false;
  }

  rsp.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::onGetBlocksDetailsByHashes(const COMMAND_RPC_GET_BLOCKS_DETAILS_BY_HASHES::request& req,
                                           COMMAND_RPC_GET_BLOCKS_DETAILS_BY_HASHES::response& rsp) {
  try {
    std::vector<BlockDetails> blockDetails;
    for (const Crypto::Hash& hash : req.blockHashes) {
      blockDetails.push_back(m_core.getBlockDetails(hash));
    }

    rsp.blocks = std::move(blockDetails);
  } catch (std::system_error& e) {
    rsp.status = e.what();
    return false;
  } catch (std::exception& e) {
    rsp.status = "Error: " + std::string(e.what());
    return false;
  }

  rsp.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::onGetBlockDetailsByHeight(const COMMAND_RPC_GET_BLOCK_DETAILS_BY_HEIGHT::request& req,
                                          COMMAND_RPC_GET_BLOCK_DETAILS_BY_HEIGHT::response& rsp) {
  try {
    if (req.blockHeight == 0) {
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_SMALL_HEIGHT, "Height must be greater zero"};
    }
    BlockDetails blockDetails = m_core.getBlockDetails(req.blockHeight - 1);
    rsp.block = blockDetails;
  } catch (std::system_error& e) {
    rsp.status = e.what();
    return false;
  } catch (std::exception& e) {
    rsp.status = "Error: " + std::string(e.what());
    return false;
  }

  rsp.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::onGetBlocksHashesByTimestamps(const COMMAND_RPC_GET_BLOCKS_HASHES_BY_TIMESTAMPS::request& req,
                                              COMMAND_RPC_GET_BLOCKS_HASHES_BY_TIMESTAMPS::response& rsp) {
  try {
    auto blockHashes = m_core.getBlockHashesByTimestamps(req.timestampBegin, req.secondsCount);
    rsp.blockHashes = std::move(blockHashes);
  } catch (std::system_error& e) {
    rsp.status = e.what();
    return false;
  } catch (std::exception& e) {
    rsp.status = "Error: " + std::string(e.what());
    return false;
  }

  rsp.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::onGetTransactionDetailsByHashes(const COMMAND_RPC_GET_TRANSACTION_DETAILS_BY_HASHES::request& req,
                                                COMMAND_RPC_GET_TRANSACTION_DETAILS_BY_HASHES::response& rsp) {
  try {
    std::vector<TransactionDetails> transactionDetails;
    transactionDetails.reserve(req.transactionHashes.size());

    for (const auto& hash : req.transactionHashes) {
      transactionDetails.push_back(m_core.getTransactionDetails(hash));
    }

    rsp.transactions = std::move(transactionDetails);
  } catch (std::system_error& e) {
    rsp.status = e.what();
    return false;
  } catch (std::exception& e) {
    rsp.status = "Error: " + std::string(e.what());
    return false;
  }

  rsp.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::onGetTransactionHashesByPaymentId(const COMMAND_RPC_GET_TRANSACTION_HASHES_BY_PAYMENT_ID::request& req,
                                                  COMMAND_RPC_GET_TRANSACTION_HASHES_BY_PAYMENT_ID::response& rsp) {
  try {
    rsp.transactionHashes = m_core.getTransactionHashesByPaymentId(req.paymentId);
  } catch (std::system_error& e) {
    rsp.status = e.what();
    return false;
  } catch (std::exception& e) {
    rsp.status = "Error: " + std::string(e.what());
    return false;
  }

  rsp.status = CORE_RPC_STATUS_OK;
  return true;
}

//
// JSON handlers
//

bool RpcServer::on_get_info(const COMMAND_RPC_GET_INFO::request& req, COMMAND_RPC_GET_INFO::response& res) {
  XI_UNUSED(req);
  res.height = m_core.getTopBlockIndex() + 1;
  res.difficulty = m_core.getDifficultyForNextBlock();
  res.tx_count = m_core.getBlockchainTransactionCount() - res.height;  // without coinbase
  res.tx_pool_size = m_core.transactionPool().size();
  res.tx_pool_state = m_core.transactionPool().stateHash();
  res.tx_min_fee = m_core.getCurrency().minimumFee();
  res.alt_blocks_count = m_core.getAlternativeBlockCount();
  uint64_t total_conn = m_p2p.get_connections_count();
  res.outgoing_connections_count = m_p2p.get_outgoing_connections_count();
  res.incoming_connections_count = total_conn - res.outgoing_connections_count;
  res.white_peerlist_size = m_p2p.getPeerlistManager().get_white_peers_count();
  res.grey_peerlist_size = m_p2p.getPeerlistManager().get_gray_peers_count();
  res.last_known_block_index = std::max(static_cast<uint32_t>(1), m_protocol.getObservedHeight()) - 1;
  res.network_height = std::max(static_cast<uint32_t>(1), m_protocol.getBlockchainHeight());
  res.upgrade_heights = Xi::Config::BlockVersion::forks();
  res.supported_height = res.upgrade_heights.empty() ? 0 : *res.upgrade_heights.rbegin();
  res.hashrate =
      (uint32_t)round(res.difficulty / Xi::Config::Time::blockTimeSeconds());  // TODO Not a good approximation
  res.synced = ((uint64_t)res.height == (uint64_t)res.network_height);
  res.network = Xi::to_string(m_core.getCurrency().network());
  res.major_version = m_core.getBlockDetails(m_core.getTopBlockIndex()).majorVersion;
  res.minor_version = m_core.getBlockDetails(m_core.getTopBlockIndex()).minorVersion;
  res.version = APP_VERSION;
  res.status = CORE_RPC_STATUS_OK;
  res.start_time = (uint64_t)m_core.getStartTime();
  return true;
}

bool RpcServer::on_get_height(const COMMAND_RPC_GET_HEIGHT::request& req, COMMAND_RPC_GET_HEIGHT::response& res) {
  XI_UNUSED(req);
  res.height = m_core.getTopBlockIndex() + 1;
  res.network_height = std::max(static_cast<uint32_t>(1), m_protocol.getBlockchainHeight());
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_get_transactions(const COMMAND_RPC_GET_TRANSACTIONS::request& req,
                                    COMMAND_RPC_GET_TRANSACTIONS::response& res) {
  std::vector<Hash> vh;
  vh.resize(req.txs_hashes.size());
  for (const auto& tx_hex_str : req.txs_hashes) {
    auto parseResult = Hash::fromString(tx_hex_str);
    if (parseResult.isError()) {
      res.status = "invalid hash hex string representation";
      return true;
    } else {
      vh.emplace_back(parseResult.take());
    }
  }

  std::vector<Hash> missed_txs;
  std::vector<BinaryArray> txs;
  m_core.getTransactions(vh, txs, missed_txs);

  for (auto& tx : txs) {
    res.txs_as_hex.push_back(toHex(tx));
  }

  for (const auto& miss_tx : missed_txs) {
    res.missed_tx.push_back(miss_tx.toString());
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_send_raw_tx(const COMMAND_RPC_SEND_RAW_TX::request& req, COMMAND_RPC_SEND_RAW_TX::response& res) {
  std::vector<BinaryArray> transactions(1);
  if (!fromHex(req.tx_as_hex, transactions.back())) {
    logger(INFO) << "[on_send_raw_tx]: Failed to parse tx from hexbuff: " << req.tx_as_hex;
    res.status = "Failed";
    return true;
  }

  Crypto::Hash transactionHash = Crypto::cn_fast_hash(transactions.back().data(), transactions.back().size());
  logger(DEBUGGING) << "transaction " << transactionHash << " came in on_send_raw_tx";

  auto txPushResult = m_core.transactionPool().pushTransaction(transactions.back());
  if (txPushResult.isError()) {
    logger(DEBUGGING) << "[on_send_raw_tx]: tx verification failed (" << txPushResult.error().message() << ")";
    res.status = "Failed";
    return true;
  }

  m_protocol.relayTransactions(transactions);
  // TODO: make sure that tx has reached other nodes here, probably wait to receive reflections from other nodes
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_get_fee_info(const COMMAND_RPC_GET_FEE_ADDRESS::request& req,
                                COMMAND_RPC_GET_FEE_ADDRESS::response& res) {
  XI_UNUSED(req);
  if (m_fee_address.empty()) {
    res.status = CORE_RPC_STATUS_OK;
    return false;
  }

  res.address = m_fee_address;
  res.amount = m_fee_amount;
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_get_peers(const COMMAND_RPC_GET_PEERS::request& req, COMMAND_RPC_GET_PEERS::response& res) {
  XI_UNUSED(req);
  std::list<PeerlistEntry> peers_white;
  std::list<PeerlistEntry> peers_gray;

  m_p2p.getPeerlistManager().get_peerlist_full(peers_gray, peers_white);

  for (const auto& peer : peers_white) {
    std::stringstream stream;
    stream << peer.adr;
    res.peers.push_back(stream.str());
  }
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

//------------------------------------------------------------------------------------------------------------------------------
// JSON RPC methods
//------------------------------------------------------------------------------------------------------------------------------
bool RpcServer::f_on_blocks_list_json(const F_COMMAND_RPC_GET_BLOCKS_LIST::request& req,
                                      F_COMMAND_RPC_GET_BLOCKS_LIST::response& res) {
  // check if blockchain explorer RPC is enabled
  if (!isBlockexplorer()) {
    return false;
  }

  if (req.height < 1) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_SMALL_HEIGHT, "Height must be at least 1."};
  }
  if (m_core.getTopBlockIndex() + 1 < req.height) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT,
                                std::string("To big height: ") + std::to_string(req.height) +
                                    ", current blockchain height = " + std::to_string(m_core.getTopBlockIndex())};
  }

  uint32_t print_blocks_count = 30;
  uint32_t last_height = static_cast<uint32_t>(req.height - print_blocks_count);
  if (req.height <= print_blocks_count) {
    last_height = 1;
  }

  for (uint32_t i = static_cast<uint32_t>(req.height); i >= last_height && i > 0; i--) {
    Hash block_hash = m_core.getBlockHashByIndex(static_cast<uint32_t>(i - 1));
    if (!m_core.hasBlock(block_hash)) {
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: can't get block by height. Height = " + std::to_string(i) + '.'};
    }
    BlockTemplate blk = m_core.getBlockByHash(block_hash);
    BlockDetails blkDetails = m_core.getBlockDetails(block_hash);

    f_block_short_response block_short;
    block_short.cumul_size = blkDetails.blockSize;
    block_short.timestamp = blk.timestamp;
    block_short.difficulty = blkDetails.difficulty;
    block_short.height = i;
    block_short.hash = Common::podToHex(block_hash);
    block_short.tx_count = blk.transactionHashes.size() + 1;

    res.blocks.push_back(block_short);
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::f_on_block_json(const F_COMMAND_RPC_GET_BLOCK_DETAILS::request& req,
                                F_COMMAND_RPC_GET_BLOCK_DETAILS::response& res) {
  if (!isBlockexplorer()) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                "The blockexplorer is not enabled on this endpoint but required by this operation."};
  }

  Hash hash;

  try {
    uint32_t height = boost::lexical_cast<uint32_t>(req.hash);
    if (height < 1) {
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: can't get block by invalid height '0'."};
    } else if (height > m_core.getTopBlockIndex() + 1) {
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT,
                                  std::string{"Requested height '"} + std::to_string(height) + "' is too big."};
    }
    hash = m_core.getBlockHashByIndex(height - 1);
  } catch (boost::bad_lexical_cast&) {
    if (!parse_hash256(req.hash, hash)) {
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM,
                                  "Failed to parse hex representation of block hash. Hex = " + req.hash + '.'};
    }
  }

  if (!m_core.hasBlock(hash)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                "Internal error: can't get block by hash. Hash = " + req.hash + '.'};
  }
  BlockTemplate blk = m_core.getBlockByHash(hash);
  BlockDetails blkDetails = m_core.getBlockDetails(hash);

  const auto validateCoinbaseTransaction = [](const auto& tx) {
    if (tx.Transfer.Inputs.size() != 1) {
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: coinbase transaction has invalid input size."};
    } else if (tx.Transfer.Inputs.front().type() != typeid(BaseInput)) {
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: coinbase transaction in the block has the wrong type"};
    }
  };

  validateCoinbaseTransaction(blk.baseTransaction);

  block_header_response block_header;
  res.block.height = boost::get<BaseInput>(blk.baseTransaction.Transfer.Inputs.front()).BlockIndex + 1;
  fill_block_header_response(blk, false, res.block.height - 1, hash, block_header);

  res.block.major_version = block_header.major_version;
  res.block.minor_version = block_header.minor_version;
  res.block.timestamp = block_header.timestamp;
  res.block.prev_hash = block_header.prev_hash;
  res.block.nonce = block_header.nonce;
  res.block.hash = Common::podToHex(hash);
  res.block.depth = (m_core.getTopBlockIndex() + 1) - res.block.height;
  res.block.difficulty = m_core.getBlockDifficulty(res.block.height - 1);
  res.block.transactionsCumulativeSize = blkDetails.transactionsCumulativeSize;
  res.block.alreadyGeneratedCoins = std::to_string(blkDetails.alreadyGeneratedCoins);
  res.block.alreadyGeneratedTransactions = blkDetails.alreadyGeneratedTransactions;
  res.block.reward = block_header.reward;
  res.block.staticReward = block_header.static_reward;
  res.block.sizeMedian = blkDetails.sizeMedian;
  res.block.blockSize = blkDetails.blockSize;
  res.block.orphan_status = blkDetails.isAlternative;

  size_t blockGrantedFullRewardZone =
      m_core.getCurrency().blockGrantedFullRewardZoneByBlockVersion(block_header.major_version);
  res.block.effectiveSizeMedian = std::max(res.block.sizeMedian, blockGrantedFullRewardZone);

  res.block.baseReward = blkDetails.baseReward;
  res.block.penalty = blkDetails.penalty;

  // Base transaction adding
  f_transaction_short_response transaction_short;
  transaction_short.hash = Common::podToHex(getObjectHash(blk.baseTransaction));
  transaction_short.fee = 0;
  transaction_short.amount_out = getOutputAmount(blk.baseTransaction);
  transaction_short.size = getObjectBinarySize(blk.baseTransaction);
  res.block.transactions.push_back(transaction_short);

  if (m_core.currency().isStaticRewardEnabledForBlockVersion(res.block.major_version)) {
    // Static reward adding
    Transaction static_reward_tx;
    m_core.currency()
        .constructStaticRewardTx(res.block.major_version, res.block.height - 1, static_reward_tx)
        .throwOnError();
    f_transaction_short_response short_static_reward_info;
    short_static_reward_info.hash = Common::podToHex(getObjectHash(static_reward_tx));
    short_static_reward_info.fee = 0;
    short_static_reward_info.amount_out = getOutputAmount(static_reward_tx);
    short_static_reward_info.size = getObjectBinarySize(static_reward_tx);
    res.block.transactions.push_back(short_static_reward_info);
  }

  std::vector<Crypto::Hash> missed_txs;
  std::vector<BinaryArray> txs;
  m_core.getTransactions(blk.transactionHashes, txs, missed_txs);

  res.block.totalFeeAmount = 0;

  for (const BinaryArray& ba : txs) {
    Transaction tx;
    if (!fromBinaryArray(tx, ba)) {
      throw std::runtime_error("Couldn't deserialize transaction");
    }
    f_transaction_short_response i_transaction_short;
    uint64_t amount_in = getInputAmount(tx);
    uint64_t amount_out = getOutputAmount(tx);

    i_transaction_short.hash = Common::podToHex(getObjectHash(tx));
    i_transaction_short.fee = amount_in - amount_out;
    i_transaction_short.amount_out = amount_out;
    i_transaction_short.size = getObjectBinarySize(tx);
    res.block.transactions.push_back(i_transaction_short);

    res.block.totalFeeAmount += i_transaction_short.fee;
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::f_on_blocks_list_raw(const F_COMMAND_RPC_GET_BLOCKS_RAW_BY_RANGE::request& req,
                                     F_COMMAND_RPC_GET_BLOCKS_RAW_BY_RANGE::response& res) {
  if (!isBlockexplorer()) {
    return false;
  }

  if (req.height < 1) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_SMALL_HEIGHT, "height param must be at least 1."};
  }

  if (req.height > m_core.getTopBlockIndex() + 1) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT, "queried height exceeds local blockchain height."};
  }

  if (req.count < 1) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM, "count must be at least 1"};
  }

  if (req.count > 100) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM, "count may not exceed 100."};
  }

  auto rawBlocks = m_core.getBlocks(req.height - 1, req.count);
  std::transform(rawBlocks.begin(), rawBlocks.end(), std::back_inserter(res.blobs), [](const auto& block) {
    BinaryArray blob = toBinaryArray(block);
    return Common::toHex(blob.data(), blob.size());
  });

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::f_on_transaction_json(const F_COMMAND_RPC_GET_TRANSACTION_DETAILS::request& req,
                                      F_COMMAND_RPC_GET_TRANSACTION_DETAILS::response& res) {
  // check if blockchain explorer RPC is enabled
  if (!isBlockexplorer()) {
    return false;
  }

  Hash hash;

  if (!parse_hash256(req.hash, hash)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM,
                                "Failed to parse hex representation of transaction hash. Hex = " + req.hash + '.'};
  }

  std::vector<Crypto::Hash> tx_ids;
  tx_ids.push_back(hash);

  std::vector<Crypto::Hash> missed_txs;
  std::vector<BinaryArray> txs;
  m_core.getTransactions(tx_ids, txs, missed_txs);

  if (1 == txs.size()) {
    Transaction transaction;
    if (!fromBinaryArray(transaction, txs.front())) {
      throw std::runtime_error("Couldn't deserialize transaction");
    }
    res.tx = transaction;
  } else {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM, "transaction wasn't found. Hash = " + req.hash + '.'};
  }
  TransactionDetails transactionDetails = m_core.getTransactionDetails(hash);

  Crypto::Hash blockHash;
  if (transactionDetails.inBlockchain) {
    uint32_t blockHeight = transactionDetails.blockIndex + 1;
    if (!blockHeight) {
      throw JsonRpc::JsonRpcError{
          CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
          "Internal error: can't get transaction by hash. Hash = " + Common::podToHex(hash) + '.'};
    }
    blockHash = m_core.getBlockHashByIndex(blockHeight - 1);
    BlockTemplate blk = m_core.getBlockByHash(blockHash);
    BlockDetails blkDetails = m_core.getBlockDetails(blockHash);

    f_block_short_response block_short;

    block_short.cumul_size = blkDetails.blockSize;
    block_short.timestamp = blk.timestamp;
    block_short.height = blockHeight;
    block_short.hash = Common::podToHex(blockHash);
    block_short.difficulty = blkDetails.difficulty;
    block_short.tx_count = blk.transactionHashes.size() + 1;
    res.block = block_short;
  }

  uint64_t amount_in = getInputAmount(res.tx);
  uint64_t amount_out = getOutputAmount(res.tx);

  res.txDetails.hash = Common::podToHex(getObjectHash(res.tx));
  res.txDetails.fee = amount_in - amount_out;
  if (amount_in == 0) res.txDetails.fee = 0;
  res.txDetails.amount_out = amount_out;
  res.txDetails.size = getObjectBinarySize(res.tx);

  uint64_t mixin;
  if (!f_getMixin(res.tx, mixin)) {
    return false;
  }
  res.txDetails.mixin = mixin;

  Crypto::Hash paymentId;
  if (CryptoNote::getPaymentIdFromTxExtra(res.tx.Transfer.Extra, paymentId)) {
    res.txDetails.paymentId = Common::podToHex(paymentId);
  } else {
    res.txDetails.paymentId = "";
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::f_on_transactions_pool_json(const F_COMMAND_RPC_GET_POOL::request& req,
                                            F_COMMAND_RPC_GET_POOL::response& res) {
  XI_UNUSED(req);
  // check if blockchain explorer RPC is enabled
  if (!isBlockexplorer()) {
    return false;
  }

  res.state = m_core.transactionPool().stateHash();
  auto pool = m_core.getPoolTransactions();
  for (const Transaction& tx : pool) {
    f_transaction_short_response transaction_short;
    uint64_t amount_in = getInputAmount(tx);
    uint64_t amount_out = getOutputAmount(tx);

    transaction_short.hash = Common::podToHex(getObjectHash(tx));
    transaction_short.fee = amount_in - amount_out;
    transaction_short.amount_out = amount_out;
    transaction_short.size = getObjectBinarySize(tx);
    res.transactions.push_back(transaction_short);
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::f_on_p2p_ban_info(const F_COMMAND_RPC_GET_P2P_BAN_INFO::request& req,
                                  F_COMMAND_RPC_GET_P2P_BAN_INFO::response& res) {
  XI_UNUSED(req);
  if (!isBlockexplorer()) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_BLOCK_EXPLORER_DISABLED,
                                "Block explorer must be enabled for this request."};
  }

  auto peerBans = m_p2p.blockedPeers();
  auto peerPanalites = m_p2p.peerPenalties();

  time_t currentTime = time(nullptr);
  for (auto iPeerBan = peerBans.begin(); iPeerBan != peerBans.end(); /* */) {
    if (iPeerBan->second < currentTime) {
      peerBans.erase(iPeerBan++);
    } else {
      ++iPeerBan;
    }
  }

  if (peerBans.empty() && peerBans.empty()) {
    return true;
  }

  for (const auto& iPeerBan : peerBans) {
    F_P2P_BAN_INFO iBanInfo{/* */};
    iBanInfo.ip_address = Common::ipAddressToString(iPeerBan.first);
    iBanInfo.ban_timestamp = iPeerBan.second;
    auto penaltySearch = peerPanalites.find(iPeerBan.first);
    if (penaltySearch != peerPanalites.end()) {
      iBanInfo.penalty_score = penaltySearch->second;
      peerPanalites.erase(penaltySearch);
    } else {
      iBanInfo.penalty_score = 0;
    }
    res.peers_ban_info.emplace_back(std::move(iBanInfo));
  }

  for (const auto& iPeerPenalty : peerPanalites) {
    F_P2P_BAN_INFO iBanInfo{/* */};
    iBanInfo.ip_address = Common::ipAddressToString(iPeerPenalty.first);
    iBanInfo.penalty_score = iPeerPenalty.second;
    iBanInfo.ban_timestamp = 0;
    res.peers_ban_info.emplace_back(std::move(iBanInfo));
  }

  return true;
}

bool RpcServer::f_getMixin(const Transaction& transaction, uint64_t& mixin) {
  mixin = 0;
  for (const TransactionInput& txin : transaction.Transfer.Inputs) {
    if (txin.type() != typeid(KeyInput)) {
      continue;
    }
    uint64_t currentMixin = boost::get<KeyInput>(txin).UsedKeyOffsets.size();
    if (currentMixin > mixin) {
      mixin = currentMixin;
    }
  }
  return true;
}

bool RpcServer::onGetRandomRctOutputs(const RpcCommands::GetRandomRctOutputs::request& req,
                                      RpcCommands::GetRandomRctOutputs::response& res) {
  if (req.count == 0 || req.count > 50) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM, "count must be greater zero and may not exceed 50"};
  }
  // TODO check busy
  auto outputsResult = m_core.getRandomRctOutputs(req.count);
  if (outputsResult.isError()) {
    logger(Logging::ERROR) << "random rct output query failed: " << outputsResult.error().message();
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR, "unable to query random outputs"};
  }
  const auto outputs = outputsResult.take();
  res.random_outputs.resize(outputs.size());
  std::transform(outputs.begin(), outputs.end(), res.random_outputs.begin(), [](const auto& iOutput) {
    RpcCommands::GetRandomRctOutputs::out_response i_res;
    i_res.public_key = iOutput.publicKey;
    i_res.rct_mask = iOutput.commitment;
    i_res.global_index = iOutput.globalIndex;
    i_res.local_index = iOutput.packedIndex.output_index;
  });
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_getblockcount(const COMMAND_RPC_GETBLOCKCOUNT::request& req,
                                 COMMAND_RPC_GETBLOCKCOUNT::response& res) {
  XI_UNUSED(req);
  res.count = m_core.getTopBlockIndex() + 1;
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_getblockhash(const COMMAND_RPC_GETBLOCKHASH::request& req, COMMAND_RPC_GETBLOCKHASH::response& res) {
  if (req.size() != 1) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM, "Wrong parameters, expected height"};
  }

  uint32_t h = static_cast<uint32_t>(req[0]);
  if (h == 0) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_SMALL_HEIGHT, "Height must be greater zero"};
  }

  Crypto::Hash blockId = m_core.getBlockHashByIndex(h - 1);
  if (blockId == NULL_HASH) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT,
                                std::string("Too big height: ") + std::to_string(h) +
                                    ", current blockchain height = " + std::to_string(m_core.getTopBlockIndex() + 1)};
  }

  res = Common::podToHex(blockId);
  return true;
}

Hash RpcServer::block_template_state_hash() const {
  const auto topBlock = m_core.getTopBlockHash();
  const auto poolState = m_core.transactionPool().stateHash();
  Hash reval{};
  for (size_t i = 0; i < sizeof(reval); ++i) {
    reval[i] = topBlock[i] ^ poolState[i];
  }
  return reval;
}

bool RpcServer::on_getblocktemplatestate(const COMMAND_RPC_GETBLOCKTEMPLATE_STATE::request& req,
                                         COMMAND_RPC_GETBLOCKTEMPLATE_STATE::response& res) {
  XI_UNUSED(req);
  const auto stateHash = block_template_state_hash();
  res.template_state = Common::podToHex(stateHash);
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

namespace {
uint64_t slow_memmem(void* start_buff, size_t buflen, void* pat, size_t patlen) {
  void* buf = memchr(start_buff, ((char*)pat)[0], buflen);
  void* end = (char*)buf + buflen - patlen;
  while (buf) {
    if (buf > end) return 0;
    if (memcmp(buf, pat, patlen) == 0) return static_cast<uint64_t>((char*)buf - (char*)start_buff);
    buf = (char*)buf + 1;
    buf = memchr(buf, ((char*)pat)[0], buflen);
  }
  return 0;
}
}  // namespace

bool RpcServer::on_getblocktemplate(const COMMAND_RPC_GETBLOCKTEMPLATE::request& req,
                                    COMMAND_RPC_GETBLOCKTEMPLATE::response& res) {
  // TODO REMOVE ME
  if (req.reserve_size > TX_EXTRA_NONCE_MAX_COUNT) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_BIG_RESERVE_SIZE, "To big reserved size, maximum 255"};
  }

  AccountPublicAddress acc = boost::value_initialized<AccountPublicAddress>();

  if (!req.wallet_address.size() || !m_core.getCurrency().parseAccountAddressString(req.wallet_address, acc)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_WALLET_ADDRESS, "Failed to parse wallet address"};
  }

  BlockTemplate blockTemplate = boost::value_initialized<BlockTemplate>();
  CryptoNote::BinaryArray blob_reserve;
  blob_reserve.resize(req.reserve_size, 0);

  if (!m_core.getBlockTemplate(blockTemplate, acc, blob_reserve, res.difficulty, res.height)) {
    logger(ERROR) << "Failed to create block template";
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR, "Internal error: failed to create block template"};
  }

  BinaryArray block_blob = toBinaryArray(blockTemplate);
  PublicKey tx_pub_key = CryptoNote::getTransactionPublicKeyFromExtra(blockTemplate.baseTransaction.Transfer.Extra);
  if (tx_pub_key == NULL_PUBLIC_KEY) {
    logger(ERROR) << "Failed to find tx pub key in coinbase extra";
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                "Internal error: failed to find tx pub key in coinbase extra"};
  }

  if (0 < req.reserve_size) {
    res.reserved_offset = slow_memmem((void*)block_blob.data(), block_blob.size(), &tx_pub_key, sizeof(tx_pub_key));
    if (!res.reserved_offset) {
      logger(ERROR) << "Failed to find tx pub key in blockblob";
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: failed to create block template"};
    }
    res.reserved_offset += sizeof(tx_pub_key) + 3;  // 3 bytes: tag for TX_EXTRA_TAG_PUBKEY(1 byte), tag for
                                                    // TX_EXTRA_NONCE(1 byte), counter in TX_EXTRA_NONCE(1 byte)
    if (res.reserved_offset + req.reserve_size > block_blob.size()) {
      logger(ERROR) << "Failed to calculate offset for reserved bytes";
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: failed to create block template"};
    }
  } else {
    res.reserved_offset = 0;
  }

  res.blocktemplate_blob = toHex(block_blob);
  const auto btstate = block_template_state_hash();
  res.template_state = Common::podToHex(btstate);
  res.status = CORE_RPC_STATUS_OK;

  return true;
}

bool RpcServer::onGetBlockTemplateState(const RpcCommands::GetBlockTemplateState::request& req,
                                        RpcCommands::GetBlockTemplateState::response& res) {
  XI_UNUSED(req);
  const auto stateHash = block_template_state_hash();
  res.template_state = Common::podToHex(stateHash);
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::onGetBlockTemplate(const RpcCommands::GetBlockTemplate::request& req,
                                   RpcCommands::GetBlockTemplate::response& res) {
  if (req.reserve_size > TX_EXTRA_NONCE_MAX_COUNT) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_BIG_RESERVE_SIZE, "To big reserved size, maximum 255"};
  }

  AccountPublicAddress acc = boost::value_initialized<AccountPublicAddress>();

  if (!req.wallet_address.size() || !m_core.getCurrency().parseAccountAddressString(req.wallet_address, acc)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_WALLET_ADDRESS, "Failed to parse wallet address"};
  }

  BlockTemplate blockTemplate = boost::value_initialized<BlockTemplate>();
  CryptoNote::BinaryArray blob_reserve;
  if (!m_core.getBlockTemplate(blockTemplate, acc, blob_reserve, res.difficulty, res.height)) {
    logger(ERROR) << "Failed to create block template";
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR, "Internal error: failed to create block template"};
  }

  BinaryArray block_blob = toBinaryArray(blockTemplate);
  PublicKey tx_pub_key = CryptoNote::getTransactionPublicKeyFromExtra(blockTemplate.baseTransaction.Transfer.Extra);
  if (tx_pub_key == NULL_PUBLIC_KEY) {
    logger(ERROR) << "Failed to find tx pub key in coinbase extra";
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                "Internal error: failed to find tx pub key in coinbase extra"};
  }

  if (0 < req.reserve_size) {
    res.reserved_offset = static_cast<uint32_t>(
        slow_memmem((void*)block_blob.data(), block_blob.size(), &tx_pub_key, sizeof(tx_pub_key)));
    if (!res.reserved_offset) {
      logger(ERROR) << "Failed to find tx pub key in blockblob";
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: failed to create block template"};
    }
    res.reserved_offset += sizeof(tx_pub_key) + 3;  // 3 bytes: tag for TX_EXTRA_TAG_PUBKEY(1 byte), tag for
                                                    // TX_EXTRA_NONCE(1 byte), counter in TX_EXTRA_NONCE(1 byte)
    if (res.reserved_offset + req.reserve_size > block_blob.size()) {
      logger(ERROR) << "Failed to calculate offset for reserved bytes";
      throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                  "Internal error: failed to create block template"};
    }
  } else {
    res.reserved_offset = 0;
  }

  res.blocktemplate_blob = toHex(block_blob);
  const auto btstate = block_template_state_hash();
  res.template_state = Common::podToHex(btstate);
  res.status = CORE_RPC_STATUS_OK;

  return true;
}

bool RpcServer::onSubmitBlock(const RpcCommands::SubmitBlock::request& req, RpcCommands::SubmitBlock::response& res) {
  XI_CONCURRENT_RLOCK(m_submissionAccess);

  BinaryArray blockblob;
  if (!fromHex(req.hex_binary_template, blockblob)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_BLOCKBLOB, "Wrong block blob"};
  }

  auto submitResult = m_core.submitBlock(BinaryArray{blockblob});
  if (submitResult != error::AddBlockErrorCondition::BLOCK_ADDED) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_BLOCK_NOT_ACCEPTED, "Block not accepted"};
  }

  res.result = submitResult.message();
  if (submitResult == error::AddBlockErrorCode::ADDED_TO_MAIN ||
      submitResult == error::AddBlockErrorCode::ADDED_TO_ALTERNATIVE_AND_SWITCHED) {
    auto blockTemplate = fromBinaryArray<BlockTemplate>(blockblob);  // safe as long as core checked it for us.
    LiteBlock blockToRelay;
    blockToRelay.blockTemplate = std::move(blockblob);
    m_protocol.relayBlock(std::move(blockToRelay));
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_get_currency_id(const COMMAND_RPC_GET_CURRENCY_ID::request& /*req*/,
                                   COMMAND_RPC_GET_CURRENCY_ID::response& res) {
  Hash genesisBlockHash = m_core.getCurrency().genesisBlockHash();
  res.currency_id_blob = Common::podToHex(genesisBlockHash);
  return true;
}

bool RpcServer::on_submitblock(const COMMAND_RPC_SUBMITBLOCK::request& req, COMMAND_RPC_SUBMITBLOCK::response& res) {
  XI_CONCURRENT_RLOCK(m_submissionAccess);
  if (req.size() != 1) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM, "Wrong param"};
  }

  BinaryArray blockblob;
  if (!fromHex(req[0], blockblob)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_BLOCKBLOB, "Wrong block blob"};
  }

  auto submitResult = m_core.submitBlock(BinaryArray{blockblob});
  if (submitResult != error::AddBlockErrorCondition::BLOCK_ADDED) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_BLOCK_NOT_ACCEPTED, "Block not accepted"};
  }

  if (submitResult == error::AddBlockErrorCode::ADDED_TO_MAIN ||
      submitResult == error::AddBlockErrorCode::ADDED_TO_ALTERNATIVE_AND_SWITCHED) {
    auto blockTemplate = fromBinaryArray<BlockTemplate>(blockblob);  // safe as long as core checked it for us.
    LiteBlock blockToRelay;
    blockToRelay.blockTemplate = std::move(blockblob);
    m_protocol.relayBlock(std::move(blockToRelay));
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

RawBlock RpcServer::prepareRawBlock(BinaryArray&& blockBlob) {
  BlockTemplate blockTemplate;
  bool result = fromBinaryArray(blockTemplate, blockBlob);
  if (result) {
  }
  assert(result);

  RawBlock rawBlock;
  rawBlock.block = std::move(blockBlob);

  if (blockTemplate.transactionHashes.empty()) {
    return rawBlock;
  }

  rawBlock.transactions.reserve(blockTemplate.transactionHashes.size());
  std::vector<Crypto::Hash> missedTransactions;
  m_core.getTransactions(blockTemplate.transactionHashes, rawBlock.transactions, missedTransactions);
  assert(missedTransactions.empty());

  return rawBlock;
}

namespace {

uint64_t get_block_reward(const BlockTemplate& blk) {
  uint64_t reward = 0;
  for (const TransactionOutput& out : blk.baseTransaction.Transfer.Outputs) {
    reward += out.Amount;
  }
  return reward;
}

}  // namespace

void RpcServer::fill_block_header_response(const BlockTemplate& blk, bool orphan_status, uint32_t index,
                                           const Hash& hash, block_header_response& response) {
  response.major_version = blk.majorVersion;
  response.minor_version = blk.minorVersion;
  response.timestamp = blk.timestamp;
  response.prev_hash = Common::podToHex(blk.previousBlockHash);
  response.nonce = blk.nonce;
  response.orphan_status = orphan_status;
  response.height = index + 1;
  response.depth = m_core.getTopBlockIndex() - index;
  response.hash = Common::podToHex(hash);
  response.difficulty = m_core.getBlockDifficulty(index);
  response.reward = get_block_reward(blk);
  response.static_reward = m_core.currency().staticRewardAmountForBlockVersion(blk.majorVersion);
  BlockDetails blkDetails = m_core.getBlockDetails(hash);
  response.num_txes = static_cast<uint32_t>(blkDetails.transactions.size());
  response.block_size = blkDetails.blockSize;
}

bool RpcServer::on_get_last_block_header(const COMMAND_RPC_GET_LAST_BLOCK_HEADER::request& req,
                                         COMMAND_RPC_GET_LAST_BLOCK_HEADER::response& res) {
  XI_UNUSED(req);
  auto topBlock = m_core.getBlockByHash(m_core.getTopBlockHash());
  fill_block_header_response(topBlock, false, m_core.getTopBlockIndex(), m_core.getTopBlockHash(), res.block_header);
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_get_block_header_by_hash(const COMMAND_RPC_GET_BLOCK_HEADER_BY_HASH::request& req,
                                            COMMAND_RPC_GET_BLOCK_HEADER_BY_HASH::response& res) {
  Hash blockHash;
  if (!parse_hash256(req.hash, blockHash)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_WRONG_PARAM,
                                "Failed to parse hex representation of block hash. Hex = " + req.hash + '.'};
  }

  if (!m_core.hasBlock(blockHash)) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_INTERNAL_ERROR,
                                "Internal error: can't get block by hash. Hash = " + req.hash + '.'};
  }

  auto block = m_core.getBlockByHash(blockHash);
  CachedBlock cachedBlock(block);
  assert(block.baseTransaction.Transfer.Inputs.front().type() != typeid(BaseInput));

  fill_block_header_response(block, false, cachedBlock.getBlockIndex(), cachedBlock.getBlockHash(), res.block_header);
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_get_block_header_by_height(const COMMAND_RPC_GET_BLOCK_HEADER_BY_HEIGHT::request& req,
                                              COMMAND_RPC_GET_BLOCK_HEADER_BY_HEIGHT::response& res) {
  if (m_core.getTopBlockIndex() + 1 < req.height) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT,
                                std::string("To big height: ") + std::to_string(req.height) +
                                    ", current blockchain height = " + std::to_string(m_core.getTopBlockIndex())};
  }
  if (req.height == 0) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_SMALL_HEIGHT, "Height must be greater zero"};
  }

  uint32_t index = static_cast<uint32_t>(req.height - 1);
  auto block = m_core.getBlockByIndex(index);
  CachedBlock cachedBlock(block);
  assert(cachedBlock.getBlockIndex() == req.height + 1);
  fill_block_header_response(block, false, index, cachedBlock.getBlockHash(), res.block_header);
  res.status = CORE_RPC_STATUS_OK;
  return true;
}

bool RpcServer::on_get_block_headers_range(const COMMAND_RPC_GET_BLOCK_HEADERS_RANGE::request& req,
                                           COMMAND_RPC_GET_BLOCK_HEADERS_RANGE::response& res,
                                           JsonRpc::JsonRpcError& error_resp) {
  // TODO: change usage to jsonRpcHandlers?
  const uint64_t bc_height = m_core.get_current_blockchain_height();
  if (req.start_height > bc_height || req.end_height >= bc_height || req.start_height > req.end_height) {
    error_resp.code = CORE_RPC_ERROR_CODE_TOO_BIG_HEIGHT;
    error_resp.message = "Invalid start/end heights.";
    return false;
  }

  if (req.start_height == 0) {
    throw JsonRpc::JsonRpcError{CORE_RPC_ERROR_CODE_TOO_SMALL_HEIGHT, "Height must be greater zero"};
  }

  for (uint32_t h = static_cast<uint32_t>(req.start_height); h <= static_cast<uint32_t>(req.end_height); ++h) {
    Crypto::Hash block_hash = m_core.getBlockHashByIndex(h - 1);
    CryptoNote::BlockTemplate blk = m_core.getBlockByHash(block_hash);

    res.headers.push_back(block_header_response());
    fill_block_header_response(blk, false, h - 1, block_hash, res.headers.back());

    // TODO: Error handling like in monero?
    /*block blk;
    bool have_block = m_core.get_block_by_hash(block_hash, blk);
    if (!have_block)
    {
            error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
            error_resp.message = "Internal error: can't get block by height. Height = " +
    boost::lexical_cast<std::string>(h) + ". Hash = " + epee::string_tools::pod_to_hex(block_hash) + '.'; return
    false;
    }
    if (blk.miner_tx.vin.size() != 1 || blk.miner_tx.vin.front().type() != typeid(txin_gen))
    {
            error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
            error_resp.message = "Internal error: coinbase transaction in the block has the wrong type";
            return false;
    }
    uint64_t block_height = boost::get<txin_gen>(blk.miner_tx.vin.front()).height;
    if (block_height != h)
    {
            error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
            error_resp.message = "Internal error: coinbase transaction in the block has the wrong height";
            return false;
    }
    res.headers.push_back(block_header_response());
    bool response_filled = fill_block_header_response(blk, false, block_height, block_hash, res.headers.back());
    if (!response_filled)
    {
            error_resp.code = CORE_RPC_ERROR_CODE_INTERNAL_ERROR;
            error_resp.message = "Internal error: can't produce valid response.";
            return false;
    }*/
  }

  res.status = CORE_RPC_STATUS_OK;
  return true;
}

}  // namespace CryptoNote
