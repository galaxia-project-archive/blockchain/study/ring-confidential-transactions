/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Algorithm/Permutation.h"

#include <algorithm>

bool Xi::Algorithm::apply_permutation(std::vector<size_t> perm,
                                      const std::function<void(const size_t, const size_t)> swap) {
  for (size_t n = 0; n < perm.size(); ++n) {
    if (std::find(perm.begin(), perm.end(), n) == perm.end()) {
      return false;
    }
  }

  for (size_t i = 0; i < perm.size(); ++i) {
    size_t current = i;
    while (i != perm[current]) {
      size_t next = perm[current];
      swap(current, next);
      perm[current] = current;
      current = next;
    }
    perm[current] = current;
  }
  return true;
}
