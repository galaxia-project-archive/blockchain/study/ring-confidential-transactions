/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Algorithm/DeltaEncodedIndices.h"

#include <algorithm>

std::vector<uint64_t> Xi::Algorithm::delta_offsets_to_indices(const std::vector<uint64_t> &offsets) {
  std::vector<uint64_t> indices;
  if (offsets.empty()) {
    return indices;
  }
  indices.resize(offsets.size());
  indices[0] = offsets[0];
  for (size_t i = 1; i < offsets.size(); ++i) {
    indices[i] = offsets[i] + indices[i - 1];
  }
  return indices;
}

std::vector<uint64_t> Xi::Algorithm::indices_to_delta_offsets(const std::vector<uint64_t> &offsets) {
  std::vector<uint64_t> deltas = offsets;
  if (offsets.empty()) {
    return deltas;
  }
  std::sort(deltas.begin(), deltas.end());
  for (size_t i = deltas.size() - 1; i != 0; i--) {
    deltas[i] -= deltas[i - 1];
  }
  return deltas;
}
