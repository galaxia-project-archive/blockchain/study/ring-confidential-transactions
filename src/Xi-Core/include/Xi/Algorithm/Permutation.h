/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>
#include <functional>
#include <utility>

namespace Xi {
namespace Algorithm {

[[nodiscard]] bool apply_permutation(std::vector<size_t> perm,
                                     const std::function<void(const size_t lhs, const size_t rhs)> swap);

template <typename _ValueT>
[[nodiscard]] bool apply_permutation(std::vector<size_t> perm, std::vector<_ValueT>& toApply) {
  if (perm.size() != toApply.size()) {
    return false;
  }
  return apply_permutation(std::move(perm),
                           [&toApply](const size_t lhs, const size_t rhs) { std::swap(toApply[lhs], toApply[rhs]); });
}

}  // namespace Algorithm
}  // namespace Xi
