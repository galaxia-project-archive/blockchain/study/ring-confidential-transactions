/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>
#include <array>
#include <stdexcept>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/beast/core/span.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include "Xi/Result.h"
#include "Xi/__impl/SpanImpl.h"

namespace Xi {
template <typename _ValueType>
using Span = boost::beast::span<_ValueType>;

template <typename _ValueT>
inline Xi::Result<Span<_ValueT>> span_slice(std::vector<_ValueT>& container, size_t startIndex, size_t endIndex) {
  XI_ERROR_TRY();
  __impl::verify_slice(container.size(), startIndex, endIndex);
  return Span<_ValueT>{container.data() + startIndex, endIndex - startIndex};
  XI_ERROR_CATCH();
}

template <typename _ValueT>
inline Xi::Result<Span<const _ValueT>> span_slice(const std::vector<_ValueT>& container, size_t startIndex,
                                                  size_t endIndex) {
  XI_ERROR_TRY();
  __impl::verify_slice(container.size(), startIndex, endIndex);
  return Span<const _ValueT>{container.data() + startIndex, endIndex - startIndex};
  XI_ERROR_CATCH();
}

template <typename _ValueT>
inline Xi::Result<Span<const _ValueT>> span_slice_const(const std::vector<_ValueT>& container, size_t startIndex,
                                                        size_t endIndex) {
  return span_slice(container, startIndex, endIndex);
}

template <typename _ValueT, size_t _Size>
inline Xi::Result<Span<_ValueT>> span_slice(std::array<_ValueT, _Size>& container, size_t startIndex, size_t endIndex) {
  XI_ERROR_TRY();
  __impl::verify_slice(container.size(), startIndex, endIndex);
  return Span<_ValueT>{container.data() + startIndex, endIndex - startIndex};
  XI_ERROR_CATCH();
}

template <typename _ValueT, size_t _Size>
inline Xi::Result<Span<const _ValueT>> span_slice(const std::array<_ValueT, _Size>& container, size_t startIndex,
                                                  size_t endIndex) {
  XI_ERROR_TRY();
  __impl::verify_slice(container.size(), startIndex, endIndex);
  return Span<const _ValueT>{container.data() + startIndex, endIndex - startIndex};
  XI_ERROR_CATCH();
}

template <typename _ValueT, size_t _Size>
inline Xi::Result<Span<const _ValueT>> span_slice_const(const std::array<_ValueT, _Size>& container, size_t startIndex,
                                                        size_t endIndex) {
  return span_slice(container, startIndex, endIndex);
}
}  // namespace Xi
