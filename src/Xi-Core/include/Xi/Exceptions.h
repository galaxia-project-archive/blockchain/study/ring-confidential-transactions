/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Exceptional.h"

namespace Xi {
XI_DECLARE_EXCEPTIONAL_CATEGORY(Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(Runtime, "generic runtime error", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(IndexOutOfRange, "array/vector index is out of bounds", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidIndex, "array/vector index is invalid in called context", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidVariantType, "unexpected type in variant", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(NotImplemented, "feature required is not implemented yet", Runtime)
}  // namespace Xi
