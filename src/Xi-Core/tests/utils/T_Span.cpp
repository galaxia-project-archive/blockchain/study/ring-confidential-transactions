/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>
#include <Xi/Span.h>

#include <vector>
#include <array>

#define XI_TESTSUITE T_Xi_Span

TEST(XI_TESTSUITE, ThrowsOnInvalidRange) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<int> values{10, 0};
    EXPECT_THROW(span_slice(values, 8, 2).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 10, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 11, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 0, 12).takeOrThrow(), std::range_error);
  }
  {
    const std::vector<int> values{10, 0};
    EXPECT_THROW(span_slice(values, 8, 2).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 10, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 11, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 0, 12).takeOrThrow(), std::range_error);
  }
  {
    std::array<int, 10> values{};
    EXPECT_THROW(span_slice(values, 8, 2).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 10, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 11, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 0, 12).takeOrThrow(), std::range_error);
  }
  {
    const std::array<int, 10> values{};
    EXPECT_THROW(span_slice(values, 8, 2).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 10, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 11, 12).takeOrThrow(), std::range_error);
    EXPECT_THROW(span_slice(values, 0, 12).takeOrThrow(), std::range_error);
  }
}

TEST(XI_TESTSUITE, HasRightSize) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    std::vector<int> values{10, 0};
    for (size_t i = 0; i < values.size(); ++i) {
      EXPECT_EQ(span_slice(values, i, i).takeOrThrow().size(), 0);
      for (size_t j = i + 1; j <= values.size(); ++j) {
        EXPECT_EQ(span_slice(values, i, j).takeOrThrow().size(), j - i);
      }
    }
  }
}
