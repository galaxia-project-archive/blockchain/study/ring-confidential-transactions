/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/Amount.h"
#include "Xi/Rct/Types/RangeSignature.h"

namespace Xi {
namespace Rct {
namespace Range {
/// prove gives C, and mask such that \sumCi = C
///    c.f. https://eprint.iacr.org/2015/1098 section 5.1
///    and Ci is a commitment to either 0 or 2^i, i=0,...,63
///    thus this proves that "amount" is in [0, 2^64]
///    mask is a such that C = aG + bH, and b = amount
RangeSignature prove(Key& C, Key& mask, const Amount& amount);
}  // namespace Range
}  // namespace Rct
}  // namespace Xi
