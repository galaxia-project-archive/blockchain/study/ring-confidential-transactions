/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Result.h>

#include "Xi/Rct/Types/Amount.h"
#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/RctSignature.h"

namespace Xi {
namespace Rct {
class IEcdhDevice;

namespace Rct {
Xi::Result<Amount> decode_amount(const RctSignature &rv, const Key &sk, unsigned int i, Key &mask, IEcdhDevice &hwdev);
Xi::Result<Amount> decode_amount(const RctSignature &rv, const Key &sk, unsigned int i, IEcdhDevice &hwdev);
Xi::Result<Amount> decode_amount_simple(const RctSignature &rv, const Key &sk, unsigned int i, Key &mask,
                                        IEcdhDevice &hwdev);
Xi::Result<Amount> decode_amount_simple(const RctSignature &rv, const Key &sk, unsigned int i, IEcdhDevice &hwdev);
}  // namespace Rct
}  // namespace Rct
}  // namespace Xi
