/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Types/Amount.h"
#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/CtKey.h"
#include "Xi/Rct/Types/MultiSignatureData.h"
#include "Xi/Rct/Types/MultiSignatureOut.h"
#include "Xi/Rct/Types/RctSignature.h"
#include "Xi/Rct/Types/RctConfig.h"
#include "Xi/Rct/DeviceInterfaces/IRctDevice.h"

namespace Xi {
namespace Rct {
namespace Rct {
RctSignature generate(const Key &message, const CtKeyVector &inSk, const KeyVector &destinations,
                      const AmountVector &amounts, const CtKeyMatrix &mixRing, const KeyVector &amount_keys,
                      const MultiSignatureData *kLRki, MultiSignatureOut *msout, unsigned int index, CtKeyVector &outSk,
                      const RctConfig &rct_config, IRctDevice &hwdev);

RctSignature generate_simple(const Key &message, const CtKeyVector &inSk, const KeyVector &destinations,
                             const AmountVector &inamounts, const AmountVector &outamounts, Amount txnFee,
                             const CtKeyMatrix &mixRing, const KeyVector &amount_keys,
                             const MultiSignatureDataVector *kLRki, MultiSignatureOut *msout,
                             const std::vector<unsigned int> &index, CtKeyVector &outSk, const RctConfig &rct_config,
                             IRctDevice &hwdev);
}  // namespace Rct
}  // namespace Rct
}  // namespace Xi
