/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Types/RctSignature.h"

namespace Xi {
namespace Rct {
class IRctDevice;

namespace Rct {
[[nodiscard]] bool verify(const RctSignature& signature, bool verifySemantics, IRctDevice& hwdev);
[[nodiscard]] bool verify(const RctSignature& signature, IRctDevice& hwdev);
[[nodiscard]] bool verify_semantics_simple(const std::vector<const RctSignature*>& rvv);
[[nodiscard]] bool verify_semantics_simple(const RctSignature& signature);
[[nodiscard]] bool verify_none_semantics_simple(const RctSignature& signature, IRctDevice& hwdev);
[[nodiscard]] bool verify_simple(const RctSignature& signature, IRctDevice& hwdev);
}  // namespace Rct
}  // namespace Rct
}  // namespace Xi
