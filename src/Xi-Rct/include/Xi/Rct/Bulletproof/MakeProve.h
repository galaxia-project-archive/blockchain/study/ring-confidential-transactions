/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Span.h>

#include "Xi/Rct/Types/Amount.h"
#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/Bulletproof.h"

namespace Xi {
namespace Rct {
Bulletproof bulletproof_make_prove(const Key &v, const Key &gamma);
Bulletproof bulletproof_make_prove(Amount v, const Key &gamma);
Bulletproof bulletproof_make_prove(const KeyVector &v, const KeyVector &gamma);
Bulletproof bulletproof_make_prove(const AmountVector &v, const KeyVector &gamma);
Bulletproof bulletproof_make_prove(KeyVector &C, KeyVector &masks, const AmountVector &amounts, Span<const Key> sk);
}  // namespace Rct
}  // namespace Xi
