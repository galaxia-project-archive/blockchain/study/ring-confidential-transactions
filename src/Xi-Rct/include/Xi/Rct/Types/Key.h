/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <array>
#include <vector>

#include <Xi/Algorithm/GenericHash.h>
#include <Xi/Algorithm/GenericComparison.h>
#include <crypto/CryptoTypes.h>
#include <Serialization/ISerializer.h>

#include "Xi/Rct/Types/Byte.h"

namespace Xi {
namespace Rct {
struct Key : std::array<Byte, 32> {
  static const Key Zero;
  static const Key Identity;
  static const Key MinusOne;
  static const Key Two;
  static const Key CurveOrder;
  static const Key BasePoint;
  static const Key Eight;
  static const Key EightInverse;
  static const Key MinusEightInverse;

  Key copy() const;
  void copyTo(Key& out) const;

  operator const ::Crypto::PublicKey&() const;
  operator const ::Crypto::SecretKey&() const;
  operator const ::Crypto::KeyImage&() const;
  operator const ::Crypto::Hash&() const;
  operator const ::Crypto::KeyDerivation&() const;

  bool operator==(const Key& rhs) const;
  bool operator!=(const Key& rhs) const;
  bool operator<(const Key& rhs) const;
  bool operator<=(const Key& rhs) const;
  bool operator>(const Key& rhs) const;
  bool operator>=(const Key& rhs) const;
};

XI_MAKE_GENERIC_HASH_FUNC(Key)

using KeyVector = std::vector<Key>;
using KeyMatrix = std::vector<KeyVector>;  // TODO replace with proper matrix class

KeyMatrix make_key_matrix(size_t rows, size_t columns);

const Key& key_cast(const ::Crypto::PublicKey& key);
const Key& key_cast(const ::Crypto::SecretKey& key);
const Key& key_cast(const ::Crypto::KeyImage& key);
const Key& key_cast(const ::Crypto::Hash& key);
const Key& key_cast(const ::Crypto::KeyDerivation& key);

void serialize(Key& key, CryptoNote::ISerializer& serializer);
}  // namespace Rct
}  // namespace Xi

XI_MAKE_GENERIC_HASH_OVERLOAD(Xi::Rct, Key)
