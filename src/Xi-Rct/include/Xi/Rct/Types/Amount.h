/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include <Xi/Types/Arithmetic.h>
#include <Serialization/ISerializer.h>

namespace Xi {
namespace Rct {
struct Amount : Xi::Types::enable_arithmetic_from_this<uint64_t, Amount> {
  using enable_arithmetic_from_this::enable_arithmetic_from_this;

  static inline constexpr uint8_t bits() { return sizeof(uint64_t) * 8; }
};

using AmountVector = std::vector<Amount>;

void serialize(Amount& amount, CryptoNote::ISerializer& serializer);
}  // namespace Rct
}  // namespace Xi
