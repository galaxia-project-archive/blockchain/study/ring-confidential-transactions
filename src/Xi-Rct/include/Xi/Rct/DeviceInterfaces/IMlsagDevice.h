/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Types/Byte.h"
#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/CtKey.h"

namespace Xi {
namespace Rct {
class IMlsagDevice {
 public:
  virtual ~IMlsagDevice() = default;

  [[nodiscard]] virtual bool mlsagPrehash(const ByteVector &blob, size_t inputs_size, size_t outputs_size,
                                          const KeyVector &hashes, const CtKeyVector &outPk, Key &prehash) = 0;
  [[nodiscard]] virtual bool mlsagPrepare(const Key &H, const Key &xx, Key &a, Key &aG, Key &aHP, Key &rvII) = 0;
  [[nodiscard]] virtual bool mlsagPrepare(Key &a, Key &aG) = 0;
  [[nodiscard]] virtual bool mlsagHash(const KeyVector &long_message, Key &c) = 0;
  [[nodiscard]] virtual bool mlsagSign(const Key &c, const KeyVector &xx, const KeyVector &alpha, const size_t rows,
                                       const size_t dsRows, KeyVector &ss) = 0;
};
}  // namespace Rct
}  // namespace Xi
