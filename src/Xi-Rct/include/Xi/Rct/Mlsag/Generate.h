/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/MlsagSignature.h"
#include "Xi/Rct/Types/MultiSignatureData.h"

namespace Xi {
namespace Rct {
class IMlsagDevice;

namespace Mlsag {
/// Multilayered Spontaneous Anonymous Group Signatures (MLSAG signatures)
/// This is a just slghtly more efficient version than the ones described below
/// (will be explained in more detail in Ring Multisig paper
/// These are aka MG signatutes in earlier drafts of the ring ct paper
///  c.f. https://eprint.iacr.org/2015/1098 section 2.
///  Gen creates a signature which proves that for some column in the keymatrix "pk"
///    the signer knows a secret key for each row in that column
///  Ver verifies that the MG sig was created correctly
MlsagSignature generate_signature(const Key &message, const KeyMatrix &pk, const KeyVector &xx,
                                  const MultiSignatureData *kLRki, Key *mscout, const unsigned int index, size_t dsRows,
                                  IMlsagDevice &hwdev);
}  // namespace Mlsag
}  // namespace Rct
}  // namespace Xi
