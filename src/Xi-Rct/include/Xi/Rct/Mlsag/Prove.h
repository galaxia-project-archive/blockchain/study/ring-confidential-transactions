/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/CtKey.h"
#include "Xi/Rct/Types/MlsagSignature.h"
#include "Xi/Rct/Types/MultiSignatureData.h"

namespace Xi {
namespace Rct {
class IMlsagDevice;

namespace Mlsag {
/// Prove:
///   c.f. https://eprint.iacr.org/2015/1098 section 4. definition 10.
///   This does the MG sig on the "dest" part of the given key matrix, and
///   the last row is the sum of input commitments from that column - sum output commitments
///   this shows that sum inputs = sum outputs
MlsagSignature prove_rct(const Key &message, const CtKeyMatrix &pubs, const CtKeyVector &inSk, const CtKeyVector &outSk,
                         const CtKeyVector &outPk, const MultiSignatureData *kLRki, Key *mscout, unsigned int index,
                         const Key &txnFeeKey, IMlsagDevice &hwdev);

/// Ring-ct MG sigs Simple
///   Simple version for when we assume only
///       post rct inputs
///       here pubs is a vector of (P, C) length mixin
///   inSk is x, a_in corresponding to signing index
///       a_out, Cout is for the output commitment
///       index is the signing index..
MlsagSignature prove_rct_simple(const Key &message, const CtKeyVector &pubs, const CtKey &inSk, const Key &a,
                                const Key &Cout, const MultiSignatureData *kLRki, Key *mscout, unsigned int index,
                                IMlsagDevice &hwdev);
}  // namespace Mlsag
}  // namespace Rct
}  // namespace Xi
