/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <memory>

#include <Xi/Exceptional.h>

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/MultiexpData.h"

namespace Xi {
namespace Rct {
namespace Pippenger {
struct PippengerCache;
using SharedPippengerCache = std::shared_ptr<PippengerCache>;

// clang-format off
XI_DECLARE_EXCEPTIONAL_CATEGORY(Pippenger)
XI_DECLARE_EXCEPTIONAL_INSTANCE(BadCacheBaseData, "provided range is out of cache index", Pippenger)
XI_DECLARE_EXCEPTIONAL_INSTANCE(OutOfMemory, "cache reallocation failed", Pippenger)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InsufficientCacheSize, "cache is too small", Pippenger)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidCParameter, "derived c is too large", Pippenger)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidPowParameter, "pow2 may only be computed on numbers less 256", Pippenger)
XI_DECLARE_EXCEPTIONAL_INSTANCE(BucketOverflow, "insufficient bucket size", Pippenger)
// clang-format on

static inline constexpr size_t size_limit() { return 0; }

SharedPippengerCache init_cache(const MultiexpDataVector &data, size_t start_offset = 0, size_t N = 0);
size_t get_cache_size(const SharedPippengerCache &cache);
size_t get_c(size_t N);
Key pippenger(const MultiexpDataVector &data, const SharedPippengerCache &cache = nullptr, size_t cache_size = 0,
              size_t c = 0);
}  // namespace Pippenger
}  // namespace Rct
}  // namespace Xi
