/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <crypto/crypto-ops.h>

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/Amount.h"
#include "Xi/Rct/Types/AmountBits.h"
#include "Xi/Rct/Types/AmountBitKeys.h"
#include "Xi/Rct/Types/BorromeanSignature.h"

namespace Xi {
namespace Rct {
BorromeanSignature generate_borromean_signature(const AmountBitKeys& x, const AmountBitKeys& P1,
                                                const AmountBitKeys& P2, const AmountBits& indices);

[[nodiscard]] bool verify_borromean_signature(const BorromeanSignature& bb, const ge_p3 P1[Amount::bits()],
                                              const ge_p3 P2[Amount::bits()]);
[[nodiscard]] bool verify_borromean_signature(const BorromeanSignature& bb, const AmountBitKeys& P1,
                                              const AmountBitKeys& P2);
}  // namespace Rct
}  // namespace Xi
