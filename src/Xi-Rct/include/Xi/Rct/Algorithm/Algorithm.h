/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Algorithm/BosCoster.h"
#include "Xi/Rct/Algorithm/Borromean.h"
#include "Xi/Rct/Algorithm/BulletproofAmounts.h"
#include "Xi/Rct/Algorithm/EcdhEncoding.h"
#include "Xi/Rct/Algorithm/EcdhHashing.h"
#include "Xi/Rct/Algorithm/GenerateCommitment.h"
#include "Xi/Rct/Algorithm/GenerateCtKey.h"
#include "Xi/Rct/Algorithm/GenerateKey.h"
#include "Xi/Rct/Algorithm/Hadamard.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/KeyHashing.h"
#include "Xi/Rct/Algorithm/Multiexp.h"
#include "Xi/Rct/Algorithm/Pippenger.h"
#include "Xi/Rct/Algorithm/RctCategoryDetection.h"
#include "Xi/Rct/Algorithm/Straus.h"
#include "Xi/Rct/Algorithm/TypeConversion.h"
