/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <memory>

#include <Xi/Exceptional.h>

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/MultiexpData.h"

namespace Xi {
namespace Rct {
namespace Straus {
struct StrausCache;
using SharedStrausCache = std::shared_ptr<StrausCache>;

// clang-format off
XI_DECLARE_EXCEPTIONAL_CATEGORY(Straus)
XI_DECLARE_EXCEPTIONAL_INSTANCE(BadCacheBaseData, "provided range is out of cache index", Straus)
XI_DECLARE_EXCEPTIONAL_INSTANCE(OutOfMemory, "cache reallocation failed", Straus)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InsufficientCacheSize, "cache is too small", Straus)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidPowParameter, "pow2 may only be computed on numbers less 256", Straus)
// clang-format on

static inline constexpr size_t size_limit() { return 232; }

SharedStrausCache init_cache(const MultiexpDataVector &data, size_t N = 0);
size_t get_cache_size(const SharedStrausCache &cache);
Key straus(const MultiexpDataVector &data, const SharedStrausCache &cache = nullptr, size_t STEP = 0);

}  // namespace Straus
}  // namespace Rct
}  // namespace Xi
