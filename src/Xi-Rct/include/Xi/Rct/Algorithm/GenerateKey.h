/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/KeyPair.h"

namespace Xi {
namespace Rct {
Key generate_random_secret_key();
void generate_random_secret_key_inplace(Key& out);

Key generate_random_public_key();
void generate_random_public_key_inplace(Key& out);

KeyVector generate_random_secret_keys(size_t count);

Key generate_random_curve_point();
void generate_random_curve_point_inplace(Key& out);

KeyPair generate_random_key_pair();
void generate_random_key_pair_inplace(KeyPair& out);
void generate_random_key_pair_inplace(Key& publicKey, Key& secretKey);

}  // namespace Rct
}  // namespace Xi
