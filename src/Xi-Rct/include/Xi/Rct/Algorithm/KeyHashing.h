/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/CtKey.h"
#include "Xi/Rct/Types/AmountBitKeys.h"

namespace Xi {
namespace Rct {
Key key_fast_hash(const void *data, const size_t l);
void key_fast_hash_inplace(const void *data, const size_t l, Key &out);

Key key_fast_hash_to_scalar(const void *data, const size_t l);
void key_fast_hash_to_scalar_inplace(const void *data, const size_t l, Key &out);

Key key_fast_hash(const Key &in);
void key_fast_hash_inplace(const Key &in, Key &out);

Key key_fast_hash_to_scalar(const Key &in);
void key_fast_hash_to_scalar_inplace(const Key &in, Key &out);

Key key_fast_hash_to_point(const Key &in);
void key_fast_hash_to_point_inplace(const Key &in, Key &out);

Key key_fast_hash_to_point_simple(const Key &in);
void key_fast_hash_to_point_simple_inplace(const Key &in, Key &out);

// for mg sigs
Key key_fast_hash128(const void *in);
void key_fast_hash128_inplace(const void *in, Key &out);

Key key_fast_hash_scalar128(const void *in);
void key_fast_hash_scalar128_inplace(const void *in, Key &out);

Key key_fast_hash(const CtKeyVector &PC);
void key_fast_hash_inplace(const CtKeyVector &PC, Key &out);

Key key_fast_hash_to_scalar(const CtKeyVector &PC);
void key_fast_hash_to_scalar_inplace(const CtKeyVector &PC, Key &out);

// for mg sigs
Key key_fast_hash(const KeyVector &keys);
void key_fast_hash_inplace(const KeyVector &keys, Key &out);

Key key_fast_hash_to_scalar(const KeyVector &keys);
void key_fast_hash_to_scalar_inplace(const KeyVector &keys, Key &out);

// for ANSL
Key key_fast_hash(const AmountBitKeys &keys);
void key_fast_hash_inplace(const AmountBitKeys &keys, Key &out);

Key key_fast_hash_to_scalar(const AmountBitKeys &keys);
void key_fast_hash_to_scalar_inplace(const AmountBitKeys &keys, Key &out);
}  // namespace Rct
}  // namespace Xi
