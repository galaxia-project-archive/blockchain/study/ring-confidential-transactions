/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <crypto/crypto-ops.h>

#include "Xi/Rct/Types/Types.h"

namespace Xi {
namespace Rct {
void amount_to_key_inplace(Amount amount, Key& out);
Key amount_to_key(Amount amount);

void amount_to_key_with_key_inplace(Amount amount, const Key& a, Key& out);
Key amount_to_key_with_key(Amount amount, const Key& a);

void amount_to_bits_inplace(Amount amount, AmountBits& bits);
void amount_to_bits_inplace(Amount amount, AmountBits::pointer bits);

Amount key_to_amount(const Key& key);

Key bits_to_key(const AmountBits& bits);
Key bits_to_key(AmountBits::const_pointer bits);
void bits_to_key_inplace(const AmountBits& bits, Key& out);
void bits_to_key_inplace(AmountBits::const_pointer bits, Key& out);

Amount bits_to_amount(const AmountBits& bits);
Amount bits_to_amount(AmountBits::const_pointer bits);

// TODO replace with simple conversion and move this to algorithm header
bool key_to_point_and_check_order(const Key& key, ge_p3& out);

// returns hashToPoint as described in https://github.com/ShenNoether/ge_fromfe_writeup
/// \throws GeFromBytesVarTimeError
Key key_hash_to_key_point_simple(const Key& in);

/// \throws GeFromBytesVarTimeError
void key_hash_to_key_point_simple_inplace(const Key& in, Key& out);

Key key_hash_to_key_point(const Key& in);
void key_hash_to_key_point_inplace(const Key& in, Key& out);
}  // namespace Rct
}  // namespace Xi
