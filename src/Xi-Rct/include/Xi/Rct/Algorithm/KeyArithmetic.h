/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Exceptional.h>
#include <Xi/Span.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Types/Key.h"

namespace Xi {
namespace Rct {
XI_DECLARE_EXCEPTIONAL_CATEGORY(RctKeyOperation)
XI_DECLARE_EXCEPTIONAL_INSTANCE(GeFromBytesVarTime, "ge_frombytes_vartime failed", RctKeyOperation)
XI_DECLARE_EXCEPTIONAL_INSTANCE(IncompatibleSize, "binary vector operation has incompatible sizes", RctKeyOperation)

Key key_multiply_scalar_with_basepoint(const Key& operand);
void key_multiply_scalar_with_basepoint_inplace(const Key& operand, Key& out);

/// \see ge_p3_h
Key key_multiply_scalar_with_h_constant(const Key& operand);
/// \see ge_p3_h
void key_multiply_scalar_with_h_constant_inplace(const Key& operand, Key& out);

///
/// \return 8 * operand
/// \throws GeFromByteVarTimeError
///
Key key_multiply_scalar_with_eight(const Key& operand);

///
/// \param out set to 8 * operand
/// \throws GeFromByteVarTimeError
///
void key_multiply_scalar_with_eight_inplace(const Key& operand, Key& out);

/// \throws GeFromBytesVarTimeError
Key key_multiply_scalar(const Key& lhs, const Key& rhs);

/// \throws GeFromBytesVarTimeError
void key_mulitply_scalar_inplace(const Key& lhs, const Key& rhs, Key& out);

/// \throws GeFromBytesVarTimeError
Key key_add(const Key& lhs, const Key& rhs);

/// \throws GeFromBytesVarTimeError
void key_add_inplace(const Key& lhs, const Key& rhs, Key& out);

/// \throws GeFromBytesVarTimeError
Key key_accumulate_sum(const KeyVector& keys);

/// \throws GeFromBytesVarTimeError
void key_accumulate_sum_inplace(const KeyVector& keys, Key& out);

/// \throws GeFromBytesVarTimeError
Key key_accumulate_point_sum(const KeyVector& keys);

/// \throws GeFromBytesVarTimeError
void key_accumulate_point_sum_inplace(const KeyVector& keys, Key& out);

/// \throws GeFromBytesVarTimeError
/// \return lhs * G + rhs
Key key_multiply_with_basepoint_and_add(const Key& lhs, const Key& rhs);

/// \throws GeFromBytesVarTimeError
/// \param out = lhs * G + rhs
void key_multiply_with_basepoint_and_add_inplace(const Key& lhs, const Key& rhs, Key& out);

/// \throws GeFromBytesVarTimeError
/// \return baseScalar * G + pointScalar * point
Key key_multiply_with_basepoint_and_add_with_multiplied_point(const Key& baseScalar, const Key& pointScalar,
                                                              const Key& point);

/// \throws GeFromBytesVarTimeError
/// \param out = baseScalar * G + pointScalar * point
void key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(const Key& baseScalar, const Key& pointScalar,
                                                                       const Key& point, Key& out);

/// \throws GeFromBytesVarTimeError
/// \return lhsScalar * lhsPoint + rhsScalar * rhsPoint
Key key_add_multiplied_points(const Key& lhsScalar, const Key& lhsPoint, const Key& rhsScalar, ge_dsmp rhsPoint);

/// \throws GeFromBytesVarTimeError
/// \param out = lhsScalar * lhsPoint + rhsScalar * rhsPoint
void key_add_multiplied_points_inplace(const Key& lhsScalar, const Key& lhsPoint, const Key& rhsScalar,
                                       ge_dsmp rhsPoint, Key& out);

/// \return lhsScalar * lhsPoint + rhsScalar * rhsPoint
Key key_add_multiplied_points(const Key& lhsScalar, ge_dsmp lhsPoint, const Key& rhsScalar, ge_dsmp rhsPoint);

/// \param out = lhsScalar * lhsPoint + rhsScalar * rhsPoint
void key_add_multiplied_points_inplace(const Key& lhsScalar, ge_dsmp lhsPoint, const Key& rhsScalar, ge_dsmp rhsPoint,
                                       Key& out);
/// \throws GeFromBytesVarTimeError
void key_precompute_dsmp(const Key& key, ge_dsmp out);

/// \throws GeFromBytesVarTimeError
/// \return lhs - rhs
Key key_subtract(const Key& lhs, const Key& rhs);

/// \throws GeFromBytesVarTimeError
/// \param out = lhs - rhs
void key_subtract_inplace(const Key& lhs, const Key& rhs, Key& out);

KeyVector key_vector_powers(const Key& x, size_t n);
Key key_vector_power_sum(Key x, size_t n);
Key key_vector_inner_product(const KeyVector& a, const KeyVector& b);
Key key_vector_inner_product(const Span<const Key>& a, const Span<const Key>& b);

KeyVector key_vector_add(const KeyVector& lhs, const KeyVector& rhs);
KeyVector key_vector_add(const KeyVector& lhs, const Key& rhs);
Key key_vector_sum(const KeyVector& A);

KeyVector key_vector_subtract(const KeyVector& lhs, const KeyVector& rhs);
KeyVector key_vector_subtract(const KeyVector& lhs, const Key& rhs);

KeyVector key_vector_scalar(const KeyVector& lhs, const KeyVector& rhs);
KeyVector key_vector_scalar(const KeyVector& lhs, const Key& rhs);
KeyVector key_vector_scalar(const Span<const Key>& lhs, const Key& rhs);

Key key_invert(const Key& key);
void key_invert_inplace(const Key& key, Key& out);

KeyVector key_vector_invert(KeyVector keys);
}  // namespace Rct
}  // namespace Xi
