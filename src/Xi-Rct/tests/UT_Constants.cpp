/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <cstring>

#include <crypto/crypto-ops.h>
#include <Xi/Rct/Constants.h>
#include <Xi/Rct/Types/Amount.h>
#include <Xi/Rct/Algorithm/KeyArithmetic.h>
#include <Xi/Rct/Algorithm/KeyHashing.h>
#include <Xi/Rct/Algorithm/TypeConversion.h>

#define XI_TESTSUITE T_Xi_Rct_Constants

TEST(XI_TESTSUITE, H) {
  using namespace ::Xi;
  using namespace ::Xi::Rct;

  ge_p3 p3;
  ASSERT_EQ(ge_frombytes_vartime(&p3, Constants::H.data()), 0);
  ASSERT_EQ(std::memcmp(&p3, &ge_p3_H, sizeof(ge_p3)), 0);
}

TEST(XI_TESTSUITE, HPow2) {
  using namespace ::Xi;
  using namespace ::Xi::Rct;

  Key G = key_multiply_scalar_with_basepoint(amount_to_key(Amount{1}));

  Key H = key_fast_hash_to_point_simple(G);
  for (size_t j = 0; j < Amount::bits(); j++) {
    ASSERT_TRUE(H == Constants::H2[j]);
    key_add_inplace(H, H, H);
  }
}
