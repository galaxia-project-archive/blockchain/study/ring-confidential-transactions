/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Rct/Rct.h>
#include <Xi/Crypto/Device/DefaultDevice.h>

#include "utils/GenerateTestRct.h"

#define XI_TESTSUITE T_Xi_Rct_Range

TEST(XI_TESTSUITE, ProofWithoutFee) {
  using namespace ::Xi;
  using namespace ::Xi::Rct;

  // Ring CT Stuff
  // ct range proofs
  CtKeyVector sc, pc;
  CtKey sctmp, pctmp;
  // add fake input 5000
  generate_pedersen_commitment_inplace(Amount{6000}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);

  generate_pedersen_commitment_inplace(Amount{7000}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);
  AmountVector amounts;
  KeyVector amount_keys;
  Key mask;

  // add output 500
  amounts.push_back(Amount{500});
  amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
  KeyVector destinations;
  Key Sk, Pk;
  generate_random_key_pair_inplace(Pk, Sk);
  destinations.push_back(Pk);

  // add output for 12500
  amounts.push_back(Amount{12500});
  amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
  generate_random_key_pair_inplace(Pk, Sk);
  destinations.push_back(Pk);

  const RctConfig rct_config{RangeProofType::Borromean, 0};

  auto& hwdevice = Xi::Crypto::Device::DefaultDevice::instance();

  // compute rct data with mixin 500
  CtKeyMatrix mixRing;
  CtKeyVector outSk;
  RctSignature s = XiTestUtils::generate_test_rct_signature(Key::Zero, sc, pc, destinations, amounts, amount_keys,
                                                            nullptr, nullptr, 3, rct_config, hwdevice);

  // verify rct data
  ASSERT_TRUE(Xi::Rct::Rct::verify(s, hwdevice));

  // decode received amount
  Xi::Rct::Rct::decode_amount(s, amount_keys[1], 1, mask, hwdevice).throwOnError();

  // Ring CT with failing MG sig part should not verify!
  // Since sum of inputs != outputs

  amounts[1] = Amount{12501};
  generate_random_key_pair_inplace(Pk, Sk);
  destinations[1] = Pk;

  // compute rct data with mixin 500
  s = XiTestUtils::generate_test_rct_signature(Key::Zero, sc, pc, destinations, amounts, amount_keys, nullptr, nullptr,
                                               3, rct_config, hwdevice);

  // verify rct data
  ASSERT_FALSE(Xi::Rct::Rct::verify(s, hwdevice));

  // decode received amount
  Xi::Rct::Rct::decode_amount(s, amount_keys[1], 1, mask, hwdevice).throwOnError();
}

TEST(XI_TESTSUITE, ProofWithFee) {
  using namespace ::Xi;
  using namespace ::Xi::Rct;

  // Ring CT Stuff
  // ct range proofs
  CtKeyVector sc, pc;
  CtKey sctmp, pctmp;
  // add fake input 5000
  generate_pedersen_commitment_inplace(Amount{6001}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);

  generate_pedersen_commitment_inplace(Amount{7000}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);
  AmountVector amounts;
  KeyVector amount_keys;
  Key mask;

  // add output 500
  amounts.push_back(Amount{500});
  amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
  KeyVector destinations;
  Key Sk, Pk;
  generate_random_key_pair_inplace(Pk, Sk);
  destinations.push_back(Pk);

  // add txn fee for 1
  // has no corresponding destination..
  amounts.push_back(Amount{1});

  // add output for 12500
  amounts.push_back(Amount{12500});
  amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
  generate_random_key_pair_inplace(Pk, Sk);
  destinations.push_back(Pk);

  const RctConfig rct_config{RangeProofType::Borromean, 0};

  auto& hwdevice = Xi::Crypto::Device::DefaultDevice::instance();

  // compute rct data with mixin 500
  RctSignature s = XiTestUtils::generate_test_rct_signature(Key::Zero, sc, pc, destinations, amounts, amount_keys,
                                                            nullptr, nullptr, 3, rct_config, hwdevice);

  // verify rct data
  ASSERT_TRUE(Xi::Rct::Rct::verify(s, hwdevice));

  // decode received amount
  Xi::Rct::Rct::decode_amount(s, amount_keys[1], 1, mask, hwdevice).throwOnError();

  // Ring CT with failing MG sig part should not verify!
  // Since sum of inputs != outputs

  amounts[1] = Amount{12501};
  generate_random_key_pair_inplace(Pk, Sk);
  destinations[1] = Pk;

  // compute rct data with mixin 500
  s = XiTestUtils::generate_test_rct_signature(Key::Zero, sc, pc, destinations, amounts, amount_keys, nullptr, nullptr,
                                               3, rct_config, hwdevice);

  // verify rct data
  ASSERT_FALSE(Xi::Rct::Rct::verify(s, hwdevice));

  // decode received amount
  Xi::Rct::Rct::decode_amount(s, amount_keys[1], 1, mask, hwdevice).throwOnError();
}

TEST(XI_TESTSUITE, Simple) {
  using namespace ::Xi;
  using namespace ::Xi::Rct;

  CtKeyVector sc, pc;
  CtKey sctmp, pctmp;
  // this vector corresponds to output amounts
  AmountVector outamounts;
  // this vector corresponds to input amounts
  AmountVector inamounts;
  // this keyV corresponds to destination pubkeys
  KeyVector destinations;
  KeyVector amount_keys;
  Key mask;

  // add fake input 3000
  // the sc is secret data
  // pc is public data
  generate_pedersen_commitment_inplace(Amount{3000}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);
  inamounts.push_back(Amount{3000});

  // add fake input 3000
  // the sc is secret data
  // pc is public data
  generate_pedersen_commitment_inplace(Amount{3000}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);
  inamounts.push_back(Amount{3000});

  // add output 5000
  outamounts.push_back(Amount{5000});
  amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
  // add the corresponding destination pubkey
  Key Sk, Pk;
  generate_random_key_pair_inplace(Pk, Sk);
  destinations.push_back(Pk);

  // add output 999
  outamounts.push_back(Amount{999});
  amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
  // add the corresponding destination pubkey
  generate_random_key_pair_inplace(Pk, Sk);
  destinations.push_back(Pk);

  Key message = generate_random_secret_key();  // real message later (hash of txn..)

  // compute sig with mixin 2
  Amount txnfee{1};

  auto& hwdevice = Xi::Crypto::Device::DefaultDevice::instance();

  const RctConfig rct_config{RangeProofType::Borromean, 0};
  RctSignature s =
      XiTestUtils::generate_test_rct_signature_simple(message, sc, pc, destinations, inamounts, outamounts, amount_keys,
                                                      nullptr, nullptr, txnfee, 2, rct_config, hwdevice);

  // verify ring ct signature
  ASSERT_TRUE(Xi::Rct::Rct::verify_simple(s, hwdevice));

  // decode received amount corresponding to output pubkey index 1
  Xi::Rct::Rct::decode_amount_simple(s, amount_keys[1], 1, mask, hwdevice).throwOnError();
}
