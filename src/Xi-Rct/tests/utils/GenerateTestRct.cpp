/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "utils/GenerateTestRct.h"

#include <utility>

#include <Xi/Global.h>
#include <Xi/Rct/Algorithm/GenerateKey.h>
#include <Xi/Rct/Rct/Generate.h>

#include "utils/GenerateAmount.h"

using namespace Xi::Rct;

namespace {
void getKeyFromBlockchain(CtKey &a, size_t reference_index) {
  XI_UNUSED(reference_index);
  a.Mask = generate_random_public_key();
  a.Dest = generate_random_public_key();
}

std::tuple<CtKeyMatrix, Amount> populateFromBlockchain(CtKeyVector inPk, size_t mixin) {
  size_t rows = inPk.size();
  CtKeyMatrix rv(mixin + 1, inPk);
  uint64_t index = XiTestUtils::random_amount(Amount{mixin}).value;
  size_t i = 0, j = 0;
  for (i = 0; i <= mixin; i++) {
    if (i != index) {
      for (j = 0; j < rows; j++) {
        getKeyFromBlockchain(rv[i][j], 0);
      }
    }
  }
  return std::make_tuple(rv, Amount{index});
}

Amount populateFromBlockchainSimple(CtKeyVector &mixRing, const CtKey &inPk, size_t mixin) {
  size_t index = XiTestUtils::random_amount(Amount{mixin}).value;
  size_t i = 0;
  for (i = 0; i <= mixin; i++) {
    if (i != index) {
      getKeyFromBlockchain(mixRing[i], (size_t)XiTestUtils::random_amount(Amount{1000}).value);
    } else {
      mixRing[i] = inPk;
    }
  }
  return Amount{index};
}
}  // namespace

RctSignature XiTestUtils::generate_test_rct_signature(const Key &message, const CtKeyVector &inSk,
                                                      const CtKeyVector &inPk, const KeyVector &destinations,
                                                      const AmountVector &amounts, const KeyVector &amount_keys,
                                                      const MultiSignatureData *kLRki, MultiSignatureOut *msout,
                                                      const size_t mixin, const RctConfig &rct_config,
                                                      IRctDevice &hwdev) {
  Amount index;
  CtKeyMatrix mixRing;
  CtKeyVector outSk;
  std::tie(mixRing, index) = populateFromBlockchain(inPk, mixin);
  return Xi::Rct::Rct::generate(message, inSk, destinations, amounts, mixRing, amount_keys, kLRki, msout,
                                (unsigned int)index.value, outSk, rct_config, hwdev);
}

RctSignature XiTestUtils::generate_test_rct_signature_simple(
    const Key &message, const CtKeyVector &inSk, const CtKeyVector &inPk, const KeyVector &destinations,
    const AmountVector &inamounts, const AmountVector &outamounts, const KeyVector &amount_keys,
    const MultiSignatureDataVector *kLRki, MultiSignatureOut *msout, const Amount txnFee, size_t mixin,
    const RctConfig &rct_config, IRctDevice &hwdev) {
  std::vector<unsigned int> index;
  index.resize(inPk.size());
  CtKeyMatrix mixRing;
  CtKeyVector outSk;
  mixRing.resize(inPk.size());
  for (size_t i = 0; i < inPk.size(); ++i) {
    mixRing[i].resize(mixin + 1);
    index[i] = (unsigned int)populateFromBlockchainSimple(mixRing[i], inPk[i], mixin).value;
  }
  return Xi::Rct::Rct::generate_simple(message, inSk, destinations, inamounts, outamounts, txnFee, mixRing, amount_keys,
                                       kLRki, msout, index, outSk, rct_config, hwdev);
}
