/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Rct/Types/RctConfig.h>
#include <Xi/Rct/Types/RctSignature.h>
#include <Xi/Rct/Types/MultiSignatureData.h>
#include <Xi/Rct/Types/MultiSignatureOut.h>
#include <Xi/Rct/DeviceInterfaces/DeviceInterfaces.h>

namespace XiTestUtils {
Xi::Rct::RctSignature generate_test_rct_signature(
    const Xi::Rct::Key &message, const Xi::Rct::CtKeyVector &inSk, const Xi::Rct::CtKeyVector &inPk,
    const Xi::Rct::KeyVector &destinations, const Xi::Rct::AmountVector &amounts, const Xi::Rct::KeyVector &amount_keys,
    const Xi::Rct::MultiSignatureData *kLRki, Xi::Rct::MultiSignatureOut *msout, const size_t mixin,
    const Xi::Rct::RctConfig &rct_config, Xi::Rct::IRctDevice &hwdev);

Xi::Rct::RctSignature generate_test_rct_signature_simple(
    const Xi::Rct::Key &message, const Xi::Rct::CtKeyVector &inSk, const Xi::Rct::CtKeyVector &inPk,
    const Xi::Rct::KeyVector &destinations, const Xi::Rct::AmountVector &inamounts,
    const Xi::Rct::AmountVector &outamounts, const Xi::Rct::KeyVector &amount_keys,
    const Xi::Rct::MultiSignatureDataVector *kLRki, Xi::Rct::MultiSignatureOut *msout, const Xi::Rct::Amount txnFee,
    size_t mixin, const Xi::Rct::RctConfig &rct_config, Xi::Rct::IRctDevice &hwdev);
}  // namespace XiTestUtils
