/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Rct/Constants.h>
#include <Xi/Rct/Algorithm/Borromean.h>
#include <Xi/Rct/Algorithm/GenerateKey.h>
#include <Xi/Rct/Algorithm/KeyArithmetic.h>
#include <Xi/Rct/Algorithm/TypeConversion.h>

#include "utils/GenerateAmount.h"

#define XI_TESTSUITE T_Xi_Rct

TEST(XI_TESTSUITE, Borromean) {
  using namespace ::testing;
  using namespace ::Xi;
  using namespace ::Xi::Rct;

  // Tests for Borromean signatures
  //#boro true one, false one, C != sum Ci, and one out of the range..
  uint16_t N = Amount::bits();
  AmountBitKeys xv;
  AmountBitKeys P1v;
  AmountBitKeys P2v;
  AmountBits indi;

  for (size_t j = 0; j < N; j++) {
    indi[j] = (uint32_t)XiTestUtils::random_amount(Amount{2}).value;

    xv[j] = generate_random_secret_key();
    if (indi[j] == 0) {
      key_multiply_scalar_with_basepoint_inplace(xv[j], P1v[j]);
    } else {
      key_multiply_with_basepoint_and_add_inplace(xv[j], Constants::H2[j], P1v[j]);
    }
    key_subtract_inplace(P1v[j], Constants::H2[j], P2v[j]);
  }

  auto bb = generate_borromean_signature(xv, P1v, P2v, indi);
  ASSERT_TRUE(verify_borromean_signature(bb, P1v, P2v));

  indi[3] = (indi[3] + 1) % 2;
  bb = generate_borromean_signature(xv, P1v, P2v, indi);
  ASSERT_FALSE(verify_borromean_signature(bb, P1v, P2v));

  indi[3] = (indi[3] + 1) % 2;
  bb = generate_borromean_signature(xv, P1v, P2v, indi);
  ASSERT_TRUE(verify_borromean_signature(bb, P1v, P2v));

  bb = generate_borromean_signature(xv, P2v, P1v, indi);
  ASSERT_FALSE(verify_borromean_signature(bb, P1v, P2v));
}
