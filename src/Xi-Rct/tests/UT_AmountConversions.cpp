/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Rct/Types/Key.h>
#include <Xi/Rct/Types/Amount.h>
#include <Xi/Rct/Algorithm/TypeConversion.h>
#include <Xi/Rct/Algorithm/GenerateKey.h>

using namespace ::testing;
using namespace ::Xi;
using namespace ::Xi::Rct;

namespace {
static const Amount TestAmounts[] = {Amount{0},
                                     Amount{1},
                                     Amount{2},
                                     Amount{3},
                                     Amount{4},
                                     Amount{5},
                                     Amount{10000},
                                     Amount{10000000000000000000ull},
                                     Amount{10203040506070809000ull},
                                     Amount{123456789123456789}};

}  // namespace

#define XI_TESTSUITE T_Xi_Rct_AmountConversions

TEST(XI_TESTSUITE, Key) {
  Xi::Rct::Key k, P1;
  generate_random_key_pair_inplace(P1, k);
  for (auto amount : TestAmounts) {
    amount_to_key_inplace(amount, k);
    ASSERT_TRUE(amount == key_to_amount(k));
  }
}

TEST(XI_TESTSUITE, Bits) {
  for (auto amount : TestAmounts) {
    AmountBits b;
    amount_to_bits_inplace(amount, b);
    ASSERT_TRUE(amount == bits_to_amount(b));
  }
}
