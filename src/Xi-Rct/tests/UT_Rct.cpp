/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <random>
#include <cstring>

#include <Xi/Rct/Rct.h>
#include <Xi/Crypto/Device/DefaultDevice.h>

#include "utils/GenerateTestRct.h"

#define XI_TESTSUITE T_Xi_Rct_Rct

using namespace Xi;
using namespace Xi::Rct;

#define NELTS(array) (sizeof(array) / sizeof(array[0]))

namespace {
static RctSignature make_sample_rct_sig(int n_inputs, const uint64_t input_amounts[], int n_outputs,
                                        const uint64_t output_amounts[], bool last_is_fee) {
  CtKeyVector sc, pc;
  CtKey sctmp, pctmp;
  AmountVector amounts;
  KeyVector destinations;
  KeyVector amount_keys;
  Key Sk, Pk;

  for (int n = 0; n < n_inputs; ++n) {
    generate_pedersen_commitment_inplace(Amount{input_amounts[n]}, pctmp, sctmp);
    sc.push_back(sctmp);
    pc.push_back(pctmp);
  }

  for (int n = 0; n < n_outputs; ++n) {
    amounts.push_back(Amount{output_amounts[n]});
    generate_random_key_pair_inplace(Pk, Sk);
    if (n < n_outputs - 1 || !last_is_fee) {
      destinations.push_back(Pk);
      amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
    }
  }

  const RctConfig rct_config{RangeProofType::Borromean, 0};
  return XiTestUtils::generate_test_rct_signature(Key::Zero, sc, pc, destinations, amounts, amount_keys, nullptr,
                                                  nullptr, 3, rct_config,
                                                  Xi::Crypto::Device::DefaultDevice::instance());
}

static RctSignature make_sample_simple_rct_sig(int n_inputs, const uint64_t input_amounts[], int n_outputs,
                                               const uint64_t output_amounts[], uint64_t fee) {
  CtKeyVector sc, pc;
  CtKey sctmp, pctmp;
  AmountVector inamounts, outamounts;
  KeyVector destinations;
  KeyVector amount_keys;
  Key Sk, Pk;

  for (int n = 0; n < n_inputs; ++n) {
    inamounts.push_back(Amount{input_amounts[n]});
    generate_pedersen_commitment_inplace(Amount{input_amounts[n]}, pctmp, sctmp);
    sc.push_back(sctmp);
    pc.push_back(pctmp);
  }

  for (int n = 0; n < n_outputs; ++n) {
    outamounts.push_back(Amount{output_amounts[n]});
    amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
    generate_random_key_pair_inplace(Pk, Sk);
    destinations.push_back(Pk);
  }

  const RctConfig rct_config{RangeProofType::Borromean, 0};
  return XiTestUtils::generate_test_rct_signature_simple(Key::Zero, sc, pc, destinations, inamounts, outamounts,
                                                         amount_keys, nullptr, nullptr, Amount{fee}, 3, rct_config,
                                                         Xi::Crypto::Device::DefaultDevice::instance());
}

static bool range_proof_test(bool expected_valid, int n_inputs, const uint64_t input_amounts[], int n_outputs,
                             const uint64_t output_amounts[], bool last_is_fee, bool simple) {
  // compute rct data
  bool valid;
  try {
    RctSignature s;
    // simple takes fee as a parameter, non-simple takes it as an extra element to output amounts
    if (simple) {
      s = make_sample_simple_rct_sig(n_inputs, input_amounts, last_is_fee ? n_outputs - 1 : n_outputs, output_amounts,
                                     last_is_fee ? output_amounts[n_outputs - 1] : 0);
      valid = Xi::Rct::Rct::verify_simple(s, Xi::Crypto::Device::DefaultDevice::instance());
    } else {
      s = make_sample_rct_sig(n_inputs, input_amounts, n_outputs, output_amounts, last_is_fee);
      valid = Xi::Rct::Rct::verify(s, Xi::Crypto::Device::DefaultDevice::instance());
    }
  } catch (const std::exception&) {
    valid = false;
  }

  if (valid == expected_valid) {
    return testing::AssertionSuccess();
  } else {
    return testing::AssertionFailure();
  }
}

static RctSignature make_sig() {
  static const uint64_t inputs[] = {1000, 1000};
  static const uint64_t outputs[] = {1000, 1000};
  static RctSignature sig = make_sample_rct_sig(NELTS(inputs), inputs, NELTS(outputs), outputs, true);
  return sig;
}

static RctSignature make_sig_simple() {
  static const uint64_t inputs[] = {1000, 1000};
  static const uint64_t outputs[] = {1000};
  static RctSignature sig = make_sample_simple_rct_sig(NELTS(inputs), inputs, NELTS(outputs), outputs, 1000);
  return sig;
}
}  // namespace

TEST(XI_TESTSUITE, range_proofs_reject_empty_outs) {
  const uint64_t inputs[] = {5000};
  uint64_t outputs;
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, 0, &outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_empty_outs_simple) {
  const uint64_t inputs[] = {5000};
  uint64_t outputs;
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, 0, &outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_empty_ins) {
  uint64_t inputs;
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, 0, &inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_empty_ins_simple) {
  uint64_t inputs;
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, 0, &inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_all_empty) {
  uint64_t inputs;
  uint64_t outputs;
  EXPECT_TRUE(range_proof_test(false, 0, &inputs, 0, &outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_all_empty_simple) {
  uint64_t inputs;
  uint64_t outputs;
  EXPECT_TRUE(range_proof_test(false, 0, &inputs, 0, &outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_empty) {
  const uint64_t inputs[] = {0};
  uint64_t outputs;
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, 0, &outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_empty_simple) {
  const uint64_t inputs[] = {0};
  uint64_t outputs;
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, 0, &outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_empty_zero) {
  uint64_t inputs;
  const uint64_t outputs[] = {0};
  EXPECT_TRUE(range_proof_test(false, 0, &inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_empty_zero_simple) {
  uint64_t inputs;
  const uint64_t outputs[] = {0};
  EXPECT_TRUE(range_proof_test(false, 0, &inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_zero) {
  const uint64_t inputs[] = {0};
  const uint64_t outputs[] = {0};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_zero_simple) {
  const uint64_t inputs[] = {0};
  const uint64_t outputs[] = {0};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_out_first) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {0, 5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_out_first_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {0, 5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_out_last) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {5000, 0};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_out_last_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {5000, 0};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_out_middle) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {2500, 0, 2500};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_out_middle_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {2500, 0, 2500};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_in_first) {
  const uint64_t inputs[] = {0, 5000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_in_first_simple) {
  const uint64_t inputs[] = {0, 5000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_in_last) {
  const uint64_t inputs[] = {5000, 0};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_in_last_simple) {
  const uint64_t inputs[] = {5000, 0};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_in_middle) {
  const uint64_t inputs[] = {2500, 0, 2500};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_zero_in_middle_simple) {
  const uint64_t inputs[] = {2500, 0, 2500};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_lower) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {1};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_lower_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {1};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_higher) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {5001};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_higher_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {5001};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_out_negative) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {(uint64_t)-1000ll};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_out_negative_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {(uint64_t)-1000ll};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_out_negative_first) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {(uint64_t)-1000ll, 6000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_out_negative_first_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {(uint64_t)-1000ll, 6000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_out_negative_last) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {6000, (uint64_t)-1000ll};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_out_negative_last_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {6000, (uint64_t)-1000ll};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_out_negative_middle) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {3000, (uint64_t)-1000ll, 3000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_out_negative_middle_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {3000, (uint64_t)-1000ll, 3000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_in_negative) {
  const uint64_t inputs[] = {(uint64_t)-1000ll};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_single_in_negative_simple) {
  const uint64_t inputs[] = {(uint64_t)-1000ll};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_in_negative_first) {
  const uint64_t inputs[] = {(uint64_t)-1000ll, 6000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_in_negative_first_simple) {
  const uint64_t inputs[] = {(uint64_t)-1000ll, 6000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_in_negative_last) {
  const uint64_t inputs[] = {6000, (uint64_t)-1000ll};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_in_negative_last_simple) {
  const uint64_t inputs[] = {6000, (uint64_t)-1000ll};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_in_negative_middle) {
  const uint64_t inputs[] = {3000, (uint64_t)-1000ll, 3000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_in_negative_middle_simple) {
  const uint64_t inputs[] = {3000, (uint64_t)-1000ll, 3000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_reject_higher_list) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {1000, 1000, 1000, 1000, 1000, 1000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_reject_higher_list_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {1000, 1000, 1000, 1000, 1000, 1000};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_1_to_1) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_1_to_1_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_1_to_N) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {1000, 1000, 1000, 1000, 1000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_1_to_N_simple) {
  const uint64_t inputs[] = {5000};
  const uint64_t outputs[] = {1000, 1000, 1000, 1000, 1000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_N_to_1) {
  const uint64_t inputs[] = {1000, 1000, 1000, 1000, 1000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_N_to_1_simple) {
  const uint64_t inputs[] = {1000, 1000, 1000, 1000, 1000};
  const uint64_t outputs[] = {5000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_N_to_N) {
  const uint64_t inputs[] = {1000, 1000, 1000, 1000, 1000};
  const uint64_t outputs[] = {1000, 1000, 1000, 1000, 1000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_N_to_N_simple) {
  const uint64_t inputs[] = {1000, 1000, 1000, 1000, 1000};
  const uint64_t outputs[] = {1000, 1000, 1000, 1000, 1000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, range_proofs_accept_very_long) {
  const size_t N = 12;
  uint64_t inputs[N];
  uint64_t outputs[N];
  for (size_t n = 0; n < N; ++n) {
    inputs[n] = n;
    outputs[n] = n;
  }
  std::random_device rng;
  std::mt19937 urng(rng());
  std::shuffle(inputs, inputs + N, urng);
  std::shuffle(outputs, outputs + N, urng);
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, false));
}

TEST(XI_TESTSUITE, range_proofs_accept_very_long_simple) {
  const size_t N = 12;
  uint64_t inputs[N];
  uint64_t outputs[N];
  for (size_t n = 0; n < N; ++n) {
    inputs[n] = n;
    outputs[n] = n;
  }
  std::random_device rng;
  std::mt19937 urng(rng());
  std::shuffle(inputs, inputs + N, urng);
  std::shuffle(outputs, outputs + N, urng);
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, false, true));
}

TEST(XI_TESTSUITE, prooveRange_is_non_deterministic) {
  Key C[2], mask[2];
  for (int n = 0; n < 2; ++n) Xi::Rct::Range::prove(C[n], mask[n], Amount{80});
  ASSERT_TRUE(memcmp(C[0].data(), C[1].data(), sizeof(C[0].size())));
  ASSERT_TRUE(memcmp(mask[0].data(), mask[1].data(), sizeof(mask[0].size())));
}

TEST(XI_TESTSUITE, fee_0_valid) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {2000, 0};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, false));
}

TEST(XI_TESTSUITE, fee_0_valid_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {2000, 0};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, true));
}

TEST(XI_TESTSUITE, fee_non_0_valid) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1900, 100};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, false));
}

TEST(XI_TESTSUITE, fee_non_0_valid_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1900, 100};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, true));
}

TEST(XI_TESTSUITE, fee_non_0_invalid_higher) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1990, 100};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, true, false));
}

TEST(XI_TESTSUITE, fee_non_0_invalid_higher_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1990, 100};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, true, true));
}

TEST(XI_TESTSUITE, fee_non_0_invalid_lower) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1000, 100};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, true, false));
}

TEST(XI_TESTSUITE, fee_non_0_invalid_lower_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1000, 100};
  EXPECT_TRUE(range_proof_test(false, NELTS(inputs), inputs, NELTS(outputs), outputs, true, true));
}

TEST(XI_TESTSUITE, fee_burn_valid_one_out) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {0, 2000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, false));
}

TEST(XI_TESTSUITE, fee_burn_valid_one_out_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {0, 2000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, true));
}

TEST(XI_TESTSUITE, fee_burn_valid_zero_out) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {2000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, false));
}

TEST(XI_TESTSUITE, fee_burn_valid_zero_out_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {2000};
  EXPECT_TRUE(range_proof_test(true, NELTS(inputs), inputs, NELTS(outputs), outputs, true, true));
}

#define TEST_rctSig_elements(name, op)                                                      \
  TEST(XI_TESTSUITE, rctSig_##name) {                                                       \
    RctSignature sig = make_sig();                                                          \
    ASSERT_TRUE(Xi::Rct::Rct::verify(sig, Xi::Crypto::Device::DefaultDevice::instance()));  \
    op;                                                                                     \
    ASSERT_FALSE(Xi::Rct::Rct::verify(sig, Xi::Crypto::Device::DefaultDevice::instance())); \
  }

TEST_rctSig_elements(rangeSigs_empty, sig.Prunable.RangeSignatures.resize(0));
TEST_rctSig_elements(rangeSigs_too_many, sig.Prunable.RangeSignatures.push_back(sig.Prunable.RangeSignatures.back()));
TEST_rctSig_elements(rangeSigs_too_few, sig.Prunable.RangeSignatures.pop_back());
TEST_rctSig_elements(mgSig_MG_empty, sig.Prunable.MLSAGs.resize(0));
TEST_rctSig_elements(mgSig_ss_empty, sig.Prunable.MLSAGs[0].ss.resize(0));
TEST_rctSig_elements(mgSig_ss_too_many, sig.Prunable.MLSAGs[0].ss.push_back(sig.Prunable.MLSAGs[0].ss.back()));
TEST_rctSig_elements(mgSig_ss_too_few, sig.Prunable.MLSAGs[0].ss.pop_back());
TEST_rctSig_elements(mgSig_ss0_empty, sig.Prunable.MLSAGs[0].ss[0].resize(0));
TEST_rctSig_elements(mgSig_ss0_too_many, sig.Prunable.MLSAGs[0].ss[0].push_back(sig.Prunable.MLSAGs[0].ss[0].back()));
TEST_rctSig_elements(mgSig_ss0_too_few, sig.Prunable.MLSAGs[0].ss[0].pop_back());
TEST_rctSig_elements(mgSig_II_empty, sig.Prunable.MLSAGs[0].II.resize(0));
TEST_rctSig_elements(mgSig_II_too_many, sig.Prunable.MLSAGs[0].II.push_back(sig.Prunable.MLSAGs[0].II.back()));
TEST_rctSig_elements(mgSig_II_too_few, sig.Prunable.MLSAGs[0].II.pop_back());
TEST_rctSig_elements(mixRing_empty, sig.MixinRing.resize(0));
TEST_rctSig_elements(mixRing_too_many, sig.MixinRing.push_back(sig.MixinRing.back()));
TEST_rctSig_elements(mixRing_too_few, sig.MixinRing.pop_back());
TEST_rctSig_elements(mixRing0_empty, sig.MixinRing[0].resize(0));
TEST_rctSig_elements(mixRing0_too_many, sig.MixinRing[0].push_back(sig.MixinRing[0].back()));
TEST_rctSig_elements(mixRing0_too_few, sig.MixinRing[0].pop_back());
TEST_rctSig_elements(ecdhInfo_empty, sig.EcdhInfo.resize(0));
TEST_rctSig_elements(ecdhInfo_too_many, sig.EcdhInfo.push_back(sig.EcdhInfo.back()));
TEST_rctSig_elements(ecdhInfo_too_few, sig.EcdhInfo.pop_back());
TEST_rctSig_elements(outPk_empty, sig.OutPublicKeys.resize(0));
TEST_rctSig_elements(outPk_too_many, sig.OutPublicKeys.push_back(sig.OutPublicKeys.back()));
TEST_rctSig_elements(outPk_too_few, sig.OutPublicKeys.pop_back());

#define TEST_rctSig_elements_simple(name, op)                                                      \
  TEST(XI_TESTSUITE, rctSig_##name##_simple) {                                                     \
    RctSignature sig = make_sig_simple();                                                          \
    ASSERT_TRUE(Xi::Rct::Rct::verify_simple(sig, Xi::Crypto::Device::DefaultDevice::instance()));  \
    op;                                                                                            \
    ASSERT_FALSE(Xi::Rct::Rct::verify_simple(sig, Xi::Crypto::Device::DefaultDevice::instance())); \
  }

TEST_rctSig_elements_simple(rangeSigs_empty, sig.Prunable.RangeSignatures.resize(0));
TEST_rctSig_elements_simple(rangeSigs_too_many,
                            sig.Prunable.RangeSignatures.push_back(sig.Prunable.RangeSignatures.back()));
TEST_rctSig_elements_simple(rangeSigs_too_few, sig.Prunable.RangeSignatures.pop_back());
TEST_rctSig_elements_simple(mgSig_empty, sig.Prunable.MLSAGs.resize(0));
TEST_rctSig_elements_simple(mgSig_too_many, sig.Prunable.MLSAGs.push_back(sig.Prunable.MLSAGs.back()));
TEST_rctSig_elements_simple(mgSig_too_few, sig.Prunable.MLSAGs.pop_back());
TEST_rctSig_elements_simple(mgSig0_ss_empty, sig.Prunable.MLSAGs[0].ss.resize(0));
TEST_rctSig_elements_simple(mgSig0_ss_too_many, sig.Prunable.MLSAGs[0].ss.push_back(sig.Prunable.MLSAGs[0].ss.back()));
TEST_rctSig_elements_simple(mgSig0_ss_too_few, sig.Prunable.MLSAGs[0].ss.pop_back());
TEST_rctSig_elements_simple(mgSig_ss0_empty, sig.Prunable.MLSAGs[0].ss[0].resize(0));
TEST_rctSig_elements_simple(mgSig_ss0_too_many,
                            sig.Prunable.MLSAGs[0].ss[0].push_back(sig.Prunable.MLSAGs[0].ss[0].back()));
TEST_rctSig_elements_simple(mgSig_ss0_too_few, sig.Prunable.MLSAGs[0].ss[0].pop_back());
TEST_rctSig_elements_simple(mgSig0_II_empty, sig.Prunable.MLSAGs[0].II.resize(0));
TEST_rctSig_elements_simple(mgSig0_II_too_many, sig.Prunable.MLSAGs[0].II.push_back(sig.Prunable.MLSAGs[0].II.back()));
TEST_rctSig_elements_simple(mgSig0_II_too_few, sig.Prunable.MLSAGs[0].II.pop_back());
TEST_rctSig_elements_simple(mixRing_empty, sig.MixinRing.resize(0));
TEST_rctSig_elements_simple(mixRing_too_many, sig.MixinRing.push_back(sig.MixinRing.back()));
TEST_rctSig_elements_simple(mixRing_too_few, sig.MixinRing.pop_back());
TEST_rctSig_elements_simple(mixRing0_empty, sig.MixinRing[0].resize(0));
TEST_rctSig_elements_simple(mixRing0_too_many, sig.MixinRing[0].push_back(sig.MixinRing[0].back()));
TEST_rctSig_elements_simple(mixRing0_too_few, sig.MixinRing[0].pop_back());
TEST_rctSig_elements_simple(pseudoOuts_empty, sig.PseudoOuts.resize(0));
TEST_rctSig_elements_simple(pseudoOuts_too_many, sig.PseudoOuts.push_back(sig.PseudoOuts.back()));
TEST_rctSig_elements_simple(pseudoOuts_too_few, sig.PseudoOuts.pop_back());
TEST_rctSig_elements_simple(ecdhInfo_empty, sig.EcdhInfo.resize(0));
TEST_rctSig_elements_simple(ecdhInfo_too_many, sig.EcdhInfo.push_back(sig.EcdhInfo.back()));
TEST_rctSig_elements_simple(ecdhInfo_too_few, sig.EcdhInfo.pop_back());
TEST_rctSig_elements_simple(outPk_empty, sig.OutPublicKeys.resize(0));
TEST_rctSig_elements_simple(outPk_too_many, sig.OutPublicKeys.push_back(sig.OutPublicKeys.back()));
TEST_rctSig_elements_simple(outPk_too_few, sig.OutPublicKeys.pop_back());

TEST(XI_TESTSUITE, reject_gen_simple_ver_non_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1000};
  RctSignature sig = make_sample_simple_rct_sig(NELTS(inputs), inputs, NELTS(outputs), outputs, 1000);
  ASSERT_FALSE(Xi::Rct::Rct::verify(sig, Xi::Crypto::Device::DefaultDevice::instance()));
}

TEST(XI_TESTSUITE, reject_gen_non_simple_ver_simple) {
  const uint64_t inputs[] = {1000, 1000};
  const uint64_t outputs[] = {1000, 1000};
  RctSignature sig = make_sample_rct_sig(NELTS(inputs), inputs, NELTS(outputs), outputs, true);
  ASSERT_FALSE(Xi::Rct::Rct::verify_simple(sig, Xi::Crypto::Device::DefaultDevice::instance()));
}

TEST(XI_TESTSUITE, aggregated) {
  static const size_t N_PROOFS = 16;
  std::vector<RctSignature> s(N_PROOFS);
  std::vector<const RctSignature*> sp(N_PROOFS);

  for (size_t n = 0; n < N_PROOFS; ++n) {
    static const uint64_t inputs[] = {1000, 1000};
    static const uint64_t outputs[] = {500, 1500};
    s[n] = make_sample_simple_rct_sig(NELTS(inputs), inputs, NELTS(outputs), outputs, 0);
    sp[n] = &s[n];
  }

  ASSERT_TRUE(Xi::Rct::Rct::verify_semantics_simple(sp));
}
