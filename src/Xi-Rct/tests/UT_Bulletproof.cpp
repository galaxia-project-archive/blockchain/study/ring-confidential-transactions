/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <random>

#include <Common/StringTools.h>
#include <Xi/Rct/Rct.h>
#include <Xi/Crypto/Device/DefaultDevice.h>

extern "C" {
#include <crypto/random.h>
}

#include "utils/GenerateAmount.h"
#include "utils/GenerateTestRct.h"

#define XI_TESTSUITE T_Xi_Bulletproof

using namespace Xi::Rct;

TEST(XI_TESTSUITE, valid_zero) {
  Bulletproof proof = bulletproof_make_prove(Amount{0}, generate_random_secret_key());
  EXPECT_TRUE(bulletproof_verify(proof));
}

TEST(XI_TESTSUITE, valid_max) {
  Bulletproof proof = bulletproof_make_prove(Amount{0xffffffffffffffff}, generate_random_secret_key());
  EXPECT_TRUE(bulletproof_verify(proof));
}

TEST(XI_TESTSUITE, valid_random) {
  std::default_random_engine rng;
  std::uniform_int_distribution<Amount::value_t> dist{};
  for (size_t n = 0; n < 8; ++n) {
    Bulletproof proof = bulletproof_make_prove(Amount{dist(rng)}, generate_random_secret_key());
    EXPECT_TRUE(bulletproof_verify(proof));
  }
}

TEST(XI_TESTSUITE, valid_multi_random) {
  std::default_random_engine rng;
  std::uniform_int_distribution<Amount::value_t> dist{};
  for (size_t n = 0; n < 8; ++n) {
    size_t outputs = 2 + n;
    AmountVector amounts;
    KeyVector gamma;
    for (size_t i = 0; i < outputs; ++i) {
      amounts.push_back(Amount{dist(rng)});
      gamma.push_back(generate_random_secret_key());
      generate_random_secret_key_inplace(gamma.back());
    }
    Bulletproof proof = bulletproof_make_prove(amounts, gamma);
    EXPECT_TRUE(bulletproof_verify(proof));
  }
}

TEST(XI_TESTSUITE, valid_multi_oracle) {
  std::vector<std::vector<std::pair<Amount, Key>>> Oracle{{
      {{Amount{571323282436666652ull},
        {{0x14, 0x71, 0xee, 0x7,  0x5,  0xe,  0x19, 0xda, 0x85, 0x40, 0xd1, 0x8, 0x24, 0xab, 0xdf, 0x21,
          0x7c, 0xda, 0xaf, 0x7a, 0x9c, 0x2e, 0x77, 0x75, 0x64, 0xdf, 0xb3, 0x8, 0x11, 0x50, 0x7d, 0x6}}},

       {Amount{5181370583343131986ull},
        {{0xcd, 0xcc, 0x7c, 0x44, 0x7e, 0x5c, 0xf9, 0x12, 0x4c, 0x6a, 0x79, 0x6b, 0xdf, 0x2f, 0x81, 0x12,
          0xc2, 0x2e, 0x67, 0xf,  0x4d, 0xe3, 0x21, 0x60, 0x5e, 0x7f, 0x15, 0xe9, 0x97, 0xe5, 0x85, 0x9}}},

       {Amount{17696535618548035941ull},
        {{0x34, 0xc4, 0x73, 0x93, 0x4f, 0x3d, 0x18, 0xf7, 0x85, 0x90, 0xe3, 0x4e, 0x72, 0x27, 0x39, 0xc8,
          0x62, 0x13, 0x44, 0x39, 0x56, 0x10, 0x59, 0x14, 0xdc, 0x82, 0x8d, 0x36, 0xd8, 0x6c, 0xda, 0xf}}},

       {Amount{10468316461987299612ull},
        {{0x4e, 0x5, 0xfa, 0xb2, 0xa4, 0xb1, 0xf7, 0x12, 0x59, 0x9b, 0x40, 0x67, 0x68, 0xce, 0xa7, 0xc2,
          0x94, 0x8, 0x60, 0x3a, 0xdd, 0x3,  0x9,  0xf8, 0x96, 0x6f, 0x2e, 0x94, 0x9,  0x48, 0xab, 0x7}}}},
  }};
  for (const auto &oracle : Oracle) {
    AmountVector amounts;
    KeyVector gamma;
    for (size_t i = 0; i < oracle.size(); ++i) {
      amounts.push_back(oracle[i].first);
      gamma.push_back(oracle[i].second);
    }
    Bulletproof proof = bulletproof_make_prove(amounts, gamma);
    EXPECT_TRUE(bulletproof_verify(proof));
  }
}

TEST(XI_TESTSUITE, multi_splitting) {
  CtKeyVector sc, pc;
  CtKey sctmp, pctmp;
  std::vector<unsigned int> index;
  AmountVector inamounts, outamounts;

  generate_pedersen_commitment_inplace(Amount{6000}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);
  inamounts.push_back(Amount{6000});
  index.push_back(1);

  generate_pedersen_commitment_inplace(Amount{7000}, pctmp, sctmp);
  sc.push_back(sctmp);
  pc.push_back(pctmp);
  inamounts.push_back(Amount{7000});
  index.push_back(1);

  const uint32_t mixin = 3, max_outputs = 16;

  for (size_t n_outputs = 1; n_outputs <= max_outputs; ++n_outputs) {
    AmountVector iOutamounts;
    KeyVector amount_keys;
    KeyVector destinations;
    Key Sk, Pk;
    uint64_t available = 6000 + 7000;
    uint64_t amount;
    CtKeyMatrix mixRing(sc.size());

    // add output
    for (size_t i = 0; i < n_outputs; ++i) {
      amount = XiTestUtils::random_amount(Amount{available}).value;
      iOutamounts.push_back(Amount{amount});
      amount_keys.push_back(key_fast_hash_to_scalar(Key::Zero));
      generate_random_key_pair_inplace(Pk, Sk);
      destinations.push_back(Pk);
      available -= amount;
    }

    for (size_t i = 0; i < sc.size(); ++i) {
      for (size_t j = 0; j <= mixin; ++j) {
        if (j == 1)
          mixRing[i].push_back(pc[i]);
        else
          mixRing[i].push_back({key_multiply_scalar_with_basepoint(generate_random_secret_key()),
                                key_multiply_scalar_with_basepoint(generate_random_secret_key())});
      }
    }

    auto &hwdevice = Xi::Crypto::Device::DefaultDevice::instance();

    CtKeyVector outSk;
    RctConfig rct_config{RangeProofType::PaddedBulletproof, 0};
    RctSignature s =
        Xi::Rct::Rct::generate_simple(Key::Zero, sc, destinations, inamounts, iOutamounts, Amount{available}, mixRing,
                                      amount_keys, nullptr, NULL, index, outSk, rct_config, hwdevice);
    EXPECT_TRUE(Xi::Rct::Rct::verify_simple(s, hwdevice));
    for (uint32_t i = 0; i < n_outputs; ++i) {
      Key mask;
      Xi::Rct::Rct::decode_amount_simple(s, amount_keys[i], i, mask, hwdevice).throwOnError();
      EXPECT_TRUE(mask == outSk[i].Mask);
    }
  }
}

TEST(XI_TESTSUITE, valid_aggregated) {
  static const size_t N_PROOFS = 8;

  std::default_random_engine eng{};
  std::uniform_int_distribution<Amount::value_t> dist{};

  std::vector<Bulletproof> proofs(N_PROOFS);
  for (size_t n = 0; n < N_PROOFS; ++n) {
    size_t outputs = 2 + n;
    AmountVector amounts;
    KeyVector gamma;
    for (size_t i = 0; i < outputs; ++i) {
      amounts.push_back(Amount{dist(eng)});
      gamma.push_back(generate_random_secret_key());
    }
    proofs[n] = bulletproof_make_prove(amounts, gamma);
  }
  EXPECT_TRUE(bulletproof_verify(proofs));
}

TEST(XI_TESTSUITE, invalid_8) {
  Key invalid_amount = Key::Zero;
  invalid_amount[8] = 1;
  Bulletproof proof = bulletproof_make_prove(invalid_amount, generate_random_secret_key());
  EXPECT_FALSE(bulletproof_verify(proof));
}

TEST(XI_TESTSUITE, invalid_31) {
  Key invalid_amount = Key::Zero;
  invalid_amount[31] = 1;
  Bulletproof proof = bulletproof_make_prove(invalid_amount, generate_random_secret_key());
  EXPECT_FALSE(bulletproof_verify(proof));
}

TEST(XI_TESTSUITE, invalid_gamma_0) {
  Key invalid_amount = Key::Zero;
  invalid_amount[8] = 1;
  Key gamma = Key::Zero;
  Bulletproof proof = bulletproof_make_prove(invalid_amount, gamma);
  EXPECT_FALSE(bulletproof_verify(proof));
}

namespace {
static const char *const torsion_elements[] = {
    "c7176a703d4dd84fba3c0b760d10670f2a2053fa2c39ccc64ec7fd7792ac03fa",
    "0000000000000000000000000000000000000000000000000000000000000000",
    "26e8958fc2b227b045c3f489f2ef98f0d5dfac05d3c63339b13802886d53fc85",
    "ecffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff7f",
    "26e8958fc2b227b045c3f489f2ef98f0d5dfac05d3c63339b13802886d53fc05",
    "0000000000000000000000000000000000000000000000000000000000000080",
    "c7176a703d4dd84fba3c0b760d10670f2a2053fa2c39ccc64ec7fd7792ac037a",
};

bool toPointCheckOrder(ge_p3 *P, const unsigned char *data) {
  if (ge_frombytes_vartime(P, data)) return false;
  ge_p2 R;
  ge_scalarmult(&R, Key::CurveOrder.data(), P);
  Key tmp;
  ge_tobytes(tmp.data(), &R);
  return tmp == Key::Identity;
}

bool isInMainSubgroup(const Key &a) {
  ge_p3 p3;
  return toPointCheckOrder(&p3, a.data());
}
}  // namespace

TEST(XI_TESTSUITE, invalid_torsion) {
  Bulletproof proof = bulletproof_make_prove(Amount{7329838943733}, generate_random_secret_key());
  EXPECT_TRUE(bulletproof_verify(proof));
  for (const auto &xs : torsion_elements) {
    Key x;

    EXPECT_TRUE(Common::fromHex(xs, x.data(), x.size()));
    EXPECT_FALSE(isInMainSubgroup(x));
    for (auto &k : proof.V) {
      const Key org_k = k;
      key_add_inplace(org_k, x, k);
      EXPECT_FALSE(bulletproof_verify(proof));
      k = org_k;
    }
    for (auto &k : proof.L) {
      const Key org_k = k;
      key_add_inplace(org_k, x, k);
      EXPECT_FALSE(bulletproof_verify(proof));
      k = org_k;
    }
    for (auto &k : proof.R) {
      const Key org_k = k;
      key_add_inplace(org_k, x, k);
      EXPECT_FALSE(bulletproof_verify(proof));
      k = org_k;
    }
    const Key org_A = proof.A;
    key_add_inplace(org_A, x, proof.A);
    EXPECT_FALSE(bulletproof_verify(proof));
    proof.A = org_A;
    const Key org_S = proof.S;
    key_add_inplace(org_S, x, proof.S);
    EXPECT_FALSE(bulletproof_verify(proof));
    proof.S = org_S;
    const Key org_T1 = proof.T1;
    key_add_inplace(org_T1, x, proof.T1);
    EXPECT_FALSE(bulletproof_verify(proof));
    proof.T1 = org_T1;
    const Key org_T2 = proof.T2;
    key_add_inplace(org_T2, x, proof.T2);
    EXPECT_FALSE(bulletproof_verify(proof));
    proof.T2 = org_T2;
  }
}
