/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <Xi/Rct/Rct.h>
#include <Xi/Crypto/Device/DefaultDevice.h>

#define XI_TESTSUITE T_Xi_Rct_Mlsag

TEST(XI_TESTSUITE, Signature) {
  using namespace ::Xi;
  using namespace ::Xi::Rct;

  size_t j = 0;
  size_t N = 0;

  // Tests for MG Sigs
  //#MG sig: true one
  N = 3;          // #cols
  uint8_t R = 3;  // #rows
  KeyVector xtmp = generate_random_secret_keys(R);
  KeyMatrix xm = make_key_matrix(R, N);  // = [[None]*N] #just used to generate test public keys
  KeyVector sk = generate_random_secret_keys(R);
  KeyMatrix P = make_key_matrix(R, N);  // = keyM[[None]*N] #stores the public keys;
  uint32_t ind = 2;
  size_t i = 0;
  for (j = 0; j < R; j++) {
    for (i = 0; i < N; i++) {
      xm[i][j] = generate_random_secret_key();
      P[i][j] = key_multiply_scalar_with_basepoint(xm[i][j]);
    }
  }
  for (j = 0; j < R; j++) {
    sk[j] = xm[ind][j];
  }
  Key message = Key::Identity;
  MlsagSignature IIccss = Mlsag::generate_signature(message, P, sk, nullptr, nullptr, ind, R,
                                                    Xi::Crypto::Device::DefaultDevice::instance());
  ASSERT_TRUE(Mlsag::verify(message, P, IIccss, R));

  //#MG sig: false one
  N = 3;  // #cols
  R = 3;  // #rows
  xtmp = generate_random_secret_keys(R);
  KeyMatrix xx(N, xtmp);  // = [[None]*N] #just used to generate test public keys
  sk = generate_random_secret_keys(R);
  // P (N, xtmp);// = keyM[[None]*N] #stores the public keys;

  ind = 2;
  for (j = 0; j < R; j++) {
    for (i = 0; i < N; i++) {
      xx[i][j] = generate_random_secret_key();
      P[i][j] = key_multiply_scalar_with_basepoint(xx[i][j]);
    }
    sk[j] = xx[ind][j];
  }
  sk[2] = generate_random_secret_key();  // assume we don't know one of the private keys..
  IIccss = Mlsag::generate_signature(message, P, sk, nullptr, nullptr, ind, R,
                                     Xi::Crypto::Device::DefaultDevice::instance());
  ASSERT_FALSE(Mlsag::verify(message, P, IIccss, R));
}
