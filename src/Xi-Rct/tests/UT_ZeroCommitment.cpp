/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <random>
#include <numeric>

#include <Xi/Rct/Rct.h>
#include <Xi/Crypto/Device/DefaultDevice.h>

#include "utils/GenerateTestRct.h"

#define XI_TESTSUITE T_Xi_Rct_ZeroCommitment

namespace {
static Xi::Rct::Key uncachedZeroCommit(uint64_t amount) {
  const Xi::Rct::Key am = Xi::Rct::amount_to_key(Xi::Rct::Amount{amount});
  const Xi::Rct::Key bH = Xi::Rct::key_multiply_scalar_with_h_constant(am);
  return Xi::Rct::key_add(Xi::Rct::Key::BasePoint, bH);
}

}  // namespace

TEST(XI_TESTSUITE, zeroCommmit) {
  using namespace Xi::Rct;

  std::default_random_engine rng;
  std::uniform_int_distribution<Amount::value_t> dist{std::numeric_limits<Amount::value_t>::min(),
                                                      std::numeric_limits<Amount::value_t>::max()};

  const uint64_t amount = dist(rng);
  const Key z = generate_zero_commitment(Amount{amount});
  const Key a = key_multiply_scalar_with_basepoint(Key::Identity);
  const Key b = key_multiply_scalar_with_h_constant(amount_to_key(Amount{amount}));
  const Key manual = key_add(a, b);
  ASSERT_EQ(z, manual);
}

TEST(XI_TESTSUITE, zeroCommitCache) {
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{0}), uncachedZeroCommit(0));
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{1}), uncachedZeroCommit(1));
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{2}), uncachedZeroCommit(2));
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{10}), uncachedZeroCommit(10));
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{200}), uncachedZeroCommit(200));
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{1000000000}), uncachedZeroCommit(1000000000));
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{3000000000000}), uncachedZeroCommit(3000000000000));
  ASSERT_EQ(Xi::Rct::generate_zero_commitment(Xi::Rct::Amount{900000000000000}), uncachedZeroCommit(900000000000000));
}
