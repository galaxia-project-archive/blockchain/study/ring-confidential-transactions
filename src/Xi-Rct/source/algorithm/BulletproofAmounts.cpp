/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/BulletproofAmounts.h"

namespace {
const size_t hidden_constant = 6;  // Guess thats something like minimum mixins, may needs to be passed as an argument
}

size_t Xi::Rct::count_bulletproof_amounts(const Xi::Rct::Bulletproof &bulletproof) {
  if (bulletproof.L.size() < hidden_constant) return InvalidCount;
  if (bulletproof.L.size() != bulletproof.R.size()) return InvalidCount;
  if (bulletproof.L.size() > hidden_constant + Bulletproof::reservedBits()) return InvalidCount;
  if (bulletproof.V.size() > (1ull << (bulletproof.L.size() - hidden_constant))) return InvalidCount;
  if (bulletproof.V.size() * 2 <= (1ull << (bulletproof.L.size() - hidden_constant))) return InvalidCount;
  if (bulletproof.V.empty()) return InvalidCount;
  return bulletproof.V.size();
}

size_t Xi::Rct::count_bulletproof_amounts(const Xi::Rct::BulletproofVector &bulletproofs) {
  size_t n = 0;
  for (const auto &bulletproof : bulletproofs) {
    size_t iCount = count_bulletproof_amounts(bulletproof);
    if (iCount >= std::numeric_limits<uint32_t>::max() - iCount) return InvalidCount;
    if (iCount == InvalidCount) return InvalidCount;
    n += iCount;
  }
  return n;
}

size_t Xi::Rct::count_bulletproof_max_amounts(const Xi::Rct::Bulletproof &bulletproof) {
  if (bulletproof.L.size() < hidden_constant) return InvalidCount;
  if (bulletproof.L.size() != bulletproof.R.size()) return InvalidCount;
  if (bulletproof.L.size() > hidden_constant + Bulletproof::reservedBits()) return InvalidCount;
  return 1ull << (bulletproof.L.size() - hidden_constant);
}

size_t Xi::Rct::count_bulletproof_max_amounts(const Xi::Rct::BulletproofVector &bulletproofs) {
  size_t n = 0;
  for (const auto &bulletproof : bulletproofs) {
    size_t iCount = count_bulletproof_max_amounts(bulletproof);
    if (iCount >= std::numeric_limits<uint32_t>::max() - iCount) return InvalidCount;
    if (iCount == InvalidCount) return InvalidCount;
    n += iCount;
  }
  return n;
}
