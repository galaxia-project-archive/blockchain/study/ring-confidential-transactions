/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/GenerateKey.h"

#include <crypto/crypto.h>

#include "Xi/Rct/Algorithm/KeyArithmetic.h"

Xi::Rct::Key Xi::Rct::generate_random_secret_key() {
  Key reval;
  generate_random_secret_key_inplace(reval);
  return reval;
}

void Xi::Rct::generate_random_secret_key_inplace(Xi::Rct::Key& out) { Crypto::random32_unbiased(out.data()); }

Xi::Rct::Key Xi::Rct::generate_random_public_key() {
  Key reval;
  generate_random_public_key_inplace(reval);
  return reval;
}

void Xi::Rct::generate_random_public_key_inplace(Xi::Rct::Key& out) {
  auto secret = generate_random_secret_key();
  key_multiply_scalar_with_basepoint_inplace(secret, out);
}

Xi::Rct::KeyVector Xi::Rct::generate_random_secret_keys(size_t count) {
  KeyVector reval{};
  reval.resize(count);
  for (auto& key : reval) {
    generate_random_secret_key_inplace(key);
  }
  return reval;
}

Xi::Rct::Key Xi::Rct::generate_random_curve_point() {
  Key reval;
  generate_random_curve_point_inplace(reval);
  return reval;
}

void Xi::Rct::generate_random_curve_point_inplace(Xi::Rct::Key& out) {
  auto secretKey = generate_random_secret_key();
  key_multiply_scalar_with_basepoint_inplace(secretKey, out);
}

Xi::Rct::KeyPair Xi::Rct::generate_random_key_pair() {
  KeyPair keyPair;
  generate_random_key_pair_inplace(keyPair);
  return keyPair;
}

void Xi::Rct::generate_random_key_pair_inplace(Xi::Rct::KeyPair& out) {
  generate_random_key_pair_inplace(out.PublicKey, out.SecretKey);
}

void Xi::Rct::generate_random_key_pair_inplace(Xi::Rct::Key& publicKey, Xi::Rct::Key& secretKey) {
  generate_random_secret_key_inplace(secretKey);
  key_multiply_scalar_with_basepoint_inplace(secretKey, publicKey);
}
