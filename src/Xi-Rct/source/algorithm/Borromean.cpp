/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/Borromean.h"

#include <crypto/crypto-ops.h>

#include "Xi/Rct/Algorithm/GenerateKey.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/KeyHashing.h"

Xi::Rct::BorromeanSignature Xi::Rct::generate_borromean_signature(const Xi::Rct::AmountBitKeys &x,
                                                                  const Xi::Rct::AmountBitKeys &P1,
                                                                  const Xi::Rct::AmountBitKeys &P2,
                                                                  const Xi::Rct::AmountBits &indices) {
  AmountBitKeys L[2], alpha;
  Key c;
  size_t naught = 0, prime = 0;
  BorromeanSignature bb;
  for (size_t ii = 0; ii < 64; ii++) {
    naught = indices[ii];
    prime = (indices[ii] + 1) % 2;
    generate_random_secret_key_inplace(alpha[ii]);
    key_multiply_scalar_with_basepoint_inplace(alpha[ii], L[naught][ii]);
    if (naught == 0) {
      generate_random_secret_key_inplace(bb.S1[ii]);
      key_fast_hash_to_scalar_inplace(L[naught][ii], c);
      key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(bb.S1[ii], c, P2[ii], L[prime][ii]);
    }
  }
  key_fast_hash_to_scalar_inplace(L[1], bb.ee);
  Key LL, cc;
  for (size_t jj = 0; jj < 64; jj++) {
    if (!indices[jj]) {
      sc_mulsub(bb.S0[jj].data(), x[jj].data(), bb.ee.data(), alpha[jj].data());
    } else {
      generate_random_secret_key_inplace(bb.S0[jj]);
      key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(bb.S0[jj], bb.ee, P1[jj], LL);
      key_fast_hash_to_scalar_inplace(LL, cc);
      sc_mulsub(bb.S1[jj].data(), x[jj].data(), cc.data(), alpha[jj].data());
    }
  }
  return bb;
}

bool Xi::Rct::verify_borromean_signature(const Xi::Rct::BorromeanSignature &bb, const ge_p3 P1[Amount::bits()],
                                         const ge_p3 P2[Amount::bits()]) {
  try {
    AmountBitKeys Lv1;
    Key chash, LL;
    ge_p2 p2;
    for (size_t ii = 0; ii < Amount::bits(); ii++) {
      // equivalent of: addKeys2(LL, bb.s0[ii], bb.ee, P1[ii]);
      ge_double_scalarmult_base_vartime(&p2, bb.ee.data(), &P1[ii], bb.S0[ii].data());
      ge_tobytes(LL.data(), &p2);
      key_fast_hash_to_scalar_inplace(LL, chash);
      // equivalent of: addKeys2(Lv1[ii], bb.s1[ii], chash, P2[ii]);
      ge_double_scalarmult_base_vartime(&p2, chash.data(), &P2[ii], bb.S1[ii].data());
      ge_tobytes(Lv1[ii].data(), &p2);
    }
    Key eeComputed = key_fast_hash_to_scalar(Lv1);
    return eeComputed == bb.ee;
  } catch (...) {
    // TODO Logging
    return false;
  }
}

bool Xi::Rct::verify_borromean_signature(const Xi::Rct::BorromeanSignature &bb, const Xi::Rct::AmountBitKeys &P1,
                                         const Xi::Rct::AmountBitKeys &P2) {
  try {
    ge_p3 P1_p3[64], P2_p3[64];
    for (size_t i = 0; i < 64; ++i) {
      if (ge_frombytes_vartime(&P1_p3[i], P1[i].data()) != 0) {
        return false;
      }
      if (ge_frombytes_vartime(&P2_p3[i], P2[i].data()) != 0) {
        return false;
      }
    }
    return verify_borromean_signature(bb, P1_p3, P2_p3);
  } catch (...) {
    // TODO Logging
    return false;
  }
}
