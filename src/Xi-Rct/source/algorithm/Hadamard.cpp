/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/Hadamard.h"

#include <Xi/Exceptional.h>

#include "Xi/Rct/Algorithm/KeyArithmetic.h"

Xi::Rct::KeyVector Xi::Rct::Hadamard::product(const Xi::Rct::KeyVector &a, const Xi::Rct::KeyVector &b) {
  KeyVector reval;
  product_inplace(a, b, reval);
  return reval;
}

void Xi::Rct::Hadamard::product_inplace(const Xi::Rct::KeyVector &a, const Xi::Rct::KeyVector &b,
                                        Xi::Rct::KeyVector &out) {
  exceptional_if_not<IncompatibleSizeError>(a.size() == b.size());
  out.resize(a.size());
  for (size_t i = 0; i < a.size(); ++i) {
    sc_mul(out[i].data(), a[i].data(), b[i].data());
  }
}

namespace {
void generic_fold(std::vector<ge_p3> &v, const Xi::Rct::KeyVector *scale, const Xi::Rct::Key &a,
                  const Xi::Rct::Key &b) {
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>((v.size() & 1) == 0, "Vector size should be even");
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(scale == nullptr || scale->size() >= v.size(),
                                                         "Insufficient scaling entries");
  const size_t sz = v.size() / 2;
  for (size_t n = 0; n < sz; ++n) {
    ge_dsmp c[2];
    ge_dsm_precomp(c[0], &v[n]);
    ge_dsm_precomp(c[1], &v[sz + n]);
    Xi::Rct::Key sa, sb;
    if (scale != nullptr)
      sc_mul(sa.data(), a.data(), (*scale)[n].data());
    else
      sa = a;
    if (scale != nullptr)
      sc_mul(sb.data(), b.data(), (*scale)[sz + n].data());
    else
      sb = b;
    ge_double_scalarmult_precomp_vartime2_p3(&v[n], sa.data(), c[0], sb.data(), c[1]);
  }
  v.resize(sz);
}
}  // namespace

void Xi::Rct::Hadamard::fold(std::vector<ge_p3> &v, const Xi::Rct::Key &a, const Xi::Rct::Key &b) {
  generic_fold(v, nullptr, a, b);
}

void Xi::Rct::Hadamard::fold(std::vector<ge_p3> &v, const Xi::Rct::KeyVector &scale, const Xi::Rct::Key &a,
                             const Xi::Rct::Key &b) {
  generic_fold(v, &scale, a, b);
}
