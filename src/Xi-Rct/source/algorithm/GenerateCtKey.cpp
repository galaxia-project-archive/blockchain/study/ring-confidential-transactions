/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/GenerateCtKey.h"

#include "Xi/Rct/Algorithm/GenerateKey.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/TypeConversion.h"

Xi::Rct::CtKeyPair Xi::Rct::generate_pedersen_commitment(const Xi::Rct::Amount amount) {
  CtKeyPair reval;
  generate_pedersen_commitment_inplace(amount, reval);
  return reval;
}

void Xi::Rct::generate_pedersen_commitment_inplace(const Xi::Rct::Amount amount, Xi::Rct::CtKeyPair &out) {
  generate_pedersen_commitment_inplace(amount, out.PublicKey, out.SecretKey);
}

void Xi::Rct::generate_pedersen_commitment_inplace(const Xi::Rct::Amount amount, Xi::Rct::CtKey &publicKey,
                                                   Xi::Rct::CtKey &secretKey) {
  generate_random_key_pair_inplace(publicKey.Dest, secretKey.Dest);
  generate_random_key_pair_inplace(publicKey.Mask, secretKey.Mask);
  Key am = amount_to_key(amount);
  Key bH = key_multiply_scalar_with_h_constant(am);
  key_add_inplace(publicKey.Mask, bH, publicKey.Mask);
}

Xi::Rct::CtKeyPair Xi::Rct::generate_pedersen_commitment(const Xi::Rct::Key &bH) {
  CtKeyPair reval;
  generate_pedersen_commitment_inplace(bH, reval);
  return reval;
}

void Xi::Rct::generate_pedersen_commitment_inplace(const Xi::Rct::Key &bH, Xi::Rct::CtKeyPair &out) {
  generate_pedersen_commitment_inplace(bH, out.PublicKey, out.SecretKey);
}

void Xi::Rct::generate_pedersen_commitment_inplace(const Xi::Rct::Key &bH, Xi::Rct::CtKey &publicKey,
                                                   Xi::Rct::CtKey &secretKey) {
  generate_random_key_pair_inplace(publicKey.Dest, secretKey.Dest);
  generate_random_key_pair_inplace(publicKey.Mask, secretKey.Mask);
  key_add_inplace(publicKey.Mask, bH, publicKey.Mask);
}
