/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/EcdhEncoding.h"

#include <cinttypes>

#include <crypto/crypto-ops.h>

#include "Xi/Rct/Algorithm/KeyHashing.h"
#include "Xi/Rct/Algorithm/EcdhHashing.h"
#include "Xi/Rct/Algorithm/GenerateCommitment.h"

namespace {
static void xor8(Xi::Rct::Key &v, const Xi::Rct::Key &k) {
  for (uint8_t i = 0; i < 8; ++i) {
    v[i] ^= k[i];
  }
}
}  // namespace

void Xi::Rct::ecdh_encode(Xi::Rct::EcdhTuple &unmasked, const Xi::Rct::Key &sharedSec, bool v2) {
  if (v2) {
    Key::Zero.copyTo(unmasked.Mask);
    xor8(unmasked.Amount, ecdh_hash(sharedSec));
  } else {
    const auto sharedSec1 = key_fast_hash_to_scalar(sharedSec);
    const auto sharedSec2 = key_fast_hash_to_scalar(sharedSec1);
    sc_add(unmasked.Mask.data(), unmasked.Mask.data(), sharedSec1.data());
    sc_add(unmasked.Amount.data(), unmasked.Amount.data(), sharedSec2.data());
  }
}

void Xi::Rct::ecdh_decode(Xi::Rct::EcdhTuple &masked, const Xi::Rct::Key &sharedSec, bool v2) {
  if (v2) {
    generate_commitment_mask_inplace(sharedSec, masked.Mask);
    xor8(masked.Amount, ecdh_hash(sharedSec));
  } else {
    const auto sharedSec1 = key_fast_hash_to_scalar(sharedSec);
    const auto sharedSec2 = key_fast_hash_to_scalar(sharedSec1);
    sc_sub(masked.Mask.data(), masked.Mask.data(), sharedSec1.data());
    sc_sub(masked.Amount.data(), masked.Amount.data(), sharedSec2.data());
  }
}
