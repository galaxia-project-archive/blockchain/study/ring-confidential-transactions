/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/GenerateCommitment.h"

#include <cstring>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Types/ZeroCommitment.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/TypeConversion.h"
#include "Xi/Rct/Algorithm/KeyHashing.h"

Xi::Rct::Key Xi::Rct::generate_commitment(const Xi::Rct::Key &a, const Xi::Rct::Amount amount) {
  Key reval;
  generate_commitment_inplace(a, amount, reval);
  return reval;
}

void Xi::Rct::generate_commitment_inplace(const Xi::Rct::Key &a, const Xi::Rct::Amount amount, Xi::Rct::Key &out) {
  key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(a, amount_to_key(amount), Constants::H, out);
}

Xi::Rct::Key Xi::Rct::generate_zero_commitment(const Xi::Rct::Amount amount) {
  Key reval;
  generate_zero_commitment_inplace(amount, reval);
  return reval;
}

void Xi::Rct::generate_zero_commitment_inplace(const Xi::Rct::Amount amount, Xi::Rct::Key &out) {
  const ZeroCommitment *begin = Constants::ZeroCommitments.data();
  const ZeroCommitment *end = Constants::ZeroCommitments.data() + Constants::ZeroCommitments.size();
  const ZeroCommitment value{amount, Key::Zero};
  const auto it = std::lower_bound(begin, end, value, [](const ZeroCommitment &e0, const ZeroCommitment &e1) {
    return e0.CommitedAmount < e1.CommitedAmount;
  });
  if (it != end && it->CommitedAmount == amount) {
    it->Commitment.copyTo(out);
  } else {
    auto am = amount_to_key(amount);
    auto bH = key_multiply_scalar_with_h_constant(am);
    key_add_inplace(Key::BasePoint, bH, out);
  }
}

Xi::Rct::Key Xi::Rct::generate_commitment_mask(const Xi::Rct::Key &secretKey) {
  Key reval;
  generate_commitment_mask_inplace(secretKey, reval);
  return reval;
}

void Xi::Rct::generate_commitment_mask_inplace(const Xi::Rct::Key &secretKey, Xi::Rct::Key &out) {
  char data[15 + sizeof(Key)];
  std::memcpy(data, "commitment_mask", 15);
  std::memcpy(data + 15, secretKey.data(), sizeof(secretKey));
  key_fast_hash_to_scalar_inplace(data, sizeof(data), out);
}
