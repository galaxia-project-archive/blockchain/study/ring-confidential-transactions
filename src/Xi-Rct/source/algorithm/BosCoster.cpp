/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/BosCoster.h"

#include <cinttypes>
#include <algorithm>

#include <crypto/crypto-ops.h>

namespace {
static inline Xi::Rct::Key div2(const Xi::Rct::Key &k) {
  Xi::Rct::Key res;
  int carry = 0;
  for (int n = 31; n >= 0; --n) {
    int new_carry = (k[(uint8_t)n] & 1) << 7;
    res[(uint8_t)n] = (Xi::Rct::Byte)(k[(uint8_t)n] / 2 + carry);
    carry = new_carry;
  }
  return res;
}
}  // namespace

Xi::Rct::Key Xi::Rct::BosCoster::heap_conv(MultiexpDataVector data) {
  const size_t points = data.size();
  if (points < 2) {
    Xi::exceptional<InsufficientPointsError>();
  }
  std::vector<size_t> heap(points);
  for (size_t n = 0; n < points; ++n) heap[n] = n;

  auto Comp = [&](size_t e0, size_t e1) { return data[e0].Scalar < data[e1].Scalar; };
  std::make_heap(heap.begin(), heap.end(), Comp);

  while (heap.size() > 1) {
    std::pop_heap(heap.begin(), heap.end(), Comp);
    size_t index1 = heap.back();
    heap.pop_back();
    std::pop_heap(heap.begin(), heap.end(), Comp);
    size_t index2 = heap.back();
    heap.pop_back();

    ge_cached cached;
    ge_p3_to_cached(&cached, &data[index1].Point);
    ge_p1p1 p1;
    ge_add(&p1, &data[index2].Point, &cached);
    ge_p1p1_to_p3(&data[index2].Point, &p1);

    sc_sub(data[index1].Scalar.data(), data[index1].Scalar.data(), data[index2].Scalar.data());

    if (!(data[index1].Scalar == Key::Zero)) {
      heap.push_back(index1);
      std::push_heap(heap.begin(), heap.end(), Comp);
    }

    heap.push_back(index2);
    std::push_heap(heap.begin(), heap.end(), Comp);
  }

  std::pop_heap(heap.begin(), heap.end(), Comp);
  size_t index1 = heap.back();
  heap.pop_back();
  ge_p2 p2;
  ge_scalarmult(&p2, data[index1].Scalar.data(), &data[index1].Point);
  Xi::Rct::Key res;
  ge_tobytes(res.data(), &p2);
  return res;
}

Xi::Rct::Key Xi::Rct::BosCoster::heap_conv_robust(MultiexpDataVector data) {
  if (data.empty()) {
    Xi::exceptional<InsufficientPointsError>();
  }
  size_t points = data.size();
  std::vector<size_t> heap;
  heap.reserve(points);
  for (size_t n = 0; n < points; ++n) {
    if (!(data[n].Scalar == Key::Zero) && !ge_p3_is_point_at_infinity(&data[n].Point)) heap.push_back(n);
  }
  points = heap.size();
  if (points == 0) return Key::Identity;

  auto Comp = [&](size_t e0, size_t e1) { return data[e0].Scalar < data[e1].Scalar; };
  std::make_heap(heap.begin(), heap.end(), Comp);

  if (points < 2) {
    std::pop_heap(heap.begin(), heap.end(), Comp);
    size_t index1 = heap.back();
    ge_p2 p2;
    ge_scalarmult(&p2, data[index1].Scalar.data(), &data[index1].Point);
    Xi::Rct::Key res;
    ge_tobytes(res.data(), &p2);
    return res;
  }

  while (heap.size() > 1) {
    std::pop_heap(heap.begin(), heap.end(), Comp);
    size_t index1 = heap.back();
    heap.pop_back();
    std::pop_heap(heap.begin(), heap.end(), Comp);
    size_t index2 = heap.back();
    heap.pop_back();

    ge_cached cached;
    ge_p1p1 p1;
    ge_p2 p2;

    while (1) {
      Key s1_2 = div2(data[index1].Scalar);
      if (!(data[index2].Scalar < s1_2)) break;
      if (data[index1].Scalar[0] & 1) {
        data.resize(data.size() + 1);
        data.back().Scalar = Key::Identity;
        data.back().Point = data[index1].Point;
        heap.push_back(data.size() - 1);
        std::push_heap(heap.begin(), heap.end(), Comp);
      }
      data[index1].Scalar = div2(data[index1].Scalar);
      ge_p3_to_p2(&p2, &data[index1].Point);
      ge_p2_dbl(&p1, &p2);
      ge_p1p1_to_p3(&data[index1].Point, &p1);
    }

    ge_p3_to_cached(&cached, &data[index1].Point);
    ge_add(&p1, &data[index2].Point, &cached);
    ge_p1p1_to_p3(&data[index2].Point, &p1);

    sc_sub(data[index1].Scalar.data(), data[index1].Scalar.data(), data[index2].Scalar.data());

    if (!(data[index1].Scalar == Key::Zero)) {
      heap.push_back(index1);
      std::push_heap(heap.begin(), heap.end(), Comp);
    }

    heap.push_back(index2);
    std::push_heap(heap.begin(), heap.end(), Comp);
  }

  std::pop_heap(heap.begin(), heap.end(), Comp);
  size_t index1 = heap.back();
  heap.pop_back();
  ge_p2 p2;
  ge_scalarmult(&p2, data[index1].Scalar.data(), &data[index1].Point);
  Key res;
  ge_tobytes(res.data(), &p2);
  return res;
}
