/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/TypeConversion.h"

#include <cstring>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Algorithm/KeyHashing.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"

void Xi::Rct::amount_to_key_inplace(Xi::Rct::Amount amount, Xi::Rct::Key &out) {
  out.fill(0);
  Amount val = amount;
  uint32_t i = 0;
  while (val != Amount{0}) {
    out[i] = (Byte)val.value & (Byte)0xFF;
    i++;
    val /= Amount{256};
  }
}

Xi::Rct::Key Xi::Rct::amount_to_key(Xi::Rct::Amount amount) {
  Key reval;
  amount_to_key_inplace(amount, reval);
  return reval;
}

Xi::Rct::Key Xi::Rct::amount_to_key_with_key(Xi::Rct::Amount amount, const Xi::Rct::Key &a) {
  Key reval;
  amount_to_key_with_key_inplace(amount, a, reval);
  return reval;
}

void Xi::Rct::amount_to_key_with_key_inplace(Xi::Rct::Amount amount, const Xi::Rct::Key &a, Xi::Rct::Key &out) {
  key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(a, amount_to_key(amount), Constants::H, out);
}

void Xi::Rct::amount_to_bits_inplace(Xi::Rct::Amount amount, Xi::Rct::AmountBits &bits) {
  amount_to_bits_inplace(amount, bits.data());
}

void Xi::Rct::amount_to_bits_inplace(Xi::Rct::Amount amount, Xi::Rct::AmountBits::pointer bits) {
  uint32_t i = 0;
  while (amount != Amount{0}) {
    bits[i] = amount.value & 1;
    i++;
    amount.value >>= 1;
  }
  std::memset(bits + i, 0, sizeof(AmountBits::value_type) * (Amount::bits() - i));
}

Xi::Rct::Amount Xi::Rct::key_to_amount(const Xi::Rct::Key &key) {
  Amount vali{0};
  int j = 0;
  for (j = 7; j >= 0; j--) {
    vali = vali * Amount{256} + Amount{key[(size_t)j]};
  }
  return vali;
}

Xi::Rct::Key Xi::Rct::bits_to_key(const Xi::Rct::AmountBits &bits) { return bits_to_key(bits.data()); }

Xi::Rct::Key Xi::Rct::bits_to_key(Xi::Rct::AmountBits::const_pointer bits) {
  Key reval;
  bits_to_key_inplace(bits, reval);
  return reval;
}

void Xi::Rct::bits_to_key_inplace(const Xi::Rct::AmountBits &bits, Xi::Rct::Key &out) {
  bits_to_key_inplace(bits.data(), out);
}

void Xi::Rct::bits_to_key_inplace(Xi::Rct::AmountBits::const_pointer bits, Xi::Rct::Key &out) {
  uint8_t j;
  uint32_t byte;
  int32_t i;
  for (j = 0; j < 8; j++) {
    byte = 0;
    i = 8 * j;
    for (i = 7; i > -1; i--) {
      byte = byte * 2 + bits[8 * j + i];
    }
    out[j] = (Byte)byte;
  }
  std::memset(out.data() + 8, 0, out.size() - 8);
}

Xi::Rct::Amount Xi::Rct::bits_to_amount(const Xi::Rct::AmountBits &bits) { return bits_to_amount(bits.data()); }

Xi::Rct::Amount Xi::Rct::bits_to_amount(Xi::Rct::AmountBits::const_pointer bits) {
  Amount vali{0};
  int8_t j = 0;
  for (j = 63; j >= 0; j--) {
    vali.value = vali.value * 2 + bits[j];
  }
  return vali;
}

bool Xi::Rct::key_to_point_and_check_order(const Xi::Rct::Key &key, ge_p3 &out) {
  if (ge_frombytes_vartime(&out, key.data())) return false;
  ge_p2 R;
  ge_scalarmult(&R, Key::CurveOrder.data(), &out);
  Key tmp;
  ge_tobytes(tmp.data(), &R);
  return tmp == Key::Identity;
}

Xi::Rct::Key Xi::Rct::key_hash_to_key_point_simple(const Xi::Rct::Key &in) {
  Key reval;
  key_hash_to_key_point_simple_inplace(in, reval);
  return reval;
}

void Xi::Rct::key_hash_to_key_point_simple_inplace(const Xi::Rct::Key &in, Xi::Rct::Key &out) {
  ge_p1p1 point2;
  ge_p2 point;
  ge_p3 res;
  Key h = key_fast_hash(in);
  if (ge_frombytes_vartime(&res, h.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_p3_to_p2(&point, &res);
  ge_mul8(&point2, &point);
  ge_p1p1_to_p3(&res, &point2);
  ge_p3_tobytes(out.data(), &res);
}

Xi::Rct::Key Xi::Rct::key_hash_to_key_point(const Xi::Rct::Key &in) {
  Key reval;
  key_hash_to_key_point_inplace(in, reval);
  return reval;
}

void Xi::Rct::key_hash_to_key_point_inplace(const Xi::Rct::Key &in, Xi::Rct::Key &out) {
  ge_p2 point;
  ge_p1p1 point2;
  ge_p3 res;
  Key h = key_fast_hash(in);
  ge_fromfe_frombytes_vartime(&point, h.data());
  ge_mul8(&point2, &point);
  ge_p1p1_to_p3(&res, &point2);
  ge_p3_tobytes(out.data(), &res);
}
