/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/Straus.h"

#include "algorithm/StrausCache.h"

#define STRAUS_C 4
#define CACHE_OFFSET(cache, point, digit) cache->multiples[(point) + cache->size * ((digit)-1)]

namespace {
static inline Xi::Rct::Key pow2(size_t n) {
  if (n > 255) {
    Xi::exceptional<Xi::Rct::Straus::InvalidPowParameterError>();
  }
  auto res = Xi::Rct::Key::Zero;
  res[n >> 3] |= 1 << (n & 7);
  return res;
}
}  // namespace

Xi::Rct::Straus::SharedStrausCache Xi::Rct::Straus::init_cache(const Xi::Rct::MultiexpDataVector &data, size_t N) {
  if (N == 0) {
    N = data.size();
  }
  if (N > data.size()) {
    exceptional<BadCacheBaseDataError>();
  }
  ge_p1p1 p1;
  ge_p3 p3;
  auto cache = std::make_shared<StrausCache>();

  const size_t offset = cache->size;
  cache->multiples = (ge_cached *)aligned_realloc(
      cache->multiples, sizeof(ge_cached) * ((1 << STRAUS_C) - 1) * std::max(offset, N), 4096);
  if (cache->multiples == nullptr) {
    exceptional<OutOfMemoryError>();
  }
  cache->size = N;
  for (size_t j = offset; j < N; ++j) {
    ge_p3_to_cached(&CACHE_OFFSET(cache, j, 1), &data[j].Point);
    for (size_t i = 2; i < 1ull << STRAUS_C; ++i) {
      ge_add(&p1, &data[j].Point, &CACHE_OFFSET(cache, j, i - 1));
      ge_p1p1_to_p3(&p3, &p1);
      ge_p3_to_cached(&CACHE_OFFSET(cache, j, i), &p3);
    }
  }
  return cache;
}

size_t Xi::Rct::Straus::get_cache_size(const Xi::Rct::Straus::SharedStrausCache &cache) {
  return cache->size * sizeof(ge_cached) * ((1 << STRAUS_C) - 1);
}

Xi::Rct::Key Xi::Rct::Straus::straus(const Xi::Rct::MultiexpDataVector &data,
                                     const Xi::Rct::Straus::SharedStrausCache &cache, size_t STEP) {
  if (cache != nullptr && cache->size < data.size()) {
    exceptional<InsufficientCacheSizeError>();
  }
  STEP = STEP ? STEP : 192;

  SharedStrausCache local_cache = cache == nullptr ? init_cache(data) : cache;
  ge_cached cached;
  ge_p1p1 p1;

  std::unique_ptr<uint8_t[]> digits{new uint8_t[64 * data.size()]};
  for (size_t j = 0; j < data.size(); ++j) {
    const unsigned char *bytes = data[j].Scalar.data();
    unsigned int i;
    for (i = 0; i < 64; i += 2, bytes++) {
      digits[j * 64 + i] = bytes[0] & 0xf;
      digits[j * 64 + i + 1] = bytes[0] >> 4;
    }
  }

  Key maxscalar = Key::Zero.copy();
  for (size_t i = 0; i < data.size(); ++i) {
    if (maxscalar < data[i].Scalar) {
      maxscalar = data[i].Scalar;
    }
  }
  size_t start_i = 0;
  while (start_i < 256 && !(maxscalar < pow2(start_i))) {
    start_i += STRAUS_C;
  }

  ge_p3 res_p3 = ge_p3_identity;

  for (size_t start_offset = 0; start_offset < data.size(); start_offset += STEP) {
    const size_t num_points = std::min(data.size() - start_offset, STEP);

    ge_p3 band_p3 = ge_p3_identity;
    size_t i = start_i;
    if (!(i < STRAUS_C)) {
      goto skipfirst;
    }
    while (!(i < STRAUS_C)) {
      ge_p2 p2;
      ge_p3_to_p2(&p2, &band_p3);
      for (size_t j = 0; j < STRAUS_C; ++j) {
        ge_p2_dbl(&p1, &p2);
        if (j == STRAUS_C - 1) {
          ge_p1p1_to_p3(&band_p3, &p1);
        } else {
          ge_p1p1_to_p2(&p2, &p1);
        }
      }
    skipfirst:
      i -= STRAUS_C;
      for (size_t j = start_offset; j < start_offset + num_points; ++j) {
        const uint8_t digit = digits[j * 64 + i / 4];
        if (digit) {
          ge_add(&p1, &band_p3, &CACHE_OFFSET(local_cache, j, digit));
          ge_p1p1_to_p3(&band_p3, &p1);
        }
      }
    }

    ge_p3_to_cached(&cached, &band_p3);
    ge_add(&p1, &res_p3, &cached);
    ge_p1p1_to_p3(&res_p3, &p1);
  }

  Key res;
  ge_p3_tobytes(res.data(), &res_p3);
  return res;
}
