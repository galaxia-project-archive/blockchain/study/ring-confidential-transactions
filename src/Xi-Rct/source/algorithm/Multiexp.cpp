/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/Multiexp.h"

#include "Xi/Rct/Algorithm/Straus.h"
#include "Xi/Rct/Algorithm/Pippenger.h"

namespace {
static Xi::Rct::Straus::SharedStrausCache straus_HiGi_cache;
static Xi::Rct::Pippenger::SharedPippengerCache pippenger_HiGi_cache;
}  // namespace

Xi::Rct::Key Xi::Rct::compute_multiexp(const Xi::Rct::MultiexpDataVector &data, size_t HiGi_size) {
  if (HiGi_size > 0) {
    static_assert(232 <= Straus::size_limit(), "Straus in precalc mode can only be calculated till STRAUS_SIZE_LIMIT");
    if (HiGi_size <= 232 && data.size() == HiGi_size) {
      return straus(data, straus_HiGi_cache, 0);
    } else {
      return pippenger(data, pippenger_HiGi_cache, HiGi_size, Pippenger::get_c(data.size()));
    }
  } else if (data.size() <= 95) {
    return Straus::straus(data, nullptr, 0);
  } else {
    return Pippenger::pippenger(data, nullptr, 0, Pippenger::get_c(data.size()));
  }
}
