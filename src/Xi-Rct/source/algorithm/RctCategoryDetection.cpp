/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/RctCategoryDetection.h"

#include "Xi/Rct/Types/RctType.h"

bool Xi::Rct::is_simple_type(const uint8_t type) { return is_simple_type(static_cast<RctType>(type)); }

bool Xi::Rct::is_bulletproof_type(uint8_t type) { return is_bulletproof_type(static_cast<RctType>(type)); }

bool Xi::Rct::is_borromean_type(const uint8_t type) { return is_borromean_type(static_cast<RctType>(type)); }

bool Xi::Rct::is_simple_type(const Xi::Rct::RctType type) {
  switch (type) {
    case RctType::Simple:
    case RctType::Bulletproof:
    case RctType::Bulletproof2:
      return true;
    default:
      return false;
  }
}

bool Xi::Rct::is_bulletproof_type(const Xi::Rct::RctType type) {
  switch (type) {
    case RctType::Bulletproof:
    case RctType::Bulletproof2:
      return true;
    default:
      return false;
  }
}

bool Xi::Rct::is_borromean_type(const Xi::Rct::RctType type) {
  switch (type) {
    case RctType::Simple:
    case RctType::Full:
      return true;
    default:
      return false;
  }
}
