/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <memory>

#include <Xi/Utils/AlignedMemoryAllocation.h>

#include "Xi/Rct/Types/Key.h"
#include "Xi/Rct/Types/MultiexpData.h"

namespace Xi {
namespace Rct {
namespace Straus {
struct StrausCache {
  size_t size;
  ge_cached *multiples;
  StrausCache() : size{0}, multiples{nullptr} {}
  ~StrausCache() { aligned_free(multiples); }
};
}  // namespace Straus
}  // namespace Rct
}  // namespace Xi
