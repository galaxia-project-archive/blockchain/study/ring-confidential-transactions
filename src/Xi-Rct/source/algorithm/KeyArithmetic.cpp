/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/KeyArithmetic.h"

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Types/Bulletproof.h"
#include "Xi/Rct/Types/MultiexpData.h"
#include "Xi/Rct/Algorithm/Multiexp.h"

namespace {
static Xi::Rct::Key sm(Xi::Rct::Key y, int n, const Xi::Rct::Key &x) {
  while (n--) sc_mul(y.data(), y.data(), y.data());
  sc_mul(y.data(), y.data(), x.data());
  return y;
}
}  // namespace

Xi::Rct::Key Xi::Rct::key_multiply_scalar_with_basepoint(const Xi::Rct::Key &operand) {
  Key reval;
  key_multiply_scalar_with_basepoint_inplace(operand, reval);
  return reval;
}

void Xi::Rct::key_multiply_scalar_with_basepoint_inplace(const Xi::Rct::Key &operand, Key &out) {
  ge_p3 point;
  sc_reduce32copy(out.data(), operand.data());  // do this beforehand!
  ge_scalarmult_base(&point, out.data());
  ge_p3_tobytes(out.data(), &point);
}

Xi::Rct::Key Xi::Rct::key_multiply_scalar_with_h_constant(const Xi::Rct::Key &operand) {
  Key reval;
  key_multiply_scalar_with_h_constant_inplace(operand, reval);
  return reval;
}

void Xi::Rct::key_multiply_scalar_with_h_constant_inplace(const Xi::Rct::Key &operand, Xi::Rct::Key &out) {
  ge_p2 R;
  ge_scalarmult(&R, operand.data(), &ge_p3_H);
  ge_tobytes(out.data(), &R);
}

Xi::Rct::Key Xi::Rct::key_multiply_scalar_with_eight(const Xi::Rct::Key &operand) {
  Key reval;
  key_multiply_scalar_with_eight_inplace(operand, reval);
  return reval;
}

void Xi::Rct::key_multiply_scalar_with_eight_inplace(const Xi::Rct::Key &operand, Xi::Rct::Key &out) {
  ge_p3 p3;
  if (ge_frombytes_vartime(&p3, operand.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_p2 p2;
  ge_p3_to_p2(&p2, &p3);
  ge_p1p1 p1;
  ge_mul8(&p1, &p2);
  ge_p1p1_to_p2(&p2, &p1);
  ge_tobytes(out.data(), &p2);
}

Xi::Rct::Key Xi::Rct::key_multiply_scalar(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs) {
  Key reval;
  key_mulitply_scalar_inplace(lhs, rhs, reval);
  return reval;
}

void Xi::Rct::key_mulitply_scalar_inplace(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs, Xi::Rct::Key &out) {
  ge_p3 A;
  ge_p2 R;
  if (ge_frombytes_vartime(&A, lhs.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_scalarmult(&R, rhs.data(), &A);
  ge_tobytes(out.data(), &R);
}

Xi::Rct::Key Xi::Rct::key_add(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs) {
  Key reval;
  key_add_inplace(lhs, rhs, reval);
  return reval;
}

void Xi::Rct::key_add_inplace(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs, Key &out) {
  ge_p3 B2, A2;
  if (ge_frombytes_vartime(&B2, rhs.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  if (ge_frombytes_vartime(&A2, lhs.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_cached tmp2;
  ge_p3_to_cached(&tmp2, &B2);
  ge_p1p1 tmp3;
  ge_add(&tmp3, &A2, &tmp2);
  ge_p1p1_to_p3(&A2, &tmp3);
  ge_p3_tobytes(out.data(), &A2);
}

Xi::Rct::Key Xi::Rct::key_accumulate_sum(const Xi::Rct::KeyVector &keys) {
  Key reval;
  key_accumulate_sum_inplace(keys, reval);
  return reval;
}

void Xi::Rct::key_accumulate_sum_inplace(const Xi::Rct::KeyVector &keys, Xi::Rct::Key &out) {
  if (keys.empty()) {
    Key::Identity.copyTo(out);
  } else {
    ge_p3 p3, tmp;
    if (ge_frombytes_vartime(&p3, keys[0].data()) != 0) {
      Xi::exceptional<GeFromBytesVarTimeError>();
    }
    for (size_t i = 1; i < keys.size(); ++i) {
      if (ge_frombytes_vartime(&tmp, keys[i].data()) != 0) {
        Xi::exceptional<GeFromBytesVarTimeError>();
      }
      ge_cached p2;
      ge_p3_to_cached(&p2, &tmp);
      ge_p1p1 p1;
      ge_add(&p1, &p3, &p2);
      ge_p1p1_to_p3(&p3, &p1);
    }
    ge_p3_tobytes(out.data(), &p3);
  }
}

Xi::Rct::Key Xi::Rct::key_accumulate_point_sum(const Xi::Rct::KeyVector &keys) {
  Key reval;
  key_accumulate_point_sum_inplace(keys, reval);
  return reval;
}

void Xi::Rct::key_accumulate_point_sum_inplace(const Xi::Rct::KeyVector &keys, Xi::Rct::Key &out) {
  Key::Identity.copyTo(out);
  for (size_t i = 0; i < keys.size(); i++) {
    key_add_inplace(out, keys[i], out);
  }
}

Xi::Rct::Key Xi::Rct::key_multiply_with_basepoint_and_add(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs) {
  Key reval;
  key_multiply_with_basepoint_and_add_inplace(lhs, rhs, reval);
  return reval;
}

void Xi::Rct::key_multiply_with_basepoint_and_add_inplace(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs,
                                                          Xi::Rct::Key &out) {
  Key aG = key_multiply_scalar_with_basepoint(lhs);
  key_add_inplace(aG, rhs, out);
}

Xi::Rct::Key Xi::Rct::key_multiply_with_basepoint_and_add_with_multiplied_point(const Xi::Rct::Key &baseScalar,
                                                                                const Xi::Rct::Key &pointScalar,
                                                                                const Xi::Rct::Key &point) {
  Key reval;
  key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(baseScalar, pointScalar, point, reval);
  return reval;
}

void Xi::Rct::key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(const Xi::Rct::Key &baseScalar,
                                                                                const Xi::Rct::Key &pointScalar,
                                                                                const Xi::Rct::Key &point,
                                                                                Xi::Rct::Key &out) {
  ge_p2 rv;
  ge_p3 B2;
  if (ge_frombytes_vartime(&B2, point.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_double_scalarmult_base_vartime(&rv, pointScalar.data(), &B2, baseScalar.data());
  ge_tobytes(out.data(), &rv);
}

Xi::Rct::Key Xi::Rct::key_add_multiplied_points(const Xi::Rct::Key &lhsScalar, const Xi::Rct::Key &lhsPoint,
                                                const Xi::Rct::Key &rhsScalar, ge_dsmp rhsPoint) {
  Key reval;
  key_add_multiplied_points_inplace(lhsScalar, lhsPoint, rhsScalar, rhsPoint, reval);
  return reval;
}

void Xi::Rct::key_add_multiplied_points_inplace(const Xi::Rct::Key &lhsScalar, const Xi::Rct::Key &lhsPoint,
                                                const Xi::Rct::Key &rhsScalar, ge_dsmp rhsPoint, Xi::Rct::Key &out) {
  ge_p2 rv;
  ge_p3 A2;
  if (ge_frombytes_vartime(&A2, lhsPoint.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_double_scalarmult_precomp_vartime(&rv, lhsScalar.data(), &A2, rhsScalar.data(), rhsPoint);
  ge_tobytes(out.data(), &rv);
}

Xi::Rct::Key Xi::Rct::key_add_multiplied_points(const Xi::Rct::Key &lhsScalar, ge_dsmp lhsPoint,
                                                const Xi::Rct::Key &rhsScalar, ge_dsmp rhsPoint) {
  Key reval;
  key_add_multiplied_points_inplace(lhsScalar, lhsPoint, rhsScalar, rhsPoint, reval);
  return reval;
}

void Xi::Rct::key_add_multiplied_points_inplace(const Xi::Rct::Key &lhsScalar, ge_dsmp lhsPoint,
                                                const Xi::Rct::Key &rhsScalar, ge_dsmp rhsPoint, Xi::Rct::Key &out) {
  ge_p2 rv;
  ge_double_scalarmult_precomp_vartime2(&rv, lhsScalar.data(), lhsPoint, rhsScalar.data(), rhsPoint);
  ge_tobytes(out.data(), &rv);
}

void Xi::Rct::key_precompute_dsmp(const Xi::Rct::Key &key, ge_dsmp out) {
  ge_p3 B2;
  if (ge_frombytes_vartime(&B2, key.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_dsm_precomp(out, &B2);
}

Xi::Rct::Key Xi::Rct::key_subtract(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs) {
  Key reval;
  key_subtract_inplace(lhs, rhs, reval);
  return reval;
}

void Xi::Rct::key_subtract_inplace(const Xi::Rct::Key &lhs, const Xi::Rct::Key &rhs, Xi::Rct::Key &out) {
  ge_p3 B2, A2;
  if (ge_frombytes_vartime(&B2, rhs.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  if (ge_frombytes_vartime(&A2, lhs.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
  ge_cached tmp2;
  ge_p3_to_cached(&tmp2, &B2);
  ge_p1p1 tmp3;
  ge_sub(&tmp3, &A2, &tmp2);
  ge_p1p1_to_p3(&A2, &tmp3);
  ge_p3_tobytes(out.data(), &A2);
}

Xi::Rct::KeyVector Xi::Rct::key_vector_powers(const Xi::Rct::Key &x, size_t n) {
  KeyVector res{n};
  if (n == 0) return res;
  res[0] = Key::Identity;
  if (n == 1) return res;
  res[1] = x;
  for (size_t i = 2; i < n; ++i) {
    sc_mul(res[i].data(), res[i - 1].data(), x.data());
  }
  return res;
}

Xi::Rct::Key Xi::Rct::key_vector_power_sum(Xi::Rct::Key x, size_t n) {
  if (n == 0) return Key::Zero;
  auto res = Key::Identity;
  if (n == 1) return res;

  const bool is_power_of_2 = (n & (n - 1)) == 0;
  if (is_power_of_2) {
    sc_add(res.data(), res.data(), x.data());
    while (n > 2) {
      sc_mul(x.data(), x.data(), x.data());
      sc_muladd(res.data(), x.data(), res.data(), res.data());
      n /= 2;
    }
  } else {
    auto prev = x;
    for (size_t i = 1; i < n; ++i) {
      if (i > 1) sc_mul(prev.data(), prev.data(), x.data());
      sc_add(res.data(), res.data(), prev.data());
    }
  }

  return res;
}

Xi::Rct::Key Xi::Rct::key_vector_inner_product(const Xi::Rct::KeyVector &a, const Xi::Rct::KeyVector &b) {
  exceptional_if_not<IncompatibleSizeError>(a.size() == b.size());
  auto res = Key::Zero;
  for (size_t i = 0; i < a.size(); ++i) {
    sc_muladd(res.data(), a[i].data(), b[i].data(), res.data());
  }
  return res;
}

Xi::Rct::Key Xi::Rct::key_vector_inner_product(const Span<const Key> &a, const Span<const Key> &b) {
  exceptional_if_not<IncompatibleSizeError>(a.size() == b.size());
  auto res = Key::Zero;
  for (size_t i = 0; i < a.size(); ++i) {
    sc_muladd(res.data(), a.data()[i].data(), b.data()[i].data(), res.data());
  }
  return res;
}

Xi::Rct::KeyVector Xi::Rct::key_vector_add(const Xi::Rct::KeyVector &lhs, const Xi::Rct::KeyVector &rhs) {
  exceptional_if_not<IncompatibleSizeError>(lhs.size() == rhs.size());
  KeyVector reval{};
  reval.resize(lhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_add(reval[i].data(), lhs[i].data(), rhs[i].data());
  }
  return reval;
}

Xi::Rct::KeyVector Xi::Rct::key_vector_add(const Xi::Rct::KeyVector &lhs, const Xi::Rct::Key &rhs) {
  KeyVector reval{};
  reval.resize(lhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_add(reval[i].data(), lhs[i].data(), rhs.data());
  }
  return reval;
}

Xi::Rct::Key Xi::Rct::key_vector_sum(const Xi::Rct::KeyVector &A) {
  if (A.empty()) return Key::Identity;
  ge_p3 p3, tmp;
  exceptional_if_not<GeFromBytesVarTimeError>(ge_frombytes_vartime(&p3, A[0].data()) == 0);
  for (size_t i = 1; i < A.size(); ++i) {
    exceptional_if_not<GeFromBytesVarTimeError>(ge_frombytes_vartime(&tmp, A[i].data()) == 0);
    ge_cached p2;
    ge_p3_to_cached(&p2, &tmp);
    ge_p1p1 p1;
    ge_add(&p1, &p3, &p2);
    ge_p1p1_to_p3(&p3, &p1);
  }
  Key res;
  ge_p3_tobytes(res.data(), &p3);
  return res;
}

Xi::Rct::KeyVector Xi::Rct::key_vector_subtract(const Xi::Rct::KeyVector &lhs, const Xi::Rct::KeyVector &rhs) {
  exceptional_if_not<IncompatibleSizeError>(lhs.size() == rhs.size());
  KeyVector reval{};
  reval.resize(lhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_sub(reval[i].data(), lhs[i].data(), rhs[i].data());
  }
  return reval;
}

Xi::Rct::KeyVector Xi::Rct::key_vector_subtract(const Xi::Rct::KeyVector &lhs, const Xi::Rct::Key &rhs) {
  KeyVector reval{};
  reval.resize(lhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_sub(reval[i].data(), lhs[i].data(), rhs.data());
  }
  return reval;
}

Xi::Rct::KeyVector Xi::Rct::key_vector_scalar(const Xi::Rct::KeyVector &lhs, const Xi::Rct::KeyVector &rhs) {
  exceptional_if_not<IncompatibleSizeError>(lhs.size() == rhs.size());
  KeyVector reval{};
  reval.resize(lhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_mul(reval[i].data(), lhs[i].data(), rhs[i].data());
  }
  return reval;
}

Xi::Rct::KeyVector Xi::Rct::key_vector_scalar(const Xi::Rct::KeyVector &lhs, const Xi::Rct::Key &rhs) {
  KeyVector reval{};
  reval.resize(lhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_mul(reval[i].data(), lhs[i].data(), rhs.data());
  }
  return reval;
}

Xi::Rct::KeyVector Xi::Rct::key_vector_scalar(const Span<const Key> &lhs, const Xi::Rct::Key &rhs) {
  KeyVector reval{};
  reval.resize(lhs.size());
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_mul(reval[i].data(), lhs.data()[i].data(), rhs.data());
  }
  return reval;
}

Xi::Rct::Key Xi::Rct::key_invert(const Xi::Rct::Key &key) {
  Key reval;
  key_invert_inplace(key, reval);
  return reval;
}

void Xi::Rct::key_invert_inplace(const Xi::Rct::Key &key, Xi::Rct::Key &out) {
  Key _1, _10, _100, _11, _101, _111, _1001, _1011, _1111;

  _1 = key;
  sc_mul(_10.data(), _1.data(), _1.data());
  sc_mul(_100.data(), _10.data(), _10.data());
  sc_mul(_11.data(), _10.data(), _1.data());
  sc_mul(_101.data(), _10.data(), _11.data());
  sc_mul(_111.data(), _10.data(), _101.data());
  sc_mul(_1001.data(), _10.data(), _111.data());
  sc_mul(_1011.data(), _10.data(), _1001.data());
  sc_mul(_1111.data(), _100.data(), _1011.data());

  sc_mul(out.data(), _1111.data(), _1.data());

  out = sm(out, 123 + 3, _101);
  out = sm(out, 2 + 2, _11);
  out = sm(out, 1 + 4, _1111);
  out = sm(out, 1 + 4, _1111);
  out = sm(out, 4, _1001);
  out = sm(out, 2, _11);
  out = sm(out, 1 + 4, _1111);
  out = sm(out, 1 + 3, _101);
  out = sm(out, 3 + 3, _101);
  out = sm(out, 3, _111);
  out = sm(out, 1 + 4, _1111);
  out = sm(out, 2 + 3, _111);
  out = sm(out, 2 + 2, _11);
  out = sm(out, 1 + 4, _1011);
  out = sm(out, 2 + 4, _1011);
  out = sm(out, 6 + 4, _1001);
  out = sm(out, 2 + 2, _11);
  out = sm(out, 3 + 2, _11);
  out = sm(out, 3 + 2, _11);
  out = sm(out, 1 + 4, _1001);
  out = sm(out, 1 + 3, _111);
  out = sm(out, 2 + 4, _1111);
  out = sm(out, 1 + 4, _1011);
  out = sm(out, 3, _101);
  out = sm(out, 2 + 4, _1111);
  out = sm(out, 3, _101);
  out = sm(out, 1 + 2, _11);
}

Xi::Rct::KeyVector Xi::Rct::key_vector_invert(Xi::Rct::KeyVector keys) {
  KeyVector scratch;
  scratch.reserve(keys.size());

  Key acc = Key::Identity;
  for (size_t n = 0; n < keys.size(); ++n) {
    scratch.push_back(acc);
    if (n == 0)
      acc = keys[0];
    else
      sc_mul(acc.data(), acc.data(), keys[n].data());
  }

  acc = key_invert(acc);

  Key tmp;
  for (size_t i = 0; i < keys.size(); ++i) {
    sc_mul(tmp.data(), acc.data(), keys[keys.size() - i - 1].data());
    sc_mul(keys[keys.size() - i - 1].data(), acc.data(), scratch[keys.size() - i - 1].data());
    acc = tmp;
  }

  return keys;
}
