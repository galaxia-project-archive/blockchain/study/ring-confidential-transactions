/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/KeyHashing.h"

#include <cinttypes>

#include <crypto/hash.h>
#include <crypto/crypto-ops.h>

extern "C" {
#include "crypto/keccak.h"
}

#include "Xi/Rct/Algorithm/KeyArithmetic.h"

Xi::Rct::Key Xi::Rct::key_fast_hash(const void *data, const size_t l) {
  Key reval;
  key_fast_hash_inplace(data, l, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_inplace(const void *data, const size_t l, Xi::Rct::Key &out) {
  keccak(reinterpret_cast<const uint8_t *>(data), l, reinterpret_cast<uint8_t *>(out.data()),
         static_cast<int>(out.size() * sizeof(Key::value_type)));
}

Xi::Rct::Key Xi::Rct::key_fast_hash_to_scalar(const void *data, const size_t l) {
  Key reval;
  key_fast_hash_to_scalar_inplace(data, l, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_to_scalar_inplace(const void *data, const size_t l, Xi::Rct::Key &out) {
  key_fast_hash_inplace(data, l, out);
  sc_reduce32(out.data());
}

Xi::Rct::Key Xi::Rct::key_fast_hash(const Xi::Rct::Key &in) {
  Key reval;
  key_fast_hash_inplace(in, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_inplace(const Xi::Rct::Key &in, Xi::Rct::Key &out) {
  key_fast_hash_inplace(in.data(), in.size(), out);
}

Xi::Rct::Key Xi::Rct::key_fast_hash_to_scalar(const Xi::Rct::Key &in) {
  Key reval;
  key_fast_hash_to_scalar_inplace(in, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_to_scalar_inplace(const Xi::Rct::Key &in, Xi::Rct::Key &out) {
  key_fast_hash_to_scalar_inplace(in.data(), in.size(), out);
}

Xi::Rct::Key Xi::Rct::key_fast_hash128(const void *in) { return key_fast_hash(in, 128); }

void Xi::Rct::key_fast_hash128_inplace(const void *in, Xi::Rct::Key &out) { key_fast_hash_inplace(in, 128, out); }

Xi::Rct::Key Xi::Rct::key_fast_hash_scalar128(const void *in) { return key_fast_hash_to_scalar(in, 128); }

void Xi::Rct::key_fast_hash_scalar128_inplace(const void *in, Xi::Rct::Key &out) {
  key_fast_hash_to_scalar_inplace(in, 128, out);
}

Xi::Rct::Key Xi::Rct::key_fast_hash(const Xi::Rct::CtKeyVector &PC) {
  Key reval;
  key_fast_hash_inplace(PC, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_inplace(const Xi::Rct::CtKeyVector &PC, Xi::Rct::Key &out) {
  if (PC.empty()) {
    Crypto::cn_fast_hash("", 0, reinterpret_cast<char *>(out.data()));
  } else {
    key_fast_hash_inplace(PC.data(), PC.size() * sizeof(CtKeyVector::value_type), out);
  }
}

Xi::Rct::Key Xi::Rct::key_fast_hash_to_scalar(const Xi::Rct::CtKeyVector &PC) {
  Key reval;
  key_fast_hash_to_scalar_inplace(PC, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_to_scalar_inplace(const Xi::Rct::CtKeyVector &PC, Xi::Rct::Key &out) {
  key_fast_hash_inplace(PC, out);
  sc_reduce32(out.data());
}

Xi::Rct::Key Xi::Rct::key_fast_hash_to_point(const Xi::Rct::Key &in) {
  Key reval;
  key_fast_hash_to_point_inplace(in, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_to_point_inplace(const Xi::Rct::Key &in, Xi::Rct::Key &out) {
  ge_p2 point;
  ge_p1p1 point2;
  ge_p3 res;
  Key h = key_fast_hash(in);
  ge_fromfe_frombytes_vartime(&point, h.data());
  ge_mul8(&point2, &point);
  ge_p1p1_to_p3(&res, &point2);
  ge_p3_tobytes(out.data(), &res);
}

Xi::Rct::Key Xi::Rct::key_fast_hash_to_point_simple(const Xi::Rct::Key &in) {
  Key reval;
  key_fast_hash_to_point_simple_inplace(in, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_to_point_simple_inplace(const Xi::Rct::Key &in, Xi::Rct::Key &out) {
  ge_p1p1 point2;
  ge_p2 point;
  ge_p3 res;
  Key h = key_fast_hash(in);
  exceptional_if_not<GeFromBytesVarTimeError>(ge_frombytes_vartime(&res, h.data()) == 0);
  ge_p3_to_p2(&point, &res);
  ge_mul8(&point2, &point);
  ge_p1p1_to_p3(&res, &point2);
  ge_p3_tobytes(out.data(), &res);
}

Xi::Rct::Key Xi::Rct::key_fast_hash(const Xi::Rct::KeyVector &keys) {
  Key reval;
  key_fast_hash_inplace(keys, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_inplace(const Xi::Rct::KeyVector &keys, Xi::Rct::Key &out) {
  if (keys.empty()) {
    Crypto::cn_fast_hash("", 0, reinterpret_cast<char *>(out.data()));
  } else {
    key_fast_hash_inplace(keys.data(), keys.size() * sizeof(KeyVector::value_type), out);
  }
}

Xi::Rct::Key Xi::Rct::key_fast_hash_to_scalar(const Xi::Rct::KeyVector &keys) {
  Key reval;
  key_fast_hash_to_scalar_inplace(keys, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_to_scalar_inplace(const Xi::Rct::KeyVector &keys, Xi::Rct::Key &out) {
  key_fast_hash_inplace(keys, out);
  sc_reduce32(out.data());
}

Xi::Rct::Key Xi::Rct::key_fast_hash(const Xi::Rct::AmountBitKeys &keys) {
  Key reval;
  key_fast_hash_inplace(keys, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_inplace(const Xi::Rct::AmountBitKeys &keys, Xi::Rct::Key &out) {
  key_fast_hash_inplace(keys.data(), keys.size() * sizeof(AmountBitKeys::value_type), out);
}

Xi::Rct::Key Xi::Rct::key_fast_hash_to_scalar(const Xi::Rct::AmountBitKeys &keys) {
  Key reval;
  key_fast_hash_to_scalar_inplace(keys, reval);
  return reval;
}

void Xi::Rct::key_fast_hash_to_scalar_inplace(const Xi::Rct::AmountBitKeys &keys, Xi::Rct::Key &out) {
  key_fast_hash_inplace(keys.data(), keys.size() * sizeof(AmountBitKeys::value_type), out);
  sc_reduce32(out.data());
}
