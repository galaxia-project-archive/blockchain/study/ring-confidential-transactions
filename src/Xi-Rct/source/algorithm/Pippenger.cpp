/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Algorithm/Pippenger.h"

#include <crypto/crypto-ops.h>

#include "algorithm/PippengerCache.h"

namespace {
static inline Xi::Rct::Key pow2(size_t n) {
  if (n > 255) {
    Xi::exceptional<Xi::Rct::Pippenger::InvalidPowParameterError>();
  }
  auto res = Xi::Rct::Key::Zero;
  res[n >> 3] |= 1 << (n & 7);
  return res;
}

static inline int test(const Xi::Rct::Key &k, size_t n) {
  if (n >= 256) return 0;
  return k[n >> 3] & (1 << (n & 7));
}

static inline void add(ge_p3 &p3, const ge_cached &other) {
  ge_p1p1 p1;
  ge_add(&p1, &p3, &other);
  ge_p1p1_to_p3(&p3, &p1);
}

static inline void add(ge_p3 &p3, const ge_p3 &other) {
  ge_cached cached;
  ge_p3_to_cached(&cached, &other);
  add(p3, cached);
}

}  // namespace

size_t Xi::Rct::Pippenger::get_cache_size(const SharedPippengerCache &cache) {
  return cache->size * sizeof(*cache->cached);
}

size_t Xi::Rct::Pippenger::get_c(size_t N) {
  if (N <= 13) return 2;
  if (N <= 29) return 3;
  if (N <= 83) return 4;
  if (N <= 185) return 5;
  if (N <= 465) return 6;
  if (N <= 1180) return 7;
  if (N <= 2295) return 8;
  return 9;
}

Xi::Rct::Pippenger::SharedPippengerCache Xi::Rct::Pippenger::init_cache(const Xi::Rct::MultiexpDataVector &data,
                                                                        size_t start_offset, size_t N) {
  if (start_offset > data.size()) {
    Xi::exceptional<BadCacheBaseDataError>();
  }
  if (N == 0) N = data.size() - start_offset;
  if (N > data.size() - start_offset) {
    Xi::exceptional<BadCacheBaseDataError>();
  }
  SharedPippengerCache cache = std::make_shared<PippengerCache>();

  cache->size = N;
  cache->cached = (ge_cached *)aligned_realloc(cache->cached, N * sizeof(ge_cached), 4096);
  if (cache->cached == nullptr) {
    Xi::exceptional<OutOfMemoryError>();
  }
  for (size_t i = 0; i < N; ++i) ge_p3_to_cached(&cache->cached[i], &data[i + start_offset].Point);

  return cache;
}

Xi::Rct::Key Xi::Rct::Pippenger::pippenger(const std::vector<Xi::Rct::MultiexpData> &data,
                                           const SharedPippengerCache &cache, size_t cache_size, size_t c) {
  if (cache != NULL && cache_size == 0) cache_size = cache->size;
  if (cache != nullptr && cache_size > cache->size) {
    Xi::exceptional<InsufficientCacheSizeError>();
  }
  if (c == 0) c = get_c(data.size());
  if (c > 9) {
    Xi::exceptional<InvalidCParameterError>();
  }

  ge_p3 result = ge_p3_identity;
  bool result_init = false;
  std::unique_ptr<ge_p3[]> buckets{new ge_p3[1ull << c]};
  bool buckets_init[1 << 9];
  SharedPippengerCache local_cache = cache == nullptr ? init_cache(data) : cache;
  SharedPippengerCache local_cache_2 = data.size() > cache_size ? init_cache(data, cache_size) : nullptr;

  Key maxscalar = Key::Zero;
  for (size_t i = 0; i < data.size(); ++i) {
    if (maxscalar < data[i].Scalar) {
      maxscalar = data[i].Scalar;
    }
  }
  size_t groups = 0;
  while (groups < 256 && !(maxscalar < pow2(groups))) ++groups;
  groups = (groups + c - 1) / c;

  for (size_t k = groups; k-- > 0;) {
    if (result_init) {
      ge_p2 p2;
      ge_p3_to_p2(&p2, &result);
      for (size_t i = 0; i < c; ++i) {
        ge_p1p1 p1;
        ge_p2_dbl(&p1, &p2);
        if (i == c - 1)
          ge_p1p1_to_p3(&result, &p1);
        else
          ge_p1p1_to_p2(&p2, &p1);
      }
    }
    std::memset(buckets_init, 0, 1ull << c);

    // partition scalars into buckets
    for (size_t i = 0; i < data.size(); ++i) {
      unsigned int bucket = 0;
      for (size_t j = 0; j < c; ++j)
        if (test(data[i].Scalar, k * c + j)) bucket |= 1 << j;
      if (bucket == 0) continue;
      if (bucket >= (1u << c)) {
        Xi::exceptional<BucketOverflowError>();
      }
      if (buckets_init[bucket]) {
        if (i < cache_size)
          add(buckets[bucket], local_cache->cached[i]);
        else
          add(buckets[bucket], local_cache_2->cached[i - cache_size]);
      } else {
        buckets[bucket] = data[i].Point;
        buckets_init[bucket] = true;
      }
    }

    // sum the buckets
    ge_p3 pail{};
    bool pail_init = false;
    for (size_t i = (1 << c) - 1; i > 0; --i) {
      if (buckets_init[i]) {
        if (pail_init)
          add(pail, buckets[i]);
        else {
          pail = buckets[i];
          pail_init = true;
        }
      }
      if (pail_init) {
        if (result_init)
          add(result, pail);
        else {
          result = pail;
          result_init = true;
        }
      }
    }
  }

  Key res;
  ge_p3_tobytes(res.data(), &result);
  return res;
}
