/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Range/Verify.h"

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Types/Amount.h"
#include "Xi/Rct/Algorithm/Borromean.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"

bool Xi::Rct::Range::verify(const Key& C, const RangeSignature& sig) {
  try {
    ge_p3 CiH[Amount::bits()], asCi[Amount::bits()];
    ge_p3 Ctmp_p3 = ge_p3_identity;
    for (uint16_t i = 0; i < Amount::bits(); i++) {
      // faster equivalent of:
      // subKeys(CiH[i], as.Ci[i], H2[i]);
      // addKeys(Ctmp, Ctmp, as.Ci[i]);
      ge_cached cached;
      ge_p3 p3;
      ge_p1p1 p1;
      exceptional_if_not<GeFromBytesVarTimeError>(ge_frombytes_vartime(&p3, Constants::H2[i].data()) == 0);
      ge_p3_to_cached(&cached, &p3);
      exceptional_if_not<GeFromBytesVarTimeError>(ge_frombytes_vartime(&asCi[i], sig.Ci[i].data()) == 0);
      ge_sub(&p1, &asCi[i], &cached);
      ge_p3_to_cached(&cached, &asCi[i]);
      ge_p1p1_to_p3(&CiH[i], &p1);
      ge_add(&p1, &Ctmp_p3, &cached);
      ge_p1p1_to_p3(&Ctmp_p3, &p1);
    }
    Key Ctmp;
    ge_p3_tobytes(Ctmp.data(), &Ctmp_p3);
    if (C != Ctmp) {
      return false;
    }
    if (!verify_borromean_signature(sig.asig, asCi, CiH)) {
      return false;
    }
    return true;
  } catch (...) {
    // TODO: Logging
    return false;
  }
}
