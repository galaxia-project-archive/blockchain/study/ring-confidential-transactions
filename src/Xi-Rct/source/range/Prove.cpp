/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Range/Prove.h"

#include <cinttypes>

#include <crypto/crypto-ops.h>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Types/AmountBits.h"
#include "Xi/Rct/Types/AmountBitKeys.h"
#include "Xi/Rct/Algorithm/TypeConversion.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/GenerateKey.h"
#include "Xi/Rct/Algorithm/Borromean.h"

Xi::Rct::RangeSignature Xi::Rct::Range::prove(Xi::Rct::Key &C, Xi::Rct::Key &mask, const Xi::Rct::Amount &amount) {
  sc_0(mask.data());
  Key::Identity.copyTo(C);
  AmountBits b;
  amount_to_bits_inplace(amount, b);
  RangeSignature sig;
  AmountBitKeys ai;
  AmountBitKeys CiH;

  for (uint16_t i = 0; i < Amount::bits(); i++) {
    generate_random_secret_key_inplace(ai[i]);
    if (b[i] == 0) {
      key_multiply_scalar_with_basepoint_inplace(ai[i], sig.Ci[i]);
    }
    if (b[i] == 1) {
      key_multiply_with_basepoint_and_add_inplace(ai[i], Constants::H2[i], sig.Ci[i]);
    }
    key_subtract_inplace(sig.Ci[i], Constants::H2[i], CiH[i]);
    sc_add(mask.data(), mask.data(), ai[i].data());
    key_add_inplace(C, sig.Ci[i], C);
  }

  sig.asig = generate_borromean_signature(ai, sig.Ci, CiH, b);
  return sig;
}
