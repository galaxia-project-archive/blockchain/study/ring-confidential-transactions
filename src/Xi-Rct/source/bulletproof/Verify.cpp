/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Bulletproof/Verify.h"

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Types/Types.h"
#include "Xi/Rct/Algorithm/Algorithm.h"

#include "bulletproof/Shared.h"

#define DEBUG_BP

using namespace Xi::Rct::__Impl;

namespace {
struct proof_data_t {
  Xi::Rct::Key x, y, z, x_ip;
  Xi::Rct::KeyVector w;
  size_t logM, inv_offset;
};
}  // namespace

bool Xi::Rct::bulletproof_verify(const Xi::Rct::Bulletproof &proof) {
  try {
    std::vector<const Bulletproof *> proofs;
    proofs.push_back(&proof);
    return bulletproof_verify(proofs);
  } catch (...) {
    // TODO Logging
    return false;
  }
}

bool Xi::Rct::bulletproof_verify(const Xi::Rct::BulletproofVector &proofs) {
  try {
    std::vector<const Bulletproof *> proof_pointers;
    proof_pointers.reserve(proofs.size());
    for (const Bulletproof &proof : proofs) proof_pointers.push_back(&proof);
    return bulletproof_verify(proof_pointers);
  } catch (...) {
    // TODO Logging
    return false;
  }
}

bool Xi::Rct::bulletproof_verify(const std::vector<const Xi::Rct::Bulletproof *> &proofs) {
  try {
    init_exponents();

    const size_t logN = 6;
    const size_t N = 1 << logN;

    // sanity and figure out which proof is longest
    size_t max_length = 0;
    size_t nV = 0;
    std::vector<proof_data_t> proof_data;
    proof_data.reserve(proofs.size());
    size_t inv_offset = 0;
    KeyVector to_invert;
    to_invert.reserve(11 * sizeof(proofs));
    for (const Bulletproof *p : proofs) {
      const Bulletproof &proof = *p;

      // check scalar range
      XI_RETURN_EC_IF_NOT(is_reduced(proof.taux), false, "Input scalar not in range");
      XI_RETURN_EC_IF_NOT(is_reduced(proof.mu), false, "Input scalar not in range");
      XI_RETURN_EC_IF_NOT(is_reduced(proof.a), false, "Input scalar not in range");
      XI_RETURN_EC_IF_NOT(is_reduced(proof.b), false, "Input scalar not in range");
      XI_RETURN_EC_IF_NOT(is_reduced(proof.t), false, "Input scalar not in range");

      XI_RETURN_EC_IF_NOT(proof.V.size() >= 1, false, "V does not have at least one element");
      XI_RETURN_EC_IF_NOT(proof.L.size() == proof.R.size(), false, "Mismatched L and R sizes");
      XI_RETURN_EC_IF_NOT(proof.L.size() > 0, false, "Empty proof");

      max_length = std::max(max_length, proof.L.size());
      nV += proof.V.size();

      // Reconstruct the challenges
      proof_data.resize(proof_data.size() + 1);
      proof_data_t &pd = proof_data.back();
      Key hash_cache = key_fast_hash_to_scalar(proof.V);
      pd.y = hash_cache_mash(hash_cache, proof.A, proof.S);
      XI_RETURN_EC_IF_NOT(!(pd.y == Key::Zero), false, "y == 0");
      pd.z = hash_cache = key_fast_hash_to_scalar(pd.y);
      XI_RETURN_EC_IF_NOT(!(pd.z == Key::Zero), false, "z == 0");
      pd.x = hash_cache_mash(hash_cache, pd.z, proof.T1, proof.T2);
      XI_RETURN_EC_IF_NOT(!(pd.x == Key::Zero), false, "x == 0");
      pd.x_ip = hash_cache_mash(hash_cache, pd.x, proof.taux, proof.mu, proof.t);
      XI_RETURN_EC_IF_NOT(!(pd.x_ip == Key::Zero), false, "x_ip == 0");

      size_t M;
      for (pd.logM = 0; (M = 1ull << pd.logM) <= maxM && M < proof.V.size(); ++pd.logM)
        ;
      XI_RETURN_EC_IF_NOT(proof.L.size() == 6 + pd.logM, false, "Proof is not the expected size");

      const size_t rounds = pd.logM + logN;
      XI_RETURN_EC_IF_NOT(rounds > 0, false, "Zero rounds");

      // PAPER LINES 21-22
      // The inner product challenges are computed per round
      pd.w.resize(rounds);
      for (size_t i = 0; i < rounds; ++i) {
        pd.w[i] = hash_cache_mash(hash_cache, proof.L[i], proof.R[i]);
        XI_RETURN_EC_IF_NOT(!(pd.w[i] == Key::Zero), false, "w[i] == 0");
      }

      pd.inv_offset = inv_offset;
      for (size_t i = 0; i < rounds; ++i) to_invert.push_back(pd.w[i]);
      to_invert.push_back(pd.y);
      inv_offset += rounds + 1;
    }
    XI_RETURN_EC_IF_NOT(max_length < 32, false, "At least one proof is too large");
    size_t maxMN = 1ull << max_length;

    Key tmp;

    std::vector<MultiexpData> multiexp_data;
    multiexp_data.reserve(nV + (2 * (10 /*logM*/ + logN) + 4) * proofs.size() + 2 * maxMN);
    multiexp_data.resize(2 * maxMN);

    const std::vector<Key> inverses = key_vector_invert(to_invert);

    // setup weighted aggregates
    Key z1 = Key::Zero;
    Key z3 = Key::Zero;
    KeyVector m_z4(maxMN, Key::Zero), m_z5(maxMN, Key::Zero);
    Key m_y0 = Key::Zero, y1 = Key::Zero;
    uint32_t proof_data_index = 0;
    for (const Bulletproof *p : proofs) {
      const Bulletproof &proof = *p;
      const proof_data_t &pd = proof_data[proof_data_index++];

      XI_RETURN_EC_IF_NOT(proof.L.size() == 6 + pd.logM, false, "Proof is not the expected size");
      const size_t M = 1ull << pd.logM;
      const size_t MN = M * N;
      const Key weight_y = generate_random_secret_key();
      const Key weight_z = generate_random_secret_key();

      // pre-multiply some points by 8
      KeyVector proof8_V = proof.V;
      for (Key &k : proof8_V) k = key_multiply_scalar_with_eight(k);
      ;
      KeyVector proof8_L = proof.L;
      for (Key &k : proof8_L) k = key_multiply_scalar_with_eight(k);
      KeyVector proof8_R = proof.R;
      for (Key &k : proof8_R) k = key_multiply_scalar_with_eight(k);
      Key proof8_T1 = key_multiply_scalar_with_eight(proof.T1);
      Key proof8_T2 = key_multiply_scalar_with_eight(proof.T2);
      Key proof8_S = key_multiply_scalar_with_eight(proof.S);
      Key proof8_A = key_multiply_scalar_with_eight(proof.A);

      // PAPER LINE 61
      sc_mulsub(m_y0.data(), proof.taux.data(), weight_y.data(), m_y0.data());

      const KeyVector zpow = key_vector_powers(pd.z, M + 3);

      Key k;
      const Key ip1y = key_vector_power_sum(pd.y, MN);
      sc_mulsub(k.data(), zpow[2].data(), ip1y.data(), Key::Zero.data());
      for (size_t j = 1; j <= M; ++j) {
        XI_RETURN_EC_IF_NOT(j + 2 < zpow.size(), false, "invalid zpow index");
        sc_mulsub(k.data(), zpow[j + 2].data(), ip12.data(), k.data());
      }

      sc_muladd(tmp.data(), pd.z.data(), ip1y.data(), k.data());
      sc_sub(tmp.data(), proof.t.data(), tmp.data());
      sc_muladd(y1.data(), tmp.data(), weight_y.data(), y1.data());
      for (size_t j = 0; j < proof8_V.size(); j++) {
        sc_mul(tmp.data(), zpow[j + 2].data(), weight_y.data());
        multiexp_data.emplace_back(tmp, proof8_V[j]);
      }
      sc_mul(tmp.data(), pd.x.data(), weight_y.data());
      multiexp_data.emplace_back(tmp, proof8_T1);
      Key xsq;
      sc_mul(xsq.data(), pd.x.data(), pd.x.data());
      sc_mul(tmp.data(), xsq.data(), weight_y.data());
      multiexp_data.emplace_back(tmp, proof8_T2);

      // PAPER LINE 62
      multiexp_data.emplace_back(weight_z, proof8_A);
      sc_mul(tmp.data(), pd.x.data(), weight_z.data());
      multiexp_data.emplace_back(tmp, proof8_S);

      // Compute the number of rounds for the inner product
      const size_t rounds = pd.logM + logN;
      XI_RETURN_EC_IF_NOT(rounds > 0, false, "Zero rounds");

      // Basically PAPER LINES 24-25
      // Compute the curvepoints from G[i] and H[i]
      Key yinvpow = Key::Identity;
      Key ypow = Key::Identity;

      const Key *winv = &inverses[pd.inv_offset];
      const Key yinv = inverses[pd.inv_offset + rounds];

      // precalc
      KeyVector w_cache(1ull << rounds);
      w_cache[0] = winv[0];
      w_cache[1] = pd.w[0];
      for (size_t j = 1; j < rounds; ++j) {
        const size_t slots = 1ull << (j + 1);
        for (size_t s = slots; s-- > 0; --s) {
          sc_mul(w_cache[s].data(), w_cache[s / 2].data(), pd.w[j].data());
          sc_mul(w_cache[s - 1].data(), w_cache[s / 2].data(), winv[j].data());
        }
      }

      for (size_t i = 0; i < MN; ++i) {
        Key g_scalar = proof.a;
        Key h_scalar;
        if (i == 0)
          h_scalar = proof.b;
        else
          sc_mul(h_scalar.data(), proof.b.data(), yinvpow.data());

        // Convert the index to binary IN REVERSE and construct the scalar exponent
        sc_mul(g_scalar.data(), g_scalar.data(), w_cache[i].data());
        sc_mul(h_scalar.data(), h_scalar.data(), w_cache[(~i) & (MN - 1)].data());

        // Adjust the scalars using the exponents from PAPER LINE 62
        sc_add(g_scalar.data(), g_scalar.data(), pd.z.data());
        XI_RETURN_EC_IF_NOT(2 + i / N < zpow.size(), false, "invalid zpow index");
        XI_RETURN_EC_IF_NOT(i % N < twoN.size(), false, "invalid twoN index");
        sc_mul(tmp.data(), zpow[2 + i / N].data(), twoN[i % N].data());
        if (i == 0) {
          sc_add(tmp.data(), tmp.data(), pd.z.data());
          sc_sub(h_scalar.data(), h_scalar.data(), tmp.data());
        } else {
          sc_muladd(tmp.data(), pd.z.data(), ypow.data(), tmp.data());
          sc_mulsub(h_scalar.data(), tmp.data(), yinvpow.data(), h_scalar.data());
        }

        sc_mulsub(m_z4[i].data(), g_scalar.data(), weight_z.data(), m_z4[i].data());
        sc_mulsub(m_z5[i].data(), h_scalar.data(), weight_z.data(), m_z5[i].data());

        if (i == 0) {
          yinvpow = yinv;
          ypow = pd.y;
        } else if (i != MN - 1) {
          sc_mul(yinvpow.data(), yinvpow.data(), yinv.data());
          sc_mul(ypow.data(), ypow.data(), pd.y.data());
        }
      }

      // PAPER LINE 26
      sc_muladd(z1.data(), proof.mu.data(), weight_z.data(), z1.data());
      for (size_t i = 0; i < rounds; ++i) {
        sc_mul(tmp.data(), pd.w[i].data(), pd.w[i].data());
        sc_mul(tmp.data(), tmp.data(), weight_z.data());
        multiexp_data.emplace_back(tmp, proof8_L[i]);
        sc_mul(tmp.data(), winv[i].data(), winv[i].data());
        sc_mul(tmp.data(), tmp.data(), weight_z.data());
        multiexp_data.emplace_back(tmp, proof8_R[i]);
      }
      sc_mulsub(tmp.data(), proof.a.data(), proof.b.data(), proof.t.data());
      sc_mul(tmp.data(), tmp.data(), pd.x_ip.data());
      sc_muladd(z3.data(), tmp.data(), weight_z.data(), z3.data());
    }

    // now check all proofs at once
    sc_sub(tmp.data(), m_y0.data(), z1.data());
    multiexp_data.emplace_back(tmp, Key::BasePoint);
    sc_sub(tmp.data(), z3.data(), y1.data());
    multiexp_data.emplace_back(tmp, Constants::H);
    for (size_t i = 0; i < maxMN; ++i) {
      m_z4[i].copyTo(multiexp_data[i * 2].Scalar);
      multiexp_data[i * 2] = {m_z4[i], Gi_p3[i]};
      multiexp_data[i * 2 + 1] = {m_z5[i], Hi_p3[i]};
    }
    const auto multiexp = compute_multiexp(multiexp_data, 2 * maxMN);
    if (multiexp != Key::Identity) {
      return false;
    }

    return true;
  } catch (...) {
    // TODO Logging
    return false;
  }
}
