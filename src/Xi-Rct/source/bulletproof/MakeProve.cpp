/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Bulletproof/MakeProve.h"

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/lock_guard.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Span.h>
#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>
#include <crypto/hash.h>
#include <Common/Varint.h>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Types/Types.h"
#include "Xi/Rct/Algorithm/Algorithm.h"

#include "bulletproof/Shared.h"

using namespace Xi::Rct::__Impl;

Xi::Rct::Bulletproof Xi::Rct::bulletproof_make_prove(const Key &v, const Key &gamma) {
  return bulletproof_make_prove(KeyVector{1, v}, KeyVector{1, gamma});
}

Xi::Rct::Bulletproof Xi::Rct::bulletproof_make_prove(Amount v, const Key &gamma) {
  return bulletproof_make_prove(AmountVector{1, v}, KeyVector{1, gamma});
}

Xi::Rct::Bulletproof Xi::Rct::bulletproof_make_prove(const KeyVector &sv, const KeyVector &gamma) {
  exceptional_if_not<IncompatibleSizeError>(sv.size() == gamma.size());
  exceptional_if_not<GenericError>(sv.size() == gamma.size(), "Incompatible sizes of sv and gamma");
  exceptional_if_not<GenericError>(!sv.empty(), "sv is empty");
  for (const Key &sve : sv) {
    exceptional_if_not<GenericError>(is_reduced(sve), "Invalid sv input");
  }
  for (const Key &g : gamma) {
    exceptional_if_not<GenericError>(is_reduced(g), "Invalid gamma input");
  }

  init_exponents();

  constexpr size_t logN = 6;  // log2(64)
  constexpr size_t N = 1ull << logN;
  size_t M, logM;
  for (logM = 0; (M = 1ull << logM) <= maxM && M < sv.size(); ++logM) {
    /* */
  }
  exceptional_if_not<GenericError>(M <= maxM, "sv/gamma are too large");
  const size_t logMN = logM + logN;
  const size_t MN = M * N;

  KeyVector V(sv.size());
  KeyVector aL(MN), aR(MN);
  KeyVector aL8(MN), aR8(MN);
  Key tmp, tmp2;

  for (size_t i = 0; i < sv.size(); ++i) {
    Key gamma8, sv8;
    sc_mul(gamma8.data(), gamma[i].data(), Key::EightInverse.data());
    sc_mul(sv8.data(), sv[i].data(), Key::EightInverse.data());
    key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(gamma8, sv8, Constants::H, V[i]);
  }

  for (size_t j = 0; j < M; ++j) {
    for (size_t i = N; i-- > 0;) {
      if (j < sv.size() && (sv[j][i / 8] & (((uint64_t)1) << (i % 8)))) {
        aL[j * N + i] = Key::Identity;
        aL8[j * N + i] = Key::EightInverse;
        aR[j * N + i] = aR8[j * N + i] = Key::Zero;
      } else {
        aL[j * N + i] = aL8[j * N + i] = Key::Zero;
        aR[j * N + i] = Key::MinusOne;
        aR8[j * N + i] = Key::MinusEightInverse;
      }
    }
  }

  // DEBUG: Test to ensure this recovers the value
#if !defined(NDEBUG)
  for (size_t j = 0; j < M; ++j) {
    uint64_t test_aL = 0, test_aR = 0;
    for (size_t i = 0; i < N; ++i) {
      if (aL[j * N + i] == Key::Identity) test_aL += ((uint64_t)1) << i;
      if (aR[j * N + i] == Key::Zero) test_aR += ((uint64_t)1) << i;
    }
    uint64_t v_test = 0;
    if (j < sv.size()) {
      for (uint8_t n = 0; n < 8; ++n) v_test |= (((uint64_t)sv[j][n]) << (8 * n));
    }
    exceptional_if_not<GenericError>(test_aL == v_test, "test_aL failed");
    exceptional_if_not<GenericError>(test_aR == v_test, "test_aR failed");
  }
#endif

try_again:
  Key hash_cache = key_fast_hash_to_scalar(V);

  // PAPER LINES 38-39
  Key alpha = generate_random_secret_key();
  Key ve = key_vector_exponent(aL8, aR8);
  Key A;
  sc_mul(tmp.data(), alpha.data(), Key::EightInverse.data());
  key_add_inplace(ve, key_multiply_scalar_with_basepoint(tmp), A);

  // PAPER LINES 40-42
  KeyVector sL = generate_random_secret_keys(MN), sR = generate_random_secret_keys(MN);
  Key rho = generate_random_secret_key();
  ve = key_vector_exponent(sL, sR);
  Key S;
  key_add_inplace(ve, key_multiply_scalar_with_basepoint(rho), S);
  S = key_multiply_scalar(S, Key::EightInverse);

  // PAPER LINES 43-45
  Key y = hash_cache_mash(hash_cache, A, S);
  if (y == Key::Zero) {
    goto try_again;
  }
  Key z = hash_cache = key_fast_hash_to_scalar(y);
  if (z == Key::Zero) {
    goto try_again;
  }

  // Polynomial construction by coefficients
  KeyVector l0 = key_vector_subtract(aL, z);
  const KeyVector &l1 = sL;

  // This computes the ugly sum/concatenation from PAPER LINE 65
  KeyVector zero_twos(MN);
  const KeyVector zpow = key_vector_powers(z, M + 2);
  for (size_t i = 0; i < MN; ++i) {
    zero_twos[i] = Key::Zero;
    for (size_t j = 1; j <= M; ++j) {
      if (i >= (j - 1) * N && i < j * N) {
        exceptional_if_not<GenericError>(1 + j < zpow.size(), "invalid zpow index");
        exceptional_if_not<GenericError>(i - (j - 1) * N < twoN.size(), "invalid twoN index");
        sc_muladd(zero_twos[i].data(), zpow[1 + j].data(), twoN[i - (j - 1) * N].data(), zero_twos[i].data());
      }
    }
  }

  KeyVector r0 = key_vector_add(aR, z);
  const auto yMN = key_vector_powers(y, MN);
  r0 = Hadamard::product(r0, yMN);
  r0 = key_vector_add(r0, zero_twos);
  KeyVector r1 = Hadamard::product(yMN, sR);

  // Polynomial construction before PAPER LINE 46
  Key t1_1 = key_vector_inner_product(l0, r1);
  Key t1_2 = key_vector_inner_product(l1, r0);
  Key t1;
  sc_add(t1.data(), t1_1.data(), t1_2.data());
  Key t2 = key_vector_inner_product(l1, r1);

  // PAPER LINES 47-48
  Key tau1 = generate_random_secret_key(), tau2 = generate_random_secret_key();

  Key T1, T2;
  ge_p3 p3;
  sc_mul(tmp.data(), t1.data(), Key::EightInverse.data());
  sc_mul(tmp2.data(), tau1.data(), Key::EightInverse.data());
  ge_double_scalarmult_base_vartime_p3(&p3, tmp.data(), &ge_p3_H, tmp2.data());
  ge_p3_tobytes(T1.data(), &p3);
  sc_mul(tmp.data(), t2.data(), Key::EightInverse.data());
  sc_mul(tmp2.data(), tau2.data(), Key::EightInverse.data());
  ge_double_scalarmult_base_vartime_p3(&p3, tmp.data(), &ge_p3_H, tmp2.data());
  ge_p3_tobytes(T2.data(), &p3);

  // PAPER LINES 49-51
  Key x = hash_cache_mash(hash_cache, z, T1, T2);
  if (x == Key::Zero) {
    goto try_again;
  }

  // PAPER LINES 52-53
  Key taux;
  sc_mul(taux.data(), tau1.data(), x.data());
  Key xsq;
  sc_mul(xsq.data(), x.data(), x.data());
  sc_muladd(taux.data(), tau2.data(), xsq.data(), taux.data());
  for (size_t j = 1; j <= sv.size(); ++j) {
    exceptional_if_not<GenericError>(j + 1 < zpow.size(), "invalid zpow index");
    sc_muladd(taux.data(), zpow[j + 1].data(), gamma[j - 1].data(), taux.data());
  }
  Key mu;
  sc_muladd(mu.data(), x.data(), rho.data(), alpha.data());

  // PAPER LINES 54-57
  KeyVector l = l0;
  l = key_vector_add(l, key_vector_scalar(l1, x));
  KeyVector r = r0;
  r = key_vector_add(r, key_vector_scalar(r1, x));

  Key t = key_vector_inner_product(l, r);

  // DEBUG: Test if the l and r vectors match the polynomial forms
#ifdef _DEBUG
  Key test_t;
  const Key t0 = key_vector_inner_product(l0, r0);
  sc_muladd(test_t.data(), t1.data(), x.data(), t0.data());
  sc_muladd(test_t.data(), t2.data(), xsq.data(), test_t.data());
  exceptional_if_not<GenericError>(test_t == t, "test_t check failed");
#endif

  // PAPER LINES 32-33
  Key x_ip = hash_cache_mash(hash_cache, x, taux, mu, t);
  if (x_ip == Key::Zero) {
    goto try_again;
  }

  // These are used in the inner product rounds
  size_t nprime = MN;
  std::vector<ge_p3> Gprime(MN);
  std::vector<ge_p3> Hprime(MN);
  KeyVector aprime(MN);
  KeyVector bprime(MN);
  const Key yinv = key_invert(y);
  KeyVector yinvpow(MN);
  yinvpow[0] = Key::Identity;
  yinvpow[1] = yinv;
  for (size_t i = 0; i < MN; ++i) {
    Gprime[i] = Gi_p3[i];
    Hprime[i] = Hi_p3[i];
    if (i > 1) sc_mul(yinvpow[i].data(), yinvpow[i - 1].data(), yinv.data());
    aprime[i] = l[i];
    bprime[i] = r[i];
  }
  KeyVector L(logMN);
  KeyVector R(logMN);
  uint32_t round = 0;
  KeyVector w(logMN);  // this is the challenge x in the inner product protocol

  // PAPER LINE 13
  const KeyVector *scale = &yinvpow;
  while (nprime > 1) {
    // PAPER LINE 15
    nprime /= 2;

    // PAPER LINES 16-17
    Key cL = key_vector_inner_product(span_slice_const(aprime, 0, nprime).takeOrThrow(),
                                      span_slice_const(bprime, nprime, bprime.size()).takeOrThrow());
    Key cR = key_vector_inner_product(span_slice_const(aprime, nprime, aprime.size()).takeOrThrow(),
                                      span_slice_const(bprime, 0, nprime).takeOrThrow());

    // PAPER LINES 18-19
    sc_mul(tmp.data(), cL.data(), x_ip.data());
    L[round] =
        cross_vector_exponent8(nprime, Gprime, nprime, Hprime, 0, aprime, 0, bprime, nprime, scale, &ge_p3_H, &tmp);
    sc_mul(tmp.data(), cR.data(), x_ip.data());
    R[round] =
        cross_vector_exponent8(nprime, Gprime, 0, Hprime, nprime, aprime, nprime, bprime, 0, scale, &ge_p3_H, &tmp);

    // PAPER LINES 21-22
    w[round] = hash_cache_mash(hash_cache, L[round], R[round]);
    if (w[round] == Key::Zero) {
      goto try_again;
    }

    // PAPER LINES 24-25
    const Key winv = key_invert(w[round]);
    if (nprime > 1) {
      Hadamard::fold(Gprime, winv, w[round]);
      if (scale != nullptr) {
        Hadamard::fold(Hprime, *scale, w[round], winv);
      } else {
        Hadamard::fold(Hprime, w[round], winv);
      }
    }

    // PAPER LINES 28-29
    aprime = key_vector_add(key_vector_scalar(span_slice_const(aprime, 0, nprime).takeOrThrow(), w[round]),
                            key_vector_scalar(span_slice_const(aprime, nprime, aprime.size()).takeOrThrow(), winv));
    bprime = key_vector_add(key_vector_scalar(span_slice_const(bprime, 0, nprime).takeOrThrow(), winv),
                            key_vector_scalar(span_slice_const(bprime, nprime, bprime.size()).takeOrThrow(), w[round]));

    scale = nullptr;
    ++round;
  }

  // PAPER LINE 58 (with inclusions from PAPER LINE 8 and PAPER LINE 20)
  return Bulletproof{std::move(V), A, S, T1, T2, taux, mu, std::move(L), std::move(R), aprime[0], bprime[0], t};
}

Xi::Rct::Bulletproof Xi::Rct::bulletproof_make_prove(const AmountVector &v, const Xi::Rct::KeyVector &gamma) {
  exceptional_if_not<IncompatibleSizeError>(v.size() == gamma.size(), "Incompatible sizes of v and gamma");

  // vG + gammaH
  KeyVector sv{v.size()};
  for (size_t i = 0; i < v.size(); ++i) {
    Key::Zero.copyTo(sv[i]);
    sv[i][0] = v[i].value & 255;
    sv[i][1] = (v[i].value >> 8) & 255;
    sv[i][2] = (v[i].value >> 16) & 255;
    sv[i][3] = (v[i].value >> 24) & 255;
    sv[i][4] = (v[i].value >> 32) & 255;
    sv[i][5] = (v[i].value >> 40) & 255;
    sv[i][6] = (v[i].value >> 48) & 255;
    sv[i][7] = (v[i].value >> 56) & 255;
  }
  return bulletproof_make_prove(sv, gamma);
}

Xi::Rct::Bulletproof Xi::Rct::bulletproof_make_prove(Xi::Rct::KeyVector &C, Xi::Rct::KeyVector &masks,
                                                     const Xi::Rct::AmountVector &amounts,
                                                     Span<const Xi::Rct::Key> sk) {
  exceptional_if_not<IncompatibleSizeError>(amounts.size() == sk.size(), "Invalid amounts/sk sizes");
  masks.resize(amounts.size());
  for (size_t i = 0; i < masks.size(); ++i) {
    masks[i] = generate_commitment_mask(sk.data()[i]);
  }
  Bulletproof proof = bulletproof_make_prove(amounts, masks);
  exceptional_if_not<IncompatibleSizeError>(proof.V.size() == amounts.size(), "V does not have the expected size");
  C = proof.V;
  return proof;
}
