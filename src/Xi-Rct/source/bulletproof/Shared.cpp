/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "bulletproof/Shared.h"

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/thread/lock_guard.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Common/Varint.h>
#include <crypto/hash.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Constants.h"

ge_p3 Xi::Rct::__Impl::Hi_p3[Xi::Rct::__Impl::maxN * Xi::Rct::__Impl::maxM];
ge_p3 Xi::Rct::__Impl::Gi_p3[Xi::Rct::__Impl::maxN * Xi::Rct::__Impl::maxM];
Xi::Rct::Key Xi::Rct::__Impl::Hi[Xi::Rct::__Impl::maxN * Xi::Rct::__Impl::maxM];
Xi::Rct::Key Xi::Rct::__Impl::Gi[Xi::Rct::__Impl::maxN * Xi::Rct::__Impl::maxM];
Xi::Rct::Straus::SharedStrausCache Xi::Rct::__Impl::straus_HiGi_cache{nullptr};
Xi::Rct::Pippenger::SharedPippengerCache Xi::Rct::__Impl::pippenger_HiGi_cache{nullptr};
const Xi::Rct::KeyVector Xi::Rct::__Impl::oneN{Xi::Rct::__Impl::maxN, Xi::Rct::Key::Identity};
const Xi::Rct::KeyVector Xi::Rct::__Impl::twoN = key_vector_powers(Xi::Rct::Key::Two, Xi::Rct::__Impl::maxN);
const Xi::Rct::Key Xi::Rct::__Impl::ip12 = key_vector_inner_product(Xi::Rct::__Impl::oneN, Xi::Rct::__Impl::twoN);

bool Xi::Rct::__Impl::is_reduced(const Xi::Rct::Key &key) { return sc_check(key.data()) == 0; }

Xi::Rct::Key Xi::Rct::__Impl::key_vector_exponent(const Xi::Rct::KeyVector &a, const Xi::Rct::KeyVector &b) {
  if (a.size() != b.size()) {
    Xi::exceptional<Xi::Rct::IncompatibleSizeError>();
  }
  if (a.size() > maxN * maxM) {
    Xi::exceptional<Xi::Rct::IncompatibleSizeError>();
  }

  std::vector<Xi::Rct::MultiexpData> multiexp_data;
  multiexp_data.reserve(a.size() * 2);
  for (size_t i = 0; i < a.size(); ++i) {
    multiexp_data.emplace_back(a[i], Gi_p3[i]);
    multiexp_data.emplace_back(b[i], Hi_p3[i]);
  }
  return compute_multiexp(multiexp_data, 2 * a.size());
}

Xi::Rct::Key Xi::Rct::__Impl::get_exponent(const Xi::Rct::Key &base, size_t idx) {
  static const std::string salt("bulletpr00f");
  std::string hashed = std::string((const char *)base.data(), base.size()) + salt + Tools::get_varint_data(idx);
  const Xi::Rct::Key e = Xi::Rct::key_hash_to_key_point(key_cast(Crypto::cn_fast_hash(hashed.data(), hashed.size())));
  Xi::exceptional_if<GenericError>(e == Xi::Rct::Key::Identity, "Exponent is point at infinity");
  return e;
}

void Xi::Rct::__Impl::init_exponents() {
  boost::lock_guard<boost::mutex> lock(init_mutex);

  static bool init_done = false;
  if (init_done) return;
  std::vector<Xi::Rct::MultiexpData> data;
  data.reserve(maxN * maxM * 2);
  for (size_t i = 0; i < maxN * maxM; ++i) {
    Hi[i] = get_exponent(Xi::Rct::Constants::H, i * 2);
    Xi::exceptional_if_not<Xi::Rct::GeFromBytesVarTimeError>(ge_frombytes_vartime(&Hi_p3[i], Hi[i].data()) == 0);
    Gi[i] = get_exponent(Xi::Rct::Constants::H, i * 2 + 1);
    Xi::exceptional_if_not<Xi::Rct::GeFromBytesVarTimeError>(ge_frombytes_vartime(&Gi_p3[i], Gi[i].data()) == 0);

    data.push_back({Xi::Rct::Key::Zero, Gi_p3[i]});
    data.push_back({Xi::Rct::Key::Zero, Hi_p3[i]});
  }

  straus_HiGi_cache = Xi::Rct::Straus::init_cache(data, Xi::Rct::Straus::size_limit());
  pippenger_HiGi_cache = Xi::Rct::Pippenger::init_cache(data, 0, Xi::Rct::Pippenger::size_limit());

  init_done = true;
}

Xi::Rct::Key Xi::Rct::__Impl::cross_vector_exponent8(size_t size, const std::vector<ge_p3> &A, size_t Ao,
                                                     const std::vector<ge_p3> &B, size_t Bo,
                                                     const Xi::Rct::KeyVector &a, size_t ao,
                                                     const Xi::Rct::KeyVector &b, size_t bo,
                                                     const Xi::Rct::KeyVector *scale, const ge_p3 *extra_point,
                                                     const Xi::Rct::Key *extra_scalar) {
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(size + Ao <= A.size(), "Incompatible size for A");
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(size + Bo <= B.size(), "Incompatible size for B");
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(size + ao <= a.size(), "Incompatible size for a");
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(size + bo <= b.size(), "Incompatible size for b");
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(size <= maxN * maxM, "size is too large");
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(!scale || size == scale->size() / 2,
                                                         "Incompatible size for scale");
  Xi::exceptional_if_not<Xi::Rct::IncompatibleSizeError>(!!extra_point == !!extra_scalar,
                                                         "only one of extra point/scalar present");

  std::vector<Xi::Rct::MultiexpData> multiexp_data;
  multiexp_data.resize(size * 2 + (!!extra_point));
  for (size_t i = 0; i < size; ++i) {
    sc_mul(multiexp_data[i * 2].Scalar.data(), a[ao + i].data(), Xi::Rct::Key::EightInverse.data());
    ;
    multiexp_data[i * 2].Point = A[Ao + i];
    sc_mul(multiexp_data[i * 2 + 1].Scalar.data(), b[bo + i].data(), Xi::Rct::Key::EightInverse.data());
    if (scale)
      sc_mul(multiexp_data[i * 2 + 1].Scalar.data(), multiexp_data[i * 2 + 1].Scalar.data(), (*scale)[Bo + i].data());
    multiexp_data[i * 2 + 1].Point = B[Bo + i];
  }
  if (extra_point) {
    sc_mul(multiexp_data.back().Scalar.data(), extra_scalar->data(), Xi::Rct::Key::EightInverse.data());
    multiexp_data.back().Point = *extra_point;
  }
  return Xi::Rct::compute_multiexp(multiexp_data, 0);
}

Xi::Rct::Key Xi::Rct::__Impl::hash_cache_mash(Xi::Rct::Key &hash_cache, const Xi::Rct::Key &mash0,
                                              const Xi::Rct::Key &mash1) {
  Xi::Rct::KeyVector data;
  data.reserve(3);
  data.push_back(hash_cache);
  data.push_back(mash0);
  data.push_back(mash1);
  return hash_cache = Xi::Rct::key_fast_hash_to_scalar(data);
}

Xi::Rct::Key Xi::Rct::__Impl::hash_cache_mash(Xi::Rct::Key &hash_cache, const Xi::Rct::Key &mash0,
                                              const Xi::Rct::Key &mash1, const Xi::Rct::Key &mash2) {
  Xi::Rct::KeyVector data;
  data.reserve(4);
  data.push_back(hash_cache);
  data.push_back(mash0);
  data.push_back(mash1);
  data.push_back(mash2);
  return hash_cache = Xi::Rct::key_fast_hash_to_scalar(data);
}

Xi::Rct::Key Xi::Rct::__Impl::hash_cache_mash(Xi::Rct::Key &hash_cache, const Xi::Rct::Key &mash0,
                                              const Xi::Rct::Key &mash1, const Xi::Rct::Key &mash2,
                                              const Xi::Rct::Key &mash3) {
  Xi::Rct::KeyVector data;
  data.reserve(5);
  data.push_back(hash_cache);
  data.push_back(mash0);
  data.push_back(mash1);
  data.push_back(mash2);
  data.push_back(mash3);
  return hash_cache = Xi::Rct::key_fast_hash_to_scalar(data);
}
