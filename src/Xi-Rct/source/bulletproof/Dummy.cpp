/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Bulletproof/Dummy.h"

#include <crypto/crypto-ops.h>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"

Xi::Rct::Bulletproof Xi::Rct::bulletproof_make_dummy(const Xi::Rct::AmountVector &outamounts, Xi::Rct::KeyVector &C,
                                                     Xi::Rct::KeyVector &masks) {
  const size_t n_outs = outamounts.size();
  const Key I = Key::Identity;
  size_t nrl = 0;
  while ((1ull << nrl) < n_outs) ++nrl;
  nrl += 6;

  C.resize(n_outs);
  masks.resize(n_outs);
  for (size_t i = 0; i < n_outs; ++i) {
    masks[i] = I;
    Key sv8, sv;
    sv = Key::Zero;
    sv[0] = outamounts[i].value & 255;
    sv[1] = (outamounts[i].value >> 8) & 255;
    sv[2] = (outamounts[i].value >> 16) & 255;
    sv[3] = (outamounts[i].value >> 24) & 255;
    sv[4] = (outamounts[i].value >> 32) & 255;
    sv[5] = (outamounts[i].value >> 40) & 255;
    sv[6] = (outamounts[i].value >> 48) & 255;
    sv[7] = (outamounts[i].value >> 56) & 255;
    sc_mul(sv8.data(), sv.data(), Key::EightInverse.data());
    key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(Key::EightInverse, sv8, Constants::H, C[i]);
  }

  return Bulletproof{KeyVector{n_outs, I}, I, I, I, I, I, I, KeyVector{nrl, I}, KeyVector{nrl, I}, I, I, I};
}
