/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/thread/mutex.hpp>
#include <boost/beast/core/span.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Exceptional.h>

#include "Xi/Rct/Types/Types.h"
#include "Xi/Rct/Algorithm/Algorithm.h"

namespace Xi {
namespace Rct {
XI_DECLARE_EXCEPTIONAL_CATEGORY(Bulletproof)
XI_DECLARE_EXCEPTIONAL_INSTANCE(Generic, "NA", Bulletproof)

namespace __Impl {
static constexpr size_t maxN = 64;
static constexpr size_t maxM = Bulletproof::maxOutputs();
extern Key Hi[maxN * maxM];
extern Key Gi[maxN * maxM];
extern ge_p3 Hi_p3[maxN * maxM];
extern ge_p3 Gi_p3[maxN * maxM];
extern Straus::SharedStrausCache straus_HiGi_cache;
extern Pippenger::SharedPippengerCache pippenger_HiGi_cache;
extern const KeyVector oneN;
extern const KeyVector twoN;
extern const Key ip12;

bool is_reduced(const Key &key);

Key key_vector_exponent(const KeyVector &a, const KeyVector &b);

Key get_exponent(const Key &base, size_t idx);

static boost::mutex init_mutex;

void init_exponents();

Key cross_vector_exponent8(size_t size, const std::vector<ge_p3> &A, size_t Ao, const std::vector<ge_p3> &B, size_t Bo,
                           const KeyVector &a, size_t ao, const KeyVector &b, size_t bo, const KeyVector *scale,
                           const ge_p3 *extra_point, const Key *extra_scalar);

Key hash_cache_mash(Key &hash_cache, const Key &mash0, const Key &mash1);
Key hash_cache_mash(Key &hash_cache, const Key &mash0, const Key &mash1, const Key &mash2);
Key hash_cache_mash(Key &hash_cache, const Key &mash0, const Key &mash1, const Key &mash2, const Key &mash3);
}  // namespace __Impl
}  // namespace Rct
}  // namespace Xi
