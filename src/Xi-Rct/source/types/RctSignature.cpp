/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Types/RctSignature.h"

#include <cassert>

#include <Serialization/SerializationOverloads.h>

#include "Xi/Rct/Algorithm/RctCategoryDetection.h"

void Xi::Rct::RctSignature::nullify() {
  this->RctSignatureBase::nullify();
  Prunable.nullify();
}

void Xi::Rct::RctSignature::serialize(CryptoNote::ISerializer &serializer) {
  serializer(Type, "type");
  if (Type == RctType::Null) {
    return;
  }
  // Message & MixinRing are reconstructed from transaction data
  if (Type == RctType::Simple) {
    serializer(PseudoOuts, "pseudo_outputs");
  }
  serializer(EcdhInfo, "ecdh_info");
  if (serializer.type() == CryptoNote::ISerializer::INPUT) {
    KeyVector outPk;
    serializer(outPk, "out_public_keys");
    OutPublicKeys.resize(outPk.size());
    for (size_t i = 0; i < outPk.size(); ++i) {
      OutPublicKeys[i].Dest = Key::Identity;
      OutPublicKeys[i].Mask = outPk[i];
    }
  } else {
    assert(serializer.type() == CryptoNote::ISerializer::OUTPUT);
    KeyVector outPk{OutPublicKeys.size()};
    for (size_t i = 0; i < outPk.size(); ++i) {
      outPk[i] = OutPublicKeys[i].Mask;
    }
    serializer(outPk, "out_public_keys");
  }

  serializer(TransactionFee.value, "transaction_fee");
  serializer(Prunable.RangeSignatures, "prunable_range_signatures");
  if (Prunable.RangeSignatures.empty()) {
    serializer(Prunable.Bulletproofs, "prunable_bulletproofs");
  }
  serializer(Prunable.MLSAGs, "prunable_mlsags");
  if (is_bulletproof_type(Type)) {
    serializer(Prunable.PseudoOutputs, "prunable_pseudo_outputs");
  }
}
