/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Types/Key.h"

#include <cstring>

const Xi::Rct::Key Xi::Rct::Key::Zero{{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

const Xi::Rct::Key Xi::Rct::Key::Identity{{0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                           0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                           0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

const Xi::Rct::Key Xi::Rct::Key::CurveOrder{{0xed, 0xd3, 0xf5, 0x5c, 0x1a, 0x63, 0x12, 0x58, 0xd6, 0x9c, 0xf7,
                                             0xa2, 0xde, 0xf9, 0xde, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                             0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10}};

const Xi::Rct::Key Xi::Rct::Key::BasePoint{{0x58, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66,
                                            0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66,
                                            0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66, 0x66}};
const Xi::Rct::Key Xi::Rct::Key::Eight{{0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};
const Xi::Rct::Key Xi::Rct::Key::EightInverse{{0x79, 0x2f, 0xdc, 0xe2, 0x29, 0xe5, 0x06, 0x61, 0xd0, 0xda, 0x1c,
                                               0x7d, 0xb3, 0x9d, 0xd3, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x06}};

const Xi::Rct::Key Xi::Rct::Key::Two = {{0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                         0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};
const Xi::Rct::Key Xi::Rct::Key::MinusOne = {{0xec, 0xd3, 0xf5, 0x5c, 0x1a, 0x63, 0x12, 0x58, 0xd6, 0x9c, 0xf7,
                                              0xa2, 0xde, 0xf9, 0xde, 0x14, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10}};
const Xi::Rct::Key Xi::Rct::Key::MinusEightInverse = {{0x74, 0xa4, 0x19, 0x7a, 0xf0, 0x7d, 0x0b, 0xf7, 0x05, 0xc2, 0xda,
                                                       0x25, 0x2b, 0x5c, 0x0b, 0x0d, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                                                       0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0a}};

Xi::Rct::Key Xi::Rct::Key::copy() const {
  Key reval;
  copyTo(reval);
  return reval;
}

void Xi::Rct::Key::copyTo(Xi::Rct::Key &out) const { std::memcpy(out.data(), data(), size()); }

bool Xi::Rct::Key::operator==(const Xi::Rct::Key &rhs) const {
  return ::std::memcmp(data(), rhs.data(), size() * sizeof(value_type)) == 0;
}

bool Xi::Rct::Key::operator!=(const Xi::Rct::Key &rhs) const {
  return ::std::memcmp(data(), rhs.data(), size() * sizeof(value_type)) != 0;
}

bool Xi::Rct::Key::operator<(const Xi::Rct::Key &rhs) const {
  for (int8_t n = 31; n >= 0; --n) {
    if ((*this)[(uint8_t)n] < rhs[(uint8_t)n]) return true;
    if ((*this)[(uint8_t)n] > rhs[(uint8_t)n]) return false;
  }
  return false;
}

bool Xi::Rct::Key::operator<=(const Xi::Rct::Key &rhs) const { return !(*this > rhs); }

bool Xi::Rct::Key::operator>(const Xi::Rct::Key &rhs) const { return rhs < *this; }

bool Xi::Rct::Key::operator>=(const Xi::Rct::Key &rhs) const { return !(*this < rhs); }

Xi::Rct::Key::operator const ::Crypto::KeyImage &() const {
  return *reinterpret_cast<const ::Crypto::KeyImage *>(data());
}

Xi::Rct::Key::operator const ::Crypto::SecretKey &() const {
  return *reinterpret_cast<const ::Crypto::SecretKey *>(data());
}

Xi::Rct::Key::operator const ::Crypto::PublicKey &() const {
  return *reinterpret_cast<const ::Crypto::PublicKey *>(data());
}

Xi::Rct::Key::operator const ::Crypto::Hash &() const { return *reinterpret_cast<const ::Crypto::Hash *>(data()); }

Xi::Rct::Key::operator const ::Crypto::KeyDerivation &() const {
  return *reinterpret_cast<const ::Crypto::KeyDerivation *>(data());
}

const Xi::Rct::Key &Xi::Rct::key_cast(const ::Crypto::PublicKey &key) { return *reinterpret_cast<const Key *>(&key); }

const Xi::Rct::Key &Xi::Rct::key_cast(const ::Crypto::SecretKey &key) { return *reinterpret_cast<const Key *>(&key); }

const Xi::Rct::Key &Xi::Rct::key_cast(const ::Crypto::KeyImage &key) { return *reinterpret_cast<const Key *>(&key); }

const Xi::Rct::Key &Xi::Rct::key_cast(const ::Crypto::Hash &key) { return *reinterpret_cast<const Key *>(&key); }

const Xi::Rct::Key &Xi::Rct::key_cast(const Crypto::KeyDerivation &key) { return *reinterpret_cast<const Key *>(&key); }

void Xi::Rct::serialize(Xi::Rct::Key &key, CryptoNote::ISerializer &serializer) {
  serializer.binary(key.data(), key.size() * sizeof(Key::value_type), "blob");
}

Xi::Rct::KeyMatrix Xi::Rct::make_key_matrix(size_t rows, size_t columns) {
  KeyMatrix reval;
  reval.resize(columns, KeyVector{rows});
  return reval;
}
