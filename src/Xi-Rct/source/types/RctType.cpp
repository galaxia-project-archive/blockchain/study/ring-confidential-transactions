/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Types/RctType.h"

#include <Xi/Exceptional.h>

void Xi::Rct::serialize(Xi::Rct::RctType &type, CryptoNote::ISerializer &serializer) {
  uint8_t typeEncoded = static_cast<uint8_t>(type);
  serializer(typeEncoded, "encoded");
  exceptional_if<CryptoNote::EnumSerializationError>(typeEncoded >= static_cast<uint8_t>(RctType::__Num),
                                                     "invalid uint8_t encoded rct type");
  if (serializer.type() == CryptoNote::ISerializer::INPUT) {
    type = static_cast<RctType>(typeEncoded);
  }
}
