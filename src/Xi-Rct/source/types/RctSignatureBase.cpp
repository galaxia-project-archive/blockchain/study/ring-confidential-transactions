/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Types/RctSignatureBase.h"

#include <Xi/Exceptional.h>
#include <Serialization/SerializationOverloads.h>

#include "Xi/Rct/Types/RctType.h"

void Xi::Rct::serialize(Xi::Rct::RctSignatureBase &sig, CryptoNote::ISerializer &serializer) {
  serializer(sig.Type, "type");
  if (sig.Type == RctType::Null) {
    return;
  }

  serializer(sig.TransactionFee, "fee");

  if (sig.Type == RctType::Simple) {
    serializer(sig.PseudoOuts, "pseudo_outs");
  }

  serializer(sig.EcdhInfo, "ecdh_info");

  if (serializer.type() == CryptoNote::ISerializer::INPUT) {
    KeyVector outPk;
    serializer(outPk, "out_public_keys");
    sig.OutPublicKeys.resize(outPk.size());
    for (size_t i = 0; i < outPk.size(); ++i) {
      sig.OutPublicKeys[i].Dest = Key::Identity;
      sig.OutPublicKeys[i].Mask = outPk[i];
    }
  } else {
    assert(serializer.type() == CryptoNote::ISerializer::OUTPUT);
    KeyVector outPk{sig.OutPublicKeys.size()};
    for (size_t i = 0; i < outPk.size(); ++i) {
      outPk[i] = sig.OutPublicKeys[i].Mask;
    }
    serializer(outPk, "out_public_keys");
  }

  serializer(sig.TransactionFee.value, "transaction_fee");
}

bool Xi::Rct::RctSignatureBase::isNull() const { return Type == RctType::Null; }

void Xi::Rct::RctSignatureBase::nullify() {
  Type = RctType::Null;
  Message = Key::Zero;
  MixinRing.clear();
  PseudoOuts.clear();
  EcdhInfo.clear();
  OutPublicKeys.clear();
  TransactionFee = Amount{0};
}
