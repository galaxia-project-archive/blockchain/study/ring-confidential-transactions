/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Types/MultiexpData.h"

#include "Xi/Rct/Algorithm/KeyArithmetic.h"

Xi::Rct::MultiexpData::MultiexpData(const Xi::Rct::Key &scalar, const ge_p3 &point) : Point{point} {
  scalar.copyTo(Scalar);
}

Xi::Rct::MultiexpData::MultiexpData(const Xi::Rct::Key &scalar, const Xi::Rct::Key &point) {
  scalar.copyTo(Scalar);
  if (ge_frombytes_vartime(&Point, point.data()) != 0) {
    Xi::exceptional<GeFromBytesVarTimeError>();
  }
}
