/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Mlsag/Prove.h"

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Mlsag/Exceptions.h"
#include "Xi/Rct/Mlsag/Generate.h"

Xi::Rct::MlsagSignature Xi::Rct::Mlsag::prove_rct(const Xi::Rct::Key &message, const Xi::Rct::CtKeyMatrix &pubs,
                                                  const Xi::Rct::CtKeyVector &inSk, const Xi::Rct::CtKeyVector &outSk,
                                                  const Xi::Rct::CtKeyVector &outPk,
                                                  const Xi::Rct::MultiSignatureData *kLRki, Xi::Rct::Key *mscout,
                                                  unsigned int index, const Xi::Rct::Key &txnFeeKey,
                                                  Xi::Rct::IMlsagDevice &hwdev) {
  // setup vars
  size_t cols = pubs.size();
  exceptional_if_not<GenericMlsagError>(cols >= 1, "Empty pubs");
  size_t rows = pubs[0].size();
  exceptional_if_not<GenericMlsagError>(rows >= 1, "Empty pubs");
  for (size_t i = 1; i < cols; ++i) {
    exceptional_if_not<GenericMlsagError>(pubs[i].size() == rows, "pubs is not rectangular");
  }
  exceptional_if_not<GenericMlsagError>(inSk.size() == rows, "Bad inSk size");
  exceptional_if_not<GenericMlsagError>(outSk.size() == outPk.size(), "Bad outSk/outPk size");
  exceptional_if_not<GenericMlsagError>((kLRki && mscout) || (!kLRki && !mscout),
                                        "Only one of kLRki/mscout is present");

  KeyVector sk(rows + 1);
  KeyVector tmp(rows + 1);
  for (size_t i = 0; i < rows + 1; i++) {
    sc_0(sk[i].data());
    Key::Identity.copyTo(tmp[i]);
  }
  KeyMatrix M(cols, tmp);
  // create the matrix to mg sig
  for (size_t i = 0; i < cols; i++) {
    Key::Identity.copyTo(M[i][rows]);
    for (size_t j = 0; j < rows; j++) {
      M[i][j] = pubs[i][j].Dest;
      key_add_inplace(M[i][rows], pubs[i][j].Mask, M[i][rows]);  // add input commitments in last row
    }
  }
  sc_0(sk[rows].data());
  for (size_t j = 0; j < rows; j++) {
    inSk[j].Dest.copyTo(sk[j]);
    sc_add(sk[rows].data(), sk[rows].data(), inSk[j].Mask.data());  // add masks in last row
  }
  for (size_t i = 0; i < cols; i++) {
    for (size_t j = 0; j < outPk.size(); j++) {
      key_subtract_inplace(M[i][rows], outPk[j].Mask, M[i][rows]);  // subtract output Ci's in last row
    }
    // subtract txn fee output in last row
    key_subtract_inplace(M[i][rows], txnFeeKey, M[i][rows]);
  }
  for (size_t j = 0; j < outPk.size(); j++) {
    sc_sub(sk[rows].data(), sk[rows].data(), outSk[j].Mask.data());  // subtract output masks in last row..
  }
  auto result = generate_signature(message, M, sk, kLRki, mscout, index, rows, hwdev);
  // TODO(memwipe)
  // memwipe(sk.data(), sk.size() * sizeof(Key));
  return result;
}

Xi::Rct::MlsagSignature Xi::Rct::Mlsag::prove_rct_simple(const Xi::Rct::Key &message, const Xi::Rct::CtKeyVector &pubs,
                                                         const Xi::Rct::CtKey &inSk, const Xi::Rct::Key &a,
                                                         const Xi::Rct::Key &Cout,
                                                         const Xi::Rct::MultiSignatureData *kLRki, Xi::Rct::Key *mscout,
                                                         unsigned int index, Xi::Rct::IMlsagDevice &hwdev) {
  // setup vars
  size_t rows = 1;
  size_t cols = pubs.size();
  exceptional_if_not<GenericMlsagError>(cols >= 1, "Empty pubs");
  exceptional_if_not<GenericMlsagError>((kLRki && mscout) || (!kLRki && !mscout),
                                        "Only one of kLRki/mscout is present");
  KeyVector tmp(rows + 1);
  KeyVector sk(rows + 1);
  size_t i;
  KeyMatrix M(cols, tmp);

  inSk.Dest.copyTo(sk[0]);
  sc_sub(sk[1].data(), inSk.Mask.data(), a.data());
  for (i = 0; i < cols; i++) {
    M[i][0] = pubs[i].Dest;
    key_subtract_inplace(pubs[i].Mask, Cout, M[i][1]);
  }
  auto result = generate_signature(message, M, sk, kLRki, mscout, index, rows, hwdev);
  // TODO(memwipe)
  // memwipe(&sk[0], sizeof(key));
  return result;
}
