/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Mlsag/PreHash.h"

#include <sstream>

#include <Xi/Exceptional.h>
#include <crypto/CryptoTypes.h>
#include <Serialization/SerializationTools.h>

extern "C" {
#include <crypto/hash-ops.h>
}

#include "Xi/Rct/Types/RctSignature.h"
#include "Xi/Rct/Algorithm/RctCategoryDetection.h"
#include "Xi/Rct/Algorithm/KeyHashing.h"
#include "Xi/Rct/Mlsag/Exceptions.h"
#include "Xi/Rct/DeviceInterfaces/IMlsagDevice.h"

Xi::Rct::Key Xi::Rct::Mlsag::compute_pre_hash(const Xi::Rct::RctSignature &rv, Xi::Rct::IMlsagDevice &hwdev) {
  KeyVector hashes;
  hashes.reserve(3);
  hashes.push_back(rv.Message);

  exceptional_if<GenericMlsagError>(rv.MixinRing.empty(), "Empty mixRing");
  const size_t inputs = is_simple_type(rv.Type) ? rv.MixinRing.size() : rv.MixinRing[0].size();
  const size_t outputs = rv.EcdhInfo.size();

  exceptional_if<GenericMlsagError>(rv.Type == RctType::Simple && rv.PseudoOuts.size() != inputs,
                                    "unexpected pseudo outs size");
  exceptional_if<GenericMlsagError>(rv.Type != RctType::Null && rv.EcdhInfo.size() != outputs,
                                    "unexpected ecdh info size");
  ByteVector sigBlob = CryptoNote::toBinaryArray<RctSignatureBase>(rv);
  Crypto::Hash h;
  cn_fast_hash(sigBlob.data(), sigBlob.size(), reinterpret_cast<char *>(h.data()));
  hashes.push_back(key_cast(h));

  KeyVector kv;
  if (is_bulletproof_type(rv.Type)) {
    kv.reserve((6 * 2 + 9) * rv.Prunable.Bulletproofs.size());
    for (const auto &p : rv.Prunable.Bulletproofs) {
      // V are not hashed as they're expanded from outPk.mask
      // (and thus hashed as part of rctSigBase above)
      kv.push_back(p.A);
      kv.push_back(p.S);
      kv.push_back(p.T1);
      kv.push_back(p.T2);
      kv.push_back(p.taux);
      kv.push_back(p.mu);
      for (size_t n = 0; n < p.L.size(); ++n) kv.push_back(p.L[n]);
      for (size_t n = 0; n < p.R.size(); ++n) kv.push_back(p.R[n]);
      kv.push_back(p.a);
      kv.push_back(p.b);
      kv.push_back(p.t);
    }
  } else {
    kv.reserve((64 * 3 + 1) * rv.Prunable.RangeSignatures.size());
    for (const auto &r : rv.Prunable.RangeSignatures) {
      for (size_t n = 0; n < 64; ++n) kv.push_back(r.asig.S0[n]);
      for (size_t n = 0; n < 64; ++n) kv.push_back(r.asig.S1[n]);
      kv.push_back(r.asig.ee);
      for (size_t n = 0; n < 64; ++n) kv.push_back(r.Ci[n]);
    }
  }
  hashes.push_back(key_fast_hash(kv));
  Key prehash;
  exceptional_if_not<MlsagDeviceError>(hwdev.mlsagPrehash(sigBlob, inputs, outputs, hashes, rv.OutPublicKeys, prehash));
  return prehash;
}
