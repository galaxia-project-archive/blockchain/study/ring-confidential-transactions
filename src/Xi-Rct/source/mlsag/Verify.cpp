/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Mlsag/Verify.h"

#include <array>

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/KeyHashing.h"
#include "Xi/Rct/Mlsag/Exceptions.h"

bool Xi::Rct::Mlsag::verify(const Xi::Rct::Key &message, const Xi::Rct::KeyMatrix &pk,
                            const Xi::Rct::MlsagSignature &rv, size_t dsRows) {
  try {
    size_t cols = pk.size();
    exceptional_if_not<GenericMlsagError>(cols >= 2, "Error! What is c if cols = 1!");
    size_t rows = pk[0].size();
    exceptional_if_not<GenericMlsagError>(rows >= 1, "Empty pk");
    for (size_t i = 1; i < cols; ++i) {
      exceptional_if_not<GenericMlsagError>(pk[i].size() == rows, "pk is not rectangular");
    }
    exceptional_if_not<GenericMlsagError>(rv.II.size() == dsRows, "Bad II size");
    exceptional_if_not<GenericMlsagError>(rv.ss.size() == cols, "Bad rv.ss size");
    for (size_t i = 0; i < cols; ++i) {
      exceptional_if_not<GenericMlsagError>(rv.ss[i].size() == rows, "rv.ss is not rectangular");
    }
    exceptional_if_not<GenericMlsagError>(dsRows <= rows, "Bad dsRows value");

    for (size_t i = 0; i < rv.ss.size(); ++i)
      for (size_t j = 0; j < rv.ss[i].size(); ++j)
        exceptional_if_not<GenericMlsagError>(sc_check(rv.ss[i][j].data()) == 0, "Bad ss slot");
    exceptional_if_not<GenericMlsagError>(sc_check(rv.cc.data()) == 0, "Bad cc");

    size_t i = 0, j = 0, ii = 0;
    Key c, L, R, Hi;
    Key c_old = rv.cc.copy();
    std::vector<std::array<ge_cached, 8>> Ip(dsRows);
    for (i = 0; i < dsRows; i++) {
      key_precompute_dsmp(rv.II[i], Ip[i].data());
    }
    size_t ndsRows = 3 * dsRows;  // non Double Spendable Rows (see identity chains paper
    KeyVector toHash(1 + 3 * dsRows + 2 * (rows - dsRows));
    toHash[0] = message;
    i = 0;
    while (i < cols) {
      sc_0(c.data());
      for (j = 0; j < dsRows; j++) {
        key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(rv.ss[i][j], c_old, pk[i][j], L);
        key_fast_hash_to_point_inplace(pk[i][j], Hi);
        exceptional_if<GenericMlsagError>(Hi == Key::Identity, "Data hashed to point at infinity");
        key_add_multiplied_points_inplace(rv.ss[i][j], Hi, c_old, Ip[j].data(), R);
        toHash[3 * j + 1] = pk[i][j];
        toHash[3 * j + 2] = L;
        toHash[3 * j + 3] = R;
      }
      for (j = dsRows, ii = 0; j < rows; j++, ii++) {
        key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(rv.ss[i][j], c_old, pk[i][j], L);
        toHash[ndsRows + 2 * ii + 1] = pk[i][j];
        toHash[ndsRows + 2 * ii + 2] = L;
      }
      c = key_fast_hash_to_scalar(toHash);
      c.copyTo(c_old);
      i = (i + 1);
    }
    sc_sub(c.data(), c_old.data(), rv.cc.data());
    return sc_isnonzero(c.data()) == 0;
  } catch (...) {
    // TODO: Logging
    return false;
  }
}

bool Xi::Rct::Mlsag::verify_rct(const MlsagSignature &mg, const CtKeyMatrix &pubs, const CtKeyVector &outPk,
                                const Key &txnFeeKey, const Key &message) {
  try {
    // setup vars
    size_t cols = pubs.size();
    exceptional_if_not<GenericMlsagError>(cols >= 1, "Empty pubs");
    size_t rows = pubs[0].size();
    exceptional_if_not<GenericMlsagError>(rows >= 1, "Empty pubs");
    for (size_t i = 1; i < cols; ++i) {
      exceptional_if_not<GenericMlsagError>(pubs[i].size() == rows, "pubs is not rectangular");
    }

    KeyVector tmp(rows + 1);
    size_t i = 0, j = 0;
    for (i = 0; i < rows + 1; i++) {
      Key::Identity.copyTo(tmp[i]);
    }
    KeyMatrix M(cols, tmp);

    // create the matrix to mg sig
    for (j = 0; j < rows; j++) {
      for (i = 0; i < cols; i++) {
        M[i][j] = pubs[i][j].Dest;
        key_add_inplace(M[i][rows], pubs[i][j].Mask, M[i][rows]);  // add Ci in last row
      }
    }
    for (i = 0; i < cols; i++) {
      for (j = 0; j < outPk.size(); j++) {
        key_subtract_inplace(M[i][rows], outPk[j].Mask, M[i][rows]);  // subtract output Ci's in last row
      }
      // subtract txn fee output in last row
      key_subtract_inplace(M[i][rows], txnFeeKey, M[i][rows]);
    }
    return verify(message, M, mg, rows);
  } catch (...) {
    // TODO: Logging
    return false;
  }
}

bool Xi::Rct::Mlsag::verify_rct_simple(const Xi::Rct::Key &message, const Xi::Rct::MlsagSignature &mg,
                                       const Xi::Rct::CtKeyVector &pubs, const Xi::Rct::Key &C) {
  try {
    // setup vars
    size_t rows = 1;
    size_t cols = pubs.size();
    exceptional_if_not<GenericMlsagError>(cols >= 1, "Empty pubs");
    KeyVector tmp(rows + 1);
    size_t i;
    KeyMatrix M(cols, tmp);
    ge_p3 Cp3;
    exceptional_if_not<GeFromBytesVarTimeError>(ge_frombytes_vartime(&Cp3, C.data()) == 0, "point conv failed");
    ge_cached Ccached;
    ge_p3_to_cached(&Ccached, &Cp3);
    ge_p1p1 p1;
    // create the matrix to mg sig
    for (i = 0; i < cols; i++) {
      M[i][0] = pubs[i].Dest;
      ge_p3 p3;
      exceptional_if_not<GeFromBytesVarTimeError>(ge_frombytes_vartime(&p3, pubs[i].Mask.data()) == 0,
                                                  "point conv failed");
      ge_sub(&p1, &p3, &Ccached);
      ge_p1p1_to_p3(&p3, &p1);
      ge_p3_tobytes(M[i][1].data(), &p3);
    }
    return verify(message, M, mg, rows);
  } catch (...) {
    // TODO: Logging
    return false;
  }
}
