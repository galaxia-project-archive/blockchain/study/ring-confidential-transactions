/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Mlsag/Generate.h"

#include <array>

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/KeyHashing.h"
#include "Xi/Rct/Algorithm/GenerateKey.h"
#include "Xi/Rct/DeviceInterfaces/IMlsagDevice.h"
#include "Xi/Rct/Mlsag/Exceptions.h"

Xi::Rct::MlsagSignature Xi::Rct::Mlsag::generate_signature(const Xi::Rct::Key &message, const Xi::Rct::KeyMatrix &pk,
                                                           const Xi::Rct::KeyVector &xx,
                                                           const Xi::Rct::MultiSignatureData *kLRki,
                                                           Xi::Rct::Key *mscout, const unsigned int index,
                                                           size_t dsRows, Xi::Rct::IMlsagDevice &hwdev) {
  MlsagSignature rv;
  size_t cols = pk.size();
  exceptional_if_not<GenericMlsagError>(cols >= 2, "Error! What is c if cols = 1!");
  exceptional_if_not<GenericMlsagError>(index < cols, "Index out of range");
  size_t rows = pk[0].size();
  exceptional_if_not<GenericMlsagError>(rows >= 1, "Empty pk");
  for (size_t i = 1; i < cols; ++i) {
    exceptional_if_not<GenericMlsagError>(pk[i].size() == rows, "pk is not rectangular");
  }
  exceptional_if_not<GenericMlsagError>(xx.size() == rows, "Bad xx size");
  exceptional_if_not<GenericMlsagError>(dsRows <= rows, "Bad dsRows size");
  exceptional_if_not<GenericMlsagError>((kLRki && mscout) || (!kLRki && !mscout),
                                        "Only one of kLRki/mscout is present");
  exceptional_if_not<GenericMlsagError>(!kLRki || dsRows == 1, "Multisig requires exactly 1 dsRows");

  size_t i = 0, j = 0, ii = 0;
  Key c, c_old, L, R, Hi;
  sc_0(c_old.data());
  std::vector<std::array<ge_cached, 8>> Ip(dsRows);
  rv.II = KeyVector(dsRows);
  KeyVector alpha(rows);
  KeyVector aG(rows);
  rv.ss = KeyMatrix(cols, aG);
  KeyVector aHP(dsRows);
  KeyVector toHash(1 + 3 * dsRows + 2 * (rows - dsRows));
  toHash[0] = message;

  for (i = 0; i < dsRows; i++) {
    toHash[3 * i + 1] = pk[index][i];
    if (kLRki != nullptr) {
      // multisig
      alpha[i] = kLRki->k;
      toHash[3 * i + 2] = kLRki->L;
      toHash[3 * i + 3] = kLRki->R;
      rv.II[i] = kLRki->ki;
    } else {
      Hi = key_fast_hash_to_point(pk[index][i]);
      exceptional_if_not<MlsagDeviceError>(hwdev.mlsagPrepare(Hi, xx[i], alpha[i], aG[i], aHP[i], rv.II[i]));
      toHash[3 * i + 2] = aG[i];
      toHash[3 * i + 3] = aHP[i];
    }
    key_precompute_dsmp(rv.II[i], Ip[i].data());
  }
  size_t ndsRows = 3 * dsRows;  // non Double Spendable Rows (see identity chains paper)
  for (i = dsRows, ii = 0; i < rows; i++, ii++) {
    generate_random_key_pair_inplace(aG[i], alpha[i]);  // need to save alphas for later..
    toHash[ndsRows + 2 * ii + 1] = pk[index][i];
    toHash[ndsRows + 2 * ii + 2] = aG[i];
  }

  exceptional_if_not<MlsagDeviceError>(hwdev.mlsagHash(toHash, c_old));

  i = (index + 1) % cols;
  if (i == 0) {
    c_old.copyTo(rv.cc);
  }
  while (i != index) {
    rv.ss[i] = generate_random_secret_keys(rows);
    sc_0(c.data());
    for (j = 0; j < dsRows; j++) {
      key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(rv.ss[i][j], c_old, pk[i][j], L);
      key_fast_hash_to_point_inplace(pk[i][j], Hi);
      key_add_multiplied_points_inplace(rv.ss[i][j], Hi, c_old, Ip[j].data(), R);
      toHash[3 * j + 1] = pk[i][j];
      toHash[3 * j + 2] = L;
      toHash[3 * j + 3] = R;
    }
    for (j = dsRows, ii = 0; j < rows; j++, ii++) {
      key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(rv.ss[i][j], c_old, pk[i][j], L);
      toHash[ndsRows + 2 * ii + 1] = pk[i][j];
      toHash[ndsRows + 2 * ii + 2] = L;
    }
    exceptional_if_not<MlsagDeviceError>(hwdev.mlsagHash(toHash, c));
    c.copyTo(c_old);
    i = (i + 1) % cols;

    if (i == 0) {
      c_old.copyTo(rv.cc);
    }
  }
  exceptional_if_not<MlsagDeviceError>(hwdev.mlsagSign(c, xx, alpha, rows, dsRows, rv.ss[index]));
  if (mscout) *mscout = c;
  return rv;
}
