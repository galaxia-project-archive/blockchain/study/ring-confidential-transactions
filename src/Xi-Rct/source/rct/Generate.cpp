/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Rct/Generate.h"

#include <cassert>

#include <Xi/Global.h>
#include <Xi/Exceptional.h>
#include <Xi/Span.h>

#include "Xi/Rct/Types/Types.h"
#include "Xi/Rct/Algorithm/Algorithm.h"
#include "Xi/Rct/Bulletproof/Bulletproof.h"
#include "Xi/Rct/Range/Range.h"
#include "Xi/Rct/Mlsag/Mlsag.h"
#include "Xi/Rct/Rct/Exceptions.h"

Xi::Rct::RctSignature Xi::Rct::Rct::generate(const Key &message, const CtKeyVector &inSk, const KeyVector &destinations,
                                             const AmountVector &amounts, const CtKeyMatrix &mixRing,
                                             const KeyVector &amount_keys, const MultiSignatureData *kLRki,
                                             MultiSignatureOut *msout, unsigned int index, CtKeyVector &outSk,
                                             const RctConfig &rct_config, IRctDevice &hwdev) {
  XI_UNUSED(rct_config);  // TODO: Evaluate need
  exceptional_if_not<GenericRctError>(
      amounts.size() == destinations.size() || amounts.size() == destinations.size() + 1,
      "Different number of amounts/destinations");
  exceptional_if_not<GenericRctError>(amount_keys.size() == destinations.size(),
                                      "Different number of amount_keys/destinations");
  exceptional_if_not<GenericRctError>(index < mixRing.size(), "Bad index into mixRing");
  for (size_t n = 0; n < mixRing.size(); ++n) {
    exceptional_if_not<GenericRctError>(mixRing[n].size() == inSk.size(), "Bad mixRing size");
  }
  exceptional_if_not<GenericRctError>((kLRki && msout) || (!kLRki && !msout), "Only one of kLRki/msout is present");

  RctSignature rv;
  rv.Type = RctType::Full;
  rv.Message = message;
  rv.OutPublicKeys.resize(destinations.size());
  rv.Prunable.RangeSignatures.resize(destinations.size());
  rv.EcdhInfo.resize(destinations.size());

  size_t i = 0;
  KeyVector masks(destinations.size());  // sk mask..
  outSk.resize(destinations.size());
  for (i = 0; i < destinations.size(); i++) {
    // add destination to sig
    destinations[i].copyTo(rv.OutPublicKeys[i].Dest);
    // compute range proof
    rv.Prunable.RangeSignatures[i] = Range::prove(rv.OutPublicKeys[i].Mask, outSk[i].Mask, amounts[i]);
    assert(Range::verify(rv.OutPublicKeys[i].Mask, rv.Prunable.RangeSignatures[i]));
    // mask amount and mask
    outSk[i].Mask.copyTo(rv.EcdhInfo[i].Mask);
    rv.EcdhInfo[i].Amount = amount_to_key(amounts[i]);
    exceptional_if_not<RctDeviceError>(
        hwdev.ecdhEncode(rv.EcdhInfo[i], amount_keys[i], rv.Type == RctType::Bulletproof2));
  }

  // set txn fee
  if (amounts.size() > destinations.size()) {
    rv.TransactionFee = amounts[destinations.size()];
  } else {
    rv.TransactionFee = Amount{0};
  }
  Key txnFeeKey = key_multiply_scalar_with_h_constant(amount_to_key(rv.TransactionFee));

  rv.MixinRing = mixRing;
  if (msout) msout->c.resize(1);
  rv.Prunable.MLSAGs.push_back(Mlsag::prove_rct(Mlsag::compute_pre_hash(rv, hwdev), rv.MixinRing, inSk, outSk,
                                                rv.OutPublicKeys, kLRki, msout ? &msout->c[0] : nullptr, index,
                                                txnFeeKey, hwdev));
  return rv;
}

Xi::Rct::RctSignature Xi::Rct::Rct::generate_simple(const Key &message, const CtKeyVector &inSk,
                                                    const KeyVector &destinations, const AmountVector &inamounts,
                                                    const AmountVector &outamounts, Amount txnFee,
                                                    const CtKeyMatrix &mixRing, const KeyVector &amount_keys,
                                                    const MultiSignatureDataVector *kLRki, MultiSignatureOut *msout,
                                                    const std::vector<unsigned int> &index, CtKeyVector &outSk,
                                                    const RctConfig &rct_config, IRctDevice &hwdev) {
  const bool bulletproof = rct_config.Type != RangeProofType::Borromean;
  exceptional_if<GenericRctError>(rct_config.Type >= RangeProofType::__Num, "invalid range proof type");
  exceptional_if<GenericRctError>(rct_config.BulletproofVersion > 2, "invalid bulletproof version");
  exceptional_if_not<GenericRctError>(inamounts.size() > 0, "Empty inamounts");
  exceptional_if_not<GenericRctError>(inamounts.size() == inSk.size(), "Different number of inamounts/inSk");
  exceptional_if_not<GenericRctError>(outamounts.size() == destinations.size(),
                                      "Different number of amounts/destinations");
  exceptional_if_not<GenericRctError>(amount_keys.size() == destinations.size(),
                                      "Different number of amount_keys/destinations");
  exceptional_if_not<GenericRctError>(index.size() == inSk.size(), "Different number of index/inSk");
  exceptional_if_not<GenericRctError>(mixRing.size() == inSk.size(), "Different number of mixRing/inSk");
  for (size_t n = 0; n < mixRing.size(); ++n) {
    exceptional_if_not<GenericRctError>(index[n] < mixRing[n].size(), "Bad index into mixRing");
  }
  exceptional_if_not<GenericRctError>((kLRki && msout) || (!kLRki && !msout), "Only one of kLRki/msout is present");
  if (kLRki && msout) {
    exceptional_if_not<GenericRctError>(kLRki->size() == inamounts.size(), "Mismatched kLRki/inamounts sizes");
  }

  RctSignature rv;
  rv.Type = bulletproof
                ? (rct_config.BulletproofVersion == 0 || rct_config.BulletproofVersion >= 2 ? RctType::Bulletproof2
                                                                                            : RctType::Bulletproof)
                : RctType::Simple;
  rv.Message = message;
  rv.OutPublicKeys.resize(destinations.size());
  if (!bulletproof) rv.Prunable.RangeSignatures.resize(destinations.size());
  rv.EcdhInfo.resize(destinations.size());

  size_t i;
  outSk.resize(destinations.size());
  for (i = 0; i < destinations.size(); i++) {
    // add destination to sig
    destinations[i].copyTo(rv.OutPublicKeys[i].Dest);
    // compute range proof
    if (!bulletproof)
      rv.Prunable.RangeSignatures[i] = Range::prove(rv.OutPublicKeys[i].Mask, outSk[i].Mask, outamounts[i]);
    if (!bulletproof) {
      assert(Range::verify(rv.OutPublicKeys[i].Mask, rv.Prunable.RangeSignatures[i]));
    }
  }

  rv.Prunable.Bulletproofs.clear();
  if (bulletproof) {
    size_t n_amounts = outamounts.size();
    size_t amounts_proved = 0;
    if (rct_config.Type == RangeProofType::PaddedBulletproof) {
      KeyVector C, masks;
      if (hwdev.isInFakeMode()) {
        // use a fake bulletproof for speed
        rv.Prunable.Bulletproofs.push_back(bulletproof_make_dummy(outamounts, C, masks));
      } else {
        const Span<const Key> keys{amount_keys.data(), amount_keys.size()};
        rv.Prunable.Bulletproofs.push_back(bulletproof_make_prove(C, masks, outamounts, keys));
        assert(bulletproof_verify(rv.Prunable.Bulletproofs.back()));
      }
      for (i = 0; i < outamounts.size(); ++i) {
        key_multiply_scalar_with_eight_inplace(C[i], rv.OutPublicKeys[i].Mask);
        outSk[i].Mask = masks[i];
      }
    } else
      while (amounts_proved < n_amounts) {
        size_t batch_size = 1;
        if (rct_config.Type == RangeProofType::MultiOutputBulletproof)
          while (batch_size * 2 + amounts_proved <= n_amounts && batch_size * 2 <= Bulletproof::maxOutputs())
            batch_size *= 2;
        KeyVector C, masks;
        AmountVector batch_amounts(batch_size);
        for (i = 0; i < batch_size; ++i) batch_amounts[i] = outamounts[i + amounts_proved];
        if (hwdev.isInFakeMode()) {
          // use a fake bulletproof for speed
          rv.Prunable.Bulletproofs.push_back(bulletproof_make_dummy(batch_amounts, C, masks));
        } else {
          const Span<const Key> keys{&amount_keys[amounts_proved], batch_size};
          rv.Prunable.Bulletproofs.push_back(bulletproof_make_prove(C, masks, batch_amounts, keys));
          assert(bulletproof_verify(rv.Prunable.Bulletproofs.back()));
        }
        for (i = 0; i < batch_size; ++i) {
          key_multiply_scalar_with_eight_inplace(C[i], rv.OutPublicKeys[i + amounts_proved].Mask);
          outSk[i + amounts_proved].Mask = masks[i];
        }
        amounts_proved += batch_size;
      }
  }

  Key sumout = Key::Zero;
  for (i = 0; i < outSk.size(); ++i) {
    sc_add(sumout.data(), outSk[i].Mask.data(), sumout.data());

    // mask amount and mask
    outSk[i].Mask.copyTo(rv.EcdhInfo[i].Mask);
    rv.EcdhInfo[i].Amount = amount_to_key(outamounts[i]);
    exceptional_if_not<RctDeviceError>(
        hwdev.ecdhEncode(rv.EcdhInfo[i], amount_keys[i], rv.Type == RctType::Bulletproof2));
  }

  // set txn fee
  rv.TransactionFee = txnFee;
  //        TODO: unused ??
  //        key txnFeeKey = scalarmultH(d2h(rv.TransactionFee));
  rv.MixinRing = mixRing;
  auto &pseudoOuts = bulletproof ? rv.Prunable.PseudoOutputs : rv.PseudoOuts;
  pseudoOuts.resize(inamounts.size());
  rv.Prunable.MLSAGs.resize(inamounts.size());
  Key sumpouts = Key::Zero;  // sum pseudoOut masks
  KeyVector a(inamounts.size());
  for (i = 0; i < inamounts.size() - 1; i++) {
    generate_random_secret_key_inplace(a[i]);
    sc_add(sumpouts.data(), a[i].data(), sumpouts.data());
    amount_to_key_with_key_inplace(inamounts[i], a[i], pseudoOuts[i]);
  }
  sc_sub(a[i].data(), sumout.data(), sumpouts.data());
  amount_to_key_with_key_inplace(inamounts[i], a[i], pseudoOuts[i]);

  Key full_message = Mlsag::compute_pre_hash(rv, hwdev);
  if (msout) msout->c.resize(inamounts.size());
  for (i = 0; i < inamounts.size(); i++) {
    rv.Prunable.MLSAGs[i] =
        Mlsag::prove_rct_simple(full_message, rv.MixinRing[i], inSk[i], a[i], pseudoOuts[i],
                                kLRki ? &(*kLRki)[i] : NULL, msout ? &msout->c[i] : NULL, index[i], hwdev);
  }
  return rv;
}
