/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Rct/Verify.h"

#include <stdexcept>
#include <vector>

#include <crypto/crypto-ops.h>

#include "Xi/Rct/Types/Types.h"
#include "Xi/Rct/Algorithm/Algorithm.h"
#include "Xi/Rct/Bulletproof/Verify.h"
#include "Xi/Rct/Mlsag/Verify.h"
#include "Xi/Rct/Mlsag/PreHash.h"
#include "Xi/Rct/Range/Verify.h"
#include "Xi/Rct/DeviceInterfaces/IRctDevice.h"
#include "Xi/Rct/Rct/Exceptions.h"

bool Xi::Rct::Rct::verify(const RctSignature& rv, bool verifySemantics, IRctDevice& hwdev) {
  try {
    exceptional_if_not<GenericRctError>(rv.Type == RctType::Full, "verRct called on non-full rctSig");
    if (verifySemantics) {
      exceptional_if_not<GenericRctError>(rv.OutPublicKeys.size() == rv.Prunable.RangeSignatures.size(),
                                          "Mismatched sizes of outPk and rv.Prunable.RangeSignatures");
      exceptional_if_not<GenericRctError>(rv.OutPublicKeys.size() == rv.EcdhInfo.size(),
                                          "Mismatched sizes of outPk and rv.EcdhInfo");
      exceptional_if_not<GenericRctError>(rv.Prunable.MLSAGs.size() == 1, "full RctSignature has not one MG");

      // TODO: consider threading
      for (size_t i = 0; i < rv.OutPublicKeys.size(); i++) {
        if (!Range::verify(rv.OutPublicKeys[i].Mask, rv.Prunable.RangeSignatures[i])) {
          // TODO: Logging
          return false;
        }
      }
    } else {
      // compute txn fee
      Key txnFeeKey = key_multiply_scalar_with_h_constant(amount_to_key(rv.TransactionFee));
      bool mgVerd = Mlsag::verify_rct(rv.Prunable.MLSAGs[0], rv.MixinRing, rv.OutPublicKeys, txnFeeKey,
                                      Mlsag::compute_pre_hash(rv, hwdev));
      exceptional_if_not<GenericRctError>(mgVerd, "MG signature verification failed");
    }
    return true;
  } catch (const std::exception& e) {
    XI_UNUSED(e);
    // TODO: Logging
    return false;
  } catch (...) {
    // TODO: Logging
    return false;
  }
}

bool Xi::Rct::Rct::verify(const RctSignature& rv, IRctDevice& hwdev) {
  return verify(rv, true, hwdev) && verify(rv, false, hwdev);
}

bool Xi::Rct::Rct::verify_semantics_simple(const std::vector<const RctSignature*>& rvv) {
  try {
    // TODO: consider threading
    std::vector<const Bulletproof*> proofs;
    size_t max_non_bp_proofs = 0;

    for (const RctSignature* rvp : rvv) {
      exceptional_if_not<GenericRctError>(rvp, "RctSignature pointer is NULL");
      const RctSignature& rv = *rvp;
      exceptional_if_not<GenericRctError>(
          rv.Type == RctType::Simple || rv.Type == RctType::Bulletproof || rv.Type == RctType::Bulletproof2,
          "verRctSemanticsSimple called on non simple rctSig");
      const bool bulletproof = is_bulletproof_type(rv.Type);
      if (bulletproof) {
        exceptional_if_not<GenericRctError>(
            rv.OutPublicKeys.size() == count_bulletproof_amounts(rv.Prunable.Bulletproofs),
            "Mismatched sizes of outPk and bulletproofs");
        exceptional_if_not<GenericRctError>(rv.Prunable.PseudoOutputs.size() == rv.Prunable.MLSAGs.size(),
                                            "Mismatched sizes of rv.Prunable.pseudoOuts and rv.Prunable.MLAGs");
        exceptional_if_not<GenericRctError>(rv.PseudoOuts.empty(), "rv.pseudoOuts is not empty");
      } else {
        exceptional_if_not<GenericRctError>(rv.OutPublicKeys.size() == rv.Prunable.RangeSignatures.size(),
                                            "Mismatched sizes of outPk and rv.Prunable.RangeSignatures");
        exceptional_if_not<GenericRctError>(rv.PseudoOuts.size() == rv.Prunable.MLSAGs.size(),
                                            "Mismatched sizes of rv.pseudoOuts and rv.Prunable.MLAGs");
        exceptional_if_not<GenericRctError>(rv.Prunable.PseudoOutputs.empty(), "rv.Prunable.pseudoOuts is not empty");
      }
      exceptional_if_not<GenericRctError>(rv.OutPublicKeys.size() == rv.EcdhInfo.size(),
                                          "Mismatched sizes of outPk and rv.EcdhInfo");

      if (!bulletproof) max_non_bp_proofs += rv.Prunable.RangeSignatures.size();
    }

    for (const RctSignature* rvp : rvv) {
      const RctSignature& rv = *rvp;

      const bool bulletproof = is_bulletproof_type(rv.Type);
      const KeyVector& pseudoOuts = bulletproof ? rv.Prunable.PseudoOutputs : rv.PseudoOuts;

      KeyVector masks(rv.OutPublicKeys.size());
      for (size_t i = 0; i < rv.OutPublicKeys.size(); i++) {
        masks[i] = rv.OutPublicKeys[i].Mask;
      }
      Key sumOutpks = key_vector_sum(masks);
      const Key txnFeeKey = key_multiply_scalar_with_h_constant(amount_to_key(rv.TransactionFee));
      key_add_inplace(txnFeeKey, sumOutpks, sumOutpks);

      Key sumPseudoOuts = key_vector_sum(pseudoOuts);
      // check pseudoOuts vs Outs..
      if (sumPseudoOuts != sumOutpks) {
        // TODO Logging
        return false;
      }

      if (bulletproof) {
        for (size_t i = 0; i < rv.Prunable.Bulletproofs.size(); i++) proofs.push_back(&rv.Prunable.Bulletproofs[i]);
      } else {
        for (size_t i = 0; i < rv.Prunable.RangeSignatures.size(); i++) {
          if (!Range::verify(rv.OutPublicKeys[i].Mask, rv.Prunable.RangeSignatures[i])) {
            // TODO: Logging
            return false;
          }
        }
      }
    }
    if (!proofs.empty() && !bulletproof_verify(proofs)) {
      // TODO: Logging
      return false;
    }

    return true;
  }
  // we can get deep throws from ge_frombytes_vartime if input isn't valid
  catch (const std::exception& e) {
    XI_UNUSED(e);
    // TODO: Logging
    return false;
  } catch (...) {
    // TODO: Logging
    return false;
  }
}

bool Xi::Rct::Rct::verify_semantics_simple(const RctSignature& signature) {
  return verify_semantics_simple(std::vector<const RctSignature*>(1, &signature));
}

bool Xi::Rct::Rct::verify_none_semantics_simple(const RctSignature& rv, IRctDevice& hwdev) {
  try {
    exceptional_if_not<GenericRctError>(
        rv.Type == RctType::Simple || rv.Type == RctType::Bulletproof || rv.Type == RctType::Bulletproof2,
        "verRctNonSemanticsSimple called on non simple rctSig");
    const bool bulletproof = is_bulletproof_type(rv.Type);
    // semantics check is early, and mixRing/MGs aren't resolved yet
    if (bulletproof)
      exceptional_if_not<GenericRctError>(rv.Prunable.PseudoOutputs.size() == rv.MixinRing.size(),
                                          "Mismatched sizes of rv.Prunable.pseudoOuts and mixRing");
    else
      exceptional_if_not<GenericRctError>(rv.PseudoOuts.size() == rv.MixinRing.size(),
                                          "Mismatched sizes of rv.pseudoOuts and mixRing");

    const KeyVector& pseudoOuts = bulletproof ? rv.Prunable.PseudoOutputs : rv.PseudoOuts;
    const Key message = Mlsag::compute_pre_hash(rv, hwdev);

    for (size_t i = 0; i < rv.MixinRing.size(); ++i) {
      if (!Mlsag::verify_rct_simple(message, rv.Prunable.MLSAGs[i], rv.MixinRing[i], pseudoOuts[i])) {
        // TODO: Logging
        return false;
      }
    }

    return true;
  }
  // we can get deep throws from ge_frombytes_vartime if input isn't valid
  catch (const std::exception& e) {
    XI_UNUSED(e);
    // TODO: Logging
    return false;
  } catch (...) {
    // TODO: Logging
    return false;
  }
}

bool Xi::Rct::Rct::verify_simple(const RctSignature& signature, IRctDevice& hwdev) {
  return verify_semantics_simple(signature) && verify_none_semantics_simple(signature, hwdev);
}
