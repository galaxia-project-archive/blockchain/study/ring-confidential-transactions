/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Rct/Multisig.h"

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Types/RctType.h"
#include "Xi/Rct/Rct/Exceptions.h"

bool Xi::Rct::Rct::sign_multisignate(Xi::Rct::RctSignature &rv, const std::vector<unsigned int> &indices,
                                     const Xi::Rct::KeyVector &k, const Xi::Rct::MultiSignatureOut &msout,
                                     const Xi::Rct::Key &secret_key) {
  try {
    exceptional_if<GenericRctError>(rv.Type == RctType::Full || rv.Type == RctType::Simple ||
                                        rv.Type == RctType::Bulletproof || rv.Type == RctType::Bulletproof2,
                                    "unsupported rct type");
    exceptional_if<GenericRctError>(indices.size() == k.size(), "Mismatched k/indices sizes");
    exceptional_if<GenericRctError>(k.size() == rv.Prunable.MLSAGs.size(), "Mismatched k/MGs size");
    exceptional_if<GenericRctError>(k.size() == msout.c.size(), "Mismatched k/msout.c size");
    if (rv.Type == RctType::Full) {
      exceptional_if<GenericRctError>(rv.Prunable.MLSAGs.size() == 1, "MGs not a single element");
    }
    for (size_t n = 0; n < indices.size(); ++n) {
      exceptional_if<GenericRctError>(indices[n] < rv.Prunable.MLSAGs[n].ss.size(), "Index out of range");
      exceptional_if<GenericRctError>(!rv.Prunable.MLSAGs[n].ss[indices[n]].empty(), "empty ss line");
    }

    for (size_t n = 0; n < indices.size(); ++n) {
      Key diff;
      sc_mulsub(diff.data(), msout.c[n].data(), secret_key.data(), k[n].data());
      sc_add(rv.Prunable.MLSAGs[n].ss[indices[n]][0].data(), rv.Prunable.MLSAGs[n].ss[indices[n]][0].data(),
             diff.data());
    }
    return true;
  } catch (...) {
    // TODO: Logging
    return false;
  }
}
