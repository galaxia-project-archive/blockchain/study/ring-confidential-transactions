/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Rct/Rct/Decode.h"

#include <Xi/Exceptional.h>
#include <crypto/crypto-ops.h>

#include "Xi/Rct/Constants.h"
#include "Xi/Rct/Types/EcdhTuple.h"
#include "Xi/Rct/Types/RctType.h"
#include "Xi/Rct/Algorithm/KeyArithmetic.h"
#include "Xi/Rct/Algorithm/TypeConversion.h"
#include "Xi/Rct/DeviceInterfaces/IEcdhDevice.h"
#include "Xi/Rct/Rct/Exceptions.h"

Xi::Result<Xi::Rct::Amount> Xi::Rct::Rct::decode_amount(const Xi::Rct::RctSignature &rv, const Xi::Rct::Key &sk,
                                                        unsigned int i, Xi::Rct::Key &mask,
                                                        Xi::Rct::IEcdhDevice &hwdev) {
  XI_ERROR_TRY();
  exceptional_if_not<GenericRctError>(rv.Type == RctType::Full, "decodeRct called on non-full rctSig");
  exceptional_if_not<GenericRctError>(i < rv.EcdhInfo.size(), "Bad index");
  exceptional_if_not<GenericRctError>(rv.OutPublicKeys.size() == rv.EcdhInfo.size(),
                                      "Mismatched sizes of rv.outPk and rv.ecdhInfo");

  // mask amount and mask
  EcdhTuple ecdh_info = rv.EcdhInfo[i];
  exceptional_if_not<RctDeviceError>(hwdev.ecdhDecode(ecdh_info, sk, rv.Type == RctType::Bulletproof2));
  mask = ecdh_info.Mask;
  Key amount = ecdh_info.Amount;
  Key C = rv.OutPublicKeys[i].Mask;
  Key Ctmp;
  exceptional_if_not<GenericRctError>(sc_check(mask.data()) == 0, "warning, bad ECDH mask");
  exceptional_if_not<GenericRctError>(sc_check(amount.data()) == 0, "warning, bad ECDH amount");
  key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(mask, amount, Constants::H, Ctmp);
  exceptional_if_not<GenericRctError>(C == Ctmp, "warning, amount decoded incorrectly, will be unable to spend");
  return key_to_amount(amount);
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Rct::Amount> Xi::Rct::Rct::decode_amount(const Xi::Rct::RctSignature &rv, const Xi::Rct::Key &sk,
                                                        unsigned int i, Xi::Rct::IEcdhDevice &hwdev) {
  XI_ERROR_TRY();
  Key mask;
  return decode_amount(rv, sk, i, mask, hwdev);
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Rct::Amount> Xi::Rct::Rct::decode_amount_simple(const Xi::Rct::RctSignature &rv, const Xi::Rct::Key &sk,
                                                               unsigned int i, Xi::Rct::Key &mask,
                                                               Xi::Rct::IEcdhDevice &hwdev) {
  XI_ERROR_TRY();
  exceptional_if_not<GenericRctError>(
      rv.Type == RctType::Simple || rv.Type == RctType::Bulletproof || rv.Type == RctType::Bulletproof2,
      "decodeRct called on non simple rctSig");
  exceptional_if_not<GenericRctError>(i < rv.EcdhInfo.size(), "Bad index");
  exceptional_if_not<GenericRctError>(rv.OutPublicKeys.size() == rv.EcdhInfo.size(),
                                      "Mismatched sizes of rv.outPk and rv.ecdhInfo");

  // mask amount and mask
  EcdhTuple ecdh_info = rv.EcdhInfo[i];
  exceptional_if_not<RctDeviceError>(hwdev.ecdhDecode(ecdh_info, sk, rv.Type == RctType::Bulletproof2));
  mask = ecdh_info.Mask;
  Key amount = ecdh_info.Amount;
  Key C = rv.OutPublicKeys[i].Mask;
  Key Ctmp;
  exceptional_if_not<GenericRctError>(sc_check(mask.data()) == 0, "warning, bad ECDH mask");
  exceptional_if_not<GenericRctError>(sc_check(amount.data()) == 0, "warning, bad ECDH amount");
  key_multiply_with_basepoint_and_add_with_multiplied_point_inplace(mask, amount, Constants::H, Ctmp);
  exceptional_if_not<GenericRctError>(C == Ctmp, "warning, amount decoded incorrectly, will be unable to spend");
  return key_to_amount(amount);
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Rct::Amount> Xi::Rct::Rct::decode_amount_simple(const Xi::Rct::RctSignature &rv, const Xi::Rct::Key &sk,
                                                               unsigned int i, Xi::Rct::IEcdhDevice &hwdev) {
  XI_ERROR_TRY();
  Key mask;
  return decode_amount_simple(rv, sk, i, mask, hwdev);
  XI_ERROR_CATCH();
}
