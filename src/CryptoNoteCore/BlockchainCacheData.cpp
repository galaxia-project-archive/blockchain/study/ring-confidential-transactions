/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CryptoNoteCore/BlockchainCacheData.h"

#include <Serialization/ISerializer.h>
#include <Serialization/SerializationOverloads.h>
#include <CryptoNoteCore/CryptoNoteSerialization.h>

void CryptoNote::SpentKeyImage::serialize(ISerializer& s) {
  s(blockIndex, "block_index");
  s(keyImage, "key_image");
}

void CryptoNote::CachedTransactionInfo::serialize(ISerializer& s) {
  s(blockIndex, "block_index");
  s(transactionIndex, "transaction_index");
  s(transactionHash, "transaction_hash");
  s(unlockTime, "unlock_time");
  s(outputs, "outputs");
  s(globalIndexes, "global_indexes");
}

void CryptoNote::CachedBlockInfo::serialize(ISerializer& s) {
  s(blockHash, "block_hash");
  s(timestamp, "timestamp");
  s(blockSize, "block_size");
  s(cumulativeDifficulty, "cumulative_difficulty");
  s(alreadyGeneratedCoins, "already_generated_coins");
  s(alreadyGeneratedTransactions, "already_generated_transaction_count");
}

void CryptoNote::OutputGlobalIndexesForAmount::serialize(ISerializer& s) {
  s(startIndex, "start_index");
  s(outputs, "outputs");
}

void CryptoNote::PaymentIdTransactionHashPair::serialize(ISerializer& s) {
  s(paymentId, "payment_id");
  s(transactionHash, "transaction_hash");
}

void CryptoNote::ExtendedTransactionInfo::serialize(CryptoNote::ISerializer& s) {
  s(static_cast<CachedTransactionInfo&>(*this), "cached_transaction");
  s(amountToKeyIndexes, "key_indexes");
}

void CryptoNote::KeyOutputInfo::serialize(ISerializer& s) {
  s(publicKey, "public_key");
  s(unlockTime, "unlock_time");
  s(packedIndex, "packed_index");
  s(globalIndex, "global_index");
  s(transactionHash, "tx_hash");
  s(commitment, "commitment");
}

bool CryptoNote::KeyOutputInfo::operator<(const CryptoNote::KeyOutputInfo& rhs) const {
  return globalIndex < rhs.globalIndex;
}

void CryptoNote::serialize(CryptoNote::PackedOutIndex& value, CryptoNote::ISerializer& serializer) {
  serializer(value.data.blockIndex, "block_index");
  serializer(value.data.outputIndex, "output_index");
  serializer(value.data.transactionIndex, "transaction_index");
}
