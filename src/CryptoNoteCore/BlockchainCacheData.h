/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <map>
#include <vector>

#include <crypto/CryptoTypes.h>
#include <CryptoNoteCore/CryptoNote.h>
#include <CryptoNoteCore/Transactions/TransactionValidatiorState.h>
#include <Xi/Transactions/TransactionOutputTarget.h>

namespace CryptoNote {
class ISerializer;

union PackedOutIndex {
  struct {
    uint32_t blockIndex;
    uint16_t transactionIndex;
    uint16_t outputIndex;
  } data;

  uint64_t packedValue;
};

bool operator<(const PackedOutIndex& lhs, const PackedOutIndex& rhs);

const uint32_t INVALID_BLOCK_INDEX = std::numeric_limits<uint32_t>::max();

struct PushedBlockInfo {
  RawBlock rawBlock;
  TransactionValidatorState validatorState;
  size_t blockSize;
  uint64_t generatedCoins;
  uint64_t blockDifficulty;
  uint64_t timestamp;
};

class UseGenesis {
 public:
  explicit UseGenesis(bool u) : use(u) {}
  // emulate boolean flag
  operator bool() { return use; }

 private:
  bool use = false;
};

struct KeyOutputInfo {
  uint64_t unlockTime;
  PackedOutIndex packedIndex;
  uint64_t globalIndex;
  ::Crypto::Hash transactionHash;
  Crypto::PublicKey publicKey;
  Xi::Rct::Key commitment;

  void serialize(CryptoNote::ISerializer& s);

  bool operator<(const KeyOutputInfo& rhs) const;
};
using KeyOutputInfoVector = std::vector<KeyOutputInfo>;

struct SpentKeyImage {
  uint32_t blockIndex;
  Crypto::KeyImage keyImage;

  void serialize(ISerializer& s);
};

struct CachedTransactionInfo {
  uint32_t blockIndex;
  uint32_t transactionIndex;
  Crypto::Hash transactionHash;
  uint64_t unlockTime;
  std::vector<TransactionOutputTarget> outputs;
  // needed for getTransactionGlobalIndexes query
  std::vector<uint64_t> globalIndexes;

  void serialize(ISerializer& s);
};

// inherit here to avoid breaking IBlockchainCache interface
struct ExtendedTransactionInfo : CachedTransactionInfo {
  /// global key output indexes spawned in this transaction
  std::map<uint64_t, std::vector<uint64_t>> amountToKeyIndexes;
  void serialize(ISerializer& s);
};

struct CachedBlockInfo {
  Crypto::Hash blockHash;
  uint64_t timestamp;
  uint64_t cumulativeDifficulty;
  uint64_t alreadyGeneratedCoins;
  uint64_t alreadyGeneratedTransactions;
  uint32_t blockSize;

  void serialize(ISerializer& s);
};

struct OutputGlobalIndexesForAmount {
  uint32_t startIndex = 0;

  // 1. This container must be sorted by PackedOutIndex::blockIndex and PackedOutIndex::transactionIndex
  // 2. GlobalOutputIndex for particular output is calculated as following: startIndex + index in vector
  std::vector<PackedOutIndex> outputs;

  void serialize(ISerializer& s);
};

struct PaymentIdTransactionHashPair {
  Crypto::Hash paymentId;
  Crypto::Hash transactionHash;

  void serialize(ISerializer& s);
};

void serialize(PackedOutIndex& value, CryptoNote::ISerializer& serializer);
}  // namespace CryptoNote
