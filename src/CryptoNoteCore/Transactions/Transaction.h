/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/variant.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Transactions/Transaction.h>
#include <Xi/Transactions/KeyOutput.h>
#include <crypto/CryptoTypes.h>

namespace CryptoNote {

using BaseInput = Xi::Transactions::CoinGenerationInput;
using KeyInput = Xi::Transactions::KeyInput;
using TransactionInput = Xi::Transactions::TransactionInput;

using KeyOutput = Xi::Transactions::KeyOutput;
using TransactionOutputTarget = Xi::Transactions::TransactionOutputTarget;
using TransactionOutput = Xi::Transactions::TransactionOutput;

using TransactionExtra = Xi::Transactions::TransactionExtra;
using TransactionPrefix = Xi::Transactions::TransactionPrefix;
using Transaction = Xi::Transactions::Transaction;

struct BaseTransaction : public TransactionPrefix {};
}  // namespace CryptoNote
