﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
//
// This file is part of Bytecoin.
//
// Bytecoin is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bytecoin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Bytecoin.  If not, see <http://www.gnu.org/licenses/>.

#include "CryptoNoteCore/Transactions/ITransactionBuilder.h"

#include <numeric>
#include <system_error>

#include <Xi/Global.h>
#include <Xi/Transactions/TransactionLookup.h>
#include <Serialization/SerializationTools.h>

#include "CryptoNoteCore/CryptoNoteBasic.h"
#include "CryptoNoteCore/Transactions/TransactionUtils.h"
#include "CryptoNoteCore/CryptoNoteTools.h"

using namespace Crypto;

namespace CryptoNote {

class TransactionPrefixImpl : public ITransactionReader {
 public:
  TransactionPrefixImpl();
  TransactionPrefixImpl(const TransactionPrefix& prefix, const Hash& transactionHash);

  virtual ~TransactionPrefixImpl() override = default;

  virtual Hash getTransactionHash() const override;
  virtual Hash getTransactionPrefixHash() const override;
  virtual uint64_t getUnlockTime() const override;

  // extra
  virtual PublicKey getTransactionPublicKey() const override;
  virtual PublicKeyVector getTransactionAdditionalPublicKeys() const override;
  virtual bool getPaymentId(Hash& paymentId) const override;
  virtual TransactionExtra getExtra() const override;

  // inputs
  virtual size_t getInputCount() const override;
  virtual uint64_t getInputTotalAmount() const override;
  virtual TransactionTypes::InputType getInputType(size_t index) const override;
  virtual void getInput(size_t index, KeyInput& input) const override;

  // outputs
  virtual size_t getOutputCount() const override;
  virtual uint64_t getOutputTotalAmount() const override;
  virtual TransactionTypes::OutputType getOutputType(size_t index) const override;
  virtual void getOutput(size_t index, KeyOutput& output, uint64_t& amount) const override;

  // signatures
  virtual size_t getRequiredSignaturesCount(size_t inputIndex) const override;
  virtual bool findOutputsToAccount(const AccountPublicAddress& addr, const SecretKey& viewSecretKey,
                                    std::vector<uint64_t>& outs, uint64_t& outputAmount) const override;

  // various checks
  virtual bool validateInputs() const override;
  virtual bool validateOutputs() const override;
  virtual bool validateSignatures() const override;

  // serialized transaction
  virtual BinaryArray getTransactionData() const override;

  virtual bool getTransactionSecretKey(SecretKey& key) const override;

 private:
  TransactionPrefix m_txPrefix;
  Hash m_txHash;
};

TransactionPrefixImpl::TransactionPrefixImpl() {}

TransactionPrefixImpl::TransactionPrefixImpl(const TransactionPrefix& prefix, const Hash& transactionHash) {
  m_txPrefix = prefix;
  m_txHash = transactionHash;
}

Hash TransactionPrefixImpl::getTransactionHash() const { return m_txHash; }

Hash TransactionPrefixImpl::getTransactionPrefixHash() const { return getObjectHash(m_txPrefix); }

PublicKey TransactionPrefixImpl::getTransactionPublicKey() const {
  if (m_txPrefix.Extra.PublicKey.has_value()) {
    return m_txPrefix.Extra.PublicKey->Key;
  } else {
    return ::Crypto::PublicKey::Null;
  }
}

PublicKeyVector TransactionPrefixImpl::getTransactionAdditionalPublicKeys() const {
  if (m_txPrefix.Extra.PublicKeys.has_value()) {
    return m_txPrefix.Extra.PublicKeys->Keys;
  } else {
    return {};
  }
}

uint64_t TransactionPrefixImpl::getUnlockTime() const { return m_txPrefix.Unlock; }

bool TransactionPrefixImpl::getPaymentId(Hash& hash) const {
  if (m_txPrefix.Extra.PublicKey.has_value()) {
    hash = m_txPrefix.Extra.PaymentId->Identifier;
    return true;
  } else {
    return false;
  }
}

TransactionExtra TransactionPrefixImpl::getExtra() const { return m_txPrefix.Extra; }

size_t TransactionPrefixImpl::getInputCount() const { return m_txPrefix.Inputs.size(); }

uint64_t TransactionPrefixImpl::getInputTotalAmount() const {
  return std::accumulate(m_txPrefix.Inputs.begin(), m_txPrefix.Inputs.end(), 0ULL,
                         [](uint64_t val, const TransactionInput& in) { return val + getTransactionInputAmount(in); });
}

TransactionTypes::InputType TransactionPrefixImpl::getInputType(size_t index) const {
  return getTransactionInputType(getInputChecked(m_txPrefix, index));
}

void TransactionPrefixImpl::getInput(size_t index, KeyInput& input) const {
  input = boost::get<KeyInput>(getInputChecked(m_txPrefix, index, TransactionTypes::InputType::Key));
}

size_t TransactionPrefixImpl::getOutputCount() const { return m_txPrefix.Outputs.size(); }

uint64_t TransactionPrefixImpl::getOutputTotalAmount() const {
  return std::accumulate(m_txPrefix.Outputs.begin(), m_txPrefix.Outputs.end(), 0ULL,
                         [](uint64_t val, const TransactionOutput& out) { return val + out.Amount; });
}

TransactionTypes::OutputType TransactionPrefixImpl::getOutputType(size_t index) const {
  return getTransactionOutputType(getOutputChecked(m_txPrefix, index).Target);
}

void TransactionPrefixImpl::getOutput(size_t index, KeyOutput& output, uint64_t& amount) const {
  const auto& out = getOutputChecked(m_txPrefix, index, TransactionTypes::OutputType::Key);
  output = boost::get<KeyOutput>(out.Target);
  amount = out.Amount;
}

size_t TransactionPrefixImpl::getRequiredSignaturesCount(size_t inputIndex) const {
  return ::CryptoNote::getRequiredSignaturesCount(getInputChecked(m_txPrefix, inputIndex));
}

bool TransactionPrefixImpl::findOutputsToAccount(const AccountPublicAddress& addr, const SecretKey& viewSecretKey,
                                                 std::vector<uint64_t>& outs, uint64_t& outputAmount) const {
  Xi::Transactions::TransactionLookup lookup{AccountKeys::createViewOnlyKeys(addr, viewSecretKey),
                                             Logging::ILogger::noLogging()};
  auto result = lookup.scanTransferOutputs(m_txPrefix);
  if (result.isError()) {
    return false;
  } else {
    const auto scan = result.take();
    for (const auto& iScan : scan) {
      outs.push_back(iScan.OutputIndex);
      outputAmount += iScan.Amount.value;
    }
    return true;
  }
}

bool TransactionPrefixImpl::validateInputs() const {
  return !m_txPrefix.Inputs.empty() && !m_txPrefix.hasAnyNoneKeyInput() && !m_txPrefix.hasInputOverflow() &&
         !m_txPrefix.containsDoubleSpending();
}

bool TransactionPrefixImpl::validateOutputs() const {
  return !m_txPrefix.Outputs.empty() && !m_txPrefix.hasAnyNoneKeyOutput() && !m_txPrefix.hasOutputOverflow();
}

bool TransactionPrefixImpl::validateSignatures() const {
  throw std::system_error(std::make_error_code(std::errc::function_not_supported),
                          "Validating signatures is not supported for transaction prefix");
}

BinaryArray TransactionPrefixImpl::getTransactionData() const { return toBinaryArray(m_txPrefix); }

bool TransactionPrefixImpl::getTransactionSecretKey(SecretKey& key) const {
  XI_UNUSED(key);
  return false;
}

std::unique_ptr<ITransactionReader> createTransactionPrefix(const TransactionPrefix& prefix,
                                                            const Hash& transactionHash) {
  return std::unique_ptr<ITransactionReader>(new TransactionPrefixImpl(prefix, transactionHash));
}

std::unique_ptr<ITransactionReader> createTransactionPrefix(const Transaction& fullTransaction) {
  return std::unique_ptr<ITransactionReader>(
      new TransactionPrefixImpl(fullTransaction.Transfer, getObjectHash(fullTransaction)));
}

}  // namespace CryptoNote
