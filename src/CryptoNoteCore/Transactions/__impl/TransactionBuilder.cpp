﻿/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "CryptoNoteCore/Transactions/__impl/TransactionBuilder.h"

#include <algorithm>
#include <numeric>

#include <Xi/Algorithm/DeltaEncodedIndices.h>
#include <Xi/Crypto/Exceptions.h>
#include <Xi/Rct/Algorithm/GenerateCommitment.h>
#include <Xi/Config/Transaction.h>
#include <Xi/Transactions/TransactionLookup.h>

#include "CryptoNoteCore/CryptoNoteTools.h"
#include "CryptoNoteCore/Transactions/TransactionUtils.h"

using namespace Crypto;

namespace {

using namespace CryptoNote;

void derivePublicKey(const AccountPublicAddress& to, const SecretKey& txKey, size_t outputIndex,
                     PublicKey& ephemeralKey) {
  KeyDerivation derivation;
  generate_key_derivation(to.ViewPublicKey, txKey, derivation);
  derive_public_key(derivation, outputIndex, to.SpendPublicKey, ephemeralKey);
}

}  // namespace

namespace CryptoNote {

TransactionBuilder::TransactionBuilder() {
  transaction.nullify();
  CryptoNote::KeyPair txKeys(CryptoNote::generateKeyPair());
  Xi::Transactions::TransactionExtraPublicKey pk = {txKeys.PublicKey};
  transaction.Transfer.Extra.PublicKey = pk;
  secretKey = txKeys.SecretKey;
}

TransactionBuilder::TransactionBuilder(const BinaryArray& ba) {
  if (!fromBinaryArray(transaction, ba)) {
    throw std::runtime_error("Invalid transaction data");
  }
  transactionHash = getBinaryArrayHash(ba);  // avoid serialization if we already have blob
}

TransactionBuilder::TransactionBuilder(const CryptoNote::Transaction& tx) : transaction(tx) {}

void TransactionBuilder::invalidateHash() {
  if (transactionHash.is_initialized()) {
    transactionHash = decltype(transactionHash)();
  }
}

const SecretKey& TransactionBuilder::txSecretKey() const {
  if (!secretKey) {
    throw std::runtime_error("Operation requires transaction secret key");
  }
  return *secretKey;
}

void TransactionBuilder::checkIfSigning() const {
  if (!transaction.Signatures.isNull()) {
    throw std::runtime_error("Cannot perform requested operation, since it will invalidate transaction signatures");
  }
}

Hash TransactionBuilder::getTransactionHash() const {
  if (!transactionHash.is_initialized()) {
    transactionHash = getObjectHash(transaction);
  }

  return transactionHash.get();
}

Hash TransactionBuilder::getTransactionPrefixHash() const { return transaction.Transfer.computeHash().takeOrThrow(); }

PublicKey TransactionBuilder::getTransactionPublicKey() const {
  if (transaction.Transfer.Extra.PublicKey.has_value()) {
    return transaction.Transfer.Extra.PublicKey->Key;
  } else {
    return PublicKey::Null;
  }
}

PublicKeyVector TransactionBuilder::getTransactionAdditionalPublicKeys() const {
  if (transaction.Transfer.Extra.PublicKeys.has_value()) {
    return transaction.Transfer.Extra.PublicKeys->Keys;
  } else {
    return {};
  }
}

uint64_t TransactionBuilder::getUnlockTime() const { return transaction.Transfer.Unlock; }

void TransactionBuilder::setUnlockTime(uint64_t unlockTime) {
  checkIfSigning();
  transaction.Transfer.Unlock = unlockTime;
  invalidateHash();
}

bool TransactionBuilder::getTransactionSecretKey(SecretKey& key) const {
  if (!secretKey) {
    return false;
  }
  key = reinterpret_cast<const SecretKey&>(secretKey.get());
  return true;
}

void TransactionBuilder::setTransactionSecretKey(const SecretKey& key) {
  const auto& sk = reinterpret_cast<const SecretKey&>(key);
  PublicKey pk;
  Xi::exceptional_if_not<Xi::Crypto::KeyGenerationError>(secret_key_to_public_key(sk, pk));

  const PublicKey txPubKey = getTransactionPublicKey();
  if (txPubKey != pk) {
    throw std::runtime_error("Secret transaction key does not match public key");
  }

  secretKey = key;
}

size_t TransactionBuilder::addInput(const KeyInput& input) {
  checkIfSigning();
  transaction.Transfer.Inputs.emplace_back(input);
  invalidateHash();
  return transaction.Transfer.Inputs.size() - 1;
}

BinaryArray TransactionBuilder::getTransactionData() const { return toBinaryArray(transaction); }

void TransactionBuilder::setPaymentId(const Hash& hash) {
  checkIfSigning();
  transaction.Transfer.Extra.PaymentId = Xi::Transactions::TransactionExtraPaymentId{hash};
}

void TransactionBuilder::addInput(const TransactionTypes::InputKeyInfo& input) {
  Xi::Transactions::TransactionSourceEntry source;

  source.Amount = input.Amount;
  source.UsingOutputs.clear();
  std::transform(input.Outputs.begin(), input.Outputs.end(), std::back_inserter(source.UsingOutputs),
                 [](const TransactionTypes::GlobalOutput& o) { return o.toFakeOutput(); });
  source.MultiSignatureInfo = boost::none;

  source.RealOutputIndex = input.RealOutput.transactionIndex;
  source.RealOutputPublicKey = input.RealOutput.TransactionPublicKey;
  source.RealOutputAdditionalPublicKeys = input.RealOutput.TransactionAdditionalPublicKeys;
  source.RealOutputOutputIndex = input.RealOutput.outputInTransaction;

  auto& realOutput = source.UsingOutputs[input.RealOutput.outputInTransaction];
  realOutput.Index = input.RealOutput.transactionIndex;
  realOutput.Key.Dest = Xi::Rct::key_cast(input.RealOutput.TransactionPublicKey);
  realOutput.Key.Mask = Xi::Rct::generate_commitment(input.RealOutput.)

  //  real_oe.first = td.m_global_output_index;
  //  rea
  //  real_oe.second.dest = rct::pk2rct(td.get_public_key());
  //  real_oe.second.mask = rct::commit(td.amount(), td.m_mask);
}

void TransactionBuilder::addOutput(const TransactionTypes::OutputKeyInfo& input) {}

bool TransactionBuilder::getPaymentId(Hash& hash) const {
  if (transaction.Transfer.Extra.PaymentId.has_value()) {
    hash = transaction.Transfer.Extra.PaymentId->Identifier;
    return true;
  } else {
    return false;
  }
}

TransactionExtra TransactionBuilder::getExtra() const { return transaction.Transfer.Extra; }

size_t TransactionBuilder::getInputCount() const { return transaction.Transfer.Inputs.size(); }

uint64_t TransactionBuilder::getInputTotalAmount() const {
  return std::accumulate(transaction.Transfer.Inputs.begin(), transaction.Transfer.Inputs.end(), 0ULL,
                         [](uint64_t val, const TransactionInput& in) { return val + getTransactionInputAmount(in); });
}

TransactionTypes::InputType TransactionBuilder::getInputType(size_t index) const {
  return getTransactionInputType(getInputChecked(transaction.Transfer, index));
}

void TransactionBuilder::getInput(size_t index, KeyInput& input) const {
  input = boost::get<KeyInput>(getInputChecked(transaction.Transfer, index, TransactionTypes::InputType::Key));
}

size_t TransactionBuilder::getOutputCount() const { return transaction.Transfer.Outputs.size(); }

uint64_t TransactionBuilder::getOutputTotalAmount() const {
  return std::accumulate(transaction.Transfer.Outputs.begin(), transaction.Transfer.Outputs.end(), 0ULL,
                         [](uint64_t val, const TransactionOutput& out) { return val + out.Amount; });
}

TransactionTypes::OutputType TransactionBuilder::getOutputType(size_t index) const {
  return getTransactionOutputType(getOutputChecked(transaction.Transfer, index).Target);
}

void TransactionBuilder::getOutput(size_t index, KeyOutput& output, uint64_t& amount) const {
  const auto& out = getOutputChecked(transaction.Transfer, index, TransactionTypes::OutputType::Key);
  output = boost::get<KeyOutput>(out.Target);
  amount = out.Amount;
}

bool TransactionBuilder::findOutputsToAccount(const AccountPublicAddress& addr, const SecretKey& viewSecretKey,
                                              std::vector<uint64_t>& out, uint64_t& amount) const {
  Xi::Transactions::TransactionLookup lookup{AccountKeys::createViewOnlyKeys(addr, viewSecretKey),
                                             Logging::ILogger::noLogging()};
  auto result = lookup.scanTransactionOutputs(transaction);
  if (result.isError()) {
    return false;
  } else {
    const auto scan = result.take();
    for (const auto& iScan : scan) {
      out.push_back(iScan.OutputIndex);
      amount += iScan.Amount.value;
    }
    return true;
  }
}

size_t TransactionBuilder::getRequiredSignaturesCount(size_t index) const {
  return ::getRequiredSignaturesCount(getInputChecked(transaction.Transfer, index));
}

bool TransactionBuilder::validateInputs() const {
  return !transaction.Transfer.Inputs.empty() && !transaction.Transfer.hasAnyNoneKeyInput() &&
         !transaction.Transfer.hasInputOverflow() && !transaction.Transfer.containsDoubleSpending();
}

bool TransactionBuilder::validateOutputs() const {
  return !transaction.Transfer.Outputs.empty() && !transaction.Transfer.hasAnyNoneKeyOutput() &&
         !transaction.Transfer.hasOutputOverflow();
}

bool TransactionBuilder::validateSignatures() const {
  // TODO
  return true;
}
}  // namespace CryptoNote
