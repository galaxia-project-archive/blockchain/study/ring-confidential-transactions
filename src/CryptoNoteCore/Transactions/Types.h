﻿/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <crypto/CryptoTypes.h>
#include <Xi/Rct/Types/Key.h>
#include <Xi/Transactions/Construction/TransactionSourceEntry.h>
#include <Xi/Transactions/Construction/TransactionOutputEntry.h>

namespace CryptoNote {

namespace TransactionTypes {

///
/// \brief The InputType enum classifies an input of a transaction.
///
enum struct InputType : uint8_t { Invalid, Key, Generating };

///
/// \brief The OutputType enum classifies an output of a transaction.
///
enum struct OutputType : uint8_t { Invalid, Key };

///
/// \brief The GlobalOutput struct stores all public available information of a transaction output used for new
/// transaction outputs generation.
///
/// Global outputs used can be either a fake output or the actual output used.
///
struct GlobalOutput {
  /// Global index of the output used.
  uint64_t GlobalIndex;
  /// The public key of the output.
  Crypto::PublicKey PublicKey;
  /// The commitment mask used for confidential amounts.
  Xi::Rct::Key Commitment;

  ///
  /// \brief toFakeOutput creates an output entry used for transacton construction
  /// \return equivalent output elligible for transaction construction
  ///
  Xi::Transactions::TransactionOutputEntry toFakeOutput() const;
};
using GlobalOutputVector = std::vector<GlobalOutput>;

///
/// \brief The OutputKeyInfo struct contains additional real output informations, required to sign the transaction.
///
struct OutputKeyInfo {
  /// Public key emplaced in the transaction transfer extra field.
  Crypto::PublicKey TransactionPublicKey;
  /// Additional public keys emaplced in the transaction transfer extra field.
  Crypto::PublicKeyVector TransactionAdditionalPublicKeys;

  size_t transactionIndex;

  size_t outputInTransaction;
};

///
/// \brief The InputKeyInfo struct stores all information to construct a single input for a transaction.
///
struct InputKeyInfo {
  /// Amount sent using this input.
  Xi::Transactions::AtomicCoins Amount;
  /// Contains all outputs used for this input (real one and fakes for mixing)
  GlobalOutputVector Outputs;
  /// The real output information
  OutputKeyInfo RealOutput;
};
}  // namespace TransactionTypes
}  // namespace CryptoNote
