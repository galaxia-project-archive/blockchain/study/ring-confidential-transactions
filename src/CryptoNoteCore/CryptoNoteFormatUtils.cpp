﻿// Copyright (c) 2012-2017, The CryptoNote developers, The Bytecoin developers
//
// This file is part of Bytecoin.
//
// Bytecoin is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bytecoin is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Bytecoin.  If not, see <http://www.gnu.org/licenses/>.

#include "CryptoNoteFormatUtils.h"

#include <set>
#include <Logging/LoggerRef.h>
#include <Common/Varint.h>

#include <Xi/Algorithm/DeltaEncodedIndices.h>
#include <Xi/Algorithm/Permutation.h>
#include <Xi/Rct/Rct.h>
#include <Xi/Crypto/Account/Account.h>
#include <Xi/Crypto/Device/ICryptoDevice.h>
#include <crypto/CryptoTypes.h>
#include "Serialization/BinaryOutputStreamSerializer.h"
#include "Serialization/BinaryInputStreamSerializer.h"

#include "CryptoNoteCore/CryptoNote.h"
#include "Account.h"
#include "CryptoNoteBasicImpl.h"
#include "CryptoNoteSerialization.h"
#include "Transactions/TransactionExtra.h"
#include "CryptoNoteTools.h"

#include <Xi/Config.h>

using namespace Logging;
using namespace Crypto;
using namespace Common;

namespace CryptoNote {

bool getInputsMoneyAmount(const Transaction& tx, uint64_t& money) {
  money = 0;

  for (const auto& in : tx.Transfer.Inputs) {
    uint64_t amount = 0;

    if (in.type() == typeid(KeyInput)) {
      amount = boost::get<KeyInput>(in).Amount;
    }

    money += amount;
  }
  return true;
}

uint64_t get_outs_money_amount(const Transaction& tx) {
  uint64_t outputs_amount = 0;
  for (const auto& o : tx.Transfer.Outputs) {
    outputs_amount += o.Amount;
  }
  return outputs_amount;
}

std::string short_hash_str(const Hash& h) {
  std::string res = Common::podToHex(h);

  if (res.size() == 64) {
    auto erased_pos = res.erase(8, 48);
    res.insert(8, "....");
  }

  return res;
}

bool is_out_to_acc(const AccountKeys& acc, const KeyOutput& out_key, const KeyDerivation& derivation, size_t keyIndex) {
  PublicKey pk;
  derive_public_key(derivation, keyIndex, acc.Address.SpendPublicKey, pk);
  return pk == out_key.PublicKey;
}

bool is_out_to_acc(const AccountKeys& acc, const KeyOutput& out_key, const PublicKey& tx_pub_key, size_t keyIndex) {
  KeyDerivation derivation;
  generate_key_derivation(tx_pub_key, acc.ViewSecretKey, derivation);
  return is_out_to_acc(acc, out_key, derivation, keyIndex);
}

bool lookup_acc_outs(const AccountKeys& acc, const Transaction& tx, std::vector<size_t>& outs,
                     uint64_t& money_transfered) {
  PublicKey transactionPublicKey = tx.Transfer.Extra.PublicKey ? tx.Transfer.Extra.PublicKey->Key : PublicKey::Null;
  if (transactionPublicKey == NULL_PUBLIC_KEY) return false;
  return lookup_acc_outs(acc, tx, transactionPublicKey, outs, money_transfered);
}

bool lookup_acc_outs(const AccountKeys& acc, const Transaction& tx, const PublicKey& tx_pub_key,
                     std::vector<size_t>& outs, uint64_t& money_transfered) {
  money_transfered = 0;
  size_t keyIndex = 0;
  size_t outputIndex = 0;

  KeyDerivation derivation;
  generate_key_derivation(tx_pub_key, acc.ViewSecretKey, derivation);

  for (const TransactionOutput& o : tx.Transfer.Outputs) {
    assert(o.Target.type() == typeid(KeyOutput));
    if (o.Target.type() == typeid(KeyOutput)) {
      if (is_out_to_acc(acc, boost::get<KeyOutput>(o.Target), derivation, keyIndex)) {
        outs.push_back(outputIndex);
        money_transfered += o.Amount;
      }

      ++keyIndex;
    }

    ++outputIndex;
  }
  return true;
}

}  // namespace CryptoNote
