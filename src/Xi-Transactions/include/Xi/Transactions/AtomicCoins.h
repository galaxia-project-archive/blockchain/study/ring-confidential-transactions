/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <type_traits>
#include <array>

#include <Xi/Types/Arithmetic.h>
#include <Xi/Rct/Types/Amount.h>
#include <Serialization/ISerializer.h>

namespace Xi {
namespace Transactions {
struct AtomicCoins : Xi::Types::enable_arithmetic_from_this<uint64_t, AtomicCoins> {
  static_assert(std::is_same_v<value_t, Rct::Amount::value_t>,
                "AtomicCoins and Rct::Amount must have the same underlying type.");

  static std::array<AtomicCoins, 172> ValidDecompositions;

  using enable_arithmetic_from_this::enable_arithmetic_from_this;

  /* implicit */ AtomicCoins(Rct::Amount amount);
  operator Rct::Amount() const;
  operator uint64_t() const;  // TODO Remove me

  bool isValidDecomposition() const;

  void serialize(CryptoNote::ISerializer& serializer);
};

}  // namespace Transactions
}  // namespace Xi

inline Xi::Transactions::AtomicCoins operator"" _XIG(unsigned long long v) { return Xi::Transactions::AtomicCoins{v}; }
