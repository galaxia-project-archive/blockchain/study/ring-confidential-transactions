/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/optional.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Result.h>
#include <crypto/Types/Byte.h>
#include <crypto/Types/Hash.h>
#include <crypto/Types/KeyImage.h>
#include <Serialization/ISerializer.h>

#include "Xi/Transactions/TransactionInput.h"
#include "Xi/Transactions/TransactionOutput.h"
#include "Xi/Transactions/Extra/TransactionExtra.h"

namespace Xi {
namespace Transactions {
struct TransactionPrefix {
  uint64_t Version;
  uint64_t Unlock;

  TransactionInputVector Inputs;
  TransactionOutputVector Outputs;

  TransactionExtra Extra;

  TransactionPrefix();
  TransactionPrefix(const TransactionPrefix& other);
  TransactionPrefix& operator=(const TransactionPrefix& other);
  ~TransactionPrefix() = default;

  void serialize(CryptoNote::ISerializer& serializer);

  ///
  /// \brief nullify resets every member to a default state consideres as null
  ///
  void nullify();

  ///
  /// \brief clearAmounts sets all input and output amounts to zero
  ///
  void clearAmounts();

  ///
  /// \brief clearInputAmounts sets all inputs amounts to zero
  ///
  void clearInputAmounts();

  ///
  /// \brief clearOutputAmounts sets all output amounts to zero
  ///
  void clearOutputAmounts();

  ///
  /// \brief emplace copies another instance into this object
  /// \param other the instance to copy
  ///
  void emplace(const TransactionPrefix& other);

  ///
  /// \brief hasOverflow checks for an coins overflow in encoded amount transfers
  /// \return true iff inputs or outputs encode an overflow in sum
  ///
  bool hasOverflow() const;

  ///
  /// \brief hasInputOverflow checks for an coins overflow in encoded input amounts
  /// \return true iff inputs encode an overflow in sum
  ///
  bool hasInputOverflow() const;

  ///
  /// \brief hasOutputOverflow checks for an coins overflow in encoded output amounts
  /// \return true iff outputs encode an overflow in sum
  ///
  bool hasOutputOverflow() const;

  ///
  /// \brief totalInputCoins sums all input coins
  /// \return An error if inputs summarization yields an overflow otherwise its sum
  ///
  Xi::Result<AtomicCoins> totalInputCoins() const;

  ///
  /// \brief totalOutputCoins sums all input coins
  /// \return An error if outputs summarization yields an overflow otherwise its sum
  ///
  Xi::Result<AtomicCoins> totalOutputCoins() const;

  ///
  /// \brief hasAnyInvalidAtomicCoinsDecomposition checks all outputs for a none conancial amount
  /// \return true if an output contains a none conancial amount
  ///
  bool hasAnyInvalidAtomicCoinsDecomposition() const;

  ///
  /// \brief hasAnyNoneZeroAmountInput checks if all input amounts are zero
  /// \return false iff all are zero otherwise true
  ///
  bool hasAnyNoneZeroAmountInput() const;

  ///
  /// \brief hasAnyNoneZeroAmountOutput checks if all outputs amounts are zero
  /// \return false iff all are zero otherwise true
  ///
  bool hasAnyNoneZeroAmountOutput() const;

  ///
  /// \brief hasAnyZeroAmountOutput checks if any outputs has a zero amount
  /// \return true iff any output has zero amount
  ///
  bool hasAnyZeroAmountOutput() const;

  ///
  /// \brief hasAnyNoneKeyInput checks if any of the used inputs is not a key input
  /// \return true iff one of the inputs is not a key input, otherwise false
  ///
  bool hasAnyNoneKeyInput() const;

  ///
  /// \brief hasnAnyNoneKeyOutput checks if any of the used outputs is not a key output
  /// \return true iff any of the outputs is not a key output, otherwise false
  ///
  bool hasAnyNoneKeyOutput() const;

  ///
  /// \brief containsDoubleSpending checks for duplicate key images
  /// \return true if there is a duplicate otherwise false
  ///
  /// \attention this only checks for duplicate key images in this transfer, it is up to the caller to check for double
  /// spending with other transactions in a block or previous blocks
  ///
  bool containsDoubleSpending() const;

  ///
  /// \brief areKeyImageInputsSorted checks if key images used as inputs are sorted (prev < next)
  /// \return true if alle key images are sorted otherwise false
  ///
  bool areKeyImageInputsSorted() const;

  ///
  /// \brief keyImages gathers all used key images as input
  /// \return A vector of all key images used
  ///
  ::Crypto::KeyImageVector keyImages() const;

  ///
  /// \brief computeHash fastHash of the serialized binary blob
  /// \return An error if serialization failed, otherwise an unique hash for the transaction prefix
  ///
  Xi::Result<::Crypto::Hash> computeHash() const;
};
}  // namespace Transactions
}  // namespace Xi
