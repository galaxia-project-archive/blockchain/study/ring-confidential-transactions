/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <typeinfo>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/optional.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Serialization/ISerializer.h>
#include <crypto/Types/PublicKey.h>

#include "Xi/Transactions/Extra/TransactionExtraPadding.h"
#include "Xi/Transactions/Extra/TransactionExtraPaymentId.h"
#include "Xi/Transactions/Extra/TransactionExtraPublicKey.h"
#include "Xi/Transactions/Extra/TransactionExtraPublicKeys.h"

namespace Xi {
namespace Transactions {
struct TransactionExtra {
  static const TransactionExtra Null;

  static uint8_t getTypeTag(const std::type_info& nfo);

  boost::optional<TransactionExtraPublicKey> PublicKey{boost::none};
  boost::optional<TransactionExtraPublicKeys> PublicKeys{boost::none};
  boost::optional<TransactionExtraPaymentId> PaymentId{boost::none};

  void serialize(CryptoNote::ISerializer& serializer);

  ::Crypto::PublicKeyVector publicKeys() const;

  void nullify();
  bool isNull() const;
};
}  // namespace Transactions
}  // namespace Xi
