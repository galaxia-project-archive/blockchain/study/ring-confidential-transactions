/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include <Xi/Result.h>
#include <Serialization/ISerializer.h>
#include <crypto/Types/Hash.h>
#include <Xi/Rct/Types/RctSignature.h>

#include "Xi/Transactions/TransactionPrefix.h"

namespace Xi {
namespace Transactions {
struct Transaction {
  /// Encodes actual coins transfered or generated in case of a coinbase transaction.
  TransactionPrefix Transfer;
  /// For non coinbase transactions, Signatures stores the proof of the owner, legitimate to transfer the coins.
  Rct::RctSignature Signatures;

  Transaction();
  Transaction(const Transaction& other);
  Transaction& operator=(const Transaction& other);
  ~Transaction() = default;

  void serialize(CryptoNote::ISerializer& serializer);

  void nullify();
  void emplace(const Transaction& other);

  AtomicCoins fee() const;

  Xi::Result<::Crypto::Hash> computeHash() const;
  Xi::Result<uint64_t> blobSize() const;
  Xi::Result<uint64_t> weight() const;
};

using TransactionVector = std::vector<Transaction>;
}  // namespace Transactions
}  // namespace Xi
