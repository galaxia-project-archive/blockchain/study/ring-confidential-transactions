/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include <Xi/Global.h>
#include <Logging/Logging.h>
#include <crypto/Types/KeyDerivation.h>
#include <Xi/Crypto/Account/Account.h>
#include <Xi/Crypto/KeyPair.h>
#include <Xi/Rct/Types/RctSignature.h>
#include <Xi/Rct/Types/Key.h>

#include "Xi/Transactions/Transaction.h"

namespace Xi {
namespace Transactions {
class TransactionLookup {
 public:
  struct ScanResult {
    Crypto::KeyPair EphemeralKeys;
    ::Crypto::KeyImage DoubleSpendingProtection;
    Rct::Key Mask;
    AtomicCoins Amount;
    size_t OutputIndex;
    Crypto::Account::SubAddressReceiveInfo Receiver;
  };
  using ScanResultVector = std::vector<ScanResult>;

 public:
  TransactionLookup(const Xi::Crypto::Account::AccountKeys &acc, Logging::ILogger &logger);
  TransactionLookup(const Xi::Crypto::Account::AccountKeys &acc, const Crypto::Account::SubAddressesMap &subadresses,
                    Logging::ILogger &logger);
  XI_DELETE_COPY(TransactionLookup);
  XI_DEFAULT_MOVE(TransactionLookup);
  ~TransactionLookup() = default;

  Xi::Result<ScanResultVector> scanTransferOutputs(const TransactionPrefix &transfer,
                                                   const std::vector<uint64_t> indices) const;
  Xi::Result<ScanResultVector> scanTransferOutputs(const TransactionPrefix &transfer) const;

  Xi::Result<ScanResultVector> scanTransactionOutputs(const Transaction &transaction) const;
  Xi::Result<ScanResultVector> scanTransactionOutputs(const Transaction &transaction,
                                                      const std::vector<uint64_t> indices) const;

 private:
  Xi::Result<AtomicCoins> decodeAmount(const Rct::RctSignature &rv, const ::Crypto::KeyDerivation &derivation,
                                       unsigned int i, Rct::Key &mask) const;

 private:
  const Xi::Crypto::Account::AccountKeys &m_acc;
  const Crypto::Account::SubAddressesMap m_noSubaddresses;  ///< used if the user did not provide sub addresses
  const Crypto::Account::SubAddressesMap *m_subaddresses;   ///< points to no subaddresses or user provided ones
  Logging::LoggerRef m_logger;
};
}  // namespace Transactions
}  // namespace Xi
