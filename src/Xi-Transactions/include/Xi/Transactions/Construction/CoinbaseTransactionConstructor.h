/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <vector>

#include <Xi/Result.h>
#include <Logging/Logging.h>
#include <crypto/Types/PublicKey.h>
#include <Xi/Crypto/KeyPair.h>
#include <Xi/Crypto/Account/AccountPublicAddress.h>

#include "Xi/Transactions/Transaction.h"

namespace Xi {
namespace Transactions {
class CoinbaseTransactionConstructor {
 public:
  CoinbaseTransactionConstructor();

  CoinbaseTransactionConstructor& withMinerUnlockWindow(uint32_t blocks);

  ///
  /// \brief withReward emplaces a new transaction output to reward the given account
  /// \param acc account to receive the reward
  /// \param reward the reward amount to generation
  /// \return *this
  ///
  /// \throws KeyGenerationError if the keys generation, required to generate the output, fails
  ///
  CoinbaseTransactionConstructor& withReward(const Crypto::Account::AccountPublicAddress& acc,
                                             const AtomicCoins reward);

  ///
  /// \brief withReward emplaces a new transaction output to reward the given account
  /// \param acc account to receive the reward
  /// \param reward the reward amount to generation
  /// \param txKeys (psueod) random keys used to compute the key derivation
  /// \return *this
  ///
  /// \throws KeyGenerationError if the keys generation, required to generate the output, fails
  ///
  CoinbaseTransactionConstructor& withReward(const Crypto::Account::AccountPublicAddress& acc, const AtomicCoins reward,
                                             Crypto::KeyPair txKeys);

  CoinbaseTransactionConstructor& withBlockIndex(uint32_t index);

  CoinbaseTransactionConstructor& withVersion(uint64_t version);

  AtomicCoins totalReward() const;

  Xi::Result<TransactionPrefix> construct() const;

 private:
  ::Crypto::PublicKeyVector m_publicKeys;
  TransactionOutputVector m_outputs;
  uint32_t m_index;
  uint32_t m_minerUnlockWindow;
  uint64_t m_txVersion;
};
}  // namespace Transactions
}  // namespace Xi
