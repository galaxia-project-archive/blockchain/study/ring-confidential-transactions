/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>
#include <unordered_map>
#include <memory>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/optional.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Result.h>
#include <Xi/Exceptional.h>
#include <Logging/ILogger.h>
#include <Logging/LoggerRef.h>
#include <crypto/CryptoTypes.h>
#include <Xi/Rct/Types/MultiSignatureOut.h>
#include <Xi/Rct/Types/RangeProofType.h>
#include <Xi/Rct/Types/RctType.h>
#include <Xi/Rct/Types/RctConfig.h>
#include <Xi/Crypto/Account/Account.h>

#include "Xi/Transactions/Transaction.h"
#include "Xi/Transactions/Construction/TransactionSourceEntry.h"
#include "Xi/Transactions/Construction/TransactionDestinationEntry.h"

namespace Xi {
namespace Transactions {
XI_DECLARE_EXCEPTIONAL_CATEGORY(TransactionConstruction)
class TransactionConstructor {
  XI_DECLARE_EXCEPTIONAL_INSTANCE(Generic, "inner method returned none success code", TransactionConstruction)
  XI_DECLARE_EXCEPTIONAL_INSTANCE(CryptoDevice, "crypto device returned none success code", TransactionConstruction)

  struct Result {
    Transaction CreatedTransaction;
    ::Crypto::Hash TransactionHash;
    const ::Crypto::SecretKey& TransactionKey;
    const ::Crypto::SecretKeyVector& AdditionalTransactionKeys;
  };

 public:
  TransactionConstructor(const Crypto::Account::AccountKeys& sender, const TransactionSourceEntryVector& sources,
                         const TransactionDestinationEntryVector& destinations);

  TransactionConstructor& withSubadresses(const Crypto::Account::SubAddressesMap& subadresses);
  TransactionConstructor& withSignatureType(Rct::RctType type);
  TransactionConstructor& withMultiSignatureWallet(Rct::MultiSignatureOut& out);
  TransactionConstructor& withVersion(uint64_t version);
  TransactionConstructor& withUnlock(uint64_t unlock);
  TransactionConstructor& withOutputShuffle();
  TransactionConstructor& withoutOutputShuffle();
  TransactionConstructor& withKey(::Crypto::SecretKey key);
  TransactionConstructor& withAdditionalKeys(::Crypto::SecretKeyVector keys);
  TransactionConstructor& withChangeAddress(Crypto::Account::AccountPublicAddress changeAddress);
  TransactionConstructor& withLogger(Logging::ILogger& logger);

  Xi::Result<Result> construct() const;

 private:
  const Crypto::Account::AccountKeys& m_sender;
  const TransactionSourceEntryVector& m_sources;
  const TransactionDestinationEntryVector& m_destinations;

  const Crypto::Account::SubAddressesMap* m_subaddresses;
  Rct::RctType m_signatureType;
  Rct::RctConfig m_rctConfig;
  Rct::MultiSignatureOut* m_multisig;
  uint64_t m_version;
  uint64_t m_unlock;
  bool m_shuffleOutput;
  mutable boost::optional<::Crypto::SecretKey> m_txkey;
  mutable boost::optional<::Crypto::SecretKeyVector> m_additionalTxKeys;
  boost::optional<Crypto::Account::AccountPublicAddress> m_changeAddress;
  Logging::ILogger* m_logger;
};
}  // namespace Transactions
}  // namespace Xi
