/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <vector>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/optional.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <crypto/Types/PublicKey.h>
#include <Xi/Rct/Types/MultiSignatureData.h>

#include "Xi/Transactions/AtomicCoins.h"
#include "Xi/Transactions/Construction/TransactionOutputEntry.h"

namespace Xi {
namespace Transactions {
struct TransactionSourceEntry {
  /// Outputs used as in inputs, contains mixed outputs as well.
  TransactionOutputEntryVector UsingOutputs;

  /// Index to the none mixed output owned by the creator of the transaction.
  size_t RealOutputIndex;

  /// The output index the none mixed input corresponds to.
  size_t RealOutputOutputIndex;

  /// The public key used generating the real output used.
  ::Crypto::PublicKey RealOutputPublicKey;

  /// The additional public keys embedded in the transaction extra used in the transaction generating the used output.
  ::Crypto::PublicKeyVector RealOutputAdditionalPublicKeys;

  /// Used for multisignature transactions, contains the data needed to successfully generate the transaction.
  boost::optional<Rct::MultiSignatureData> MultiSignatureInfo;

  /// Coins transfered from this input.
  AtomicCoins Amount;

  /// Rct mask used for confidential amounts sent.
  Rct::Key Mask;
};

using TransactionSourceEntryVector = std::vector<TransactionSourceEntry>;
}  // namespace Transactions
}  // namespace Xi
