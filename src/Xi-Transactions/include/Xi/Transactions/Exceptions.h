/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <Xi/Exceptional.h>

namespace Xi {
namespace Transactions {
XI_DECLARE_EXCEPTIONAL_CATEGORY(Transaction)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidVersion, "unsupported transaction version", Transaction)
XI_DECLARE_EXCEPTIONAL_INSTANCE(DoublyUsedKeyImage, "same input was used multiple times", Transaction)
XI_DECLARE_EXCEPTIONAL_INSTANCE(UnexpectedSignatureType, "unexpected transaction signature", Transaction)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidExtra, "transaction extra is invalid", Transaction)
XI_DECLARE_EXCEPTIONAL_INSTANCE(CoinsOverflowException, "encoded coins encode an overflow", Transaction)
XI_DECLARE_EXCEPTIONAL_INSTANCE(MissingKey, "a key required for the operation is missing", Transaction)
}  // namespace Transactions
}  // namespace Xi
