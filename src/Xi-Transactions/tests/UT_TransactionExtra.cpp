/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include <gmock/gmock.h>

#include <cstring>

#include <Serialization/SerializationTools.h>
#include <Xi/Transactions/Extra/TransactionExtra.h>

extern "C" {
#include <crypto/random.h>
}

#define XI_TESTSUITE T_Xi_Transactions_Extra

// TEST(XI_TESTSUITE, Padding) {
//  using namespace CryptoNote;
//  using namespace Xi::Transactions;

//  {
//    TransactionExtraPadding pad;
//    pad.Size = 0;
//    EXPECT_NO_THROW(toBinaryArray(pad));
//    pad.Size = TransactionExtraPadding::maxSize() + 1;
//    EXPECT_ANY_THROW(toBinaryArray(pad));
//  }

//  {
//    std::vector<uint8_t> binPadding;
//    EXPECT_ANY_THROW(fromBinaryArray<TransactionExtraPadding>(binPadding));
//  }

//  {
//    TransactionExtraPadding pad;
//    pad.Size = 10;
//    auto binPadding = toBinaryArray(pad);
//    binPadding.resize(binPadding.size() - 1);
//    EXPECT_ANY_THROW(fromBinaryArray<TransactionExtraPadding>(binPadding));
//  }

//  {
//    TransactionExtraPadding pad;
//    pad.Size = 10;
//    auto binPadding = toBinaryArray(pad);
//    binPadding[2] = 0xFF;
//    EXPECT_ANY_THROW(fromBinaryArray<TransactionExtraPadding>(binPadding));
//    binPadding[2] = 0x00;
//    binPadding[5] = 0x01;
//    EXPECT_ANY_THROW(fromBinaryArray<TransactionExtraPadding>(binPadding));
//  }

//  {
//    TransactionExtraPadding pad;
//    pad.Size = 7;
//    auto binPadding = toBinaryArray(pad);
//    EXPECT_NO_THROW(fromBinaryArray<TransactionExtraPadding>(binPadding));
//    pad = fromBinaryArray<TransactionExtraPadding>(binPadding);
//    EXPECT_EQ(pad.Size, 7);
//  }
//}

// TEST(XI_TESTSUITE, PublicKey) {
//  using namespace CryptoNote;
//  using namespace Xi::Transactions;
//}

// TEST(XI_TESTSUITE, PublicKeys) {
//  using namespace CryptoNote;
//  using namespace Xi::Transactions;

//  TransactionExtraPublicKeys keys{};
//  ASSERT_TRUE(keys.Keys.empty());

//  EXPECT_FALSE(keys.validate());
//  for (size_t i = TransactionExtraPublicKeys::minSize(); i <= TransactionExtraPublicKeys::maxSize(); ++i) {
//    keys.Keys.resize(i);
//    generate_random_bytes(sizeof(Crypto::PublicKey) * keys.Keys.size(), keys.Keys.data());
//    auto deserialized = fromBinaryArray<TransactionExtraPublicKeys>(toBinaryArray(keys));
//    EXPECT_EQ(std::memcmp(keys.Keys.data(), deserialized.Keys.data(), sizeof(Crypto::PublicKey) * keys.Keys.size()),
//    0);
//  }

//  keys.Keys.resize(TransactionExtraPublicKeys::maxSize() + 1);
//  generate_random_bytes(sizeof(Crypto::PublicKey) * keys.Keys.size(), keys.Keys.data());
//  EXPECT_FALSE(keys.validate());
//}

// TEST(XI_TESTSUITE, Full) {
//  using namespace CryptoNote;
//  using namespace Xi::Transactions;
//}
