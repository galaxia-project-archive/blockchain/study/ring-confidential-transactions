/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/TransactionLookup.h"

#include <Xi/Exceptions.h>
#include <Xi/Algorithm.h>
#include <Xi/Crypto/Exceptions.h>
#include <Xi/Crypto/Device/DefaultDevice.h>
#include <Xi/Rct/Rct/Decode.h>

#include "Xi/Transactions/Exceptions.h"

#include "utils/KeyImageGeneration.h"
#include "utils/AccountClassification.h"

Xi::Transactions::TransactionLookup::TransactionLookup(const Xi::Crypto::Account::AccountKeys &acc,
                                                       Logging::ILogger &logger)
    : m_acc{acc},
      m_noSubaddresses{{acc.Address.SpendPublicKey, {0, 0}}},
      m_subaddresses{&m_noSubaddresses},
      m_logger{logger, "TxLookup"} {}

Xi::Transactions::TransactionLookup::TransactionLookup(const Xi::Crypto::Account::AccountKeys &acc,
                                                       const Xi::Crypto::Account::SubAddressesMap &subadresses,
                                                       Logging::ILogger &logger)
    : TransactionLookup(acc, logger) {
  m_subaddresses = &subadresses;
}

Xi::Result<Xi::Transactions::TransactionLookup::ScanResultVector>
Xi::Transactions::TransactionLookup::scanTransferOutputs(const TransactionPrefix &transfer,
                                                         const std::vector<uint64_t> indices) const {
  XI_ERROR_TRY();
  exceptional_if_not<InvalidIndexError>(Algorithm::is_unique(indices.begin(), indices.end()),
                                        "output indices to scan has ducplicates");
  auto &device = m_acc.device();

  ::Crypto::KeyDerivation accKeyDerivation{};
  ::Crypto::PublicKey txPublicKey;
  ::Crypto::KeyDerivationVector additionalAccKeyDerivations{};
  ::Crypto::PublicKeyVector txAdditionalPublicKeys{};
  {
    XI_UNUSED_REVAL(device.switchMode(Crypto::Device::DeviceMode::ParseTransaction).takeOrThrow());
    exceptional_if_not<MissingKeyError>(transfer.Extra.PublicKey.has_value());
    txPublicKey = transfer.Extra.PublicKey->Key;
    if (!device.generateKeyDerivation(txPublicKey, m_acc.ViewSecretKey, accKeyDerivation)) {
      m_logger(Logging::WARNING) << "key derivation faild for transaction public key, skipping the key";
      accKeyDerivation = Rct::Key::Identity;
    }
    if (transfer.Extra.PublicKeys.has_value()) {
      additionalAccKeyDerivations.resize(transfer.Extra.PublicKeys->Keys.size());
      txAdditionalPublicKeys.resize(transfer.Extra.PublicKeys->Keys.size());
      for (size_t i = 0; i < transfer.Extra.PublicKeys->Keys.size(); ++i) {
        txAdditionalPublicKeys[i] = transfer.Extra.PublicKeys->Keys[i];
        if (!device.generateKeyDerivation(txAdditionalPublicKeys[i], m_acc.ViewSecretKey,
                                          additionalAccKeyDerivations[i])) {
          m_logger(Logging::WARNING) << "key derivation faild for transaction public key, skipping the key";
          additionalAccKeyDerivations[i] = Rct::Key::Identity;
        }
      }
    }
  }

  ScanResultVector reval;
  std::vector<size_t> foundTransactionOutputIndices;

  for (const auto index : indices) {
    ScanResult iResult;
    exceptional_if_not<IndexOutOfRangeError>(index < transfer.Outputs.size());
    const auto &out = transfer.Outputs[index];
    exceptional_if_not<InvalidVariantTypeError>(out.Target.type() == typeid(KeyOutput));
    const auto &outKey = boost::get<KeyOutput>(out.Target);

    // check_acc_out_precomp_once(tx.vout[i], derivation, additional_derivations, i, is_out_data_ptr, tx_scan_info[i],
    // output_found[i]);
    {
      boost::optional<Crypto::Account::SubAddressReceiveInfo> receiverComputation{boost::none};
      {
        XI_UNUSED_REVAL(device.switchMode(Crypto::Device::DeviceMode::ParseTransaction).takeOrThrow());
        receiverComputation = __Impl::isOutToAccPreComputation(*m_subaddresses, outKey.PublicKey, accKeyDerivation,
                                                               additionalAccKeyDerivations, index, device);
      }
      if (receiverComputation.has_value()) {
        iResult.Receiver = std::move(receiverComputation.value());
        iResult.OutputIndex = index;

        exceptional_if_not<Crypto::CryptoDeviceError>(
            device.generateConcealKeyDerivation(txPublicKey, txAdditionalPublicKeys, accKeyDerivation,
                                                additionalAccKeyDerivations, iResult.Receiver.Derivation));

        // scan_output(tx, miner_tx, tx_pub_key, i, tx_scan_info[i], num_vouts_received, tx_money_got_in_outs, outs,
        // pool);

        if (m_acc.isMultiSignatureAccount()) {
          iResult.EphemeralKeys.PublicKey = outKey.PublicKey;
          iResult.EphemeralKeys.SecretKey.nullify();
          iResult.DoubleSpendingProtection.nullify();
        } else {
          exceptional_if_not<Crypto::KeyGenerationError>(__Impl::generateKeyImageHelperPrecomp(
              m_acc, outKey.PublicKey, iResult.Receiver.Derivation, index, iResult.Receiver.Index,
              iResult.EphemeralKeys, iResult.DoubleSpendingProtection, device));
          exceptional_if_not<Crypto::KeyGenerationError>(
              iResult.EphemeralKeys.PublicKey == outKey.PublicKey,
              "key generation from transaction output did not yield the expected public key used in the output");
        }

        foundTransactionOutputIndices.push_back(index);

        iResult.Amount = out.Amount;
      }
    }

    reval.emplace_back(std::move(iResult));
  }

  return std::move(reval);
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Transactions::TransactionLookup::ScanResultVector>
Xi::Transactions::TransactionLookup::scanTransferOutputs(const Xi::Transactions::TransactionPrefix &transfer) const {
  std::vector<uint64_t> indices;
  indices.resize(transfer.Outputs.size());
  for (uint64_t i = 0; i < indices.size(); ++i) {
    indices[i] = i;
  }
  return scanTransferOutputs(transfer, indices);
}

Xi::Result<Xi::Transactions::TransactionLookup::ScanResultVector>
Xi::Transactions::TransactionLookup::scanTransactionOutputs(const Xi::Transactions::Transaction &transaction) const {
  std::vector<uint64_t> indices;
  indices.resize(transaction.Transfer.Outputs.size());
  for (uint64_t i = 0; i < indices.size(); ++i) {
    indices[i] = i;
  }
  return scanTransactionOutputs(transaction, indices);
}

Xi::Result<Xi::Transactions::TransactionLookup::ScanResultVector>
Xi::Transactions::TransactionLookup::scanTransactionOutputs(const Xi::Transactions::Transaction &transaction,
                                                            const std::vector<uint64_t> indices) const {
  XI_ERROR_TRY();
  auto reval = scanTransferOutputs(transaction.Transfer, indices).takeOrThrow();
  for (auto &iResult : reval) {
    if (iResult.Amount == 0_XIG) {
      exceptional_if<IndexOutOfRangeError>(iResult.OutputIndex > std::numeric_limits<unsigned int>::max());
      iResult.Amount = decodeAmount(transaction.Signatures, iResult.Receiver.Derivation,
                                    static_cast<unsigned int>(iResult.OutputIndex), iResult.Mask)
                           .valueOrThrow();
    } else {
      // TODO
    }
  }
  return std::move(reval);
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Transactions::AtomicCoins> Xi::Transactions::TransactionLookup::decodeAmount(
    const Xi::Rct::RctSignature &rv, const ::Crypto::KeyDerivation &derivation, unsigned int i,
    Xi::Rct::Key &mask) const {
  XI_ERROR_TRY();
  auto &device = m_acc.device();
  ::Crypto::SecretKey scalar1;
  exceptional_if_not<Crypto::CryptoDeviceError>(
      device.keyDerivationToScalar(derivation, i, reinterpret_cast<::Crypto::EllipticCurveScalar &>(scalar1)));
  Rct::Amount decoded;
  switch (rv.Type) {
    case Rct::RctType::Simple:
    case Rct::RctType::Bulletproof:
    case Rct::RctType::Bulletproof2:
      decoded = Rct::Rct::decode_amount_simple(rv, Rct::key_cast(scalar1), i, mask, device).valueOrThrow();
      break;
    case Rct::RctType::Full:
      decoded = Rct::Rct::decode_amount(rv, Rct::key_cast(scalar1), i, mask, device).valueOrThrow();
      break;
    default:
      exceptional<InvalidVariantTypeError>("unknown signature type, cannot decode amount.");
  }
  return AtomicCoins{decoded};
  XI_ERROR_CATCH();
}
