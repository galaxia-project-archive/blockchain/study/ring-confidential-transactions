/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/TransactionPrefix.h"

#include <algorithm>
#include <numeric>
#include <utility>
#include <cassert>

#include <Xi/Exceptional.h>
#include <Serialization/SerializationTools.h>
#include <crypto/hash.h>

#include "Xi/Transactions/Exceptions.h"

Xi::Transactions::TransactionPrefix::TransactionPrefix() { nullify(); }

Xi::Transactions::TransactionPrefix::TransactionPrefix(const Xi::Transactions::TransactionPrefix &other) {
  emplace(other);
}

Xi::Transactions::TransactionPrefix &Xi::Transactions::TransactionPrefix::operator=(
    const Xi::Transactions::TransactionPrefix &other) {
  emplace(other);
  return *this;
}

void Xi::Transactions::TransactionPrefix::serialize(CryptoNote::ISerializer &serializer) {
  serializer(Version, "version");
  exceptional_if_not<CryptoNote::InvalidVersionSerializationError>(Version == 1);
  serializer(Unlock, "unlock");
}

void Xi::Transactions::TransactionPrefix::nullify() {
  Version = 0;
  Unlock = 0;
  Inputs.clear();
  Outputs.clear();
  Extra.nullify();
}

void Xi::Transactions::TransactionPrefix::clearAmounts() {
  clearInputAmounts();
  clearOutputAmounts();
}

void Xi::Transactions::TransactionPrefix::clearInputAmounts() {
  for (auto &in : Inputs) {
    if (in.type() == typeid(KeyInput)) {
      boost::get<KeyInput>(in).Amount = 0_XIG;
    } else {
      assert(in.type() == typeid(CoinGenerationInput));
    }
  }
}

void Xi::Transactions::TransactionPrefix::clearOutputAmounts() {
  for (auto &out : Outputs) {
    out.Amount = 0_XIG;
  }
}

void Xi::Transactions::TransactionPrefix::emplace(const Xi::Transactions::TransactionPrefix &other) {
  Version = other.Version;
  Unlock = other.Unlock;
  Inputs = other.Inputs;
  Outputs = other.Outputs;
  Extra = other.Extra;
}

bool Xi::Transactions::TransactionPrefix::hasOverflow() const { return hasInputOverflow() || hasOutputOverflow(); }

bool Xi::Transactions::TransactionPrefix::hasInputOverflow() const {
  AtomicCoins acc{0};
  for (const auto &in : Inputs) {
    auto amount = in.amount();
    XI_RETURN_EC_IF(acc > amount + acc, true, "input overflow");
    acc += amount;
  }
  return false;
}

bool Xi::Transactions::TransactionPrefix::hasOutputOverflow() const {
  AtomicCoins acc{0};
  for (const auto &o : Outputs) {
    XI_RETURN_EC_IF(acc > o.Amount + acc, true, "output overflow");
    acc += o.Amount;
  }
  return true;
}

Xi::Result<Xi::Transactions::AtomicCoins> Xi::Transactions::TransactionPrefix::totalInputCoins() const {
  XI_ERROR_TRY();
  exceptional_if<CoinsOverflowExceptionError>(hasInputOverflow(), "input overflow");
  return std::accumulate(Inputs.begin(), Inputs.end(), AtomicCoins{0},
                         [](const auto acc, const auto &in) { return acc + in.amount(); });
  XI_ERROR_CATCH();
}

Xi::Result<Xi::Transactions::AtomicCoins> Xi::Transactions::TransactionPrefix::totalOutputCoins() const {
  XI_ERROR_TRY();
  exceptional_if<CoinsOverflowExceptionError>(hasOutputOverflow(), "output overflow");
  return std::accumulate(Outputs.begin(), Outputs.end(), AtomicCoins{0},
                         [](const auto acc, const auto &out) { return acc + out.Amount; });
  XI_ERROR_CATCH();
}

bool Xi::Transactions::TransactionPrefix::hasAnyInvalidAtomicCoinsDecomposition() const {
  return std::any_of(Outputs.begin(), Outputs.end(),
                     [](const auto &out) { return !out.Amount.isValidDecomposition(); });
}

bool Xi::Transactions::TransactionPrefix::hasAnyNoneZeroAmountInput() const {
  return std::any_of(Inputs.begin(), Inputs.end(), [](const auto &in) { return in.amount() != AtomicCoins{0}; });
}

bool Xi::Transactions::TransactionPrefix::hasAnyNoneZeroAmountOutput() const {
  return std::any_of(Outputs.begin(), Outputs.end(), [](const auto &out) { return out.Amount != AtomicCoins{0}; });
}

bool Xi::Transactions::TransactionPrefix::hasAnyZeroAmountOutput() const {
  return std::any_of(Outputs.begin(), Outputs.end(), [](const auto &out) { return out.Amount == AtomicCoins{0}; });
}

bool Xi::Transactions::TransactionPrefix::hasAnyNoneKeyInput() const {
  return std::any_of(Inputs.begin(), Inputs.end(), [](const auto &in) { return in.type() != typeid(KeyInput); });
}

bool Xi::Transactions::TransactionPrefix::hasAnyNoneKeyOutput() const {
  return std::any_of(Outputs.begin(), Outputs.end(),
                     [](const auto &out) { return out.Target.type() != typeid(KeyOutput); });
}

bool Xi::Transactions::TransactionPrefix::containsDoubleSpending() const {
  ::Crypto::KeyImageSet reval;
  for (const auto &input : Inputs) {
    if (input.type() == typeid(KeyInput)) {
      const auto &keyImage = boost::get<KeyInput>(input).DoubleSpendingProtection;
      if (!reval.insert(keyImage).second) {
        return true;
      }
    }
  }
  return false;
}

bool Xi::Transactions::TransactionPrefix::areKeyImageInputsSorted() const {
  ::Crypto::KeyImage last{::Crypto::KeyImage::Null};
  for (const auto &input : Inputs) {
    if (input.type() == typeid(KeyInput)) {
      const auto &keyImage = boost::get<KeyInput>(input).DoubleSpendingProtection;
      if (last >= keyImage) {
        return false;
      }
      last = keyImage;
    }
  }
  return true;
}

Crypto::KeyImageVector Xi::Transactions::TransactionPrefix::keyImages() const {
  ::Crypto::KeyImageVector reval;
  for (const auto &input : Inputs) {
    if (input.type() == typeid(KeyInput)) {
      const auto &keyImage = boost::get<KeyInput>(input).DoubleSpendingProtection;
      reval.emplace_back(keyImage);
    }
  }
  return reval;
}

Xi::Result<Crypto::Hash> Xi::Transactions::TransactionPrefix::computeHash() const {
  XI_ERROR_TRY();
  exceptional_if_not<InvalidVersionError>(Version == 1);
  std::vector<uint8_t> binary;
  exceptional_if_not<CryptoNote::FailedSerializationError>(CryptoNote::toBinaryArray(*this, binary));
  return ::Crypto::cn_fast_hash(binary.data(), binary.size());
  XI_ERROR_CATCH();
}
