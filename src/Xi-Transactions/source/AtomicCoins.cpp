/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/AtomicCoins.h"

#include <algorithm>

std::array<Xi::Transactions::AtomicCoins, 172> Xi::Transactions::AtomicCoins::ValidDecompositions{
    {Xi::Transactions::AtomicCoins{1ull},
     Xi::Transactions::AtomicCoins{2ull},
     Xi::Transactions::AtomicCoins{3ull},
     Xi::Transactions::AtomicCoins{4ull},
     Xi::Transactions::AtomicCoins{5ull},
     Xi::Transactions::AtomicCoins{6ull},
     Xi::Transactions::AtomicCoins{7ull},
     Xi::Transactions::AtomicCoins{8ull},
     Xi::Transactions::AtomicCoins{9ull},
     Xi::Transactions::AtomicCoins{10ull},
     Xi::Transactions::AtomicCoins{20ull},
     Xi::Transactions::AtomicCoins{30ull},
     Xi::Transactions::AtomicCoins{40ull},
     Xi::Transactions::AtomicCoins{50ull},
     Xi::Transactions::AtomicCoins{60ull},
     Xi::Transactions::AtomicCoins{70ull},
     Xi::Transactions::AtomicCoins{80ull},
     Xi::Transactions::AtomicCoins{90ull},
     Xi::Transactions::AtomicCoins{100ull},
     Xi::Transactions::AtomicCoins{200ull},
     Xi::Transactions::AtomicCoins{300ull},
     Xi::Transactions::AtomicCoins{400ull},
     Xi::Transactions::AtomicCoins{500ull},
     Xi::Transactions::AtomicCoins{600ull},
     Xi::Transactions::AtomicCoins{700ull},
     Xi::Transactions::AtomicCoins{800ull},
     Xi::Transactions::AtomicCoins{900ull},
     Xi::Transactions::AtomicCoins{1000ull},
     Xi::Transactions::AtomicCoins{2000ull},
     Xi::Transactions::AtomicCoins{3000ull},
     Xi::Transactions::AtomicCoins{4000ull},
     Xi::Transactions::AtomicCoins{5000ull},
     Xi::Transactions::AtomicCoins{6000ull},
     Xi::Transactions::AtomicCoins{7000ull},
     Xi::Transactions::AtomicCoins{8000ull},
     Xi::Transactions::AtomicCoins{9000ull},
     Xi::Transactions::AtomicCoins{10000ull},
     Xi::Transactions::AtomicCoins{20000ull},
     Xi::Transactions::AtomicCoins{30000ull},
     Xi::Transactions::AtomicCoins{40000ull},
     Xi::Transactions::AtomicCoins{50000ull},
     Xi::Transactions::AtomicCoins{60000ull},
     Xi::Transactions::AtomicCoins{70000ull},
     Xi::Transactions::AtomicCoins{80000ull},
     Xi::Transactions::AtomicCoins{90000ull},
     Xi::Transactions::AtomicCoins{100000ull},
     Xi::Transactions::AtomicCoins{200000ull},
     Xi::Transactions::AtomicCoins{300000ull},
     Xi::Transactions::AtomicCoins{400000ull},
     Xi::Transactions::AtomicCoins{500000ull},
     Xi::Transactions::AtomicCoins{600000ull},
     Xi::Transactions::AtomicCoins{700000ull},
     Xi::Transactions::AtomicCoins{800000ull},
     Xi::Transactions::AtomicCoins{900000ull},
     Xi::Transactions::AtomicCoins{1000000ull},
     Xi::Transactions::AtomicCoins{2000000ull},
     Xi::Transactions::AtomicCoins{3000000ull},
     Xi::Transactions::AtomicCoins{4000000ull},
     Xi::Transactions::AtomicCoins{5000000ull},
     Xi::Transactions::AtomicCoins{6000000ull},
     Xi::Transactions::AtomicCoins{7000000ull},
     Xi::Transactions::AtomicCoins{8000000ull},
     Xi::Transactions::AtomicCoins{9000000ull},
     Xi::Transactions::AtomicCoins{10000000ull},
     Xi::Transactions::AtomicCoins{20000000ull},
     Xi::Transactions::AtomicCoins{30000000ull},
     Xi::Transactions::AtomicCoins{40000000ull},
     Xi::Transactions::AtomicCoins{50000000ull},
     Xi::Transactions::AtomicCoins{60000000ull},
     Xi::Transactions::AtomicCoins{70000000ull},
     Xi::Transactions::AtomicCoins{80000000ull},
     Xi::Transactions::AtomicCoins{90000000ull},
     Xi::Transactions::AtomicCoins{100000000ull},
     Xi::Transactions::AtomicCoins{200000000ull},
     Xi::Transactions::AtomicCoins{300000000ull},
     Xi::Transactions::AtomicCoins{400000000ull},
     Xi::Transactions::AtomicCoins{500000000ull},
     Xi::Transactions::AtomicCoins{600000000ull},
     Xi::Transactions::AtomicCoins{700000000ull},
     Xi::Transactions::AtomicCoins{800000000ull},
     Xi::Transactions::AtomicCoins{900000000ull},
     Xi::Transactions::AtomicCoins{1000000000ull},
     Xi::Transactions::AtomicCoins{2000000000ull},
     Xi::Transactions::AtomicCoins{3000000000ull},
     Xi::Transactions::AtomicCoins{4000000000ull},
     Xi::Transactions::AtomicCoins{5000000000ull},
     Xi::Transactions::AtomicCoins{6000000000ull},
     Xi::Transactions::AtomicCoins{7000000000ull},
     Xi::Transactions::AtomicCoins{8000000000ull},
     Xi::Transactions::AtomicCoins{9000000000ull},
     Xi::Transactions::AtomicCoins{10000000000ull},
     Xi::Transactions::AtomicCoins{20000000000ull},
     Xi::Transactions::AtomicCoins{30000000000ull},
     Xi::Transactions::AtomicCoins{40000000000ull},
     Xi::Transactions::AtomicCoins{50000000000ull},
     Xi::Transactions::AtomicCoins{60000000000ull},
     Xi::Transactions::AtomicCoins{70000000000ull},
     Xi::Transactions::AtomicCoins{80000000000ull},
     Xi::Transactions::AtomicCoins{90000000000ull},
     Xi::Transactions::AtomicCoins{100000000000ull},
     Xi::Transactions::AtomicCoins{200000000000ull},
     Xi::Transactions::AtomicCoins{300000000000ull},
     Xi::Transactions::AtomicCoins{400000000000ull},
     Xi::Transactions::AtomicCoins{500000000000ull},
     Xi::Transactions::AtomicCoins{600000000000ull},
     Xi::Transactions::AtomicCoins{700000000000ull},
     Xi::Transactions::AtomicCoins{800000000000ull},
     Xi::Transactions::AtomicCoins{900000000000ull},
     Xi::Transactions::AtomicCoins{1000000000000ull},
     Xi::Transactions::AtomicCoins{2000000000000ull},
     Xi::Transactions::AtomicCoins{3000000000000ull},
     Xi::Transactions::AtomicCoins{4000000000000ull},
     Xi::Transactions::AtomicCoins{5000000000000ull},
     Xi::Transactions::AtomicCoins{6000000000000ull},
     Xi::Transactions::AtomicCoins{7000000000000ull},
     Xi::Transactions::AtomicCoins{8000000000000ull},
     Xi::Transactions::AtomicCoins{9000000000000ull},
     Xi::Transactions::AtomicCoins{10000000000000ull},
     Xi::Transactions::AtomicCoins{20000000000000ull},
     Xi::Transactions::AtomicCoins{30000000000000ull},
     Xi::Transactions::AtomicCoins{40000000000000ull},
     Xi::Transactions::AtomicCoins{50000000000000ull},
     Xi::Transactions::AtomicCoins{60000000000000ull},
     Xi::Transactions::AtomicCoins{70000000000000ull},
     Xi::Transactions::AtomicCoins{80000000000000ull},
     Xi::Transactions::AtomicCoins{90000000000000ull},
     Xi::Transactions::AtomicCoins{100000000000000ull},
     Xi::Transactions::AtomicCoins{200000000000000ull},
     Xi::Transactions::AtomicCoins{300000000000000ull},
     Xi::Transactions::AtomicCoins{400000000000000ull},
     Xi::Transactions::AtomicCoins{500000000000000ull},
     Xi::Transactions::AtomicCoins{600000000000000ull},
     Xi::Transactions::AtomicCoins{700000000000000ull},
     Xi::Transactions::AtomicCoins{800000000000000ull},
     Xi::Transactions::AtomicCoins{900000000000000ull},
     Xi::Transactions::AtomicCoins{1000000000000000ull},
     Xi::Transactions::AtomicCoins{2000000000000000ull},
     Xi::Transactions::AtomicCoins{3000000000000000ull},
     Xi::Transactions::AtomicCoins{4000000000000000ull},
     Xi::Transactions::AtomicCoins{5000000000000000ull},
     Xi::Transactions::AtomicCoins{6000000000000000ull},
     Xi::Transactions::AtomicCoins{7000000000000000ull},
     Xi::Transactions::AtomicCoins{8000000000000000ull},
     Xi::Transactions::AtomicCoins{9000000000000000ull},
     Xi::Transactions::AtomicCoins{10000000000000000ull},
     Xi::Transactions::AtomicCoins{20000000000000000ull},
     Xi::Transactions::AtomicCoins{30000000000000000ull},
     Xi::Transactions::AtomicCoins{40000000000000000ull},
     Xi::Transactions::AtomicCoins{50000000000000000ull},
     Xi::Transactions::AtomicCoins{60000000000000000ull},
     Xi::Transactions::AtomicCoins{70000000000000000ull},
     Xi::Transactions::AtomicCoins{80000000000000000ull},
     Xi::Transactions::AtomicCoins{90000000000000000ull},
     Xi::Transactions::AtomicCoins{100000000000000000ull},
     Xi::Transactions::AtomicCoins{200000000000000000ull},
     Xi::Transactions::AtomicCoins{300000000000000000ull},
     Xi::Transactions::AtomicCoins{400000000000000000ull},
     Xi::Transactions::AtomicCoins{500000000000000000ull},
     Xi::Transactions::AtomicCoins{600000000000000000ull},
     Xi::Transactions::AtomicCoins{700000000000000000ull},
     Xi::Transactions::AtomicCoins{800000000000000000ull},
     Xi::Transactions::AtomicCoins{900000000000000000ull},
     Xi::Transactions::AtomicCoins{1000000000000000000ull},
     Xi::Transactions::AtomicCoins{2000000000000000000ull},
     Xi::Transactions::AtomicCoins{3000000000000000000ull},
     Xi::Transactions::AtomicCoins{4000000000000000000ull},
     Xi::Transactions::AtomicCoins{5000000000000000000ull},
     Xi::Transactions::AtomicCoins{6000000000000000000ull},
     Xi::Transactions::AtomicCoins{7000000000000000000ull},
     Xi::Transactions::AtomicCoins{8000000000000000000ull},
     Xi::Transactions::AtomicCoins{9000000000000000000ull},
     Xi::Transactions::AtomicCoins{10000000000000000000ull}}};

Xi::Transactions::AtomicCoins::AtomicCoins(Xi::Rct::Amount amount) { value = amount.value; }

Xi::Transactions::AtomicCoins::operator uint64_t() const { return value; }

bool Xi::Transactions::AtomicCoins::isValidDecomposition() const {
  return std::binary_search(ValidDecompositions.begin(), ValidDecompositions.end(), *this);
}

Xi::Transactions::AtomicCoins::operator Xi::Rct::Amount() const { return Xi::Rct::Amount{value}; }

void Xi::Transactions::AtomicCoins::serialize(CryptoNote::ISerializer &serializer) {
  serializer(value, "atomic_units");
}
