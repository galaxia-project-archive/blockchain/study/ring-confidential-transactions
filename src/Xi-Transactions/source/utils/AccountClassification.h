/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <crypto/CryptoTypes.h>
#include <Xi/Crypto/KeyPair.h>
#include <Xi/Crypto/Account/Account.h>
#include <Xi/Crypto/Device/ICryptoDevice.h>

namespace Xi {
namespace Transactions {
namespace __Impl {
[[nodiscard]] boost::optional<Xi::Crypto::Account::SubAddressReceiveInfo> isOutToAccPreComputation(
    const std::unordered_map<::Crypto::PublicKey, ::Xi::Crypto::Account::SubAddressIndex> &subaddresses,
    const ::Crypto::PublicKey &out_key, const ::Crypto::KeyDerivation &derivation,
    const ::Crypto::KeyDerivationVector &additional_derivations, size_t output_index,
    Xi::Crypto::Device::ICryptoDevice &hwdev);
}  // namespace __Impl
}  // namespace Transactions
}  // namespace Xi
