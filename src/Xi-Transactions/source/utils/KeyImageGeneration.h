/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#pragma once

#include <cinttypes>

#include <crypto/CryptoTypes.h>
#include <Xi/Crypto/KeyPair.h>
#include <Xi/Crypto/Account/Account.h>
#include <Xi/Crypto/Device/ICryptoDevice.h>

namespace Xi {
namespace Transactions {
namespace __Impl {
[[nodiscard]] bool generateKeyImageHelperPrecomp(const Xi::Crypto::Account::AccountKeys &ack,
                                                 const ::Crypto::PublicKey &out_key,
                                                 const ::Crypto::KeyDerivation &recv_derivation,
                                                 size_t real_output_index,
                                                 const Xi::Crypto::Account::SubAddressIndex &received_index,
                                                 Xi::Crypto::KeyPair &in_ephemeral, ::Crypto::KeyImage &ki,
                                                 Xi::Crypto::Device::ICryptoDevice &hwdev);

[[nodiscard]] bool generateKeyImageHelper(
    const Xi::Crypto::Account::AccountKeys &sender_account_keys,
    const std::unordered_map<::Crypto::PublicKey, Xi::Crypto::Account::SubAddressIndex> &subaddresses,
    const ::Crypto::PublicKey &out_key, const ::Crypto::PublicKey &tx_public_key,
    const ::Crypto::PublicKeyVector &additional_tx_public_keys, size_t real_output_index,
    Xi::Crypto::KeyPair &in_ephemeral, ::Crypto::KeyImage &ki, Xi::Crypto::Device::ICryptoDevice &hwdev);
}  // namespace __Impl
}  // namespace Transactions
}  // namespace Xi
