/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "utils/AccountClassification.h"

boost::optional<Xi::Crypto::Account::SubAddressReceiveInfo> Xi::Transactions::__Impl::isOutToAccPreComputation(
    const std::unordered_map<::Crypto::PublicKey, Xi::Crypto::Account::SubAddressIndex> &subaddresses,
    const ::Crypto::PublicKey &out_key, const ::Crypto::KeyDerivation &derivation,
    const ::Crypto::KeyDerivationVector &additional_derivations, size_t output_index,
    Xi::Crypto::Device::ICryptoDevice &hwdev) {
  // try the shared tx pubkey
  ::Crypto::PublicKey subaddress_spendkey;
  const auto deriveResult = hwdev.deriveSubaddressPublicKey(out_key, derivation, output_index, subaddress_spendkey);
  if (!deriveResult) {
    // TODO Logging
    return boost::none;
  }
  auto found = subaddresses.find(subaddress_spendkey);
  if (found != subaddresses.end()) return Xi::Crypto::Account::SubAddressReceiveInfo{found->second, derivation};
  // try additional tx pubkeys if available
  if (!additional_derivations.empty()) {
    // TODO Logging
    const auto innerDeriveResult = hwdev.deriveSubaddressPublicKey(out_key, additional_derivations[output_index],
                                                                   output_index, subaddress_spendkey);
    if (!innerDeriveResult) {
      // TODO Logging
      return boost::none;
    }
    found = subaddresses.find(subaddress_spendkey);
    if (found != subaddresses.end())
      return Xi::Crypto::Account::SubAddressReceiveInfo{found->second, additional_derivations[output_index]};
  }
  return boost::none;
}
