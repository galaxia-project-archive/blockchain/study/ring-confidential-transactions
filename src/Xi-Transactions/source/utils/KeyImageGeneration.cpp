/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "utils/KeyImageGeneration.h"

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/utility/value_init.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Rct/Algorithm/KeyArithmetic.h>

#include "utils/AccountClassification.h"

bool Xi::Transactions::__Impl::generateKeyImageHelperPrecomp(const Xi::Crypto::Account::AccountKeys &ack,
                                                             const ::Crypto::PublicKey &out_key,
                                                             const ::Crypto::KeyDerivation &recv_derivation,
                                                             size_t real_output_index,
                                                             const Xi::Crypto::Account::SubAddressIndex &received_index,
                                                             Xi::Crypto::KeyPair &in_ephemeral, ::Crypto::KeyImage &ki,
                                                             Xi::Crypto::Device::ICryptoDevice &hwdev) {
  if (ack.SpendSecretKey == ::Crypto::SecretKey::Null)  // ViewOnly Wallet
  {
    in_ephemeral.PublicKey = out_key;
    in_ephemeral.SecretKey = ::Crypto::SecretKey::Null;
  } else {
    // derive secret key with subaddress - step 1: original CN derivation
    ::Crypto::SecretKey scalar_step1;

    {
      const bool deriveResult = hwdev.deriveSecretKey(recv_derivation, real_output_index, ack.SpendSecretKey,
                                                      scalar_step1);  // computes Hs(a*R || idx) + b
      if (!deriveResult) {
        // TODO Logging
        return false;
      }
    }

    // step 2: add Hs(a || index_major || index_minor)
    ::Crypto::SecretKey subaddr_sk;
    ::Crypto::SecretKey scalar_step2;
    if (received_index.isNull()) {
      scalar_step2 = scalar_step1;  // treat index=(0,0) as a special case representing the main address
    } else {
      if (!hwdev.deriveSubaddressSecretKey(ack.ViewSecretKey, received_index, subaddr_sk)) {
        // TODO Logging
        return false;
      }
      if (!hwdev.addSecretScalar(scalar_step1, subaddr_sk, scalar_step2)) {
        // TODO Logging
        return false;
      }
    }

    in_ephemeral.SecretKey = scalar_step2;

    if (!ack.isMultiSignatureAccount()) {
      if (!hwdev.secretKeyToPublicKey(in_ephemeral.SecretKey, in_ephemeral.PublicKey)) {
        // TODO Logging
        return false;
      }
    } else {
      // when in multisig, we only know the partial spend secret key. but we do know the full spend public key, so the
      // output pubkey can be obtained by using the standard CN key derivation
      if (!hwdev.derivePublicKey(recv_derivation, real_output_index, ack.Address.SpendPublicKey,
                                 in_ephemeral.PublicKey)) {
        // TODO Logging
        return false;
      }
      // and don't forget to add the contribution from the subaddress part
      if (!received_index.isNull()) {
        ::Crypto::PublicKey subaddr_pk;
        if (!hwdev.secretKeyToPublicKey(subaddr_sk, subaddr_pk)) {
          // TODO Logging
          return false;
        }
        try {
          in_ephemeral.PublicKey =
              Xi::Rct::key_add(Xi::Rct::key_cast(in_ephemeral.PublicKey), Xi::Rct::key_cast(subaddr_pk));
        } catch (...) {
          // TODO Logging
          return false;
        }
      }
    }

    if (in_ephemeral.PublicKey != out_key) {
      // TODO Logging
      return false;
    }
  }

  if (!hwdev.generateKeyImage(in_ephemeral.PublicKey, in_ephemeral.SecretKey, ki)) {
    // TODO Logging
    return false;
  } else {
    return true;
  }
}

bool Xi::Transactions::__Impl::generateKeyImageHelper(
    const Xi::Crypto::Account::AccountKeys &sender_account_keys,
    const std::unordered_map<::Crypto::PublicKey, Xi::Crypto::Account::SubAddressIndex> &subaddresses,
    const ::Crypto::PublicKey &out_key, const ::Crypto::PublicKey &tx_public_key,
    const ::Crypto::PublicKeyVector &additional_tx_public_keys, size_t real_output_index,
    Xi::Crypto::KeyPair &in_ephemeral, ::Crypto::KeyImage &ki, Xi::Crypto::Device::ICryptoDevice &hwdev) {
  ::Crypto::KeyDerivation recv_derivation = boost::value_initialized<::Crypto::KeyDerivation>();
  const auto keyDerivationResult =
      hwdev.generateKeyDerivation(tx_public_key, sender_account_keys.ViewSecretKey, recv_derivation);
  if (!keyDerivationResult) {
    // TODO Logging
    recv_derivation = Xi::Rct::Key::Identity;
  }

  ::Crypto::KeyDerivationVector additional_recv_derivations{};
  for (size_t i = 0; i < additional_tx_public_keys.size(); ++i) {
    ::Crypto::KeyDerivation additional_recv_derivation = boost::value_initialized<::Crypto::KeyDerivation>();
    const auto innerKeyDerivationResult = hwdev.generateKeyDerivation(
        additional_tx_public_keys[i], sender_account_keys.ViewSecretKey, additional_recv_derivation);
    if (!innerKeyDerivationResult) {
      // TODO Logging
    } else {
      additional_recv_derivations.push_back(additional_recv_derivation);
    }
  }

  auto subaddr_recv_info = isOutToAccPreComputation(subaddresses, out_key, recv_derivation, additional_recv_derivations,
                                                    real_output_index, hwdev);
  // TODO Logging

  return Xi::Transactions::__Impl::generateKeyImageHelperPrecomp(sender_account_keys, out_key,
                                                                 subaddr_recv_info->Derivation, real_output_index,
                                                                 subaddr_recv_info->Index, in_ephemeral, ki, hwdev);
}
