/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/Extra/TransactionExtraPadding.h"

#include <cassert>
#include <vector>
#include <algorithm>

#include <Xi/Exceptional.h>

#include "Xi/Transactions/Exceptions.h"

void Xi::Transactions::TransactionExtraPadding::serialize(CryptoNote::ISerializer &serializer) {
  serializer(Size, "size");
  std::vector<uint8_t> padding;
  padding.resize(Size, 0);
  serializer.binary(padding.data(), Size, "padding");
  if (serializer.type() == CryptoNote::ISerializer::INPUT) {
    exceptional_if<InvalidExtraError>(
        std::any_of(padding.begin(), padding.end(), [](const auto ipad) { return ipad != 0; }),
        "extra padding may only consists of zeros");
  } else {
    assert(serializer.type() == CryptoNote::ISerializer::OUTPUT);
  }
}
