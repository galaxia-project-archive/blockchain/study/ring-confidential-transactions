/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/Extra/TransactionExtraPublicKeys.h"

#include <algorithm>

#include <Xi/Exceptional.h>
#include <Xi/Algorithm/IsUnique.h>
#include <Serialization/SerializationOverloads.h>
#include <crypto/crypto.h>

#include "Xi/Transactions/Exceptions.h"

bool Xi::Transactions::TransactionExtraPublicKeys::contains(const ::Crypto::PublicKey& key) const {
  return std::find(Keys.begin(), Keys.end(), key) != Keys.end();
}

bool Xi::Transactions::TransactionExtraPublicKeys::hasAnyNullKey() const {
  return std::any_of(Keys.begin(), Keys.end(), [](const auto& key) { return key == ::Crypto::PublicKey::Null; });
}

bool Xi::Transactions::TransactionExtraPublicKeys::hasAnyDuplicate() const {
  return !Algorithm::is_unique(Keys.begin(), Keys.end());
}

bool Xi::Transactions::TransactionExtraPublicKeys::validate() const {
  if (Keys.size() < minSize()) {
    return false;
  } else if (Keys.size() > maxSize()) {
    return false;
  } else {
    return std::all_of(Keys.begin(), Keys.end(), [](const auto& key) { return Crypto::check_key(key); });
  }
}

void Xi::Transactions::TransactionExtraPublicKeys::serialize(CryptoNote::ISerializer& serializer) {
  serializer(Keys, "keys");
}
