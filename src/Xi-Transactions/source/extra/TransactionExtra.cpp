/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/Extra/TransactionExtra.h"

#include <typeinfo>
#include <string>
#include <cassert>
#include <iterator>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/utility/value_init.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Exceptional.h>

#include "Xi/Transactions/Exceptions.h"
#include "Xi/Transactions/Extra/TransactionExtraPadding.h"

namespace {
const uint8_t EndOfStream = 0x00;
}  // namespace

const Xi::Transactions::TransactionExtra Xi::Transactions::TransactionExtra::Null{};

uint8_t Xi::Transactions::TransactionExtra::getTypeTag(const type_info& type) {
  if (type == typeid(TransactionExtraPublicKey)) {
    return 0x01;
  } else if (type == typeid(TransactionExtraPublicKeys)) {
    return 0x02;
  } else if (type == typeid(TransactionExtraPaymentId)) {
    return 0x03;
  } else {
    Xi::exceptional<CryptoNote::InvalidVariantTypeSerializationError>("invalid transaction extra type tag");
  }
}

void Xi::Transactions::TransactionExtra::serialize(CryptoNote::ISerializer& serializer) {
  if (serializer.type() == CryptoNote::ISerializer::INPUT) {
    uint8_t lastTypeTag = EndOfStream;
    uint8_t currentTag = EndOfStream;
    uint8_t fieldIndex = 0;
    do {
      serializer(currentTag, std::string{"field_type_"} + std::to_string(fieldIndex++));
      exceptional_if<InvalidExtraError>(currentTag <= lastTypeTag, "invalid extra ordering");
      if (currentTag == getTypeTag(typeid(TransactionExtraPublicKey))) {
        PublicKey = boost::value_initialized<TransactionExtraPublicKey>();
        serializer(PublicKey.value(), "public_key");
      } else if (currentTag == getTypeTag(typeid(TransactionExtraPublicKeys))) {
        PublicKeys = boost::value_initialized<TransactionExtraPublicKeys>();
        serializer(PublicKeys.value(), "public_keys");
      } else if (currentTag == getTypeTag(typeid(TransactionExtraPaymentId))) {
        PaymentId = boost::value_initialized<TransactionExtraPaymentId>();
        serializer(PaymentId.value(), "payment_id");
      } else {
        exceptional_if_not<InvalidExtraError>(currentTag != EndOfStream, "invalid extra type");
      }
      lastTypeTag = currentTag;
    } while (lastTypeTag != EndOfStream);
  } else {
    assert(serializer.type() == CryptoNote::ISerializer::OUTPUT);
    uint8_t fieldIndex = 0;
    if (PublicKey.has_value()) {
      uint8_t tag = getTypeTag(typeid(TransactionExtraPublicKey));
      serializer(tag, std::string{"field_type_"} + std::to_string(fieldIndex++));
      serializer(PublicKey.value(), "public_key");
    }
    if (PublicKeys.has_value()) {
      uint8_t tag = getTypeTag(typeid(TransactionExtraPublicKey));
      serializer(tag, std::string{"field_type_"} + std::to_string(fieldIndex++));
      serializer(PublicKeys.value(), "public_keys");
    }
    if (PaymentId.has_value()) {
      uint8_t tag = getTypeTag(typeid(TransactionExtraPublicKey));
      serializer(tag, std::string{"field_type_"} + std::to_string(fieldIndex++));
      serializer(PaymentId.value(), "payment_id");
    }
    uint8_t tag = EndOfStream;
    serializer(tag, std::string{"field_type_"} + std::to_string(fieldIndex++));
  }
}

Crypto::PublicKeyVector Xi::Transactions::TransactionExtra::publicKeys() const {
  ::Crypto::PublicKeyVector reval{};
  if (PublicKey.has_value()) {
    reval.push_back(PublicKey->Key);
  }
  if (PublicKeys.has_value()) {
    std::copy(PublicKeys->Keys.begin(), PublicKeys->Keys.end(), std::back_inserter(reval));
  }
  return reval;
}

void Xi::Transactions::TransactionExtra::nullify() {
  PublicKey = boost::none;
  PublicKeys = boost::none;
  PaymentId = boost::none;
}

bool Xi::Transactions::TransactionExtra::isNull() const {
  return !(PublicKey.has_value() || PublicKeys.has_value() || PaymentId.has_value());
}
