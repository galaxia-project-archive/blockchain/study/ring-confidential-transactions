/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/TransactionOutputTarget.h"

#include <typeinfo>
#include <cassert>

#include <Xi/Exceptional.h>

namespace {
uint8_t getTypeTag(const std::type_info& t) {
  using namespace Xi::Transactions;
  if (t == typeid(KeyOutput)) {
    return 0x01;
  } else {
    Xi::exceptional<CryptoNote::InvalidVariantTypeSerializationError>();
  }
}
}  // namespace

void Xi::Transactions::TransactionOutputTarget::serialize(CryptoNote::ISerializer& serializer) {
  uint8_t typeTag;
  if (serializer.type() == CryptoNote::ISerializer::INPUT) {
    serializer(typeTag, "type");
    if (typeTag == getTypeTag(typeid(KeyOutput))) {
      KeyOutput value;
      value.serialize(serializer);
      this->operator=(value);
    } else {
      Xi::exceptional<CryptoNote::InvalidVariantTypeSerializationError>();
    }
  } else {
    assert(serializer.type() == CryptoNote::ISerializer::OUTPUT);
    typeTag = getTypeTag(type());
    serializer(typeTag, "type");
    if (type() == typeid(KeyOutput)) {
      boost::get<KeyOutput>(this)->serialize(serializer);
    } else {
      Xi::exceptional<CryptoNote::InvalidVariantTypeSerializationError>();
    }
  }
}
