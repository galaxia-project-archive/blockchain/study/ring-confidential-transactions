/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/Construction/CoinbaseTransactionConstructor.h"

#include <vector>
#include <numeric>
#include <utility>
#include <algorithm>
#include <iterator>
#include <cassert>

#include <Xi/Exceptional.h>
#include <Xi/Exceptions.h>
#include <Xi/Crypto/Exceptions.h>
#include <Xi/Crypto/Device/DefaultDevice.h>

#include "Xi/Transactions/Exceptions.h"

Xi::Transactions::CoinbaseTransactionConstructor::CoinbaseTransactionConstructor()
    : m_publicKeys{}, m_outputs{}, m_index{0}, m_minerUnlockWindow{0}, m_txVersion{1} {}

Xi::Transactions::CoinbaseTransactionConstructor &
Xi::Transactions::CoinbaseTransactionConstructor::withMinerUnlockWindow(uint32_t blocks) {
  m_minerUnlockWindow = blocks;
  return *this;
}

Xi::Transactions::CoinbaseTransactionConstructor &Xi::Transactions::CoinbaseTransactionConstructor::withReward(
    const Xi::Crypto::Account::AccountPublicAddress &acc, const Xi::Transactions::AtomicCoins reward) {
  auto &device = Crypto::Device::DefaultDevice::instance();
  Crypto::KeyPair txKeys;
  exceptional_if_not<Crypto::KeyGenerationError>(device.generateKeyPair(txKeys));
  return withReward(acc, reward, std::move(txKeys));
}

Xi::Transactions::CoinbaseTransactionConstructor &Xi::Transactions::CoinbaseTransactionConstructor::withReward(
    const Xi::Crypto::Account::AccountPublicAddress &acc, const Xi::Transactions::AtomicCoins reward,
    Xi::Crypto::KeyPair txKeys) {
  auto &device = Crypto::Device::DefaultDevice::instance();
  ::Crypto::KeyDerivation derivation;
  exceptional_if_not<Crypto::KeyGenerationError>(
      device.generateKeyDerivation(txKeys.PublicKey, txKeys.SecretKey, derivation));
  ::Crypto::PublicKey ephPublicKey;
  exceptional_if_not<Crypto::KeyGenerationError>(
      device.derivePublicKey(derivation, m_outputs.size(), acc.ViewPublicKey, ephPublicKey));
  m_publicKeys.emplace_back(std::move(txKeys.PublicKey));
  m_outputs.emplace_back(TransactionOutput{reward, KeyOutput{ephPublicKey}});
  return *this;
}

Xi::Transactions::CoinbaseTransactionConstructor &Xi::Transactions::CoinbaseTransactionConstructor::withBlockIndex(
    uint32_t index) {
  m_index = index;
  return *this;
}

Xi::Transactions::CoinbaseTransactionConstructor &Xi::Transactions::CoinbaseTransactionConstructor::withVersion(
    uint64_t version) {
  m_txVersion = version;
  return *this;
}

Xi::Transactions::AtomicCoins Xi::Transactions::CoinbaseTransactionConstructor::totalReward() const {
  return std::accumulate(m_outputs.begin(), m_outputs.end(), 0_XIG,
                         [](auto acc, const auto &out) { return acc + out.Amount; });
}

Xi::Result<Xi::Transactions::TransactionPrefix> Xi::Transactions::CoinbaseTransactionConstructor::construct() const {
  XI_ERROR_TRY();
  exceptional_if<RuntimeError>(m_outputs.empty(), "a coinbase transaction required at least one output");
  exceptional_if_not<InvalidVersionError>(m_txVersion == 1, "invalid version provided for coinbase transaction");
  TransactionPrefix reval;
  reval.nullify();
  reval.Version = m_txVersion;
  reval.Inputs = {TransactionInput{CoinGenerationInput{m_index}}};
  reval.Unlock = m_index + m_minerUnlockWindow;
  reval.Outputs = m_outputs;
  reval.Extra.PublicKey = TransactionExtraPublicKey{m_publicKeys.front()};
  if (m_publicKeys.size() > 1) {
    reval.Extra.PublicKeys = TransactionExtraPublicKeys{};
    std::copy(std::next(m_publicKeys.begin()), m_publicKeys.end(), std::back_inserter(reval.Extra.PublicKeys->Keys));
  }
  return std::move(reval);
  XI_ERROR_CATCH();
}
