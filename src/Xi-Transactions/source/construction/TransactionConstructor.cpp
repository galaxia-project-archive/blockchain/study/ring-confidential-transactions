/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/Construction/TransactionConstructor.h"

#include <utility>

#include <Xi/Utils/ExternalIncludePush.h>
#include <boost/utility/value_init.hpp>
#include <Xi/Utils/ExternalIncludePop.h>

#include <Xi/Algorithm.h>
#include <Logging/LoggerGroup.h>
#include <crypto/crypto.h>
#include <Xi/Crypto/Device/ICryptoDevice.h>
#include <Xi/Rct/Rct.h>

#include "utils/KeyImageGeneration.h"

namespace {
class Utils {
 public:
  static void classifyAddresses(const Xi::Transactions::TransactionDestinationEntryVector &destinations,
                                const boost::optional<Xi::Crypto::Account::AccountPublicAddress> &change_addr,
                                size_t &num_stdaddresses, size_t &num_subaddresses,
                                Xi::Crypto::Account::AccountPublicAddress &single_dest_subaddress) {
    num_stdaddresses = 0;
    num_subaddresses = 0;
    std::unordered_set<Xi::Crypto::Account::AccountPublicAddress> unique_dst_addresses{};
    for (const auto &dst_entr : destinations) {
      if (change_addr && dst_entr.Receiver == change_addr) continue;
      if (unique_dst_addresses.count(dst_entr.Receiver) == 0) {
        unique_dst_addresses.insert(dst_entr.Receiver);
        if (dst_entr.IsSubAddress) {
          ++num_subaddresses;
          single_dest_subaddress = dst_entr.Receiver;
        } else {
          ++num_stdaddresses;
        }
      }
    }
  }

  [[nodiscard]] static bool generateOutputEphemeralKeys(
      const size_t tx_version, const Xi::Crypto::Account::AccountKeys &sender_account_keys,
      const ::Crypto::PublicKey &txkey_pub, const ::Crypto::SecretKey &tx_key,
      const Xi::Transactions::TransactionDestinationEntry &dst_entr,
      const boost::optional<Xi::Crypto::Account::AccountPublicAddress> &change_addr, const size_t output_index,
      const bool &need_additional_txkeys, const ::Crypto::SecretKeyVector &additional_tx_keys,
      ::Crypto::PublicKeyVector &additional_tx_public_keys, Xi::Rct::KeyVector &amount_keys,
      ::Crypto::PublicKey &out_eph_public_key, Xi::Crypto::Device::ICryptoDevice &hwdev) {
    if (tx_version != 1) {
      // TODO Logging
      return false;
    }

    ::Crypto::KeyDerivation derivation;

    // make additional tx pubkey if necessary
    Xi::Crypto::KeyPair additional_txkey;
    if (need_additional_txkeys) {
      additional_txkey.SecretKey = additional_tx_keys[output_index];
      if (dst_entr.IsSubAddress) {
        additional_txkey.PublicKey = Xi::Rct::key_multiply_scalar(Xi::Rct::key_cast(dst_entr.Receiver.SpendPublicKey),
                                                                  Xi::Rct::key_cast(additional_txkey.SecretKey));
      } else {
        additional_txkey.PublicKey =
            Xi::Rct::key_multiply_scalar_with_basepoint(Xi::Rct::key_cast(additional_txkey.SecretKey));
      }
    }

    if (change_addr && dst_entr.Receiver == *change_addr) {
      if (!hwdev.generateKeyDerivation(txkey_pub, sender_account_keys.ViewSecretKey, derivation)) {
        // TODO Logging
        return false;
      }
    } else {
      const auto &secretKey = (dst_entr.IsSubAddress && need_additional_txkeys) ? additional_txkey.SecretKey : tx_key;
      if (!hwdev.generateKeyDerivation(dst_entr.Receiver.ViewPublicKey, secretKey, derivation)) {
        // TODO Logging
        return false;
      }
    }

    if (need_additional_txkeys) {
      additional_tx_public_keys.push_back(additional_txkey.PublicKey);
    }

    ::Crypto::SecretKey scalar1;
    ::Crypto::derivation_to_scalar(derivation, output_index,
                                   reinterpret_cast<::Crypto::EllipticCurveScalar &>(scalar1));
    amount_keys.push_back(Xi::Rct::key_cast(scalar1));

    if (!hwdev.derivePublicKey(derivation, output_index, dst_entr.Receiver.SpendPublicKey, out_eph_public_key)) {
      // TODO Logging
      return false;
    } else {
      return true;
    }
  }

  [[nodiscard]] static bool constructTransaction(
      const uint64_t version, const Xi::Crypto::Account::AccountKeys &sender_account_keys,
      std::vector<Xi::Transactions::TransactionSourceEntry> sources,
      const std::vector<Xi::Transactions::TransactionDestinationEntry> &destinations, Xi::Transactions::Transaction &tx,
      uint64_t unlock_time,

      Xi::Rct::MultiSignatureOut *multisigOut,
      std::unordered_map<::Crypto::PublicKey, Xi::Crypto::Account::SubAddressIndex> subaddresses,
      const boost::optional<Xi::Crypto::Account::AccountPublicAddress> &change_addr, const ::Crypto::SecretKey &txKey,
      const ::Crypto::SecretKeyVector &additional_tx_keys, bool shuffleOuts, const Xi::Rct::RctConfig rctConfig,

      Logging::ILogger &log) {
    using namespace Logging;
    LoggerRef logger(log, "construct_tx");
    try {
      // @BEGIN Currently not supported interfaces

      // @END Currently not supported interfaces

      XI_RETURN_EC_IF(sources.empty(), false, "no transaction inputs given");
      XI_RETURN_EC_IF(multisigOut != nullptr && !sender_account_keys.isMultiSignatureAccount(), false, "");
      XI_RETURN_EC_IF(sender_account_keys.isMultiSignatureAccount() && multisigOut == nullptr, false, "");

      tx.nullify();
      if (multisigOut != nullptr) {
        multisigOut->nullify();
      }

      tx.Transfer.Version = version;
      tx.Transfer.Unlock = unlock_time;

      auto &device = sender_account_keys.device();
      Xi::Crypto::KeyPairVector inputContext{/* */};
      Xi::Rct::KeyVector amountKeys{/* */};

      Xi::Transactions::AtomicCoins inputsAmount{0};
      inputContext.reserve(sources.size());
      for (size_t i = 0; i < sources.size(); ++i) {
        const auto &iSource = sources[i];
        XI_RETURN_EC_IF(iSource.RealOutputIndex > iSource.UsingOutputs.size(), false,
                        "real output index exceeds outputs size");
        inputsAmount += iSource.Amount;

        inputContext.push_back(Xi::Crypto::KeyPair{});
        auto &ephemeralKeyPair = inputContext[i];
        ::Crypto::KeyImage doubleSpendingProof{};
        const ::Crypto::PublicKey &outKey = iSource.UsingOutputs[iSource.RealOutputIndex].Key.Dest;

        if (!Xi::Transactions::__Impl::generateKeyImageHelper(
                sender_account_keys, subaddresses, outKey, iSource.RealOutputPublicKey,
                iSource.RealOutputAdditionalPublicKeys, iSource.RealOutputOutputIndex, ephemeralKeyPair,
                doubleSpendingProof, device)) {
          logger(Logging::FATAL) << "unable to generate double spending protection proof";
          return false;
        }

        if (!sender_account_keys.isMultiSignatureAccount() &&
            Xi::Rct::key_cast(ephemeralKeyPair.PublicKey) != iSource.UsingOutputs[iSource.RealOutputIndex].Key.Dest) {
          logger(Logging::ERROR) << "derived public key mismatch with output public key";
          return false;
        }

        Xi::Transactions::KeyInput input;
        input.Amount = iSource.Amount;
        if (multisigOut != nullptr) {
          if (!iSource.MultiSignatureInfo.has_value()) {
            logger(Logging::ERROR)
                << "expected multi signature transaction but a none multisignature input was provided";
            return false;
          } else {
            input.DoubleSpendingProtection = iSource.MultiSignatureInfo->ki;
          }
        } else {
          input.DoubleSpendingProtection = doubleSpendingProof;
        }

        input.UsedKeyOffsets.reserve(iSource.UsingOutputs.size() + 1);
        for (const auto &iOut : iSource.UsingOutputs) {
          input.UsedKeyOffsets.push_back(iOut.Index);
        }
        input.UsedKeyOffsets = Xi::Algorithm::indices_to_delta_offsets(input.UsedKeyOffsets);
        tx.Transfer.Inputs.emplace_back(input);
      }

      if (shuffleOuts) {
        // TODO
      }

      std::vector<size_t> ins_order(sources.size());
      for (size_t n = 0; n < sources.size(); ++n) {
        ins_order[n] = n;
      }
      std::sort(ins_order.begin(), ins_order.end(), [&](const size_t lhs, const size_t rhs) {
        const Xi::Transactions::KeyInput &lhsInput = boost::get<Xi::Transactions::KeyInput>(tx.Transfer.Inputs[lhs]);
        const Xi::Transactions::KeyInput &rhsInput = boost::get<Xi::Transactions::KeyInput>(tx.Transfer.Inputs[rhs]);
        return lhsInput.DoubleSpendingProtection < rhsInput.DoubleSpendingProtection;
      });
      if (!Xi::Algorithm::apply_permutation(ins_order, [&](const size_t lhs, const size_t rhs) {
            std::swap(tx.Transfer.Inputs[lhs], tx.Transfer.Inputs[rhs]);
            std::swap(inputContext[lhs], inputContext[rhs]);
            std::swap(sources[lhs], sources[rhs]);
          })) {
        logger(Logging::ERROR) << "unable to sort inputs";
        return false;
      }

      // figure out if we need to make additional tx pubkeys
      size_t num_stdaddresses = 0;
      size_t num_subaddresses = 0;
      Xi::Crypto::Account::AccountPublicAddress single_dest_subaddress;
      classifyAddresses(destinations, change_addr, num_stdaddresses, num_subaddresses, single_dest_subaddress);

      ::Crypto::PublicKey txkey_pub;
      if (num_stdaddresses == 0 && num_subaddresses == 1) {
        try {
          txkey_pub = Xi::Rct::key_multiply_scalar(Xi::Rct::key_cast(single_dest_subaddress.SpendPublicKey),
                                                   Xi::Rct::key_cast(txKey));
        } catch (...) {
          // TODO Logging
          return false;
        }
      } else {
        txkey_pub = Xi::Rct::key_multiply_scalar_with_basepoint(Xi::Rct::key_cast(txKey));
      }
      tx.Transfer.Extra.PublicKey = Xi::Transactions::TransactionExtraPublicKey{txkey_pub};
      bool need_additional_txkeys = num_subaddresses > 0 && (num_stdaddresses > 0 || num_subaddresses > 1);
      if (need_additional_txkeys && destinations.size() != additional_tx_keys.size()) {
        logger(Logging::ERROR) << "amount of addition txkeys does not correspond to destinations size";
        return false;
      }

      uint64_t summary_outs_money = 0;
      size_t output_index = 0;
      ::Crypto::PublicKeyVector additional_tx_public_keys{/* */};
      for (const auto &iDest : destinations) {
        ::Crypto::PublicKey out_eph_public_key;
        if (!generateOutputEphemeralKeys(tx.Transfer.Version, sender_account_keys, txkey_pub, txKey, iDest, change_addr,
                                         output_index, need_additional_txkeys, additional_tx_keys,
                                         additional_tx_public_keys, amountKeys, out_eph_public_key, device)) {
          logger(Logging::ERROR) << "failed to generate ephemeral output key";
          return false;
        }

        Xi::Transactions::TransactionOutput out;
        out.Amount = iDest.Amount;
        out.Target = Xi::Transactions::KeyOutput{out_eph_public_key};
        tx.Transfer.Outputs.push_back(std::move(out));
        output_index += 1;
        summary_outs_money += iDest.Amount;
      }

      if (additional_tx_public_keys.size() != additional_tx_keys.size()) {
        logger(Logging::ERROR) << "error creating destinations public keys";
        return false;
      }

      if (need_additional_txkeys) {
        logger(Logging::TRACE) << "additional transaction public keys:";
        for (size_t i = 0; i < additional_tx_public_keys.size(); ++i) {
          logger(Logging::TRACE) << "\t" << additional_tx_public_keys[i].toString();
        }
        tx.Transfer.Extra.PublicKeys = Xi::Transactions::TransactionExtraPublicKeys{additional_tx_public_keys};
      }

      if (summary_outs_money > inputsAmount) {
        logger(Logging::ERROR) << "transaction outputs more coins than it has inputs";
        return false;
      }

      const bool zero_secret_key = sender_account_keys.SpendSecretKey == ::Crypto::SecretKey::Null;
      if (zero_secret_key) {
        logger(Logging::DEBUGGING) << "account spend secret key is null, skipping signatures";
      }

      // RCT signature generation
      {
        size_t n_total_outs = sources[0].UsingOutputs.size();  // only for non-simple rct

        // the non-simple version is slightly smaller, but assumes all real inputs
        // are on the same index, so can only be used if there just one ring.
        // Should be always true starting with bulletproof 2
        const bool use_simple_rct = sources.size() > 1 || rctConfig.Type != Xi::Rct::RangeProofType::Borromean;

        if (!use_simple_rct) {
          // non simple ringct requires all real inputs to be at the same index for all inputs
          for (const auto &src_entr : sources) {
            if (src_entr.RealOutputIndex != sources.begin()->RealOutputIndex) {
              logger(Logging::ERROR) << "for none simple rct transactions all inputs must have the same index";
              return false;
            }
          }

          // enforce same mixin for all outputs
          for (size_t i = 1; i < sources.size(); ++i) {
            if (n_total_outs != sources[i].UsingOutputs.size()) {
              logger(Logging::ERROR) << "non simple rct has varying ring size";
              return false;
            }
          }
        }

        uint64_t amount_in = 0, amount_out = 0;
        Xi::Rct::CtKeyVector inSk;
        inSk.reserve(sources.size());
        // mixRing indexing is done the other way round for simple
        Xi::Rct::CtKeyMatrix mixRing(use_simple_rct ? sources.size() : n_total_outs);
        Xi::Rct::KeyVector ctDestinations;
        std::vector<Xi::Rct::Amount> inamounts, outamounts;
        std::vector<unsigned int> index;
        Xi::Rct::MultiSignatureDataVector kLRki;
        for (size_t i = 0; i < sources.size(); ++i) {
          Xi::Rct::CtKey ctkey;
          amount_in += sources[i].Amount;
          inamounts.push_back(Xi::Transactions::AtomicCoins{sources[i].Amount});
          if (sources[i].RealOutputIndex > std::numeric_limits<unsigned int>::max()) {
            logger(Logging::ERROR) << "output index exceeds max supported index";
            return false;
          }
          index.push_back(static_cast<unsigned int>(sources[i].RealOutputIndex));
          // inSk: (secret key, mask)
          ctkey.Dest = Xi::Rct::key_cast(inputContext[i].SecretKey);
          ctkey.Mask = sources[i].Mask;
          inSk.push_back(ctkey);
          // memwipe(&ctkey, sizeof(rct::ctkey)); // TODO wipe memory
          // inPk: (public key, commitment)
          // will be done when filling in mixRing
          if (multisigOut != nullptr) {
            if (!sources[i].MultiSignatureInfo.has_value()) {
              logger(Logging::ERROR) << "using multisignature wallet but one of the inputs missing the multisig data";
              return false;
            } else {
              kLRki.push_back(*sources[i].MultiSignatureInfo);
            }
          }
        }
        for (size_t i = 0; i < tx.Transfer.Outputs.size(); ++i) {
          ctDestinations.push_back(
              Xi::Rct::key_cast(boost::get<Xi::Transactions::KeyOutput>(tx.Transfer.Outputs[i].Target).PublicKey));
          outamounts.push_back(tx.Transfer.Outputs[i].Amount);
          amount_out += tx.Transfer.Outputs[i].Amount;
        }

        if (use_simple_rct) {
          // mixRing indexing is done the other way round for simple
          for (size_t i = 0; i < sources.size(); ++i) {
            mixRing[i].resize(sources[i].UsingOutputs.size());
            for (size_t n = 0; n < sources[i].UsingOutputs.size(); ++n) {
              mixRing[i][n] = sources[i].UsingOutputs[n].Key;
            }
          }
        } else {
          for (size_t i = 0; i < n_total_outs; ++i)  // same index assumption
          {
            mixRing[i].resize(sources.size());
            for (size_t n = 0; n < sources.size(); ++n) {
              mixRing[i][n] = sources[n].UsingOutputs[i].Key;
            }
          }
        }

        // fee
        Xi::Transactions::AtomicCoins fee{amount_in - amount_out};
        if (!use_simple_rct && amount_in > amount_out) {
          outamounts.push_back(fee);
        }

        tx.Transfer.clearAmounts();
        ::Crypto::Hash tx_prefix_hash;
        {
          auto prefixHashComputation = tx.Transfer.computeHash();
          if (prefixHashComputation.isError()) {
            logger(Logging::ERROR) << "transfer hash computation failed: " << prefixHashComputation.error().message();
            return false;
          } else {
            tx_prefix_hash = prefixHashComputation.take();
          }
        }

        Xi::Rct::CtKeyVector outSk;
        try {
          if (use_simple_rct) {
            tx.Signatures = Xi::Rct::Rct::generate_simple(
                Xi::Rct::key_cast(tx_prefix_hash), inSk, ctDestinations, inamounts, outamounts, fee, mixRing,
                amountKeys, multisigOut != nullptr ? &kLRki : nullptr, multisigOut, index, outSk, rctConfig, device);

          } else {
            tx.Signatures =
                Xi::Rct::Rct::generate(Xi::Rct::key_cast(tx_prefix_hash), inSk, ctDestinations, outamounts, mixRing,
                                       amountKeys, multisigOut != nullptr ? kLRki.data() : nullptr, multisigOut,
                                       static_cast<unsigned int>(sources[0].RealOutputIndex), outSk, rctConfig, device);
          }
        } catch (std::exception &ex) {
          logger(Logging::ERROR) << "signature generation failed: " << ex.what();
          return false;
        } catch (...) {
          logger(Logging::ERROR) << "signature generation faield with none exceptional error";
          return false;
        }
        // TODO memwipe inSk
        if (tx.Transfer.Outputs.size() != outSk.size()) {
          logger(Logging::ERROR) << "signature generation yield more/less secret keys than outputs";
          return false;
        }

        ::Crypto::Hash txHash;
        {
          auto hashComputation = tx.computeHash();
          if (hashComputation.isError()) {
            logger(Logging::ERROR) << "generated transaction hash evaluation failed: "
                                   << hashComputation.error().message();
            return false;
          } else {
            txHash = hashComputation.take();
          }
        }
        logger(Logging::DEBUGGING) << "transaction generated successfully: " << txHash.toString();
      }

      return true;
    } catch (std::exception &e) {
      logger(Logging::FATAL) << "transaction creation failed: " << e.what();
      return false;
    } catch (...) {
      logger(Logging::FATAL) << "transaction creation failed with non ecxeptional type";
      return false;
    }
  }
};
}  // namespace

Xi::Transactions::TransactionConstructor::TransactionConstructor(
    const Xi::Crypto::Account::AccountKeys &sender, const Xi::Transactions::TransactionSourceEntryVector &sources,
    const Xi::Transactions::TransactionDestinationEntryVector &destinations)
    : m_sender{sender}, m_sources{sources}, m_destinations{destinations} {}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withSubadresses(
    const Crypto::Account::SubAddressesMap &subadresses) {
  m_subaddresses = &subadresses;
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withSignatureType(
    Xi::Rct::RctType type) {
  m_signatureType = type;
  if (Rct::is_bulletproof_type(type)) {
    m_rctConfig.Type = Rct::RangeProofType::Bulletproof;
    m_rctConfig.BulletproofVersion = 2;
  } else if (Rct::is_borromean_type(type)) {
    m_rctConfig.Type = Rct::RangeProofType::Borromean;
    m_rctConfig.BulletproofVersion = 0;
  }
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withMultiSignatureWallet(
    Rct::MultiSignatureOut &out) {
  m_multisig = &out;
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withVersion(uint64_t version) {
  m_version = version;
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withUnlock(uint64_t unlock) {
  m_unlock = unlock;
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withOutputShuffle() {
  m_shuffleOutput = true;
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withoutOutputShuffle() {
  m_shuffleOutput = false;
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withKey(::Crypto::SecretKey key) {
  m_txkey = std::move(key);
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withAdditionalKeys(
    ::Crypto::SecretKeyVector keys) {
  m_additionalTxKeys = std::move(keys);
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withChangeAddress(
    Xi::Crypto::Account::AccountPublicAddress changeAddress) {
  m_changeAddress = std::move(changeAddress);
  return *this;
}

Xi::Transactions::TransactionConstructor &Xi::Transactions::TransactionConstructor::withLogger(
    Logging::ILogger &logger) {
  m_logger = &logger;
  return *this;
}

Xi::Result<Xi::Transactions::TransactionConstructor::Result> Xi::Transactions::TransactionConstructor::construct()
    const {
  XI_ERROR_TRY();
  // TODO Sanity Checks
  auto &device = m_sender.device();

  // Check if subaddresses is not in use
  const Crypto::Account::SubAddressesMap noSubaddress{{m_sender.Address.SpendPublicKey, {0, 0}}};
  const Crypto::Account::SubAddressesMap *subaddressesToUse =
      m_subaddresses == nullptr ? &noSubaddress : m_subaddresses;

  // Check if keys must be generated
  if (!m_additionalTxKeys.has_value()) {
    m_additionalTxKeys = {};
  }
  const bool constructTxKey = !m_txkey.has_value();
  if (constructTxKey) {
    m_txkey = boost::value_initialized<::Crypto::SecretKey>();
    exceptional_if_not<CryptoDeviceError>(device.beginTransactionConstruction(*m_txkey));

    size_t num_stdaddresses = 0;
    size_t num_subaddresses = 0;
    Crypto::Account::AccountPublicAddress single_dest_subaddress;
    Utils::classifyAddresses(m_destinations, m_changeAddress, num_stdaddresses, num_subaddresses,
                             single_dest_subaddress);
    bool need_additional_txkeys = num_subaddresses > 0 && (num_stdaddresses > 0 || num_subaddresses > 1);
    if (need_additional_txkeys) {
      m_additionalTxKeys = {};
      for (const auto &_ : m_destinations) {
        XI_UNUSED(_);
        Crypto::KeyPair keyPair;
        exceptional_if_not<CryptoDeviceError>(device.generateKeyPair(keyPair));
        m_additionalTxKeys->emplace_back(std::move(keyPair.SecretKey));
      }
    }
  }

  Result reval{{}, ::Crypto::Hash::Null, *m_txkey, *m_additionalTxKeys};
  reval.CreatedTransaction.nullify();

  Logging::LoggerGroup proxyLogger{};
  Logging::ILogger *loggerToUse = m_logger == nullptr ? &proxyLogger : m_logger;
  if (!Utils::constructTransaction(m_version, m_sender, m_sources, m_destinations, reval.CreatedTransaction, m_unlock,
                                   m_multisig, *subaddressesToUse, m_changeAddress, reval.TransactionKey,
                                   reval.AdditionalTransactionKeys, m_shuffleOutput, m_rctConfig, *loggerToUse)) {
    exceptional<GenericError>("construction returned none success code");
  }

  if (constructTxKey) {
    exceptional_if_not<CryptoDeviceError>(device.endTransactionConstruction());
  }

  return std::move(reval);
  XI_ERROR_CATCH();
}
