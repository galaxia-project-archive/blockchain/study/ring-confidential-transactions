/* ============================================================================================== *
 *                                                                                                *
 *                                       Xi Blockchain                                            *
 *                                                                                                *
 * ---------------------------------------------------------------------------------------------- *
 * This file is part of the Galaxia Project - Xi Blockchain                                       *
 * ---------------------------------------------------------------------------------------------- *
 *                                                                                                *
 * Copyright 2018-2019 Galaxia Project Developers                                                 *
 *                                                                                                *
 * This program is free software: you can redistribute it and/or modify it under the terms of the *
 * GNU General Public License as published by the Free Software Foundation, either version 3 of   *
 * the License, or (at your option) any later version.                                            *
 *                                                                                                *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;      *
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.      *
 * See the GNU General Public License for more details.                                           *
 *                                                                                                *
 * You should have received a copy of the GNU General Public License along with this program.     *
 * If not, see <https://www.gnu.org/licenses/>.                                                   *
 *                                                                                                *
 * ============================================================================================== */

#include "Xi/Transactions/Transaction.h"

#include <Serialization/SerializationTools.h>
#include <crypto/hash.h>
#include <Xi/Rct/Algorithm/RctCategoryDetection.h>
#include <Xi/Rct/Algorithm/BulletproofAmounts.h>

#include "Xi/Transactions/Exceptions.h"

Xi::Transactions::Transaction::Transaction() { nullify(); }

Xi::Transactions::Transaction::Transaction(const Xi::Transactions::Transaction &other) { emplace(other); }

Xi::Transactions::Transaction &Xi::Transactions::Transaction::operator=(const Xi::Transactions::Transaction &other) {
  emplace(other);
  return *this;
}

void Xi::Transactions::Transaction::serialize(CryptoNote::ISerializer &serializer) {
  serializer(Transfer, "prefix");
  serializer(Signatures, "signature");
}

void Xi::Transactions::Transaction::nullify() {
  Transfer.nullify();
  Signatures.nullify();
}

void Xi::Transactions::Transaction::emplace(const Xi::Transactions::Transaction &other) {
  Transfer.emplace(other.Transfer);
  Signatures = other.Signatures;
}

Xi::Transactions::AtomicCoins Xi::Transactions::Transaction::fee() const { return Signatures.TransactionFee; }

Xi::Result<::Crypto::Hash> Xi::Transactions::Transaction::computeHash() const {
  XI_ERROR_TRY();
  std::vector<uint8_t> binary;
  exceptional_if_not<CryptoNote::FailedSerializationError>(CryptoNote::toBinaryArray(*this, binary));
  return Crypto::cn_fast_hash(binary.data(), binary.size());
  XI_ERROR_CATCH();
}

Xi::Result<uint64_t> Xi::Transactions::Transaction::blobSize() const {
  XI_ERROR_TRY();
  std::vector<uint8_t> binary;
  exceptional_if_not<CryptoNote::FailedSerializationError>(CryptoNote::toBinaryArray(*this, binary));
  return static_cast<uint64_t>(binary.size());
  XI_ERROR_CATCH();
}

Xi::Result<uint64_t> Xi::Transactions::Transaction::weight() const {
  XI_ERROR_TRY();
  uint64_t size = blobSize().valueOrThrow();
  if (!Rct::is_bulletproof_type(Signatures.Type)) {
    return size;
  }
  const auto outsCount = Transfer.Outputs.size();
  if (outsCount <= 2) {
    return size;
  }
  const uint64_t bulletproofBaseSize = 368;
  const size_t paddOutsCount = Rct::count_bulletproof_max_amounts(Signatures.Prunable.Bulletproofs);
  size_t nlr = 0;
  for (const auto &bp : Signatures.Prunable.Bulletproofs) {
    nlr += bp.L.size() * 2;
  }
  const size_t bpSize = 32 * (9 + nlr);
  exceptional_if_not<std::runtime_error>(bulletproofBaseSize * paddOutsCount >= bpSize, "Invalid bulletproof clawback");
  const uint64_t bpClawback = (bulletproofBaseSize * paddOutsCount - bpSize) * 4 / 5;
  exceptional_if_not<std::runtime_error>(bpClawback <= std::numeric_limits<uint64_t>::max() - size, "Weight overflow");
  return size + bpClawback;
  XI_ERROR_CATCH();
}
