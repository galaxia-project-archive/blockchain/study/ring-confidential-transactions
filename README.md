## Prebuilt Binaries

| Channel | Releases |
|---------|----------|
| stable  |[downloads](https://releases.xiproject.io/stable/)|
| beta    |[downloads](https://releases.xiproject.io/beta/)|
| edge    |[downloads](https://releases.xiproject.io/edge/)|
| clutter |[downloads](https://releases.xiproject.io/clutter/)|

## Build Matrix

| Branch  | Status | CppCheck | CppLint |
|---------|--------|----------|---------|
| master  | ![Build Status](https://gitlab.com/galaxia-project/blockchain/xi/badges/master/build.svg)  | [![View](https://img.shields.io/badge/%20-CppCheck-orange.svg?logo=Google-Analytics)](https://gitlab.com/galaxia-project/blockchain/xi/-/jobs/artifacts/master/download?job=CppCheck) | [![View](https://img.shields.io/badge/%20-CppLint-orange.svg?logo=ESLint)](https://gitlab.com/galaxia-project/blockchain/xi/-/jobs/artifacts/master/download?job=CppLint) |
| develop  | ![Build Status](https://gitlab.com/galaxia-project/blockchain/xi/badges/develop/build.svg)  | [![View](https://img.shields.io/badge/%20-CppCheck-orange.svg?logo=Google-Analytics)](https://gitlab.com/galaxia-project/blockchain/xi/-/jobs/artifacts/develop/download?job=CppCheck) | [![View](https://img.shields.io/badge/%20-CppLint-orange.svg?logo=ESLint)](https://gitlab.com/galaxia-project/blockchain/xi/-/jobs/artifacts/develop/download?job=CppLint) |